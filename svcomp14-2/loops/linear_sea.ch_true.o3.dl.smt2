(declare-var %_2_0 Real)
(declare-var %_3_0 Real)
(declare-var %_7_1_0 Real)
(declare-var entry_0 Bool)
(declare-var %_1_0 Real)
(declare-var %_7_1_1 Real)
(declare-var bb2.i_0 Bool)
(declare-var bb1.i_0 Bool)
(declare-var E0x46107c0 Bool)
(declare-var %_8_0 Bool)
(declare-var bb.i_0 Bool)
(declare-var E0x4610520 Bool)
(declare-var %_10_0 Bool)
(declare-var %_6_0 Real)
(declare-var %_9_0 Real)
(declare-var %_7_0 Real)
(declare-var __UFO__0_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-ERROR.i ())
(declare-rel cp-rel-bb1.i (Real Real Real ))
(declare-rel cp-rel-__UFO__0 (Real Real Real ))
(declare-rel E_cp-rel-entry_cp-rel-bb1.i (Real Real Real ))
(declare-rel E_cp-rel-bb1.i_cp-rel-bb1.i (Real Real Real Real Real Real ))
(declare-rel E_cp-rel-bb1.i_cp-rel-__UFO__0 (Real Real Real Real Real Real ))
(declare-rel E_cp-rel-__UFO__0_cp-rel-__UFO__0 (Real Real Real Real Real Real ))
(declare-rel E_cp-rel-bb1.i_cp-rel-ERROR.i (Real Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (E_cp-rel-entry_cp-rel-bb1.i |%_2_0| |%_3_0| |%_7_1_0| )) (cp-rel-bb1.i |%_2_0| |%_3_0| |%_7_1_0| )))

(rule (=> (and (and entry_0 (and (<= |%_7_1_0| 0.0) (>= |%_7_1_0| 0.0))) (= |%_2_0| (+ |%_1_0| 1.0))) (E_cp-rel-entry_cp-rel-bb1.i |%_2_0| |%_3_0| |%_7_1_0| )))

(rule (=> (and (cp-rel-bb1.i |%_2_0| |%_3_0| |%_7_1_0| ) (E_cp-rel-bb1.i_cp-rel-bb1.i |%_2_0| |%_3_0| |%_7_1_0| |%_2_0| |%_3_0| |%_7_1_1| )) (cp-rel-bb1.i |%_2_0| |%_3_0| |%_7_1_1| )))

(rule (=> (let (($x60 (=  |%_10_0| (= |%_9_0| 3.0))))
(let (($x55 (=  |%_8_0| (< |%_7_1_0| |%_2_0|))))
(let (($x46 (and (=>  bb.i_0 (and (and bb2.i_0 E0x4610520) (not |%_10_0|))) (=>  bb.i_0 E0x4610520))))
(let (($x35 (and (=>  bb2.i_0 (and (and bb1.i_0 E0x46107c0) |%_8_0|)) (=>  bb2.i_0 E0x46107c0))))
(let (($x53 (and $x35 $x46 (and bb.i_0 (and (<= |%_7_1_1| |%_6_0|) (>= |%_7_1_1| |%_6_0|))))))
(and $x53 (and $x55 $x60 (= |%_6_0| (+ |%_7_1_0| 1.0))))))))) (E_cp-rel-bb1.i_cp-rel-bb1.i |%_2_0| |%_3_0| |%_7_1_0| |%_2_0| |%_3_0| |%_7_1_1| )))

(rule (=> (and (cp-rel-bb1.i |%_2_0| |%_3_0| |%_7_1_0| ) (E_cp-rel-bb1.i_cp-rel-__UFO__0 |%_2_0| |%_3_0| |%_7_1_0| |%_2_0| |%_3_0| |%_7_1_0| )) (cp-rel-__UFO__0 |%_2_0| |%_3_0| |%_7_1_0| )))

(rule (=> (let (($x60 (=  |%_10_0| (= |%_9_0| 3.0))))
(let (($x55 (=  |%_8_0| (< |%_7_1_0| |%_2_0|))))
(let (($x35 (and (=>  bb2.i_0 (and (and bb1.i_0 E0x46107c0) |%_8_0|)) (=>  bb2.i_0 E0x46107c0))))
(and (and $x35 (and bb2.i_0 |%_10_0|)) (and $x55 $x60))))) (E_cp-rel-bb1.i_cp-rel-__UFO__0 |%_2_0| |%_3_0| |%_7_1_0| |%_2_0| |%_3_0| |%_7_1_0| )))

(rule (=> (and (cp-rel-__UFO__0 |%_2_0| |%_3_0| |%_7_0| ) (E_cp-rel-__UFO__0_cp-rel-__UFO__0 |%_2_0| |%_3_0| |%_7_0| |%_2_0| |%_3_0| |%_7_0| )) (cp-rel-__UFO__0 |%_2_0| |%_3_0| |%_7_0| )))

(rule (=> __UFO__0_0 (E_cp-rel-__UFO__0_cp-rel-__UFO__0 |%_2_0| |%_3_0| |%_7_0| |%_2_0| |%_3_0| |%_7_0| )))

(rule (=> (and (cp-rel-bb1.i |%_2_0| |%_3_0| |%_7_1_0| ) (E_cp-rel-bb1.i_cp-rel-ERROR.i |%_2_0| |%_3_0| |%_7_1_0| )) cp-rel-ERROR.i))

(rule (=> (let (($x55 (=  |%_8_0| (< |%_7_1_0| |%_2_0|))))
(and (and bb1.i_0 (not |%_8_0|)) $x55)) (E_cp-rel-bb1.i_cp-rel-ERROR.i |%_2_0| |%_3_0| |%_7_1_0| )))



(query cp-rel-ERROR.i)
