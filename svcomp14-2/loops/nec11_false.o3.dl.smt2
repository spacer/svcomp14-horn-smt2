(declare-var entry_0 Bool)
(declare-var %toBool2_0 Bool)
(declare-var %_0_0 Real)
(declare-var bb_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb4 ())
(declare-rel cp-rel-bb ())
(declare-rel E_cp-rel-entry_cp-rel-bb ())
(declare-rel E_cp-rel-bb_cp-rel-bb ())
(declare-rel E_cp-rel-entry_cp-rel-bb4 ())


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry E_cp-rel-entry_cp-rel-bb) cp-rel-bb))

(rule (=> (let (($x13 (=  |%toBool2_0| (= |%_0_0| 0.0))))
(and (and entry_0 (not |%toBool2_0|)) $x13)) E_cp-rel-entry_cp-rel-bb))

(rule (=> (and cp-rel-bb E_cp-rel-bb_cp-rel-bb) cp-rel-bb))

(rule (=> bb_0 E_cp-rel-bb_cp-rel-bb))

(rule (=> (and cp-rel-entry E_cp-rel-entry_cp-rel-bb4) cp-rel-bb4))

(rule (=> (let (($x13 (=  |%toBool2_0| (= |%_0_0| 0.0))))
(and (and entry_0 |%toBool2_0|) $x13)) E_cp-rel-entry_cp-rel-bb4))



(query cp-rel-bb4)
