(declare-var %indvar_1_0 Real)
(declare-var entry_0 Bool)
(declare-var %indvar_1_1 Real)
(declare-var bb.i_0 Bool)
(declare-var bb1.i_0 Bool)
(declare-var E0x432d5d0 Bool)
(declare-var %_1_0 Bool)
(declare-var __VERIFIER_assert.exit.i_0 Bool)
(declare-var E0x432e5b0 Bool)
(declare-var %_0_0 Bool)
(declare-var %indvar.next_0 Real)
(declare-var %indvar_0 Real)
(declare-var __UFO__0_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-ERROR.i.i ())
(declare-rel cp-rel-bb1.i (Real ))
(declare-rel cp-rel-__UFO__0 (Real ))
(declare-rel E_cp-rel-entry_cp-rel-bb1.i (Real ))
(declare-rel E_cp-rel-bb1.i_cp-rel-bb1.i (Real Real ))
(declare-rel E_cp-rel-bb1.i_cp-rel-__UFO__0 (Real Real ))
(declare-rel E_cp-rel-__UFO__0_cp-rel-__UFO__0 (Real Real ))
(declare-rel E_cp-rel-bb1.i_cp-rel-ERROR.i.i (Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (E_cp-rel-entry_cp-rel-bb1.i |%indvar_1_0| )) (cp-rel-bb1.i |%indvar_1_0| )))

(rule (=> (and entry_0 (and (<= |%indvar_1_0| 0.0) (>= |%indvar_1_0| 0.0))) (E_cp-rel-entry_cp-rel-bb1.i |%indvar_1_0| )))

(rule (=> (and (cp-rel-bb1.i |%indvar_1_0| ) (E_cp-rel-bb1.i_cp-rel-bb1.i |%indvar_1_0| |%indvar_1_1| )) (cp-rel-bb1.i |%indvar_1_1| )))

(rule (=> (let (($x50 (=  |%_0_0| (> |%indvar_1_0| 1.0))))
(let (($x48 (=  |%_1_0| (> |%indvar_1_0| 7.0))))
(let (($x43 (and (<= |%indvar_1_1| |%indvar.next_0|) (>= |%indvar_1_1| |%indvar.next_0|))))
(let (($x36 (=>  __VERIFIER_assert.exit.i_0 (and (and bb.i_0 E0x432e5b0) (not |%_0_0|)))))
(let (($x27 (and (=>  bb.i_0 (and (and bb1.i_0 E0x432d5d0) (not |%_1_0|))) (=>  bb.i_0 E0x432d5d0))))
(let (($x45 (and $x27 (and $x36 (=>  __VERIFIER_assert.exit.i_0 E0x432e5b0)) (and __VERIFIER_assert.exit.i_0 $x43))))
(and $x45 (and $x48 $x50 (= |%indvar.next_0| (+ |%indvar_1_0| 1.0)))))))))) (E_cp-rel-bb1.i_cp-rel-bb1.i |%indvar_1_0| |%indvar_1_1| )))

(rule (=> (and (cp-rel-bb1.i |%indvar_1_0| ) (E_cp-rel-bb1.i_cp-rel-__UFO__0 |%indvar_1_0| |%indvar_1_0| )) (cp-rel-__UFO__0 |%indvar_1_0| )))

(rule (=> (let (($x48 (=  |%_1_0| (> |%indvar_1_0| 7.0))))
(and (and bb1.i_0 |%_1_0|) $x48)) (E_cp-rel-bb1.i_cp-rel-__UFO__0 |%indvar_1_0| |%indvar_1_0| )))

(rule (=> (and (cp-rel-__UFO__0 |%indvar_0| ) (E_cp-rel-__UFO__0_cp-rel-__UFO__0 |%indvar_0| |%indvar_0| )) (cp-rel-__UFO__0 |%indvar_0| )))

(rule (=> __UFO__0_0 (E_cp-rel-__UFO__0_cp-rel-__UFO__0 |%indvar_0| |%indvar_0| )))

(rule (=> (and (cp-rel-bb1.i |%indvar_1_0| ) (E_cp-rel-bb1.i_cp-rel-ERROR.i.i |%indvar_1_0| )) cp-rel-ERROR.i.i))

(rule (=> (let (($x50 (=  |%_0_0| (> |%indvar_1_0| 1.0))))
(let (($x48 (=  |%_1_0| (> |%indvar_1_0| 7.0))))
(let (($x27 (and (=>  bb.i_0 (and (and bb1.i_0 E0x432d5d0) (not |%_1_0|))) (=>  bb.i_0 E0x432d5d0))))
(and (and $x27 (and bb.i_0 |%_0_0|)) (and $x48 $x50))))) (E_cp-rel-bb1.i_cp-rel-ERROR.i.i |%indvar_1_0| )))



(query cp-rel-ERROR.i.i)
