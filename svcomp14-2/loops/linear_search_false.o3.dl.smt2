(declare-var %_2_0 Real)
(declare-var %_3_0 Real)
(declare-var %j.0.i_1_0 Real)
(declare-var entry_0 Bool)
(declare-var %_1_0 Real)
(declare-var %j.0.i_1_1 Real)
(declare-var bb3.i_0 Bool)
(declare-var bb2.i_0 Bool)
(declare-var E0x38b99b0 Bool)
(declare-var %_8_0 Bool)
(declare-var bb.i_0 Bool)
(declare-var E0x38b9710 Bool)
(declare-var %_11_0 Bool)
(declare-var %j.0.i.be_0 Real)
(declare-var %_10_0 Real)
(declare-var %_6_0 Real)
(declare-var %_7_0 Bool)
(declare-var %j.0.i_0 Real)
(declare-var __UFO__0_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-ERROR.i ())
(declare-rel cp-rel-bb2.i (Real Real Real ))
(declare-rel cp-rel-__UFO__0 (Real Real Real ))
(declare-rel E_cp-rel-entry_cp-rel-bb2.i (Real Real Real ))
(declare-rel E_cp-rel-bb2.i_cp-rel-bb2.i (Real Real Real Real Real Real ))
(declare-rel E_cp-rel-bb2.i_cp-rel-__UFO__0 (Real Real Real Real Real Real ))
(declare-rel E_cp-rel-__UFO__0_cp-rel-__UFO__0 (Real Real Real Real Real Real ))
(declare-rel E_cp-rel-bb2.i_cp-rel-ERROR.i (Real Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (E_cp-rel-entry_cp-rel-bb2.i |%_2_0| |%_3_0| |%j.0.i_1_0| )) (cp-rel-bb2.i |%_2_0| |%_3_0| |%j.0.i_1_0| )))

(rule (=> (and (and entry_0 (and (<= |%j.0.i_1_0| 0.0) (>= |%j.0.i_1_0| 0.0))) (= |%_2_0| (+ |%_1_0| 1.0))) (E_cp-rel-entry_cp-rel-bb2.i |%_2_0| |%_3_0| |%j.0.i_1_0| )))

(rule (=> (and (cp-rel-bb2.i |%_2_0| |%_3_0| |%j.0.i_1_0| ) (E_cp-rel-bb2.i_cp-rel-bb2.i |%_2_0| |%_3_0| |%j.0.i_1_0| |%_2_0| |%_3_0| |%j.0.i_1_1| )) (cp-rel-bb2.i |%_2_0| |%_3_0| |%j.0.i_1_1| )))

(rule (=> (let (($x60 (=  |%_11_0| (= |%_10_0| 3.0))))
(let (($x55 (=  |%_8_0| (< |%j.0.i_1_0| |%_2_0|))))
(let (($x73 (and $x55 $x60 (= |%_6_0| (+ |%j.0.i_1_0| 1.0)) (=  |%_7_0| (= |%_6_0| 20.0)) (= |%j.0.i.be_0| (ite  |%_7_0| (~ 1.0) |%_6_0|)))))
(let (($x52 (and bb.i_0 (and (<= |%j.0.i_1_1| |%j.0.i.be_0|) (>= |%j.0.i_1_1| |%j.0.i.be_0|)))))
(let (($x46 (and (=>  bb.i_0 (and (and bb3.i_0 E0x38b9710) (not |%_11_0|))) (=>  bb.i_0 E0x38b9710))))
(let (($x35 (and (=>  bb3.i_0 (and (and bb2.i_0 E0x38b99b0) |%_8_0|)) (=>  bb3.i_0 E0x38b99b0))))
(and (and $x35 $x46 $x52) $x73))))))) (E_cp-rel-bb2.i_cp-rel-bb2.i |%_2_0| |%_3_0| |%j.0.i_1_0| |%_2_0| |%_3_0| |%j.0.i_1_1| )))

(rule (=> (and (cp-rel-bb2.i |%_2_0| |%_3_0| |%j.0.i_1_0| ) (E_cp-rel-bb2.i_cp-rel-__UFO__0 |%_2_0| |%_3_0| |%j.0.i_1_0| |%_2_0| |%_3_0| |%j.0.i_1_0| )) (cp-rel-__UFO__0 |%_2_0| |%_3_0| |%j.0.i_1_0| )))

(rule (=> (let (($x60 (=  |%_11_0| (= |%_10_0| 3.0))))
(let (($x55 (=  |%_8_0| (< |%j.0.i_1_0| |%_2_0|))))
(let (($x35 (and (=>  bb3.i_0 (and (and bb2.i_0 E0x38b99b0) |%_8_0|)) (=>  bb3.i_0 E0x38b99b0))))
(and (and $x35 (and bb3.i_0 |%_11_0|)) (and $x55 $x60))))) (E_cp-rel-bb2.i_cp-rel-__UFO__0 |%_2_0| |%_3_0| |%j.0.i_1_0| |%_2_0| |%_3_0| |%j.0.i_1_0| )))

(rule (=> (and (cp-rel-__UFO__0 |%_2_0| |%_3_0| |%j.0.i_0| ) (E_cp-rel-__UFO__0_cp-rel-__UFO__0 |%_2_0| |%_3_0| |%j.0.i_0| |%_2_0| |%_3_0| |%j.0.i_0| )) (cp-rel-__UFO__0 |%_2_0| |%_3_0| |%j.0.i_0| )))

(rule (=> __UFO__0_0 (E_cp-rel-__UFO__0_cp-rel-__UFO__0 |%_2_0| |%_3_0| |%j.0.i_0| |%_2_0| |%_3_0| |%j.0.i_0| )))

(rule (=> (and (cp-rel-bb2.i |%_2_0| |%_3_0| |%j.0.i_1_0| ) (E_cp-rel-bb2.i_cp-rel-ERROR.i |%_2_0| |%_3_0| |%j.0.i_1_0| )) cp-rel-ERROR.i))

(rule (=> (let (($x55 (=  |%_8_0| (< |%j.0.i_1_0| |%_2_0|))))
(and (and bb2.i_0 (not |%_8_0|)) $x55)) (E_cp-rel-bb2.i_cp-rel-ERROR.i |%_2_0| |%_3_0| |%j.0.i_1_0| )))



(query cp-rel-ERROR.i)
