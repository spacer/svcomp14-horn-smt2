(declare-var %_1_0 Real)
(declare-var %indvar3_1_0 Real)
(declare-var %x.0.ph_1_0 Real)
(declare-var entry_0 Bool)
(declare-var %_0_0 Real)
(declare-var %indvar3_0 Real)
(declare-var %x.0.ph_0 Real)
(declare-var %indvar_1_0 Real)
(declare-var bb.us_0 Bool)
(declare-var bb3.us_0 Bool)
(declare-var E0x4c5eb40 Bool)
(declare-var %.not.us_0 Bool)
(declare-var bb2.us_0 Bool)
(declare-var E0x4c5fae0 Bool)
(declare-var %toBool.us_0 Bool)
(declare-var %indvar.next4_0 Real)
(declare-var %phitmp_0 Real)
(declare-var %x.0.us_0 Real)
(declare-var %_3_0 Real)
(declare-var bb3.outer_0 Bool)
(declare-var %_2_0 Bool)
(declare-var %z.0.ph_0 Real)
(declare-var %indvar_1_1 Real)
(declare-var bb1.us_0 Bool)
(declare-var E0x4c78c90 Bool)
(declare-var %indvar.next_0 Real)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb5.split ())
(declare-rel cp-rel-bb3.outer (Real Real Real ))
(declare-rel cp-rel-bb3.us (Real Real Real Real Real ))
(declare-rel E_cp-rel-entry_cp-rel-bb3.outer (Real Real Real ))
(declare-rel E_cp-rel-bb3.us_cp-rel-bb3.outer (Real Real Real Real Real Real Real Real ))
(declare-rel E_cp-rel-bb3.outer_cp-rel-bb3.us (Real Real Real Real Real Real Real Real ))
(declare-rel E_cp-rel-bb3.us_cp-rel-bb3.us (Real Real Real Real Real Real Real Real Real Real ))
(declare-rel E_cp-rel-bb3.outer_cp-rel-bb5.split (Real Real Real ))
(declare-rel E_cp-rel-bb3.us_cp-rel-bb5.split (Real Real Real Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (E_cp-rel-entry_cp-rel-bb3.outer |%_1_0| |%indvar3_1_0| |%x.0.ph_1_0| )) (cp-rel-bb3.outer |%_1_0| |%indvar3_1_0| |%x.0.ph_1_0| )))

(rule (=> (let (($x18 (and entry_0 (and (<= |%indvar3_1_0| 0.0) (>= |%indvar3_1_0| 0.0)))))
(and $x18 (and (<= |%x.0.ph_1_0| |%_0_0|) (>= |%x.0.ph_1_0| |%_0_0|)))) (E_cp-rel-entry_cp-rel-bb3.outer |%_1_0| |%indvar3_1_0| |%x.0.ph_1_0| )))

(rule (=> (and (cp-rel-bb3.us |%_0_0| |%_1_0| |%indvar3_0| |%x.0.ph_0| |%indvar_1_0| ) (E_cp-rel-bb3.us_cp-rel-bb3.outer |%_0_0| |%_1_0| |%indvar3_0| |%x.0.ph_0| |%indvar_1_0| |%_1_0| |%indvar3_1_0| |%x.0.ph_1_0| )) (cp-rel-bb3.outer |%_1_0| |%indvar3_1_0| |%x.0.ph_1_0| )))

(rule (=> (let (($x75 (=  |%toBool.us_0| (= |%_3_0| 0.0))))
(let (($x71 (=  |%.not.us_0| (< |%x.0.us_0| 100.0))))
(let (($x68 (= |%x.0.us_0| (+ |%indvar_1_0| |%x.0.ph_0|))))
(let (($x81 (and $x68 $x71 $x75 (= |%phitmp_0| (+ |%x.0.us_0| (~ 1.0))) (= |%indvar.next4_0| (+ |%indvar3_0| 1.0)))))
(let (($x56 (and (<= |%indvar3_1_0| |%indvar.next4_0|) (>= |%indvar3_1_0| |%indvar.next4_0|))))
(let (($x63 (and (and bb2.us_0 $x56) (and (<= |%x.0.ph_1_0| |%phitmp_0|) (>= |%x.0.ph_1_0| |%phitmp_0|)))))
(let (($x51 (and (=>  bb2.us_0 (and (and bb.us_0 E0x4c5fae0) |%toBool.us_0|)) (=>  bb2.us_0 E0x4c5fae0))))
(let (($x41 (and (=>  bb.us_0 (and (and bb3.us_0 E0x4c5eb40) |%.not.us_0|)) (=>  bb.us_0 E0x4c5eb40))))
(and (and $x41 $x51 $x63) $x81))))))))) (E_cp-rel-bb3.us_cp-rel-bb3.outer |%_0_0| |%_1_0| |%indvar3_0| |%x.0.ph_0| |%indvar_1_0| |%_1_0| |%indvar3_1_0| |%x.0.ph_1_0| )))

(rule (=> (and (cp-rel-bb3.outer |%_1_0| |%indvar3_1_0| |%x.0.ph_1_0| ) (E_cp-rel-bb3.outer_cp-rel-bb3.us |%_1_0| |%indvar3_1_0| |%x.0.ph_1_0| |%_0_0| |%_1_0| |%indvar3_1_0| |%x.0.ph_1_0| |%indvar_1_0| )) (cp-rel-bb3.us |%_0_0| |%_1_0| |%indvar3_1_0| |%x.0.ph_1_0| |%indvar_1_0| )))

(rule (=> (let (($x99 (and (= |%z.0.ph_0| (+ |%_1_0| (* (~ 1.0) |%indvar3_1_0|))) (=  |%_2_0| (> |%z.0.ph_0| 100.0)))))
(let (($x91 (and (and bb3.outer_0 |%_2_0|) (and (<= |%indvar_1_0| 0.0) (>= |%indvar_1_0| 0.0)))))
(and $x91 $x99))) (E_cp-rel-bb3.outer_cp-rel-bb3.us |%_1_0| |%indvar3_1_0| |%x.0.ph_1_0| |%_0_0| |%_1_0| |%indvar3_1_0| |%x.0.ph_1_0| |%indvar_1_0| )))

(rule (=> (and (cp-rel-bb3.us |%_0_0| |%_1_0| |%indvar3_0| |%x.0.ph_0| |%indvar_1_0| ) (E_cp-rel-bb3.us_cp-rel-bb3.us |%_0_0| |%_1_0| |%indvar3_0| |%x.0.ph_0| |%indvar_1_0| |%_0_0| |%_1_0| |%indvar3_0| |%x.0.ph_0| |%indvar_1_1| )) (cp-rel-bb3.us |%_0_0| |%_1_0| |%indvar3_0| |%x.0.ph_0| |%indvar_1_1| )))

(rule (=> (let (($x75 (=  |%toBool.us_0| (= |%_3_0| 0.0))))
(let (($x71 (=  |%.not.us_0| (< |%x.0.us_0| 100.0))))
(let (($x68 (= |%x.0.us_0| (+ |%indvar_1_0| |%x.0.ph_0|))))
(let (($x115 (and (<= |%indvar_1_1| |%indvar.next_0|) (>= |%indvar_1_1| |%indvar.next_0|))))
(let (($x108 (=>  bb1.us_0 (and (and bb.us_0 E0x4c78c90) (not |%toBool.us_0|)))))
(let (($x41 (and (=>  bb.us_0 (and (and bb3.us_0 E0x4c5eb40) |%.not.us_0|)) (=>  bb.us_0 E0x4c5eb40))))
(let (($x117 (and $x41 (and $x108 (=>  bb1.us_0 E0x4c78c90)) (and bb1.us_0 $x115))))
(and $x117 (and $x68 $x71 $x75 (= |%indvar.next_0| (+ |%indvar_1_0| 1.0))))))))))) (E_cp-rel-bb3.us_cp-rel-bb3.us |%_0_0| |%_1_0| |%indvar3_0| |%x.0.ph_0| |%indvar_1_0| |%_0_0| |%_1_0| |%indvar3_0| |%x.0.ph_0| |%indvar_1_1| )))

(rule (=> (and (cp-rel-bb3.outer |%_1_0| |%indvar3_1_0| |%x.0.ph_1_0| ) (E_cp-rel-bb3.outer_cp-rel-bb5.split |%_1_0| |%indvar3_1_0| |%x.0.ph_1_0| )) cp-rel-bb5.split))

(rule (=> (let (($x99 (and (= |%z.0.ph_0| (+ |%_1_0| (* (~ 1.0) |%indvar3_1_0|))) (=  |%_2_0| (> |%z.0.ph_0| 100.0)))))
(and (and bb3.outer_0 (not |%_2_0|)) $x99)) (E_cp-rel-bb3.outer_cp-rel-bb5.split |%_1_0| |%indvar3_1_0| |%x.0.ph_1_0| )))

(rule (=> (and (cp-rel-bb3.us |%_0_0| |%_1_0| |%indvar3_0| |%x.0.ph_0| |%indvar_1_0| ) (E_cp-rel-bb3.us_cp-rel-bb5.split |%_0_0| |%_1_0| |%indvar3_0| |%x.0.ph_0| |%indvar_1_0| )) cp-rel-bb5.split))

(rule (=> (let (($x71 (=  |%.not.us_0| (< |%x.0.us_0| 100.0))))
(let (($x68 (= |%x.0.us_0| (+ |%indvar_1_0| |%x.0.ph_0|))))
(and (and bb3.us_0 (not |%.not.us_0|)) (and $x68 $x71)))) (E_cp-rel-bb3.us_cp-rel-bb5.split |%_0_0| |%_1_0| |%indvar3_0| |%x.0.ph_0| |%indvar_1_0| )))



(query cp-rel-bb5.split)
