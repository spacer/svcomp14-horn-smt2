(declare-var %_0_0 Real)
(declare-var %_1_0 Real)
(declare-var %tmp12_0 Real)
(declare-var %_3_1_0 Real)
(declare-var bb.nph_0 Bool)
(declare-var entry_0 Bool)
(declare-var E0x3f5b8f0 Bool)
(declare-var %_2_0 Bool)
(declare-var %_3_0 Real)
(declare-var %tmp7_0 Real)
(declare-var %_4_0 Real)
(declare-var %indvar3_1_0 Real)
(declare-var bb5_0 Bool)
(declare-var bb4_0 Bool)
(declare-var E0x3f75b00 Bool)
(declare-var %_6_0 Bool)
(declare-var bb6_0 Bool)
(declare-var E0x3f76bc0 Bool)
(declare-var %_8_0 Bool)
(declare-var E0x3f76fc0 Bool)
(declare-var %exitcond_0 Bool)
(declare-var %i.0.in_0 Real)
(declare-var %i.0_0 Real)
(declare-var %_7_0 Real)
(declare-var bb_0 Bool)
(declare-var %indvar3_1_1 Real)
(declare-var bb1_0 Bool)
(declare-var E0x3f78d30 Bool)
(declare-var bb2_0 Bool)
(declare-var E0x3f798e0 Bool)
(declare-var %_5_0 Bool)
(declare-var bb4.backedge_0 Bool)
(declare-var E0x3f7a280 Bool)
(declare-var E0x3f7a500 Bool)
(declare-var %indvar.next4_0 Real)
(declare-var %indvar_1_0 Real)
(declare-var %indvar_1_1 Real)
(declare-var bb9_0 Bool)
(declare-var bb10_0 Bool)
(declare-var E0x3f5cc70 Bool)
(declare-var %_12_0 Bool)
(declare-var %_11_0 Bool)
(declare-var %k.0_0 Real)
(declare-var %_9_0 Real)
(declare-var %_10_0 Real)
(declare-var %indvar_0 Real)
(declare-var __UFO__0_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-ERROR.i ())
(declare-rel cp-rel-bb (Real Real Real Real ))
(declare-rel cp-rel-bb4 (Real Real Real Real Real Real Real ))
(declare-rel cp-rel-bb10 (Real Real Real ))
(declare-rel cp-rel-__UFO__0 (Real Real Real Real ))
(declare-rel E_cp-rel-entry_cp-rel-bb (Real Real Real Real ))
(declare-rel E_cp-rel-bb4_cp-rel-bb (Real Real Real Real Real Real Real Real Real Real Real ))
(declare-rel E_cp-rel-bb_cp-rel-bb4 (Real Real Real Real Real Real Real Real Real Real Real ))
(declare-rel E_cp-rel-bb4_cp-rel-bb4 (Real Real Real Real Real Real Real Real Real Real Real Real Real Real ))
(declare-rel E_cp-rel-entry_cp-rel-bb10 (Real Real Real ))
(declare-rel E_cp-rel-bb4_cp-rel-bb10 (Real Real Real Real Real Real Real Real Real Real ))
(declare-rel E_cp-rel-bb10_cp-rel-bb10 (Real Real Real Real Real Real ))
(declare-rel E_cp-rel-bb10_cp-rel-__UFO__0 (Real Real Real Real Real Real Real ))
(declare-rel E_cp-rel-__UFO__0_cp-rel-__UFO__0 (Real Real Real Real Real Real Real Real ))
(declare-rel E_cp-rel-bb10_cp-rel-ERROR.i (Real Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (E_cp-rel-entry_cp-rel-bb |%_0_0| |%_1_0| |%tmp12_0| |%_3_1_0| )) (cp-rel-bb |%_0_0| |%_1_0| |%tmp12_0| |%_3_1_0| )))

(rule (=> (let (($x32 (=  |%_2_0| (> |%_0_0| 1.0))))
(let (($x25 (and (=>  bb.nph_0 (and (and entry_0 E0x3f5b8f0) |%_2_0|)) (=>  bb.nph_0 E0x3f5b8f0))))
(let (($x30 (and $x25 (and bb.nph_0 (and (<= |%_3_1_0| 0.0) (>= |%_3_1_0| 0.0))))))
(and $x30 (and $x32 (= |%tmp12_0| (+ |%_0_0| (~ 1.0)))))))) (E_cp-rel-entry_cp-rel-bb |%_0_0| |%_1_0| |%tmp12_0| |%_3_1_0| )))

(rule (=> (and (cp-rel-bb4 |%_0_0| |%_1_0| |%tmp12_0| |%_3_0| |%tmp7_0| |%_4_0| |%indvar3_1_0| ) (E_cp-rel-bb4_cp-rel-bb |%_0_0| |%_1_0| |%tmp12_0| |%_3_0| |%tmp7_0| |%_4_0| |%indvar3_1_0| |%_0_0| |%_1_0| |%tmp12_0| |%_3_1_0| )) (cp-rel-bb |%_0_0| |%_1_0| |%tmp12_0| |%_3_1_0| )))

(rule (=> (let (($x102 (=  |%_8_0| (> |%_7_0| |%_4_0|))))
(let (($x98 (=  |%_6_0| (< |%i.0_0| 0.0))))
(let (($x96 (= |%i.0_0| (+ |%_3_0| (* (~ 1.0) |%indvar3_1_0|)))))
(let (($x92 (= |%i.0.in_0| (+ |%tmp7_0| (* (~ 1.0) |%indvar3_1_0|)))))
(let (($x105 (and $x92 $x96 $x98 $x102 (=  |%exitcond_0| (= |%tmp7_0| |%tmp12_0|)))))
(let (($x86 (and (and bb6_0 (not |%exitcond_0|)) (and (<= |%_3_1_0| |%tmp7_0|) (>= |%_3_1_0| |%tmp7_0|)))))
(let (($x76 (or (and E0x3f76bc0 (not E0x3f76fc0)) (and E0x3f76fc0 (not E0x3f76bc0)))))
(let (($x70 (or (and (and bb5_0 E0x3f76bc0) (not |%_8_0|)) (and (and bb4_0 E0x3f76fc0) |%_6_0|))))
(let (($x78 (and (=>  bb6_0 $x70) (=>  bb6_0 $x76))))
(let (($x58 (and (=>  bb5_0 (and (and bb4_0 E0x3f75b00) (not |%_6_0|))) (=>  bb5_0 E0x3f75b00))))
(and (and $x58 $x78 $x86) $x105))))))))))) (E_cp-rel-bb4_cp-rel-bb |%_0_0| |%_1_0| |%tmp12_0| |%_3_0| |%tmp7_0| |%_4_0| |%indvar3_1_0| |%_0_0| |%_1_0| |%tmp12_0| |%_3_1_0| )))

(rule (=> (and (cp-rel-bb |%_0_0| |%_1_0| |%tmp12_0| |%_3_1_0| ) (E_cp-rel-bb_cp-rel-bb4 |%_0_0| |%_1_0| |%tmp12_0| |%_3_1_0| |%_0_0| |%_1_0| |%tmp12_0| |%_3_1_0| |%tmp7_0| |%_4_0| |%indvar3_1_0| )) (cp-rel-bb4 |%_0_0| |%_1_0| |%tmp12_0| |%_3_1_0| |%tmp7_0| |%_4_0| |%indvar3_1_0| )))

(rule (=> (let (($x112 (and bb_0 (and (<= |%indvar3_1_0| 0.0) (>= |%indvar3_1_0| 0.0)))))
(and $x112 (= |%tmp7_0| (+ |%_3_1_0| 1.0)))) (E_cp-rel-bb_cp-rel-bb4 |%_0_0| |%_1_0| |%tmp12_0| |%_3_1_0| |%_0_0| |%_1_0| |%tmp12_0| |%_3_1_0| |%tmp7_0| |%_4_0| |%indvar3_1_0| )))

(rule (=> (and (cp-rel-bb4 |%_0_0| |%_1_0| |%tmp12_0| |%_3_0| |%tmp7_0| |%_4_0| |%indvar3_1_0| ) (E_cp-rel-bb4_cp-rel-bb4 |%_0_0| |%_1_0| |%tmp12_0| |%_3_0| |%tmp7_0| |%_4_0| |%indvar3_1_0| |%_0_0| |%_1_0| |%tmp12_0| |%_3_0| |%tmp7_0| |%_4_0| |%indvar3_1_1| )) (cp-rel-bb4 |%_0_0| |%_1_0| |%tmp12_0| |%_3_0| |%tmp7_0| |%_4_0| |%indvar3_1_1| )))

(rule (=> (let (($x102 (=  |%_8_0| (> |%_7_0| |%_4_0|))))
(let (($x98 (=  |%_6_0| (< |%i.0_0| 0.0))))
(let (($x96 (= |%i.0_0| (+ |%_3_0| (* (~ 1.0) |%indvar3_1_0|)))))
(let (($x92 (= |%i.0.in_0| (+ |%tmp7_0| (* (~ 1.0) |%indvar3_1_0|)))))
(let (($x163 (and $x92 $x96 $x98 $x102 (=  |%_5_0| (> |%i.0_0| 1.0)) (= |%indvar.next4_0| (+ |%indvar3_1_0| 1.0)))))
(let (($x156 (and (<= |%indvar3_1_1| |%indvar.next4_0|) (>= |%indvar3_1_1| |%indvar.next4_0|))))
(let (($x149 (or (and E0x3f7a280 (not E0x3f7a500)) (and E0x3f7a500 (not E0x3f7a280)))))
(let (($x143 (or (and bb2_0 E0x3f7a280) (and (and bb1_0 E0x3f7a500) |%_5_0|))))
(let (($x135 (and (=>  bb2_0 (and (and bb1_0 E0x3f798e0) (not |%_5_0|))) (=>  bb2_0 E0x3f798e0))))
(let (($x124 (and (=>  bb1_0 (and (and bb5_0 E0x3f78d30) |%_8_0|)) (=>  bb1_0 E0x3f78d30))))
(let (($x58 (and (=>  bb5_0 (and (and bb4_0 E0x3f75b00) (not |%_6_0|))) (=>  bb5_0 E0x3f75b00))))
(let (($x158 (and $x58 $x124 $x135 (and (=>  bb4.backedge_0 $x143) (=>  bb4.backedge_0 $x149)) (and bb4.backedge_0 $x156))))
(and $x158 $x163))))))))))))) (E_cp-rel-bb4_cp-rel-bb4 |%_0_0| |%_1_0| |%tmp12_0| |%_3_0| |%tmp7_0| |%_4_0| |%indvar3_1_0| |%_0_0| |%_1_0| |%tmp12_0| |%_3_0| |%tmp7_0| |%_4_0| |%indvar3_1_1| )))

(rule (=> (and cp-rel-entry (E_cp-rel-entry_cp-rel-bb10 |%_0_0| |%_1_0| |%indvar_1_0| )) (cp-rel-bb10 |%_0_0| |%_1_0| |%indvar_1_0| )))

(rule (=> (let (($x32 (=  |%_2_0| (> |%_0_0| 1.0))))
(let (($x172 (and (<= |%indvar_1_0| 0.0) (>= |%indvar_1_0| 0.0))))
(and (and (and entry_0 (not |%_2_0|)) $x172) $x32))) (E_cp-rel-entry_cp-rel-bb10 |%_0_0| |%_1_0| |%indvar_1_0| )))

(rule (=> (and (cp-rel-bb4 |%_0_0| |%_1_0| |%tmp12_0| |%_3_0| |%tmp7_0| |%_4_0| |%indvar3_1_0| ) (E_cp-rel-bb4_cp-rel-bb10 |%_0_0| |%_1_0| |%tmp12_0| |%_3_0| |%tmp7_0| |%_4_0| |%indvar3_1_0| |%_0_0| |%_1_0| |%indvar_1_0| )) (cp-rel-bb10 |%_0_0| |%_1_0| |%indvar_1_0| )))

(rule (=> (let (($x102 (=  |%_8_0| (> |%_7_0| |%_4_0|))))
(let (($x98 (=  |%_6_0| (< |%i.0_0| 0.0))))
(let (($x96 (= |%i.0_0| (+ |%_3_0| (* (~ 1.0) |%indvar3_1_0|)))))
(let (($x92 (= |%i.0.in_0| (+ |%tmp7_0| (* (~ 1.0) |%indvar3_1_0|)))))
(let (($x105 (and $x92 $x96 $x98 $x102 (=  |%exitcond_0| (= |%tmp7_0| |%tmp12_0|)))))
(let (($x172 (and (<= |%indvar_1_0| 0.0) (>= |%indvar_1_0| 0.0))))
(let (($x76 (or (and E0x3f76bc0 (not E0x3f76fc0)) (and E0x3f76fc0 (not E0x3f76bc0)))))
(let (($x70 (or (and (and bb5_0 E0x3f76bc0) (not |%_8_0|)) (and (and bb4_0 E0x3f76fc0) |%_6_0|))))
(let (($x78 (and (=>  bb6_0 $x70) (=>  bb6_0 $x76))))
(let (($x58 (and (=>  bb5_0 (and (and bb4_0 E0x3f75b00) (not |%_6_0|))) (=>  bb5_0 E0x3f75b00))))
(and (and $x58 $x78 (and (and bb6_0 |%exitcond_0|) $x172)) $x105))))))))))) (E_cp-rel-bb4_cp-rel-bb10 |%_0_0| |%_1_0| |%tmp12_0| |%_3_0| |%tmp7_0| |%_4_0| |%indvar3_1_0| |%_0_0| |%_1_0| |%indvar_1_0| )))

(rule (=> (and (cp-rel-bb10 |%_0_0| |%_1_0| |%indvar_1_0| ) (E_cp-rel-bb10_cp-rel-bb10 |%_0_0| |%_1_0| |%indvar_1_0| |%_0_0| |%_1_0| |%indvar_1_1| )) (cp-rel-bb10 |%_0_0| |%_1_0| |%indvar_1_1| )))

(rule (=> (let (($x206 (=  |%_12_0| (< |%k.0_0| |%_0_0|))))
(let (($x204 (= |%k.0_0| (+ |%indvar_1_0| 1.0))))
(let (($x213 (and $x204 $x206 (=  |%_11_0| (> |%_9_0| |%_10_0|)))))
(let (($x201 (and (and bb9_0 (not |%_11_0|)) (and (<= |%indvar_1_1| |%k.0_0|) (>= |%indvar_1_1| |%k.0_0|)))))
(let (($x191 (and (=>  bb9_0 (and (and bb10_0 E0x3f5cc70) |%_12_0|)) (=>  bb9_0 E0x3f5cc70))))
(and (and $x191 $x201) $x213)))))) (E_cp-rel-bb10_cp-rel-bb10 |%_0_0| |%_1_0| |%indvar_1_0| |%_0_0| |%_1_0| |%indvar_1_1| )))

(rule (=> (and (cp-rel-bb10 |%_0_0| |%_1_0| |%indvar_1_0| ) (E_cp-rel-bb10_cp-rel-__UFO__0 |%_0_0| |%_1_0| |%indvar_1_0| |%_0_0| |%_1_0| |%indvar_1_0| |%k.0_0| )) (cp-rel-__UFO__0 |%_0_0| |%_1_0| |%indvar_1_0| |%k.0_0| )))

(rule (=> (let (($x206 (=  |%_12_0| (< |%k.0_0| |%_0_0|))))
(let (($x204 (= |%k.0_0| (+ |%indvar_1_0| 1.0))))
(and (and bb10_0 (not |%_12_0|)) (and $x204 $x206)))) (E_cp-rel-bb10_cp-rel-__UFO__0 |%_0_0| |%_1_0| |%indvar_1_0| |%_0_0| |%_1_0| |%indvar_1_0| |%k.0_0| )))

(rule (=> (and (cp-rel-__UFO__0 |%_0_0| |%_1_0| |%indvar_0| |%k.0_0| ) (E_cp-rel-__UFO__0_cp-rel-__UFO__0 |%_0_0| |%_1_0| |%indvar_0| |%k.0_0| |%_0_0| |%_1_0| |%indvar_0| |%k.0_0| )) (cp-rel-__UFO__0 |%_0_0| |%_1_0| |%indvar_0| |%k.0_0| )))

(rule (=> __UFO__0_0 (E_cp-rel-__UFO__0_cp-rel-__UFO__0 |%_0_0| |%_1_0| |%indvar_0| |%k.0_0| |%_0_0| |%_1_0| |%indvar_0| |%k.0_0| )))

(rule (=> (and (cp-rel-bb10 |%_0_0| |%_1_0| |%indvar_1_0| ) (E_cp-rel-bb10_cp-rel-ERROR.i |%_0_0| |%_1_0| |%indvar_1_0| )) cp-rel-ERROR.i))

(rule (=> (let (($x206 (=  |%_12_0| (< |%k.0_0| |%_0_0|))))
(let (($x204 (= |%k.0_0| (+ |%indvar_1_0| 1.0))))
(let (($x213 (and $x204 $x206 (=  |%_11_0| (> |%_9_0| |%_10_0|)))))
(let (($x191 (and (=>  bb9_0 (and (and bb10_0 E0x3f5cc70) |%_12_0|)) (=>  bb9_0 E0x3f5cc70))))
(and (and $x191 (and bb9_0 |%_11_0|)) $x213))))) (E_cp-rel-bb10_cp-rel-ERROR.i |%_0_0| |%_1_0| |%indvar_1_0| )))



(query cp-rel-ERROR.i)
