(declare-var %pumpRunning.2_1_0 Real)
(declare-var %systemActive.1_1_0 Real)
(declare-var %waterLevel.2_1_0 Real)
(declare-var %methaneLevelCritical.1_1_0 Real)
(declare-var entry_0 Bool)
(declare-var %pumpRunning.2_1_1 Real)
(declare-var %systemActive.1_1_1 Real)
(declare-var %waterLevel.2_1_1 Real)
(declare-var %methaneLevelCritical.1_1_1 Real)
(declare-var bb2.i.i_0 Bool)
(declare-var bb1.i.i_0 Bool)
(declare-var E0x371d110 Bool)
(declare-var %_1_0 Bool)
(declare-var bb3.i.i_0 Bool)
(declare-var E0x371ec60 Bool)
(declare-var %waterLevel.1_0 Real)
(declare-var %waterLevel.0_0 Real)
(declare-var E0x3735ee0 Bool)
(declare-var bb4.i.i_0 Bool)
(declare-var E0x3737460 Bool)
(declare-var %_4_0 Bool)
(declare-var bb5.i.i_0 Bool)
(declare-var E0x3738530 Bool)
(declare-var %methaneLevelCritical.0_0 Real)
(declare-var %storemerge.i.i.i_0 Real)
(declare-var E0x3738b10 Bool)
(declare-var bb6.i.i_0 Bool)
(declare-var E0x373a030 Bool)
(declare-var %_7_0 Bool)
(declare-var bb8.i.i_0 Bool)
(declare-var E0x373ae60 Bool)
(declare-var %_9_0 Bool)
(declare-var E0x373b1a0 Bool)
(declare-var bb.i1.i.i_0 Bool)
(declare-var E0x373c1f0 Bool)
(declare-var %_10_0 Bool)
(declare-var bb1.i.i.i_0 Bool)
(declare-var E0x373d7b0 Bool)
(declare-var %waterLevel.4_0 Real)
(declare-var %waterLevel.3_0 Real)
(declare-var E0x373de70 Bool)
(declare-var bb2.i.i.i_0 Bool)
(declare-var E0x373f280 Bool)
(declare-var %_13_0 Bool)
(declare-var bb.i.i.i.i.i_0 Bool)
(declare-var E0x373fb50 Bool)
(declare-var bb.i1.i.i.i_0 Bool)
(declare-var E0x3740910 Bool)
(declare-var bb2.i3.i.i.i_0 Bool)
(declare-var E0x3741510 Bool)
(declare-var %_14_0 Bool)
(declare-var bb3.i.i.i_0 Bool)
(declare-var E0x3742470 Bool)
(declare-var %pumpRunning.1.reg2mem.0_0 Real)
(declare-var %systemActive.0.reg2mem.0_0 Real)
(declare-var %waterLevel.4.reg2mem.0_0 Real)
(declare-var %pumpRunning.4_0 Real)
(declare-var %pumpRunning.6_0 Real)
(declare-var E0x37434f0 Bool)
(declare-var E0x3743b80 Bool)
(declare-var E0x3744460 Bool)
(declare-var E0x3744a40 Bool)
(declare-var %or.cond1_0 Bool)
(declare-var %_0_0 Real)
(declare-var %not._0 Bool)
(declare-var %_2_0 Real)
(declare-var %_3_0 Real)
(declare-var %_5_0 Bool)
(declare-var %_6_0 Real)
(declare-var %_8_0 Real)
(declare-var %_11_0 Bool)
(declare-var %_12_0 Real)
(declare-var %_15_0 Bool)
(declare-var %_16_0 Bool)
(declare-var %_17_0 Bool)
(declare-var %or.cond_0 Bool)
(declare-var %or.cond.not_0 Bool)
(declare-var %_18_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb2.i.i.i.i ())
(declare-rel cp-rel-bb1.i.i (Real Real Real Real ))
(declare-rel E_cp-rel-entry_cp-rel-bb1.i.i (Real Real Real Real ))
(declare-rel E_cp-rel-bb1.i.i_cp-rel-bb1.i.i (Real Real Real Real Real Real Real Real ))
(declare-rel E_cp-rel-bb1.i.i_cp-rel-bb2.i.i.i.i (Real Real Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (E_cp-rel-entry_cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )) (cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )))

(rule (=> (let (($x33 (and (<= |%methaneLevelCritical.1_1_0| 0.0) (>= |%methaneLevelCritical.1_1_0| 0.0))))
(let (($x22 (and entry_0 (and (<= |%pumpRunning.2_1_0| 0.0) (>= |%pumpRunning.2_1_0| 0.0)))))
(let (($x26 (and $x22 (and (<= |%systemActive.1_1_0| 1.0) (>= |%systemActive.1_1_0| 1.0)))))
(let (($x30 (and $x26 (and (<= |%waterLevel.2_1_0| 1.0) (>= |%waterLevel.2_1_0| 1.0)))))
(and $x30 $x33))))) (E_cp-rel-entry_cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )))

(rule (=> (and (cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| ) (E_cp-rel-bb1.i.i_cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| |%pumpRunning.2_1_1| |%systemActive.1_1_1| |%waterLevel.2_1_1| |%methaneLevelCritical.1_1_1| )) (cp-rel-bb1.i.i |%pumpRunning.2_1_1| |%systemActive.1_1_1| |%waterLevel.2_1_1| |%methaneLevelCritical.1_1_1| )))

(rule (=> (let (($x413 (and (=  |%_1_0| (= |%_0_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_2_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_2_0| |%waterLevel.2_1_0|)) (=  |%_4_0| (= |%_3_0| 0.0)) (=  |%_5_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_5_0| 1.0 0.0)) (=  |%_7_0| (= |%_6_0| 0.0)) (=  |%_9_0| (= |%_8_0| 0.0)) (=  |%_10_0| (= |%pumpRunning.2_1_0| 0.0)) (=  |%_11_0| (> |%waterLevel.1_0| 0.0)) (= |%_12_0| (+ |%waterLevel.1_0| (~ 1.0))) (= |%waterLevel.3_0| (ite  |%_11_0| |%_12_0| |%waterLevel.1_0|)) (=  |%_13_0| (= |%systemActive.1_1_0| 0.0)) (=  |%_15_0| (> |%waterLevel.4_0| 1.0)) (= |%pumpRunning.6_0| (ite  |%_15_0| 1.0 |%pumpRunning.2_1_0|)) (=  |%_14_0| (= |%methaneLevelCritical.0_0| 0.0)) (=  |%_16_0| (= |%waterLevel.4.reg2mem.0_0| 2.0)) (=  |%_17_0| (= |%pumpRunning.4_0| 0.0)) (=  |%or.cond_0| (or |%_17_0| |%_16_0|)) (=  |%or.cond.not_0| (xor |%or.cond_0| true)) (=  |%_18_0| (= |%pumpRunning.1.reg2mem.0_0| 0.0)) (=  |%or.cond1_0| (and |%_18_0| |%or.cond.not_0|)))))
(let (($x332 (and (<= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|) (>= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|))))
(let (($x328 (and (<= |%waterLevel.2_1_1| |%waterLevel.4.reg2mem.0_0|) (>= |%waterLevel.2_1_1| |%waterLevel.4.reg2mem.0_0|))))
(let (($x324 (and (<= |%systemActive.1_1_1| |%systemActive.0.reg2mem.0_0|) (>= |%systemActive.1_1_1| |%systemActive.0.reg2mem.0_0|))))
(let (($x320 (and (<= |%pumpRunning.2_1_1| |%pumpRunning.4_0|) (>= |%pumpRunning.2_1_1| |%pumpRunning.4_0|))))
(let (($x329 (and (and (and (and bb3.i.i.i_0 (not |%or.cond1_0|)) $x320) $x324) $x328)))
(let (($x303 (not E0x3744460)))
(let (($x302 (not E0x3743b80)))
(let (($x301 (not E0x37434f0)))
(let (($x306 (not E0x3742470)))
(let (($x311 (or (and E0x3742470 $x301 $x302 $x303 (not E0x3744a40)) (and E0x37434f0 $x306 $x302 $x303 (not E0x3744a40)) (and E0x3743b80 $x306 $x301 $x303 (not E0x3744a40)) (and E0x3744460 $x306 $x301 $x302 (not E0x3744a40)) (and E0x3744a40 $x306 $x301 $x302 $x303))))
(let (($x273 (and (<= |%pumpRunning.4_0| 0.0) (>= |%pumpRunning.4_0| 0.0))))
(let (($x296 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|))))
(let (($x292 (and (<= |%systemActive.0.reg2mem.0_0| 0.0) (>= |%systemActive.0.reg2mem.0_0| 0.0))))
(let (($x288 (and (<= |%pumpRunning.1.reg2mem.0_0| 0.0) (>= |%pumpRunning.1.reg2mem.0_0| 0.0))))
(let (($x293 (and (and (and (and bb6.i.i_0 E0x3744a40) (not |%_9_0|)) $x288) $x292)))
(let (($x262 (and (<= |%pumpRunning.4_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.4_0| |%pumpRunning.2_1_0|))))
(let (($x245 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.4_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.4_0|))))
(let (($x239 (and (<= |%systemActive.0.reg2mem.0_0| |%systemActive.1_1_0|) (>= |%systemActive.0.reg2mem.0_0| |%systemActive.1_1_0|))))
(let (($x233 (and (<= |%pumpRunning.1.reg2mem.0_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.1.reg2mem.0_0| |%pumpRunning.2_1_0|))))
(let (($x280 (and (and (and (and (and bb1.i.i.i_0 E0x3744460) |%_13_0|) $x233) $x239) $x245)))
(let (($x269 (and (and (and (and bb.i1.i.i.i_0 E0x3743b80) (not |%_14_0|)) $x233) $x239)))
(let (($x263 (and (and (and (and (and bb2.i3.i.i.i_0 E0x37434f0) $x233) $x239) $x245) $x262)))
(let (($x253 (and (<= |%pumpRunning.4_0| |%pumpRunning.6_0|) (>= |%pumpRunning.4_0| |%pumpRunning.6_0|))))
(let (($x254 (and (and (and (and (and bb.i.i.i.i.i_0 E0x3742470) $x233) $x239) $x245) $x253)))
(let (($x299 (or $x254 $x263 (and (and $x269 $x245) $x273) (and $x280 $x262) (and (and $x293 $x296) $x273))))
(let (($x313 (and (=>  bb3.i.i.i_0 $x299) (=>  bb3.i.i.i_0 $x311))))
(let (($x222 (=>  bb2.i3.i.i.i_0 (and (and bb.i1.i.i.i_0 E0x3741510) |%_14_0|))))
(let (($x224 (and $x222 (=>  bb2.i3.i.i.i_0 E0x3741510))))
(let (($x212 (=>  bb.i1.i.i.i_0 (and (and bb2.i.i.i_0 E0x3740910) (not |%_10_0|)))))
(let (($x214 (and $x212 (=>  bb.i1.i.i.i_0 E0x3740910))))
(let (($x204 (=>  bb.i.i.i.i.i_0 (and (and bb2.i.i.i_0 E0x373fb50) |%_10_0|))))
(let (($x206 (and $x204 (=>  bb.i.i.i.i.i_0 E0x373fb50))))
(let (($x196 (=>  bb2.i.i.i_0 (and (and bb1.i.i.i_0 E0x373f280) (not |%_13_0|)))))
(let (($x198 (and $x196 (=>  bb2.i.i.i_0 E0x373f280))))
(let (($x185 (or (and E0x373d7b0 (not E0x373de70)) (and E0x373de70 (not E0x373d7b0)))))
(let (($x177 (and (<= |%waterLevel.4_0| |%waterLevel.1_0|) (>= |%waterLevel.4_0| |%waterLevel.1_0|))))
(let (($x170 (and (<= |%waterLevel.4_0| |%waterLevel.3_0|) (>= |%waterLevel.4_0| |%waterLevel.3_0|))))
(let (($x179 (or (and (and bb.i1.i.i_0 E0x373d7b0) $x170) (and (and (and bb8.i.i_0 E0x373de70) |%_10_0|) $x177))))
(let (($x187 (and (=>  bb1.i.i.i_0 $x179) (=>  bb1.i.i.i_0 $x185))))
(let (($x157 (=>  bb.i1.i.i_0 (and (and bb8.i.i_0 E0x373c1f0) (not |%_10_0|)))))
(let (($x159 (and $x157 (=>  bb.i1.i.i_0 E0x373c1f0))))
(let (($x146 (or (and E0x373ae60 (not E0x373b1a0)) (and E0x373b1a0 (not E0x373ae60)))))
(let (($x140 (or (and (and bb6.i.i_0 E0x373ae60) |%_9_0|) (and (and bb5.i.i_0 E0x373b1a0) (not |%_7_0|)))))
(let (($x148 (and (=>  bb8.i.i_0 $x140) (=>  bb8.i.i_0 $x146))))
(let (($x128 (and (=>  bb6.i.i_0 (and (and bb5.i.i_0 E0x373a030) |%_7_0|)) (=>  bb6.i.i_0 E0x373a030))))
(let (($x116 (or (and E0x3738530 (not E0x3738b10)) (and E0x3738b10 (not E0x3738530)))))
(let (($x108 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x101 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x110 (or (and (and bb4.i.i_0 E0x3738530) $x101) (and (and (and bb3.i.i_0 E0x3738b10) |%_4_0|) $x108))))
(let (($x118 (and (=>  bb5.i.i_0 $x110) (=>  bb5.i.i_0 $x116))))
(let (($x90 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x3737460) (not |%_4_0|))) (=>  bb4.i.i_0 E0x3737460))))
(let (($x77 (or (and E0x371ec60 (not E0x3735ee0)) (and E0x3735ee0 (not E0x371ec60)))))
(let (($x69 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x62 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x71 (or (and (and bb2.i.i_0 E0x371ec60) $x62) (and (and (and bb1.i.i_0 E0x3735ee0) |%_1_0|) $x69))))
(let (($x79 (and (=>  bb3.i.i_0 $x71) (=>  bb3.i.i_0 $x77))))
(let (($x51 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x371d110) (not |%_1_0|))) (=>  bb2.i.i_0 E0x371d110))))
(let (($x334 (and $x51 $x79 $x90 $x118 $x128 $x148 $x159 $x187 $x198 $x206 $x214 $x224 $x313 (and $x329 $x332))))
(and $x334 $x413)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))) (E_cp-rel-bb1.i.i_cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| |%pumpRunning.2_1_1| |%systemActive.1_1_1| |%waterLevel.2_1_1| |%methaneLevelCritical.1_1_1| )))

(rule (=> (and (cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| ) (E_cp-rel-bb1.i.i_cp-rel-bb2.i.i.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )) cp-rel-bb2.i.i.i.i))

(rule (=> (let (($x413 (and (=  |%_1_0| (= |%_0_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_2_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_2_0| |%waterLevel.2_1_0|)) (=  |%_4_0| (= |%_3_0| 0.0)) (=  |%_5_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_5_0| 1.0 0.0)) (=  |%_7_0| (= |%_6_0| 0.0)) (=  |%_9_0| (= |%_8_0| 0.0)) (=  |%_10_0| (= |%pumpRunning.2_1_0| 0.0)) (=  |%_11_0| (> |%waterLevel.1_0| 0.0)) (= |%_12_0| (+ |%waterLevel.1_0| (~ 1.0))) (= |%waterLevel.3_0| (ite  |%_11_0| |%_12_0| |%waterLevel.1_0|)) (=  |%_13_0| (= |%systemActive.1_1_0| 0.0)) (=  |%_15_0| (> |%waterLevel.4_0| 1.0)) (= |%pumpRunning.6_0| (ite  |%_15_0| 1.0 |%pumpRunning.2_1_0|)) (=  |%_14_0| (= |%methaneLevelCritical.0_0| 0.0)) (=  |%_16_0| (= |%waterLevel.4.reg2mem.0_0| 2.0)) (=  |%_17_0| (= |%pumpRunning.4_0| 0.0)) (=  |%or.cond_0| (or |%_17_0| |%_16_0|)) (=  |%or.cond.not_0| (xor |%or.cond_0| true)) (=  |%_18_0| (= |%pumpRunning.1.reg2mem.0_0| 0.0)) (=  |%or.cond1_0| (and |%_18_0| |%or.cond.not_0|)))))
(let (($x303 (not E0x3744460)))
(let (($x302 (not E0x3743b80)))
(let (($x301 (not E0x37434f0)))
(let (($x306 (not E0x3742470)))
(let (($x311 (or (and E0x3742470 $x301 $x302 $x303 (not E0x3744a40)) (and E0x37434f0 $x306 $x302 $x303 (not E0x3744a40)) (and E0x3743b80 $x306 $x301 $x303 (not E0x3744a40)) (and E0x3744460 $x306 $x301 $x302 (not E0x3744a40)) (and E0x3744a40 $x306 $x301 $x302 $x303))))
(let (($x273 (and (<= |%pumpRunning.4_0| 0.0) (>= |%pumpRunning.4_0| 0.0))))
(let (($x296 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|))))
(let (($x292 (and (<= |%systemActive.0.reg2mem.0_0| 0.0) (>= |%systemActive.0.reg2mem.0_0| 0.0))))
(let (($x288 (and (<= |%pumpRunning.1.reg2mem.0_0| 0.0) (>= |%pumpRunning.1.reg2mem.0_0| 0.0))))
(let (($x293 (and (and (and (and bb6.i.i_0 E0x3744a40) (not |%_9_0|)) $x288) $x292)))
(let (($x262 (and (<= |%pumpRunning.4_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.4_0| |%pumpRunning.2_1_0|))))
(let (($x245 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.4_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.4_0|))))
(let (($x239 (and (<= |%systemActive.0.reg2mem.0_0| |%systemActive.1_1_0|) (>= |%systemActive.0.reg2mem.0_0| |%systemActive.1_1_0|))))
(let (($x233 (and (<= |%pumpRunning.1.reg2mem.0_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.1.reg2mem.0_0| |%pumpRunning.2_1_0|))))
(let (($x280 (and (and (and (and (and bb1.i.i.i_0 E0x3744460) |%_13_0|) $x233) $x239) $x245)))
(let (($x269 (and (and (and (and bb.i1.i.i.i_0 E0x3743b80) (not |%_14_0|)) $x233) $x239)))
(let (($x263 (and (and (and (and (and bb2.i3.i.i.i_0 E0x37434f0) $x233) $x239) $x245) $x262)))
(let (($x253 (and (<= |%pumpRunning.4_0| |%pumpRunning.6_0|) (>= |%pumpRunning.4_0| |%pumpRunning.6_0|))))
(let (($x254 (and (and (and (and (and bb.i.i.i.i.i_0 E0x3742470) $x233) $x239) $x245) $x253)))
(let (($x299 (or $x254 $x263 (and (and $x269 $x245) $x273) (and $x280 $x262) (and (and $x293 $x296) $x273))))
(let (($x313 (and (=>  bb3.i.i.i_0 $x299) (=>  bb3.i.i.i_0 $x311))))
(let (($x222 (=>  bb2.i3.i.i.i_0 (and (and bb.i1.i.i.i_0 E0x3741510) |%_14_0|))))
(let (($x224 (and $x222 (=>  bb2.i3.i.i.i_0 E0x3741510))))
(let (($x212 (=>  bb.i1.i.i.i_0 (and (and bb2.i.i.i_0 E0x3740910) (not |%_10_0|)))))
(let (($x214 (and $x212 (=>  bb.i1.i.i.i_0 E0x3740910))))
(let (($x204 (=>  bb.i.i.i.i.i_0 (and (and bb2.i.i.i_0 E0x373fb50) |%_10_0|))))
(let (($x206 (and $x204 (=>  bb.i.i.i.i.i_0 E0x373fb50))))
(let (($x196 (=>  bb2.i.i.i_0 (and (and bb1.i.i.i_0 E0x373f280) (not |%_13_0|)))))
(let (($x198 (and $x196 (=>  bb2.i.i.i_0 E0x373f280))))
(let (($x185 (or (and E0x373d7b0 (not E0x373de70)) (and E0x373de70 (not E0x373d7b0)))))
(let (($x177 (and (<= |%waterLevel.4_0| |%waterLevel.1_0|) (>= |%waterLevel.4_0| |%waterLevel.1_0|))))
(let (($x170 (and (<= |%waterLevel.4_0| |%waterLevel.3_0|) (>= |%waterLevel.4_0| |%waterLevel.3_0|))))
(let (($x179 (or (and (and bb.i1.i.i_0 E0x373d7b0) $x170) (and (and (and bb8.i.i_0 E0x373de70) |%_10_0|) $x177))))
(let (($x187 (and (=>  bb1.i.i.i_0 $x179) (=>  bb1.i.i.i_0 $x185))))
(let (($x157 (=>  bb.i1.i.i_0 (and (and bb8.i.i_0 E0x373c1f0) (not |%_10_0|)))))
(let (($x159 (and $x157 (=>  bb.i1.i.i_0 E0x373c1f0))))
(let (($x146 (or (and E0x373ae60 (not E0x373b1a0)) (and E0x373b1a0 (not E0x373ae60)))))
(let (($x140 (or (and (and bb6.i.i_0 E0x373ae60) |%_9_0|) (and (and bb5.i.i_0 E0x373b1a0) (not |%_7_0|)))))
(let (($x148 (and (=>  bb8.i.i_0 $x140) (=>  bb8.i.i_0 $x146))))
(let (($x128 (and (=>  bb6.i.i_0 (and (and bb5.i.i_0 E0x373a030) |%_7_0|)) (=>  bb6.i.i_0 E0x373a030))))
(let (($x116 (or (and E0x3738530 (not E0x3738b10)) (and E0x3738b10 (not E0x3738530)))))
(let (($x108 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x101 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x110 (or (and (and bb4.i.i_0 E0x3738530) $x101) (and (and (and bb3.i.i_0 E0x3738b10) |%_4_0|) $x108))))
(let (($x118 (and (=>  bb5.i.i_0 $x110) (=>  bb5.i.i_0 $x116))))
(let (($x90 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x3737460) (not |%_4_0|))) (=>  bb4.i.i_0 E0x3737460))))
(let (($x77 (or (and E0x371ec60 (not E0x3735ee0)) (and E0x3735ee0 (not E0x371ec60)))))
(let (($x69 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x62 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x71 (or (and (and bb2.i.i_0 E0x371ec60) $x62) (and (and (and bb1.i.i_0 E0x3735ee0) |%_1_0|) $x69))))
(let (($x79 (and (=>  bb3.i.i_0 $x71) (=>  bb3.i.i_0 $x77))))
(let (($x51 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x371d110) (not |%_1_0|))) (=>  bb2.i.i_0 E0x371d110))))
(let (($x416 (and $x51 $x79 $x90 $x118 $x128 $x148 $x159 $x187 $x198 $x206 $x214 $x224 $x313 (and bb3.i.i.i_0 |%or.cond1_0|))))
(and $x416 $x413))))))))))))))))))))))))))))))))))))))))))))))))))))))) (E_cp-rel-bb1.i.i_cp-rel-bb2.i.i.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )))



(query cp-rel-bb2.i.i.i.i)
