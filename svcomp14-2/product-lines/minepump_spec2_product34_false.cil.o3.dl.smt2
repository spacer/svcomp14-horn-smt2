(declare-var %waterLevel.2_1_0 Real)
(declare-var %methaneLevelCritical.1_1_0 Real)
(declare-var %methAndRunningLastTime.1_1_0 Real)
(declare-var %pumpRunning.2_1_0 Real)
(declare-var entry_0 Bool)
(declare-var %waterLevel.2_1_1 Real)
(declare-var %methaneLevelCritical.1_1_1 Real)
(declare-var %methAndRunningLastTime.1_1_1 Real)
(declare-var %pumpRunning.2_1_1 Real)
(declare-var bb2.i.i_0 Bool)
(declare-var bb1.i.i_0 Bool)
(declare-var E0x3da6c80 Bool)
(declare-var %_1_0 Bool)
(declare-var bb3.i.i_0 Bool)
(declare-var E0x3da87d0 Bool)
(declare-var %waterLevel.1_0 Real)
(declare-var %waterLevel.0_0 Real)
(declare-var E0x3dbfa40 Bool)
(declare-var bb4.i.i_0 Bool)
(declare-var E0x3dc0fc0 Bool)
(declare-var %_4_0 Bool)
(declare-var bb5.i.i_0 Bool)
(declare-var E0x3dc2090 Bool)
(declare-var %methaneLevelCritical.0_0 Real)
(declare-var %storemerge.i.i.i_0 Real)
(declare-var E0x3dc2670 Bool)
(declare-var bb.i2.i.i.i_0 Bool)
(declare-var E0x3dc3b90 Bool)
(declare-var %_7_0 Bool)
(declare-var bb2.i.i.i_0 Bool)
(declare-var E0x3dc4880 Bool)
(declare-var bb2.i4.i.i.i_0 Bool)
(declare-var E0x3dc5bc0 Bool)
(declare-var %_10_0 Bool)
(declare-var bb3.i.i.i_0 Bool)
(declare-var E0x3dc6760 Bool)
(declare-var %waterLevel.4.reg2mem.0_0 Real)
(declare-var %pumpRunning.0_0 Real)
(declare-var E0x3dc6fe0 Bool)
(declare-var E0x3dc2990 Bool)
(declare-var %waterLevel.3_0 Real)
(declare-var bb1.i.i.i.i_0 Bool)
(declare-var E0x3dc9650 Bool)
(declare-var %or.cond_0 Bool)
(declare-var %_13_0 Bool)
(declare-var %_0_0 Real)
(declare-var %not._0 Bool)
(declare-var %_2_0 Real)
(declare-var %_3_0 Real)
(declare-var %_5_0 Bool)
(declare-var %_8_0 Bool)
(declare-var %_9_0 Real)
(declare-var %_11_0 Bool)
(declare-var %_12_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb2.i.i.i.i ())
(declare-rel cp-rel-bb1.i.i (Real Real Real Real ))
(declare-rel E_cp-rel-entry_cp-rel-bb1.i.i (Real Real Real Real ))
(declare-rel E_cp-rel-bb1.i.i_cp-rel-bb1.i.i (Real Real Real Real Real Real Real Real ))
(declare-rel E_cp-rel-bb1.i.i_cp-rel-bb2.i.i.i.i (Real Real Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (E_cp-rel-entry_cp-rel-bb1.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| |%methAndRunningLastTime.1_1_0| |%pumpRunning.2_1_0| )) (cp-rel-bb1.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| |%methAndRunningLastTime.1_1_0| |%pumpRunning.2_1_0| )))

(rule (=> (let (($x29 (and (<= |%methAndRunningLastTime.1_1_0| 0.0) (>= |%methAndRunningLastTime.1_1_0| 0.0))))
(let (($x25 (and (<= |%methaneLevelCritical.1_1_0| 0.0) (>= |%methaneLevelCritical.1_1_0| 0.0))))
(let (($x22 (and entry_0 (and (<= |%waterLevel.2_1_0| 1.0) (>= |%waterLevel.2_1_0| 1.0)))))
(and (and (and $x22 $x25) $x29) (and (<= |%pumpRunning.2_1_0| 0.0) (>= |%pumpRunning.2_1_0| 0.0)))))) (E_cp-rel-entry_cp-rel-bb1.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| |%methAndRunningLastTime.1_1_0| |%pumpRunning.2_1_0| )))

(rule (=> (and (cp-rel-bb1.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| |%methAndRunningLastTime.1_1_0| |%pumpRunning.2_1_0| ) (E_cp-rel-bb1.i.i_cp-rel-bb1.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| |%methAndRunningLastTime.1_1_0| |%pumpRunning.2_1_0| |%waterLevel.2_1_1| |%methaneLevelCritical.1_1_1| |%methAndRunningLastTime.1_1_1| |%pumpRunning.2_1_1| )) (cp-rel-bb1.i.i |%waterLevel.2_1_1| |%methaneLevelCritical.1_1_1| |%methAndRunningLastTime.1_1_1| |%pumpRunning.2_1_1| )))

(rule (=> (let (($x285 (and (=  |%_1_0| (= |%_0_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_2_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_2_0| |%waterLevel.2_1_0|)) (=  |%_4_0| (= |%_3_0| 0.0)) (=  |%_5_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_5_0| 1.0 0.0)) (=  |%_7_0| (= |%pumpRunning.2_1_0| 0.0)) (=  |%_10_0| (> |%waterLevel.1_0| 1.0)) (=  |%_8_0| (> |%waterLevel.1_0| 0.0)) (= |%_9_0| (+ |%waterLevel.1_0| (~ 1.0))) (= |%waterLevel.3_0| (ite  |%_8_0| |%_9_0| |%waterLevel.1_0|)) (=  |%_11_0| (= |%methaneLevelCritical.0_0| 0.0)) (=  |%_12_0| (= |%pumpRunning.0_0| 0.0)) (=  |%or.cond_0| (or |%_12_0| |%_11_0|)) (=  |%_13_0| (= |%methAndRunningLastTime.1_1_0| 0.0)))))
(let (($x221 (and (<= |%pumpRunning.2_1_1| |%pumpRunning.0_0|) (>= |%pumpRunning.2_1_1| |%pumpRunning.0_0|))))
(let (($x228 (and (<= |%methAndRunningLastTime.1_1_1| 0.0) (>= |%methAndRunningLastTime.1_1_1| 0.0))))
(let (($x213 (and (<= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|) (>= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|))))
(let (($x209 (and (<= |%waterLevel.2_1_1| |%waterLevel.4.reg2mem.0_0|) (>= |%waterLevel.2_1_1| |%waterLevel.4.reg2mem.0_0|))))
(let (($x230 (and (and (and (and (and bb3.i.i.i_0 |%or.cond_0|) $x209) $x213) $x228) $x221)))
(let (($x217 (and (<= |%methAndRunningLastTime.1_1_1| 1.0) (>= |%methAndRunningLastTime.1_1_1| 1.0))))
(let (($x222 (and (and (and (and (and bb1.i.i.i.i_0 |%_13_0|) $x209) $x213) $x217) $x221)))
(let (($x201 (=>  bb1.i.i.i.i_0 (and (and bb3.i.i.i_0 E0x3dc9650) (not |%or.cond_0|)))))
(let (($x203 (and $x201 (=>  bb1.i.i.i.i_0 E0x3dc9650))))
(let (($x190 (or (and E0x3dc6760 (not E0x3dc6fe0) (not E0x3dc2990)) (and E0x3dc6fe0 (not E0x3dc6760) (not E0x3dc2990)) (and E0x3dc2990 (not E0x3dc6760) (not E0x3dc6fe0)))))
(let (($x163 (and (<= |%pumpRunning.0_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.0_0| |%pumpRunning.2_1_0|))))
(let (($x179 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.3_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.3_0|))))
(let (($x157 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|))))
(let (($x172 (and (and (and (and bb.i2.i.i.i_0 E0x3dc6fe0) |%_10_0|) $x157) (and (<= |%pumpRunning.0_0| 1.0) (>= |%pumpRunning.0_0| 1.0)))))
(let (($x182 (or (and (and (and bb2.i4.i.i.i_0 E0x3dc6760) $x157) $x163) $x172 (and (and (and bb2.i.i.i_0 E0x3dc2990) $x179) $x163))))
(let (($x192 (and (=>  bb3.i.i.i_0 $x182) (=>  bb3.i.i.i_0 $x190))))
(let (($x146 (=>  bb2.i4.i.i.i_0 (and (and bb.i2.i.i.i_0 E0x3dc5bc0) (not |%_10_0|)))))
(let (($x148 (and $x146 (=>  bb2.i4.i.i.i_0 E0x3dc5bc0))))
(let (($x135 (=>  bb2.i.i.i_0 (and (and bb5.i.i_0 E0x3dc4880) (not |%_7_0|)))))
(let (($x137 (and $x135 (=>  bb2.i.i.i_0 E0x3dc4880))))
(let (($x128 (and (=>  bb.i2.i.i.i_0 (and (and bb5.i.i_0 E0x3dc3b90) |%_7_0|)) (=>  bb.i2.i.i.i_0 E0x3dc3b90))))
(let (($x116 (or (and E0x3dc2090 (not E0x3dc2670)) (and E0x3dc2670 (not E0x3dc2090)))))
(let (($x108 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x101 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x110 (or (and (and bb4.i.i_0 E0x3dc2090) $x101) (and (and (and bb3.i.i_0 E0x3dc2670) |%_4_0|) $x108))))
(let (($x118 (and (=>  bb5.i.i_0 $x110) (=>  bb5.i.i_0 $x116))))
(let (($x90 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x3dc0fc0) (not |%_4_0|))) (=>  bb4.i.i_0 E0x3dc0fc0))))
(let (($x77 (or (and E0x3da87d0 (not E0x3dbfa40)) (and E0x3dbfa40 (not E0x3da87d0)))))
(let (($x69 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x62 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x71 (or (and (and bb2.i.i_0 E0x3da87d0) $x62) (and (and (and bb1.i.i_0 E0x3dbfa40) |%_1_0|) $x69))))
(let (($x79 (and (=>  bb3.i.i_0 $x71) (=>  bb3.i.i_0 $x77))))
(let (($x51 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x3da6c80) (not |%_1_0|))) (=>  bb2.i.i_0 E0x3da6c80))))
(and (and $x51 $x79 $x90 $x118 $x128 $x137 $x148 $x192 $x203 (or $x222 $x230)) $x285))))))))))))))))))))))))))))))))))) (E_cp-rel-bb1.i.i_cp-rel-bb1.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| |%methAndRunningLastTime.1_1_0| |%pumpRunning.2_1_0| |%waterLevel.2_1_1| |%methaneLevelCritical.1_1_1| |%methAndRunningLastTime.1_1_1| |%pumpRunning.2_1_1| )))

(rule (=> (and (cp-rel-bb1.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| |%methAndRunningLastTime.1_1_0| |%pumpRunning.2_1_0| ) (E_cp-rel-bb1.i.i_cp-rel-bb2.i.i.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| |%methAndRunningLastTime.1_1_0| |%pumpRunning.2_1_0| )) cp-rel-bb2.i.i.i.i))

(rule (=> (let (($x285 (and (=  |%_1_0| (= |%_0_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_2_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_2_0| |%waterLevel.2_1_0|)) (=  |%_4_0| (= |%_3_0| 0.0)) (=  |%_5_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_5_0| 1.0 0.0)) (=  |%_7_0| (= |%pumpRunning.2_1_0| 0.0)) (=  |%_10_0| (> |%waterLevel.1_0| 1.0)) (=  |%_8_0| (> |%waterLevel.1_0| 0.0)) (= |%_9_0| (+ |%waterLevel.1_0| (~ 1.0))) (= |%waterLevel.3_0| (ite  |%_8_0| |%_9_0| |%waterLevel.1_0|)) (=  |%_11_0| (= |%methaneLevelCritical.0_0| 0.0)) (=  |%_12_0| (= |%pumpRunning.0_0| 0.0)) (=  |%or.cond_0| (or |%_12_0| |%_11_0|)) (=  |%_13_0| (= |%methAndRunningLastTime.1_1_0| 0.0)))))
(let (($x201 (=>  bb1.i.i.i.i_0 (and (and bb3.i.i.i_0 E0x3dc9650) (not |%or.cond_0|)))))
(let (($x203 (and $x201 (=>  bb1.i.i.i.i_0 E0x3dc9650))))
(let (($x190 (or (and E0x3dc6760 (not E0x3dc6fe0) (not E0x3dc2990)) (and E0x3dc6fe0 (not E0x3dc6760) (not E0x3dc2990)) (and E0x3dc2990 (not E0x3dc6760) (not E0x3dc6fe0)))))
(let (($x163 (and (<= |%pumpRunning.0_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.0_0| |%pumpRunning.2_1_0|))))
(let (($x179 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.3_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.3_0|))))
(let (($x157 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|))))
(let (($x172 (and (and (and (and bb.i2.i.i.i_0 E0x3dc6fe0) |%_10_0|) $x157) (and (<= |%pumpRunning.0_0| 1.0) (>= |%pumpRunning.0_0| 1.0)))))
(let (($x182 (or (and (and (and bb2.i4.i.i.i_0 E0x3dc6760) $x157) $x163) $x172 (and (and (and bb2.i.i.i_0 E0x3dc2990) $x179) $x163))))
(let (($x192 (and (=>  bb3.i.i.i_0 $x182) (=>  bb3.i.i.i_0 $x190))))
(let (($x146 (=>  bb2.i4.i.i.i_0 (and (and bb.i2.i.i.i_0 E0x3dc5bc0) (not |%_10_0|)))))
(let (($x148 (and $x146 (=>  bb2.i4.i.i.i_0 E0x3dc5bc0))))
(let (($x135 (=>  bb2.i.i.i_0 (and (and bb5.i.i_0 E0x3dc4880) (not |%_7_0|)))))
(let (($x137 (and $x135 (=>  bb2.i.i.i_0 E0x3dc4880))))
(let (($x128 (and (=>  bb.i2.i.i.i_0 (and (and bb5.i.i_0 E0x3dc3b90) |%_7_0|)) (=>  bb.i2.i.i.i_0 E0x3dc3b90))))
(let (($x116 (or (and E0x3dc2090 (not E0x3dc2670)) (and E0x3dc2670 (not E0x3dc2090)))))
(let (($x108 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x101 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x110 (or (and (and bb4.i.i_0 E0x3dc2090) $x101) (and (and (and bb3.i.i_0 E0x3dc2670) |%_4_0|) $x108))))
(let (($x118 (and (=>  bb5.i.i_0 $x110) (=>  bb5.i.i_0 $x116))))
(let (($x90 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x3dc0fc0) (not |%_4_0|))) (=>  bb4.i.i_0 E0x3dc0fc0))))
(let (($x77 (or (and E0x3da87d0 (not E0x3dbfa40)) (and E0x3dbfa40 (not E0x3da87d0)))))
(let (($x69 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x62 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x71 (or (and (and bb2.i.i_0 E0x3da87d0) $x62) (and (and (and bb1.i.i_0 E0x3dbfa40) |%_1_0|) $x69))))
(let (($x79 (and (=>  bb3.i.i_0 $x71) (=>  bb3.i.i_0 $x77))))
(let (($x51 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x3da6c80) (not |%_1_0|))) (=>  bb2.i.i_0 E0x3da6c80))))
(let (($x289 (and $x51 $x79 $x90 $x118 $x128 $x137 $x148 $x192 $x203 (and bb1.i.i.i.i_0 (not |%_13_0|)))))
(and $x289 $x285))))))))))))))))))))))))))))) (E_cp-rel-bb1.i.i_cp-rel-bb2.i.i.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| |%methAndRunningLastTime.1_1_0| |%pumpRunning.2_1_0| )))



(query cp-rel-bb2.i.i.i.i)
