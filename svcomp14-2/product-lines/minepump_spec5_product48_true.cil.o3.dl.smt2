(declare-var %pumpRunning.2_1_0 Real)
(declare-var %systemActive.1_1_0 Real)
(declare-var %waterLevel.2_1_0 Real)
(declare-var %methaneLevelCritical.1_1_0 Real)
(declare-var entry_0 Bool)
(declare-var %pumpRunning.2_1_1 Real)
(declare-var %systemActive.1_1_1 Real)
(declare-var %waterLevel.2_1_1 Real)
(declare-var %methaneLevelCritical.1_1_1 Real)
(declare-var bb2.i.i_0 Bool)
(declare-var bb1.i.i_0 Bool)
(declare-var E0x391ba00 Bool)
(declare-var %_1_0 Bool)
(declare-var bb3.i.i_0 Bool)
(declare-var E0x391d550 Bool)
(declare-var %waterLevel.1_0 Real)
(declare-var %waterLevel.0_0 Real)
(declare-var E0x39347d0 Bool)
(declare-var bb4.i.i_0 Bool)
(declare-var E0x3935d50 Bool)
(declare-var %_4_0 Bool)
(declare-var bb5.i.i_0 Bool)
(declare-var E0x3936e20 Bool)
(declare-var %methaneLevelCritical.0_0 Real)
(declare-var %storemerge.i.i.i_0 Real)
(declare-var E0x3937400 Bool)
(declare-var bb7.i.i_0 Bool)
(declare-var E0x3938920 Bool)
(declare-var %_7_0 Bool)
(declare-var bb9.i.i_0 Bool)
(declare-var E0x3939930 Bool)
(declare-var %_9_0 Bool)
(declare-var %systemActive.0_0 Real)
(declare-var E0x3939f70 Bool)
(declare-var bb.i1.i.i_0 Bool)
(declare-var E0x393b2c0 Bool)
(declare-var %_10_0 Bool)
(declare-var bb1.i.i.i_0 Bool)
(declare-var E0x393c880 Bool)
(declare-var %waterLevel.4_0 Real)
(declare-var %waterLevel.3_0 Real)
(declare-var E0x3937720 Bool)
(declare-var bb2.i.i.i_0 Bool)
(declare-var E0x393e490 Bool)
(declare-var %_13_0 Bool)
(declare-var bb.i.i.i.i.i_0 Bool)
(declare-var E0x393ed60 Bool)
(declare-var bb.i1.i.i.i_0 Bool)
(declare-var E0x393f7e0 Bool)
(declare-var bb1.i.i.i.i.i_0 Bool)
(declare-var E0x3940300 Bool)
(declare-var %_15_0 Bool)
(declare-var bb2.i3.i.i.i_0 Bool)
(declare-var E0x3941150 Bool)
(declare-var %_14_0 Bool)
(declare-var bb3.i.i.i_0 Bool)
(declare-var E0x3942180 Bool)
(declare-var %pumpRunning.1.reg2mem.0_0 Real)
(declare-var %systemActive.0.reg2mem.0_0 Real)
(declare-var %waterLevel.4.reg2mem.0_0 Real)
(declare-var %pumpRunning.4_0 Real)
(declare-var %pumpRunning.7_0 Real)
(declare-var E0x3942e30 Bool)
(declare-var E0x3943810 Bool)
(declare-var E0x3943d10 Bool)
(declare-var E0x39445e0 Bool)
(declare-var E0x3944c90 Bool)
(declare-var %or.cond1_0 Bool)
(declare-var %_0_0 Real)
(declare-var %not._0 Bool)
(declare-var %_2_0 Real)
(declare-var %_3_0 Real)
(declare-var %_5_0 Bool)
(declare-var %_6_0 Real)
(declare-var %_8_0 Real)
(declare-var %_11_0 Bool)
(declare-var %_12_0 Real)
(declare-var %_16_0 Bool)
(declare-var %_17_0 Bool)
(declare-var %_18_0 Bool)
(declare-var %or.cond_0 Bool)
(declare-var %or.cond.not_0 Bool)
(declare-var %_19_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb2.i.i.i.i ())
(declare-rel cp-rel-bb1.i.i (Real Real Real Real ))
(declare-rel E_cp-rel-entry_cp-rel-bb1.i.i (Real Real Real Real ))
(declare-rel E_cp-rel-bb1.i.i_cp-rel-bb1.i.i (Real Real Real Real Real Real Real Real ))
(declare-rel E_cp-rel-bb1.i.i_cp-rel-bb2.i.i.i.i (Real Real Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (E_cp-rel-entry_cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )) (cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )))

(rule (=> (let (($x33 (and (<= |%methaneLevelCritical.1_1_0| 0.0) (>= |%methaneLevelCritical.1_1_0| 0.0))))
(let (($x22 (and entry_0 (and (<= |%pumpRunning.2_1_0| 0.0) (>= |%pumpRunning.2_1_0| 0.0)))))
(let (($x26 (and $x22 (and (<= |%systemActive.1_1_0| 1.0) (>= |%systemActive.1_1_0| 1.0)))))
(let (($x30 (and $x26 (and (<= |%waterLevel.2_1_0| 1.0) (>= |%waterLevel.2_1_0| 1.0)))))
(and $x30 $x33))))) (E_cp-rel-entry_cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )))

(rule (=> (and (cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| ) (E_cp-rel-bb1.i.i_cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| |%pumpRunning.2_1_1| |%systemActive.1_1_1| |%waterLevel.2_1_1| |%methaneLevelCritical.1_1_1| )) (cp-rel-bb1.i.i |%pumpRunning.2_1_1| |%systemActive.1_1_1| |%waterLevel.2_1_1| |%methaneLevelCritical.1_1_1| )))

(rule (=> (let (($x444 (and (=  |%_1_0| (= |%_0_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_2_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_2_0| |%waterLevel.2_1_0|)) (=  |%_4_0| (= |%_3_0| 0.0)) (=  |%_5_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_5_0| 1.0 0.0)) (=  |%_7_0| (= |%_6_0| 0.0)) (=  |%_9_0| (= |%_8_0| 0.0)) (=  |%_10_0| (= |%pumpRunning.2_1_0| 0.0)) (=  |%_11_0| (> |%waterLevel.1_0| 0.0)) (= |%_12_0| (+ |%waterLevel.1_0| (~ 1.0))) (= |%waterLevel.3_0| (ite  |%_11_0| |%_12_0| |%waterLevel.1_0|)) (=  |%_13_0| (= |%systemActive.0_0| 0.0)) (=  |%_15_0| (> |%waterLevel.4_0| 1.0)) (=  |%_14_0| (= |%methaneLevelCritical.0_0| 0.0)) (=  |%_16_0| (= |%methaneLevelCritical.0_0| 0.0)) (= |%pumpRunning.7_0| (ite  |%_16_0| 1.0 |%pumpRunning.2_1_0|)) (=  |%_17_0| (= |%waterLevel.4.reg2mem.0_0| 2.0)) (=  |%_18_0| (= |%pumpRunning.4_0| 0.0)) (=  |%or.cond_0| (or |%_18_0| |%_17_0|)) (=  |%or.cond.not_0| (xor |%or.cond_0| true)) (=  |%_19_0| (= |%pumpRunning.1.reg2mem.0_0| 0.0)) (=  |%or.cond1_0| (and |%_19_0| |%or.cond.not_0|)))))
(let (($x362 (and (<= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|) (>= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|))))
(let (($x358 (and (<= |%waterLevel.2_1_1| |%waterLevel.4.reg2mem.0_0|) (>= |%waterLevel.2_1_1| |%waterLevel.4.reg2mem.0_0|))))
(let (($x354 (and (<= |%systemActive.1_1_1| |%systemActive.0.reg2mem.0_0|) (>= |%systemActive.1_1_1| |%systemActive.0.reg2mem.0_0|))))
(let (($x350 (and (<= |%pumpRunning.2_1_1| |%pumpRunning.4_0|) (>= |%pumpRunning.2_1_1| |%pumpRunning.4_0|))))
(let (($x359 (and (and (and (and bb3.i.i.i_0 (not |%or.cond1_0|)) $x350) $x354) $x358)))
(let (($x332 (not E0x39445e0)))
(let (($x331 (not E0x3943d10)))
(let (($x330 (not E0x3943810)))
(let (($x329 (not E0x3942e30)))
(let (($x335 (not E0x3942180)))
(let (($x341 (or (and E0x3942180 $x329 $x330 $x331 $x332 (not E0x3944c90)) (and E0x3942e30 $x335 $x330 $x331 $x332 (not E0x3944c90)) (and E0x3943810 $x335 $x329 $x331 $x332 (not E0x3944c90)) (and E0x3943d10 $x335 $x329 $x330 $x332 (not E0x3944c90)) (and E0x39445e0 $x335 $x329 $x330 $x331 (not E0x3944c90)) (and E0x3944c90 $x335 $x329 $x330 $x331 $x332))))
(let (($x301 (and (<= |%pumpRunning.4_0| 0.0) (>= |%pumpRunning.4_0| 0.0))))
(let (($x324 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|))))
(let (($x320 (and (<= |%systemActive.0.reg2mem.0_0| 0.0) (>= |%systemActive.0.reg2mem.0_0| 0.0))))
(let (($x316 (and (<= |%pumpRunning.1.reg2mem.0_0| 0.0) (>= |%pumpRunning.1.reg2mem.0_0| 0.0))))
(let (($x321 (and (and (and (and bb7.i.i_0 E0x3944c90) (not |%_9_0|)) $x316) $x320)))
(let (($x284 (and (<= |%pumpRunning.4_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.4_0| |%pumpRunning.2_1_0|))))
(let (($x265 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.4_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.4_0|))))
(let (($x259 (and (<= |%systemActive.0.reg2mem.0_0| |%systemActive.0_0|) (>= |%systemActive.0.reg2mem.0_0| |%systemActive.0_0|))))
(let (($x253 (and (<= |%pumpRunning.1.reg2mem.0_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.1.reg2mem.0_0| |%pumpRunning.2_1_0|))))
(let (($x308 (and (and (and (and (and bb1.i.i.i_0 E0x39445e0) |%_13_0|) $x253) $x259) $x265)))
(let (($x297 (and (and (and (and bb.i1.i.i.i_0 E0x3943d10) (not |%_14_0|)) $x253) $x259)))
(let (($x291 (and (and (and (and (and bb2.i3.i.i.i_0 E0x3943810) $x253) $x259) $x265) $x284)))
(let (($x280 (and (and (and (and bb.i.i.i.i.i_0 E0x3942e30) (not |%_15_0|)) $x253) $x259)))
(let (($x273 (and (<= |%pumpRunning.4_0| |%pumpRunning.7_0|) (>= |%pumpRunning.4_0| |%pumpRunning.7_0|))))
(let (($x274 (and (and (and (and (and bb1.i.i.i.i.i_0 E0x3942180) $x253) $x259) $x265) $x273)))
(let (($x327 (or $x274 (and (and $x280 $x265) $x284) $x291 (and (and $x297 $x265) $x301) (and $x308 $x284) (and (and $x321 $x324) $x301))))
(let (($x343 (and (=>  bb3.i.i.i_0 $x327) (=>  bb3.i.i.i_0 $x341))))
(let (($x242 (=>  bb2.i3.i.i.i_0 (and (and bb.i1.i.i.i_0 E0x3941150) |%_14_0|))))
(let (($x244 (and $x242 (=>  bb2.i3.i.i.i_0 E0x3941150))))
(let (($x232 (=>  bb1.i.i.i.i.i_0 (and (and bb.i.i.i.i.i_0 E0x3940300) |%_15_0|))))
(let (($x234 (and $x232 (=>  bb1.i.i.i.i.i_0 E0x3940300))))
(let (($x222 (=>  bb.i1.i.i.i_0 (and (and bb2.i.i.i_0 E0x393f7e0) (not |%_10_0|)))))
(let (($x224 (and $x222 (=>  bb.i1.i.i.i_0 E0x393f7e0))))
(let (($x214 (=>  bb.i.i.i.i.i_0 (and (and bb2.i.i.i_0 E0x393ed60) |%_10_0|))))
(let (($x216 (and $x214 (=>  bb.i.i.i.i.i_0 E0x393ed60))))
(let (($x206 (=>  bb2.i.i.i_0 (and (and bb1.i.i.i_0 E0x393e490) (not |%_13_0|)))))
(let (($x208 (and $x206 (=>  bb2.i.i.i_0 E0x393e490))))
(let (($x195 (or (and E0x393c880 (not E0x3937720)) (and E0x3937720 (not E0x393c880)))))
(let (($x187 (and (<= |%waterLevel.4_0| |%waterLevel.1_0|) (>= |%waterLevel.4_0| |%waterLevel.1_0|))))
(let (($x180 (and (<= |%waterLevel.4_0| |%waterLevel.3_0|) (>= |%waterLevel.4_0| |%waterLevel.3_0|))))
(let (($x189 (or (and (and bb.i1.i.i_0 E0x393c880) $x180) (and (and (and bb9.i.i_0 E0x3937720) |%_10_0|) $x187))))
(let (($x197 (and (=>  bb1.i.i.i_0 $x189) (=>  bb1.i.i.i_0 $x195))))
(let (($x167 (=>  bb.i1.i.i_0 (and (and bb9.i.i_0 E0x393b2c0) (not |%_10_0|)))))
(let (($x169 (and $x167 (=>  bb.i1.i.i_0 E0x393b2c0))))
(let (($x156 (or (and E0x3939930 (not E0x3939f70)) (and E0x3939f70 (not E0x3939930)))))
(let (($x149 (and (and (and bb5.i.i_0 E0x3939f70) (not |%_7_0|)) (and (<= |%systemActive.0_0| 1.0) (>= |%systemActive.0_0| 1.0)))))
(let (($x140 (and (<= |%systemActive.0_0| |%systemActive.1_1_0|) (>= |%systemActive.0_0| |%systemActive.1_1_0|))))
(let (($x151 (=>  bb9.i.i_0 (or (and (and (and bb7.i.i_0 E0x3939930) |%_9_0|) $x140) $x149))))
(let (($x158 (and $x151 (=>  bb9.i.i_0 $x156))))
(let (($x128 (and (=>  bb7.i.i_0 (and (and bb5.i.i_0 E0x3938920) |%_7_0|)) (=>  bb7.i.i_0 E0x3938920))))
(let (($x116 (or (and E0x3936e20 (not E0x3937400)) (and E0x3937400 (not E0x3936e20)))))
(let (($x108 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x101 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x110 (or (and (and bb4.i.i_0 E0x3936e20) $x101) (and (and (and bb3.i.i_0 E0x3937400) |%_4_0|) $x108))))
(let (($x118 (and (=>  bb5.i.i_0 $x110) (=>  bb5.i.i_0 $x116))))
(let (($x90 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x3935d50) (not |%_4_0|))) (=>  bb4.i.i_0 E0x3935d50))))
(let (($x77 (or (and E0x391d550 (not E0x39347d0)) (and E0x39347d0 (not E0x391d550)))))
(let (($x69 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x62 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x71 (or (and (and bb2.i.i_0 E0x391d550) $x62) (and (and (and bb1.i.i_0 E0x39347d0) |%_1_0|) $x69))))
(let (($x79 (and (=>  bb3.i.i_0 $x71) (=>  bb3.i.i_0 $x77))))
(let (($x51 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x391ba00) (not |%_1_0|))) (=>  bb2.i.i_0 E0x391ba00))))
(let (($x364 (and $x51 $x79 $x90 $x118 $x128 $x158 $x169 $x197 $x208 $x216 $x224 $x234 $x244 $x343 (and $x359 $x362))))
(and $x364 $x444)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))) (E_cp-rel-bb1.i.i_cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| |%pumpRunning.2_1_1| |%systemActive.1_1_1| |%waterLevel.2_1_1| |%methaneLevelCritical.1_1_1| )))

(rule (=> (and (cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| ) (E_cp-rel-bb1.i.i_cp-rel-bb2.i.i.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )) cp-rel-bb2.i.i.i.i))

(rule (=> (let (($x444 (and (=  |%_1_0| (= |%_0_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_2_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_2_0| |%waterLevel.2_1_0|)) (=  |%_4_0| (= |%_3_0| 0.0)) (=  |%_5_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_5_0| 1.0 0.0)) (=  |%_7_0| (= |%_6_0| 0.0)) (=  |%_9_0| (= |%_8_0| 0.0)) (=  |%_10_0| (= |%pumpRunning.2_1_0| 0.0)) (=  |%_11_0| (> |%waterLevel.1_0| 0.0)) (= |%_12_0| (+ |%waterLevel.1_0| (~ 1.0))) (= |%waterLevel.3_0| (ite  |%_11_0| |%_12_0| |%waterLevel.1_0|)) (=  |%_13_0| (= |%systemActive.0_0| 0.0)) (=  |%_15_0| (> |%waterLevel.4_0| 1.0)) (=  |%_14_0| (= |%methaneLevelCritical.0_0| 0.0)) (=  |%_16_0| (= |%methaneLevelCritical.0_0| 0.0)) (= |%pumpRunning.7_0| (ite  |%_16_0| 1.0 |%pumpRunning.2_1_0|)) (=  |%_17_0| (= |%waterLevel.4.reg2mem.0_0| 2.0)) (=  |%_18_0| (= |%pumpRunning.4_0| 0.0)) (=  |%or.cond_0| (or |%_18_0| |%_17_0|)) (=  |%or.cond.not_0| (xor |%or.cond_0| true)) (=  |%_19_0| (= |%pumpRunning.1.reg2mem.0_0| 0.0)) (=  |%or.cond1_0| (and |%_19_0| |%or.cond.not_0|)))))
(let (($x332 (not E0x39445e0)))
(let (($x331 (not E0x3943d10)))
(let (($x330 (not E0x3943810)))
(let (($x329 (not E0x3942e30)))
(let (($x335 (not E0x3942180)))
(let (($x341 (or (and E0x3942180 $x329 $x330 $x331 $x332 (not E0x3944c90)) (and E0x3942e30 $x335 $x330 $x331 $x332 (not E0x3944c90)) (and E0x3943810 $x335 $x329 $x331 $x332 (not E0x3944c90)) (and E0x3943d10 $x335 $x329 $x330 $x332 (not E0x3944c90)) (and E0x39445e0 $x335 $x329 $x330 $x331 (not E0x3944c90)) (and E0x3944c90 $x335 $x329 $x330 $x331 $x332))))
(let (($x301 (and (<= |%pumpRunning.4_0| 0.0) (>= |%pumpRunning.4_0| 0.0))))
(let (($x324 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|))))
(let (($x320 (and (<= |%systemActive.0.reg2mem.0_0| 0.0) (>= |%systemActive.0.reg2mem.0_0| 0.0))))
(let (($x316 (and (<= |%pumpRunning.1.reg2mem.0_0| 0.0) (>= |%pumpRunning.1.reg2mem.0_0| 0.0))))
(let (($x321 (and (and (and (and bb7.i.i_0 E0x3944c90) (not |%_9_0|)) $x316) $x320)))
(let (($x284 (and (<= |%pumpRunning.4_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.4_0| |%pumpRunning.2_1_0|))))
(let (($x265 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.4_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.4_0|))))
(let (($x259 (and (<= |%systemActive.0.reg2mem.0_0| |%systemActive.0_0|) (>= |%systemActive.0.reg2mem.0_0| |%systemActive.0_0|))))
(let (($x253 (and (<= |%pumpRunning.1.reg2mem.0_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.1.reg2mem.0_0| |%pumpRunning.2_1_0|))))
(let (($x308 (and (and (and (and (and bb1.i.i.i_0 E0x39445e0) |%_13_0|) $x253) $x259) $x265)))
(let (($x297 (and (and (and (and bb.i1.i.i.i_0 E0x3943d10) (not |%_14_0|)) $x253) $x259)))
(let (($x291 (and (and (and (and (and bb2.i3.i.i.i_0 E0x3943810) $x253) $x259) $x265) $x284)))
(let (($x280 (and (and (and (and bb.i.i.i.i.i_0 E0x3942e30) (not |%_15_0|)) $x253) $x259)))
(let (($x273 (and (<= |%pumpRunning.4_0| |%pumpRunning.7_0|) (>= |%pumpRunning.4_0| |%pumpRunning.7_0|))))
(let (($x274 (and (and (and (and (and bb1.i.i.i.i.i_0 E0x3942180) $x253) $x259) $x265) $x273)))
(let (($x327 (or $x274 (and (and $x280 $x265) $x284) $x291 (and (and $x297 $x265) $x301) (and $x308 $x284) (and (and $x321 $x324) $x301))))
(let (($x343 (and (=>  bb3.i.i.i_0 $x327) (=>  bb3.i.i.i_0 $x341))))
(let (($x242 (=>  bb2.i3.i.i.i_0 (and (and bb.i1.i.i.i_0 E0x3941150) |%_14_0|))))
(let (($x244 (and $x242 (=>  bb2.i3.i.i.i_0 E0x3941150))))
(let (($x232 (=>  bb1.i.i.i.i.i_0 (and (and bb.i.i.i.i.i_0 E0x3940300) |%_15_0|))))
(let (($x234 (and $x232 (=>  bb1.i.i.i.i.i_0 E0x3940300))))
(let (($x222 (=>  bb.i1.i.i.i_0 (and (and bb2.i.i.i_0 E0x393f7e0) (not |%_10_0|)))))
(let (($x224 (and $x222 (=>  bb.i1.i.i.i_0 E0x393f7e0))))
(let (($x214 (=>  bb.i.i.i.i.i_0 (and (and bb2.i.i.i_0 E0x393ed60) |%_10_0|))))
(let (($x216 (and $x214 (=>  bb.i.i.i.i.i_0 E0x393ed60))))
(let (($x206 (=>  bb2.i.i.i_0 (and (and bb1.i.i.i_0 E0x393e490) (not |%_13_0|)))))
(let (($x208 (and $x206 (=>  bb2.i.i.i_0 E0x393e490))))
(let (($x195 (or (and E0x393c880 (not E0x3937720)) (and E0x3937720 (not E0x393c880)))))
(let (($x187 (and (<= |%waterLevel.4_0| |%waterLevel.1_0|) (>= |%waterLevel.4_0| |%waterLevel.1_0|))))
(let (($x180 (and (<= |%waterLevel.4_0| |%waterLevel.3_0|) (>= |%waterLevel.4_0| |%waterLevel.3_0|))))
(let (($x189 (or (and (and bb.i1.i.i_0 E0x393c880) $x180) (and (and (and bb9.i.i_0 E0x3937720) |%_10_0|) $x187))))
(let (($x197 (and (=>  bb1.i.i.i_0 $x189) (=>  bb1.i.i.i_0 $x195))))
(let (($x167 (=>  bb.i1.i.i_0 (and (and bb9.i.i_0 E0x393b2c0) (not |%_10_0|)))))
(let (($x169 (and $x167 (=>  bb.i1.i.i_0 E0x393b2c0))))
(let (($x156 (or (and E0x3939930 (not E0x3939f70)) (and E0x3939f70 (not E0x3939930)))))
(let (($x149 (and (and (and bb5.i.i_0 E0x3939f70) (not |%_7_0|)) (and (<= |%systemActive.0_0| 1.0) (>= |%systemActive.0_0| 1.0)))))
(let (($x140 (and (<= |%systemActive.0_0| |%systemActive.1_1_0|) (>= |%systemActive.0_0| |%systemActive.1_1_0|))))
(let (($x151 (=>  bb9.i.i_0 (or (and (and (and bb7.i.i_0 E0x3939930) |%_9_0|) $x140) $x149))))
(let (($x158 (and $x151 (=>  bb9.i.i_0 $x156))))
(let (($x128 (and (=>  bb7.i.i_0 (and (and bb5.i.i_0 E0x3938920) |%_7_0|)) (=>  bb7.i.i_0 E0x3938920))))
(let (($x116 (or (and E0x3936e20 (not E0x3937400)) (and E0x3937400 (not E0x3936e20)))))
(let (($x108 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x101 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x110 (or (and (and bb4.i.i_0 E0x3936e20) $x101) (and (and (and bb3.i.i_0 E0x3937400) |%_4_0|) $x108))))
(let (($x118 (and (=>  bb5.i.i_0 $x110) (=>  bb5.i.i_0 $x116))))
(let (($x90 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x3935d50) (not |%_4_0|))) (=>  bb4.i.i_0 E0x3935d50))))
(let (($x77 (or (and E0x391d550 (not E0x39347d0)) (and E0x39347d0 (not E0x391d550)))))
(let (($x69 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x62 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x71 (or (and (and bb2.i.i_0 E0x391d550) $x62) (and (and (and bb1.i.i_0 E0x39347d0) |%_1_0|) $x69))))
(let (($x79 (and (=>  bb3.i.i_0 $x71) (=>  bb3.i.i_0 $x77))))
(let (($x51 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x391ba00) (not |%_1_0|))) (=>  bb2.i.i_0 E0x391ba00))))
(let (($x447 (and $x51 $x79 $x90 $x118 $x128 $x158 $x169 $x197 $x208 $x216 $x224 $x234 $x244 $x343 (and bb3.i.i.i_0 |%or.cond1_0|))))
(and $x447 $x444))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))) (E_cp-rel-bb1.i.i_cp-rel-bb2.i.i.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )))



(query cp-rel-bb2.i.i.i.i)
