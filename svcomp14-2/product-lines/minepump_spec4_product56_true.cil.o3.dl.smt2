(declare-var %pumpRunning.2_1_0 Real)
(declare-var %systemActive.1_1_0 Real)
(declare-var %waterLevel.2_1_0 Real)
(declare-var %methaneLevelCritical.1_1_0 Real)
(declare-var entry_0 Bool)
(declare-var %pumpRunning.2_1_1 Real)
(declare-var %systemActive.1_1_1 Real)
(declare-var %waterLevel.2_1_1 Real)
(declare-var %methaneLevelCritical.1_1_1 Real)
(declare-var bb2.i.i_0 Bool)
(declare-var bb1.i.i_0 Bool)
(declare-var E0x39b6f60 Bool)
(declare-var %_1_0 Bool)
(declare-var bb3.i.i_0 Bool)
(declare-var E0x39b8ab0 Bool)
(declare-var %waterLevel.1_0 Real)
(declare-var %waterLevel.0_0 Real)
(declare-var E0x39cfd30 Bool)
(declare-var bb4.i.i_0 Bool)
(declare-var E0x39d12b0 Bool)
(declare-var %_4_0 Bool)
(declare-var bb5.i.i_0 Bool)
(declare-var E0x39d2380 Bool)
(declare-var %methaneLevelCritical.0_0 Real)
(declare-var %storemerge.i.i.i_0 Real)
(declare-var E0x39d2960 Bool)
(declare-var bb7.i.i_0 Bool)
(declare-var E0x39d3e80 Bool)
(declare-var %_7_0 Bool)
(declare-var bb9.i.i_0 Bool)
(declare-var E0x39d4e90 Bool)
(declare-var %_9_0 Bool)
(declare-var %systemActive.0_0 Real)
(declare-var E0x39d54d0 Bool)
(declare-var bb.i1.i.i_0 Bool)
(declare-var E0x39d6820 Bool)
(declare-var %_10_0 Bool)
(declare-var bb1.i.i.i_0 Bool)
(declare-var E0x39d7de0 Bool)
(declare-var %waterLevel.4_0 Real)
(declare-var %waterLevel.3_0 Real)
(declare-var E0x39d2c80 Bool)
(declare-var bb2.i.i.i_0 Bool)
(declare-var E0x39d99f0 Bool)
(declare-var %_13_0 Bool)
(declare-var bb.i.i.i.i.i.i_0 Bool)
(declare-var E0x39da2c0 Bool)
(declare-var bb.i3.i.i.i_0 Bool)
(declare-var E0x39db080 Bool)
(declare-var bb2.i4.i.i.i.i_0 Bool)
(declare-var E0x39dc400 Bool)
(declare-var %or.cond17_0 Bool)
(declare-var bb3.i.i.i_0 Bool)
(declare-var E0x39d4620 Bool)
(declare-var %systemActive.0.reg2mem.0_0 Real)
(declare-var %waterLevel.4.reg2mem.0_0 Real)
(declare-var %pumpRunning.4_0 Real)
(declare-var %pumpRunning.10_0 Real)
(declare-var E0x39d1c90 Bool)
(declare-var E0x39de2c0 Bool)
(declare-var E0x39d84c0 Bool)
(declare-var E0x39df0a0 Bool)
(declare-var %or.cond_0 Bool)
(declare-var %_0_0 Real)
(declare-var %not._0 Bool)
(declare-var %_2_0 Real)
(declare-var %_3_0 Real)
(declare-var %_5_0 Bool)
(declare-var %_6_0 Real)
(declare-var %_8_0 Real)
(declare-var %_11_0 Bool)
(declare-var %_12_0 Real)
(declare-var %_16_0 Bool)
(declare-var %_14_0 Bool)
(declare-var %_15_0 Bool)
(declare-var %.not_0 Bool)
(declare-var %_17_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb1.i.i.i.i ())
(declare-rel cp-rel-bb1.i.i (Real Real Real Real ))
(declare-rel E_cp-rel-entry_cp-rel-bb1.i.i (Real Real Real Real ))
(declare-rel E_cp-rel-bb1.i.i_cp-rel-bb1.i.i (Real Real Real Real Real Real Real Real ))
(declare-rel E_cp-rel-bb1.i.i_cp-rel-bb1.i.i.i.i (Real Real Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (E_cp-rel-entry_cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )) (cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )))

(rule (=> (let (($x33 (and (<= |%methaneLevelCritical.1_1_0| 0.0) (>= |%methaneLevelCritical.1_1_0| 0.0))))
(let (($x22 (and entry_0 (and (<= |%pumpRunning.2_1_0| 0.0) (>= |%pumpRunning.2_1_0| 0.0)))))
(let (($x26 (and $x22 (and (<= |%systemActive.1_1_0| 1.0) (>= |%systemActive.1_1_0| 1.0)))))
(let (($x30 (and $x26 (and (<= |%waterLevel.2_1_0| 1.0) (>= |%waterLevel.2_1_0| 1.0)))))
(and $x30 $x33))))) (E_cp-rel-entry_cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )))

(rule (=> (and (cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| ) (E_cp-rel-bb1.i.i_cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| |%pumpRunning.2_1_1| |%systemActive.1_1_1| |%waterLevel.2_1_1| |%methaneLevelCritical.1_1_1| )) (cp-rel-bb1.i.i |%pumpRunning.2_1_1| |%systemActive.1_1_1| |%waterLevel.2_1_1| |%methaneLevelCritical.1_1_1| )))

(rule (=> (let (($x406 (and (=  |%_1_0| (= |%_0_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_2_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_2_0| |%waterLevel.2_1_0|)) (=  |%_4_0| (= |%_3_0| 0.0)) (=  |%_5_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_5_0| 1.0 0.0)) (=  |%_7_0| (= |%_6_0| 0.0)) (=  |%_9_0| (= |%_8_0| 0.0)) (=  |%_10_0| (= |%pumpRunning.2_1_0| 0.0)) (=  |%_11_0| (> |%waterLevel.1_0| 0.0)) (= |%_12_0| (+ |%waterLevel.1_0| (~ 1.0))) (= |%waterLevel.3_0| (ite  |%_11_0| |%_12_0| |%waterLevel.1_0|)) (=  |%_13_0| (= |%systemActive.0_0| 0.0)) (=  |%_16_0| (> |%waterLevel.4_0| 1.0)) (= |%pumpRunning.10_0| (ite  |%_16_0| 1.0 |%pumpRunning.2_1_0|)) (=  |%_14_0| (= |%methaneLevelCritical.0_0| 0.0)) (=  |%_15_0| (= |%waterLevel.4_0| 0.0)) (=  |%or.cond17_0| (and |%_14_0| |%_15_0|)) (=  |%.not_0| (not (= |%waterLevel.4.reg2mem.0_0| 0.0))) (=  |%_17_0| (= |%pumpRunning.4_0| 0.0)) (=  |%or.cond_0| (or |%_17_0| |%.not_0|)))))
(let (($x328 (and (<= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|) (>= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|))))
(let (($x324 (and (<= |%waterLevel.2_1_1| |%waterLevel.4.reg2mem.0_0|) (>= |%waterLevel.2_1_1| |%waterLevel.4.reg2mem.0_0|))))
(let (($x320 (and (<= |%systemActive.1_1_1| |%systemActive.0.reg2mem.0_0|) (>= |%systemActive.1_1_1| |%systemActive.0.reg2mem.0_0|))))
(let (($x316 (and (<= |%pumpRunning.2_1_1| |%pumpRunning.4_0|) (>= |%pumpRunning.2_1_1| |%pumpRunning.4_0|))))
(let (($x329 (and (and (and (and (and bb3.i.i.i_0 |%or.cond_0|) $x316) $x320) $x324) $x328)))
(let (($x300 (not E0x39d84c0)))
(let (($x299 (not E0x39de2c0)))
(let (($x298 (not E0x39d1c90)))
(let (($x303 (not E0x39d4620)))
(let (($x308 (or (and E0x39d4620 $x298 $x299 $x300 (not E0x39df0a0)) (and E0x39d1c90 $x303 $x299 $x300 (not E0x39df0a0)) (and E0x39de2c0 $x303 $x298 $x300 (not E0x39df0a0)) (and E0x39d84c0 $x303 $x298 $x299 (not E0x39df0a0)) (and E0x39df0a0 $x303 $x298 $x299 $x300))))
(let (($x275 (and (<= |%pumpRunning.4_0| 0.0) (>= |%pumpRunning.4_0| 0.0))))
(let (($x293 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|))))
(let (($x289 (and (<= |%systemActive.0.reg2mem.0_0| 0.0) (>= |%systemActive.0.reg2mem.0_0| 0.0))))
(let (($x294 (and (and (and (and bb7.i.i_0 E0x39df0a0) (not |%_9_0|)) $x289) $x293)))
(let (($x265 (and (<= |%pumpRunning.4_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.4_0| |%pumpRunning.2_1_0|))))
(let (($x249 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.4_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.4_0|))))
(let (($x243 (and (<= |%systemActive.0.reg2mem.0_0| |%systemActive.0_0|) (>= |%systemActive.0.reg2mem.0_0| |%systemActive.0_0|))))
(let (($x282 (and (and (and (and (and bb1.i.i.i_0 E0x39d84c0) |%_13_0|) $x243) $x249) $x265)))
(let (($x272 (and (and (and (and bb.i3.i.i.i_0 E0x39de2c0) (not |%or.cond17_0|)) $x243) $x249)))
(let (($x257 (and (<= |%pumpRunning.4_0| |%pumpRunning.10_0|) (>= |%pumpRunning.4_0| |%pumpRunning.10_0|))))
(let (($x296 (or (and (and (and (and bb.i.i.i.i.i.i_0 E0x39d4620) $x243) $x249) $x257) (and (and (and (and bb2.i4.i.i.i.i_0 E0x39d1c90) $x243) $x249) $x265) (and $x272 $x275) $x282 (and $x294 $x275))))
(let (($x310 (and (=>  bb3.i.i.i_0 $x296) (=>  bb3.i.i.i_0 $x308))))
(let (($x232 (=>  bb2.i4.i.i.i.i_0 (and (and bb.i3.i.i.i_0 E0x39dc400) |%or.cond17_0|))))
(let (($x234 (and $x232 (=>  bb2.i4.i.i.i.i_0 E0x39dc400))))
(let (($x222 (=>  bb.i3.i.i.i_0 (and (and bb2.i.i.i_0 E0x39db080) (not |%_10_0|)))))
(let (($x224 (and $x222 (=>  bb.i3.i.i.i_0 E0x39db080))))
(let (($x214 (=>  bb.i.i.i.i.i.i_0 (and (and bb2.i.i.i_0 E0x39da2c0) |%_10_0|))))
(let (($x216 (and $x214 (=>  bb.i.i.i.i.i.i_0 E0x39da2c0))))
(let (($x206 (=>  bb2.i.i.i_0 (and (and bb1.i.i.i_0 E0x39d99f0) (not |%_13_0|)))))
(let (($x208 (and $x206 (=>  bb2.i.i.i_0 E0x39d99f0))))
(let (($x195 (or (and E0x39d7de0 (not E0x39d2c80)) (and E0x39d2c80 (not E0x39d7de0)))))
(let (($x187 (and (<= |%waterLevel.4_0| |%waterLevel.1_0|) (>= |%waterLevel.4_0| |%waterLevel.1_0|))))
(let (($x180 (and (<= |%waterLevel.4_0| |%waterLevel.3_0|) (>= |%waterLevel.4_0| |%waterLevel.3_0|))))
(let (($x189 (or (and (and bb.i1.i.i_0 E0x39d7de0) $x180) (and (and (and bb9.i.i_0 E0x39d2c80) |%_10_0|) $x187))))
(let (($x197 (and (=>  bb1.i.i.i_0 $x189) (=>  bb1.i.i.i_0 $x195))))
(let (($x167 (=>  bb.i1.i.i_0 (and (and bb9.i.i_0 E0x39d6820) (not |%_10_0|)))))
(let (($x169 (and $x167 (=>  bb.i1.i.i_0 E0x39d6820))))
(let (($x156 (or (and E0x39d4e90 (not E0x39d54d0)) (and E0x39d54d0 (not E0x39d4e90)))))
(let (($x149 (and (and (and bb5.i.i_0 E0x39d54d0) (not |%_7_0|)) (and (<= |%systemActive.0_0| 1.0) (>= |%systemActive.0_0| 1.0)))))
(let (($x140 (and (<= |%systemActive.0_0| |%systemActive.1_1_0|) (>= |%systemActive.0_0| |%systemActive.1_1_0|))))
(let (($x151 (=>  bb9.i.i_0 (or (and (and (and bb7.i.i_0 E0x39d4e90) |%_9_0|) $x140) $x149))))
(let (($x158 (and $x151 (=>  bb9.i.i_0 $x156))))
(let (($x128 (and (=>  bb7.i.i_0 (and (and bb5.i.i_0 E0x39d3e80) |%_7_0|)) (=>  bb7.i.i_0 E0x39d3e80))))
(let (($x116 (or (and E0x39d2380 (not E0x39d2960)) (and E0x39d2960 (not E0x39d2380)))))
(let (($x108 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x101 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x110 (or (and (and bb4.i.i_0 E0x39d2380) $x101) (and (and (and bb3.i.i_0 E0x39d2960) |%_4_0|) $x108))))
(let (($x118 (and (=>  bb5.i.i_0 $x110) (=>  bb5.i.i_0 $x116))))
(let (($x90 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x39d12b0) (not |%_4_0|))) (=>  bb4.i.i_0 E0x39d12b0))))
(let (($x77 (or (and E0x39b8ab0 (not E0x39cfd30)) (and E0x39cfd30 (not E0x39b8ab0)))))
(let (($x69 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x62 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x71 (or (and (and bb2.i.i_0 E0x39b8ab0) $x62) (and (and (and bb1.i.i_0 E0x39cfd30) |%_1_0|) $x69))))
(let (($x79 (and (=>  bb3.i.i_0 $x71) (=>  bb3.i.i_0 $x77))))
(let (($x51 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x39b6f60) (not |%_1_0|))) (=>  bb2.i.i_0 E0x39b6f60))))
(let (($x330 (and $x51 $x79 $x90 $x118 $x128 $x158 $x169 $x197 $x208 $x216 $x224 $x234 $x310 $x329)))
(and $x330 $x406)))))))))))))))))))))))))))))))))))))))))))))))))))))))))) (E_cp-rel-bb1.i.i_cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| |%pumpRunning.2_1_1| |%systemActive.1_1_1| |%waterLevel.2_1_1| |%methaneLevelCritical.1_1_1| )))

(rule (=> (and (cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| ) (E_cp-rel-bb1.i.i_cp-rel-bb1.i.i.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )) cp-rel-bb1.i.i.i.i))

(rule (=> (let (($x406 (and (=  |%_1_0| (= |%_0_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_2_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_2_0| |%waterLevel.2_1_0|)) (=  |%_4_0| (= |%_3_0| 0.0)) (=  |%_5_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_5_0| 1.0 0.0)) (=  |%_7_0| (= |%_6_0| 0.0)) (=  |%_9_0| (= |%_8_0| 0.0)) (=  |%_10_0| (= |%pumpRunning.2_1_0| 0.0)) (=  |%_11_0| (> |%waterLevel.1_0| 0.0)) (= |%_12_0| (+ |%waterLevel.1_0| (~ 1.0))) (= |%waterLevel.3_0| (ite  |%_11_0| |%_12_0| |%waterLevel.1_0|)) (=  |%_13_0| (= |%systemActive.0_0| 0.0)) (=  |%_16_0| (> |%waterLevel.4_0| 1.0)) (= |%pumpRunning.10_0| (ite  |%_16_0| 1.0 |%pumpRunning.2_1_0|)) (=  |%_14_0| (= |%methaneLevelCritical.0_0| 0.0)) (=  |%_15_0| (= |%waterLevel.4_0| 0.0)) (=  |%or.cond17_0| (and |%_14_0| |%_15_0|)) (=  |%.not_0| (not (= |%waterLevel.4.reg2mem.0_0| 0.0))) (=  |%_17_0| (= |%pumpRunning.4_0| 0.0)) (=  |%or.cond_0| (or |%_17_0| |%.not_0|)))))
(let (($x300 (not E0x39d84c0)))
(let (($x299 (not E0x39de2c0)))
(let (($x298 (not E0x39d1c90)))
(let (($x303 (not E0x39d4620)))
(let (($x308 (or (and E0x39d4620 $x298 $x299 $x300 (not E0x39df0a0)) (and E0x39d1c90 $x303 $x299 $x300 (not E0x39df0a0)) (and E0x39de2c0 $x303 $x298 $x300 (not E0x39df0a0)) (and E0x39d84c0 $x303 $x298 $x299 (not E0x39df0a0)) (and E0x39df0a0 $x303 $x298 $x299 $x300))))
(let (($x275 (and (<= |%pumpRunning.4_0| 0.0) (>= |%pumpRunning.4_0| 0.0))))
(let (($x293 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|))))
(let (($x289 (and (<= |%systemActive.0.reg2mem.0_0| 0.0) (>= |%systemActive.0.reg2mem.0_0| 0.0))))
(let (($x294 (and (and (and (and bb7.i.i_0 E0x39df0a0) (not |%_9_0|)) $x289) $x293)))
(let (($x265 (and (<= |%pumpRunning.4_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.4_0| |%pumpRunning.2_1_0|))))
(let (($x249 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.4_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.4_0|))))
(let (($x243 (and (<= |%systemActive.0.reg2mem.0_0| |%systemActive.0_0|) (>= |%systemActive.0.reg2mem.0_0| |%systemActive.0_0|))))
(let (($x282 (and (and (and (and (and bb1.i.i.i_0 E0x39d84c0) |%_13_0|) $x243) $x249) $x265)))
(let (($x272 (and (and (and (and bb.i3.i.i.i_0 E0x39de2c0) (not |%or.cond17_0|)) $x243) $x249)))
(let (($x257 (and (<= |%pumpRunning.4_0| |%pumpRunning.10_0|) (>= |%pumpRunning.4_0| |%pumpRunning.10_0|))))
(let (($x296 (or (and (and (and (and bb.i.i.i.i.i.i_0 E0x39d4620) $x243) $x249) $x257) (and (and (and (and bb2.i4.i.i.i.i_0 E0x39d1c90) $x243) $x249) $x265) (and $x272 $x275) $x282 (and $x294 $x275))))
(let (($x310 (and (=>  bb3.i.i.i_0 $x296) (=>  bb3.i.i.i_0 $x308))))
(let (($x232 (=>  bb2.i4.i.i.i.i_0 (and (and bb.i3.i.i.i_0 E0x39dc400) |%or.cond17_0|))))
(let (($x234 (and $x232 (=>  bb2.i4.i.i.i.i_0 E0x39dc400))))
(let (($x222 (=>  bb.i3.i.i.i_0 (and (and bb2.i.i.i_0 E0x39db080) (not |%_10_0|)))))
(let (($x224 (and $x222 (=>  bb.i3.i.i.i_0 E0x39db080))))
(let (($x214 (=>  bb.i.i.i.i.i.i_0 (and (and bb2.i.i.i_0 E0x39da2c0) |%_10_0|))))
(let (($x216 (and $x214 (=>  bb.i.i.i.i.i.i_0 E0x39da2c0))))
(let (($x206 (=>  bb2.i.i.i_0 (and (and bb1.i.i.i_0 E0x39d99f0) (not |%_13_0|)))))
(let (($x208 (and $x206 (=>  bb2.i.i.i_0 E0x39d99f0))))
(let (($x195 (or (and E0x39d7de0 (not E0x39d2c80)) (and E0x39d2c80 (not E0x39d7de0)))))
(let (($x187 (and (<= |%waterLevel.4_0| |%waterLevel.1_0|) (>= |%waterLevel.4_0| |%waterLevel.1_0|))))
(let (($x180 (and (<= |%waterLevel.4_0| |%waterLevel.3_0|) (>= |%waterLevel.4_0| |%waterLevel.3_0|))))
(let (($x189 (or (and (and bb.i1.i.i_0 E0x39d7de0) $x180) (and (and (and bb9.i.i_0 E0x39d2c80) |%_10_0|) $x187))))
(let (($x197 (and (=>  bb1.i.i.i_0 $x189) (=>  bb1.i.i.i_0 $x195))))
(let (($x167 (=>  bb.i1.i.i_0 (and (and bb9.i.i_0 E0x39d6820) (not |%_10_0|)))))
(let (($x169 (and $x167 (=>  bb.i1.i.i_0 E0x39d6820))))
(let (($x156 (or (and E0x39d4e90 (not E0x39d54d0)) (and E0x39d54d0 (not E0x39d4e90)))))
(let (($x149 (and (and (and bb5.i.i_0 E0x39d54d0) (not |%_7_0|)) (and (<= |%systemActive.0_0| 1.0) (>= |%systemActive.0_0| 1.0)))))
(let (($x140 (and (<= |%systemActive.0_0| |%systemActive.1_1_0|) (>= |%systemActive.0_0| |%systemActive.1_1_0|))))
(let (($x151 (=>  bb9.i.i_0 (or (and (and (and bb7.i.i_0 E0x39d4e90) |%_9_0|) $x140) $x149))))
(let (($x158 (and $x151 (=>  bb9.i.i_0 $x156))))
(let (($x128 (and (=>  bb7.i.i_0 (and (and bb5.i.i_0 E0x39d3e80) |%_7_0|)) (=>  bb7.i.i_0 E0x39d3e80))))
(let (($x116 (or (and E0x39d2380 (not E0x39d2960)) (and E0x39d2960 (not E0x39d2380)))))
(let (($x108 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x101 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x110 (or (and (and bb4.i.i_0 E0x39d2380) $x101) (and (and (and bb3.i.i_0 E0x39d2960) |%_4_0|) $x108))))
(let (($x118 (and (=>  bb5.i.i_0 $x110) (=>  bb5.i.i_0 $x116))))
(let (($x90 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x39d12b0) (not |%_4_0|))) (=>  bb4.i.i_0 E0x39d12b0))))
(let (($x77 (or (and E0x39b8ab0 (not E0x39cfd30)) (and E0x39cfd30 (not E0x39b8ab0)))))
(let (($x69 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x62 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x71 (or (and (and bb2.i.i_0 E0x39b8ab0) $x62) (and (and (and bb1.i.i_0 E0x39cfd30) |%_1_0|) $x69))))
(let (($x79 (and (=>  bb3.i.i_0 $x71) (=>  bb3.i.i_0 $x77))))
(let (($x51 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x39b6f60) (not |%_1_0|))) (=>  bb2.i.i_0 E0x39b6f60))))
(let (($x410 (and $x51 $x79 $x90 $x118 $x128 $x158 $x169 $x197 $x208 $x216 $x224 $x234 $x310 (and bb3.i.i.i_0 (not |%or.cond_0|)))))
(and $x410 $x406))))))))))))))))))))))))))))))))))))))))))))))))))))) (E_cp-rel-bb1.i.i_cp-rel-bb1.i.i.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )))



(query cp-rel-bb1.i.i.i.i)
