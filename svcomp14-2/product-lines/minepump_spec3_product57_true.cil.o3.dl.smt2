(declare-var %methaneLevelCritical.1_1_0 Real)
(declare-var %pumpRunning.2_1_0 Real)
(declare-var %waterLevel.2_1_0 Real)
(declare-var entry_0 Bool)
(declare-var %methaneLevelCritical.1_1_1 Real)
(declare-var %pumpRunning.2_1_1 Real)
(declare-var %waterLevel.2_1_1 Real)
(declare-var bb2.i.i_0 Bool)
(declare-var bb1.i.i_0 Bool)
(declare-var E0x3de75b0 Bool)
(declare-var %_1_0 Bool)
(declare-var bb3.i.i_0 Bool)
(declare-var E0x3de9000 Bool)
(declare-var %waterLevel.1_0 Real)
(declare-var %waterLevel.0_0 Real)
(declare-var E0x3de9620 Bool)
(declare-var bb4.i.i_0 Bool)
(declare-var E0x3e01900 Bool)
(declare-var %_4_0 Bool)
(declare-var bb5.i.i_0 Bool)
(declare-var E0x3e029d0 Bool)
(declare-var %methaneLevelCritical.0_0 Real)
(declare-var %storemerge.i.i.i_0 Real)
(declare-var E0x3e02fb0 Bool)
(declare-var bb.i.i.i.i.i_0 Bool)
(declare-var E0x3e04290 Bool)
(declare-var %_6_0 Bool)
(declare-var bb.i3.i.i.i_0 Bool)
(declare-var E0x3e04ec0 Bool)
(declare-var bb1.i.i.i.i.i_0 Bool)
(declare-var E0x3e06680 Bool)
(declare-var %_10_0 Bool)
(declare-var bb2.i5.i.i.i_0 Bool)
(declare-var E0x3e07540 Bool)
(declare-var %_9_0 Bool)
(declare-var bb3.i.i.i_0 Bool)
(declare-var E0x3e080c0 Bool)
(declare-var %waterLevel.4.reg2mem.0_0 Real)
(declare-var %pumpRunning.0_0 Real)
(declare-var %pumpRunning.5_0 Real)
(declare-var E0x3e08a80 Bool)
(declare-var E0x3e035d0 Bool)
(declare-var %waterLevel.3_0 Real)
(declare-var E0x3e099f0 Bool)
(declare-var %or.cond1_0 Bool)
(declare-var %_0_0 Real)
(declare-var %not._0 Bool)
(declare-var %_2_0 Real)
(declare-var %_3_0 Real)
(declare-var %_5_0 Bool)
(declare-var %_7_0 Bool)
(declare-var %_8_0 Real)
(declare-var %_11_0 Bool)
(declare-var %_12_0 Bool)
(declare-var %_13_0 Bool)
(declare-var %_14_0 Bool)
(declare-var %or.cond_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb2.i.i.i.i ())
(declare-rel cp-rel-bb1.i.i (Real Real Real ))
(declare-rel E_cp-rel-entry_cp-rel-bb1.i.i (Real Real Real ))
(declare-rel E_cp-rel-bb1.i.i_cp-rel-bb1.i.i (Real Real Real Real Real Real ))
(declare-rel E_cp-rel-bb1.i.i_cp-rel-bb2.i.i.i.i (Real Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (E_cp-rel-entry_cp-rel-bb1.i.i |%methaneLevelCritical.1_1_0| |%pumpRunning.2_1_0| |%waterLevel.2_1_0| )) (cp-rel-bb1.i.i |%methaneLevelCritical.1_1_0| |%pumpRunning.2_1_0| |%waterLevel.2_1_0| )))

(rule (=> (let (($x18 (and (<= |%methaneLevelCritical.1_1_0| 0.0) (>= |%methaneLevelCritical.1_1_0| 0.0))))
(let (($x23 (and (and entry_0 $x18) (and (<= |%pumpRunning.2_1_0| 0.0) (>= |%pumpRunning.2_1_0| 0.0)))))
(and $x23 (and (<= |%waterLevel.2_1_0| 1.0) (>= |%waterLevel.2_1_0| 1.0))))) (E_cp-rel-entry_cp-rel-bb1.i.i |%methaneLevelCritical.1_1_0| |%pumpRunning.2_1_0| |%waterLevel.2_1_0| )))

(rule (=> (and (cp-rel-bb1.i.i |%methaneLevelCritical.1_1_0| |%pumpRunning.2_1_0| |%waterLevel.2_1_0| ) (E_cp-rel-bb1.i.i_cp-rel-bb1.i.i |%methaneLevelCritical.1_1_0| |%pumpRunning.2_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_1| |%pumpRunning.2_1_1| |%waterLevel.2_1_1| )) (cp-rel-bb1.i.i |%methaneLevelCritical.1_1_1| |%pumpRunning.2_1_1| |%waterLevel.2_1_1| )))

(rule (=> (let (($x290 (and (=  |%_1_0| (= |%_0_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_2_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_2_0| |%waterLevel.2_1_0|)) (=  |%_4_0| (= |%_3_0| 0.0)) (=  |%_5_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_5_0| 1.0 0.0)) (=  |%_6_0| (= |%pumpRunning.2_1_0| 0.0)) (=  |%_10_0| (> |%waterLevel.1_0| 1.0)) (=  |%_7_0| (> |%waterLevel.1_0| 0.0)) (= |%_8_0| (+ |%waterLevel.1_0| (~ 1.0))) (= |%waterLevel.3_0| (ite  |%_7_0| |%_8_0| |%waterLevel.1_0|)) (=  |%_9_0| (= |%waterLevel.3_0| 0.0)) (=  |%_11_0| (= |%methaneLevelCritical.0_0| 0.0)) (= |%pumpRunning.5_0| (ite  |%_11_0| 1.0 |%pumpRunning.2_1_0|)) (=  |%_12_0| (= |%methaneLevelCritical.0_0| 0.0)) (=  |%_13_0| (= |%waterLevel.4.reg2mem.0_0| 2.0)) (=  |%_14_0| (= |%pumpRunning.0_0| 0.0)) (=  |%or.cond_0| (and |%_13_0| |%_12_0|)) (=  |%or.cond1_0| (and |%or.cond_0| |%_14_0|)))))
(let (($x222 (and (<= |%waterLevel.2_1_1| |%waterLevel.4.reg2mem.0_0|) (>= |%waterLevel.2_1_1| |%waterLevel.4.reg2mem.0_0|))))
(let (($x218 (and (<= |%pumpRunning.2_1_1| |%pumpRunning.0_0|) (>= |%pumpRunning.2_1_1| |%pumpRunning.0_0|))))
(let (($x214 (and (<= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|) (>= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|))))
(let (($x223 (and (and (and (and bb3.i.i.i_0 (not |%or.cond1_0|)) $x214) $x218) $x222)))
(let (($x198 (not E0x3e035d0)))
(let (($x197 (not E0x3e08a80)))
(let (($x201 (not E0x3e080c0)))
(let (($x205 (or (and E0x3e080c0 $x197 $x198 (not E0x3e099f0)) (and E0x3e08a80 $x201 $x198 (not E0x3e099f0)) (and E0x3e035d0 $x201 $x197 (not E0x3e099f0)) (and E0x3e099f0 $x201 $x197 $x198))))
(let (($x183 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.3_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.3_0|))))
(let (($x194 (and (and (and (and bb.i3.i.i.i_0 E0x3e099f0) (not |%_9_0|)) $x183) (and (<= |%pumpRunning.0_0| 0.0) (>= |%pumpRunning.0_0| 0.0)))))
(let (($x175 (and (<= |%pumpRunning.0_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.0_0| |%pumpRunning.2_1_0|))))
(let (($x158 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|))))
(let (($x176 (and (and (and (and bb.i.i.i.i.i_0 E0x3e08a80) (not |%_10_0|)) $x158) $x175)))
(let (($x166 (and (<= |%pumpRunning.0_0| |%pumpRunning.5_0|) (>= |%pumpRunning.0_0| |%pumpRunning.5_0|))))
(let (($x195 (or (and (and (and bb1.i.i.i.i.i_0 E0x3e080c0) $x158) $x166) $x176 (and (and (and bb2.i5.i.i.i_0 E0x3e035d0) $x183) $x175) $x194)))
(let (($x207 (and (=>  bb3.i.i.i_0 $x195) (=>  bb3.i.i.i_0 $x205))))
(let (($x147 (=>  bb2.i5.i.i.i_0 (and (and bb.i3.i.i.i_0 E0x3e07540) |%_9_0|))))
(let (($x149 (and $x147 (=>  bb2.i5.i.i.i_0 E0x3e07540))))
(let (($x137 (=>  bb1.i.i.i.i.i_0 (and (and bb.i.i.i.i.i_0 E0x3e06680) |%_10_0|))))
(let (($x139 (and $x137 (=>  bb1.i.i.i.i.i_0 E0x3e06680))))
(let (($x127 (=>  bb.i3.i.i.i_0 (and (and bb5.i.i_0 E0x3e04ec0) (not |%_6_0|)))))
(let (($x129 (and $x127 (=>  bb.i3.i.i.i_0 E0x3e04ec0))))
(let (($x120 (and (=>  bb.i.i.i.i.i_0 (and (and bb5.i.i_0 E0x3e04290) |%_6_0|)) (=>  bb.i.i.i.i.i_0 E0x3e04290))))
(let (($x108 (or (and E0x3e029d0 (not E0x3e02fb0)) (and E0x3e02fb0 (not E0x3e029d0)))))
(let (($x100 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x93 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x102 (or (and (and bb4.i.i_0 E0x3e029d0) $x93) (and (and (and bb3.i.i_0 E0x3e02fb0) |%_4_0|) $x100))))
(let (($x110 (and (=>  bb5.i.i_0 $x102) (=>  bb5.i.i_0 $x108))))
(let (($x82 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x3e01900) (not |%_4_0|))) (=>  bb4.i.i_0 E0x3e01900))))
(let (($x69 (or (and E0x3de9000 (not E0x3de9620)) (and E0x3de9620 (not E0x3de9000)))))
(let (($x61 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x54 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x63 (or (and (and bb2.i.i_0 E0x3de9000) $x54) (and (and (and bb1.i.i_0 E0x3de9620) |%_1_0|) $x61))))
(let (($x71 (and (=>  bb3.i.i_0 $x63) (=>  bb3.i.i_0 $x69))))
(let (($x43 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x3de75b0) (not |%_1_0|))) (=>  bb2.i.i_0 E0x3de75b0))))
(and (and $x43 $x71 $x82 $x110 $x120 $x129 $x139 $x149 $x207 $x223) $x290))))))))))))))))))))))))))))))))))))) (E_cp-rel-bb1.i.i_cp-rel-bb1.i.i |%methaneLevelCritical.1_1_0| |%pumpRunning.2_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_1| |%pumpRunning.2_1_1| |%waterLevel.2_1_1| )))

(rule (=> (and (cp-rel-bb1.i.i |%methaneLevelCritical.1_1_0| |%pumpRunning.2_1_0| |%waterLevel.2_1_0| ) (E_cp-rel-bb1.i.i_cp-rel-bb2.i.i.i.i |%methaneLevelCritical.1_1_0| |%pumpRunning.2_1_0| |%waterLevel.2_1_0| )) cp-rel-bb2.i.i.i.i))

(rule (=> (let (($x290 (and (=  |%_1_0| (= |%_0_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_2_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_2_0| |%waterLevel.2_1_0|)) (=  |%_4_0| (= |%_3_0| 0.0)) (=  |%_5_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_5_0| 1.0 0.0)) (=  |%_6_0| (= |%pumpRunning.2_1_0| 0.0)) (=  |%_10_0| (> |%waterLevel.1_0| 1.0)) (=  |%_7_0| (> |%waterLevel.1_0| 0.0)) (= |%_8_0| (+ |%waterLevel.1_0| (~ 1.0))) (= |%waterLevel.3_0| (ite  |%_7_0| |%_8_0| |%waterLevel.1_0|)) (=  |%_9_0| (= |%waterLevel.3_0| 0.0)) (=  |%_11_0| (= |%methaneLevelCritical.0_0| 0.0)) (= |%pumpRunning.5_0| (ite  |%_11_0| 1.0 |%pumpRunning.2_1_0|)) (=  |%_12_0| (= |%methaneLevelCritical.0_0| 0.0)) (=  |%_13_0| (= |%waterLevel.4.reg2mem.0_0| 2.0)) (=  |%_14_0| (= |%pumpRunning.0_0| 0.0)) (=  |%or.cond_0| (and |%_13_0| |%_12_0|)) (=  |%or.cond1_0| (and |%or.cond_0| |%_14_0|)))))
(let (($x198 (not E0x3e035d0)))
(let (($x197 (not E0x3e08a80)))
(let (($x201 (not E0x3e080c0)))
(let (($x205 (or (and E0x3e080c0 $x197 $x198 (not E0x3e099f0)) (and E0x3e08a80 $x201 $x198 (not E0x3e099f0)) (and E0x3e035d0 $x201 $x197 (not E0x3e099f0)) (and E0x3e099f0 $x201 $x197 $x198))))
(let (($x183 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.3_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.3_0|))))
(let (($x194 (and (and (and (and bb.i3.i.i.i_0 E0x3e099f0) (not |%_9_0|)) $x183) (and (<= |%pumpRunning.0_0| 0.0) (>= |%pumpRunning.0_0| 0.0)))))
(let (($x175 (and (<= |%pumpRunning.0_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.0_0| |%pumpRunning.2_1_0|))))
(let (($x158 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|))))
(let (($x176 (and (and (and (and bb.i.i.i.i.i_0 E0x3e08a80) (not |%_10_0|)) $x158) $x175)))
(let (($x166 (and (<= |%pumpRunning.0_0| |%pumpRunning.5_0|) (>= |%pumpRunning.0_0| |%pumpRunning.5_0|))))
(let (($x195 (or (and (and (and bb1.i.i.i.i.i_0 E0x3e080c0) $x158) $x166) $x176 (and (and (and bb2.i5.i.i.i_0 E0x3e035d0) $x183) $x175) $x194)))
(let (($x207 (and (=>  bb3.i.i.i_0 $x195) (=>  bb3.i.i.i_0 $x205))))
(let (($x147 (=>  bb2.i5.i.i.i_0 (and (and bb.i3.i.i.i_0 E0x3e07540) |%_9_0|))))
(let (($x149 (and $x147 (=>  bb2.i5.i.i.i_0 E0x3e07540))))
(let (($x137 (=>  bb1.i.i.i.i.i_0 (and (and bb.i.i.i.i.i_0 E0x3e06680) |%_10_0|))))
(let (($x139 (and $x137 (=>  bb1.i.i.i.i.i_0 E0x3e06680))))
(let (($x127 (=>  bb.i3.i.i.i_0 (and (and bb5.i.i_0 E0x3e04ec0) (not |%_6_0|)))))
(let (($x129 (and $x127 (=>  bb.i3.i.i.i_0 E0x3e04ec0))))
(let (($x120 (and (=>  bb.i.i.i.i.i_0 (and (and bb5.i.i_0 E0x3e04290) |%_6_0|)) (=>  bb.i.i.i.i.i_0 E0x3e04290))))
(let (($x108 (or (and E0x3e029d0 (not E0x3e02fb0)) (and E0x3e02fb0 (not E0x3e029d0)))))
(let (($x100 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x93 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x102 (or (and (and bb4.i.i_0 E0x3e029d0) $x93) (and (and (and bb3.i.i_0 E0x3e02fb0) |%_4_0|) $x100))))
(let (($x110 (and (=>  bb5.i.i_0 $x102) (=>  bb5.i.i_0 $x108))))
(let (($x82 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x3e01900) (not |%_4_0|))) (=>  bb4.i.i_0 E0x3e01900))))
(let (($x69 (or (and E0x3de9000 (not E0x3de9620)) (and E0x3de9620 (not E0x3de9000)))))
(let (($x61 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x54 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x63 (or (and (and bb2.i.i_0 E0x3de9000) $x54) (and (and (and bb1.i.i_0 E0x3de9620) |%_1_0|) $x61))))
(let (($x71 (and (=>  bb3.i.i_0 $x63) (=>  bb3.i.i_0 $x69))))
(let (($x43 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x3de75b0) (not |%_1_0|))) (=>  bb2.i.i_0 E0x3de75b0))))
(let (($x293 (and $x43 $x71 $x82 $x110 $x120 $x129 $x139 $x149 $x207 (and bb3.i.i.i_0 |%or.cond1_0|))))
(and $x293 $x290)))))))))))))))))))))))))))))))))) (E_cp-rel-bb1.i.i_cp-rel-bb2.i.i.i.i |%methaneLevelCritical.1_1_0| |%pumpRunning.2_1_0| |%waterLevel.2_1_0| )))



(query cp-rel-bb2.i.i.i.i)
