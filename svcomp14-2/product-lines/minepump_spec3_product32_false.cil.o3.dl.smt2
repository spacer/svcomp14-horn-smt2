(declare-var %waterLevel.2_1_0 Real)
(declare-var %methaneLevelCritical.1_1_0 Real)
(declare-var entry_0 Bool)
(declare-var %waterLevel.2_1_1 Real)
(declare-var %methaneLevelCritical.1_1_1 Real)
(declare-var bb2.i.i_0 Bool)
(declare-var bb1.i.i_0 Bool)
(declare-var E0x4efc100 Bool)
(declare-var %_6_0 Bool)
(declare-var bb3.i.i_0 Bool)
(declare-var E0x4efdc10 Bool)
(declare-var %waterLevel.1_0 Real)
(declare-var %waterLevel.0_0 Real)
(declare-var E0x4efe190 Bool)
(declare-var bb4.i.i_0 Bool)
(declare-var E0x4f16480 Bool)
(declare-var %_9_0 Bool)
(declare-var bb5.i.i_0 Bool)
(declare-var E0x4f174d0 Bool)
(declare-var %methaneLevelCritical.0_0 Real)
(declare-var %storemerge.i.i.i_0 Real)
(declare-var E0x4f17b10 Bool)
(declare-var bb7.i.i_0 Bool)
(declare-var E0x4f18fd0 Bool)
(declare-var %_12_0 Bool)
(declare-var bb1.i.i.i_0 Bool)
(declare-var E0x4f19a10 Bool)
(declare-var E0x4f19c90 Bool)
(declare-var %or.cond_0 Bool)
(declare-var %_5_0 Real)
(declare-var %not._0 Bool)
(declare-var %_7_0 Real)
(declare-var %_8_0 Real)
(declare-var %_10_0 Bool)
(declare-var %_11_0 Real)
(declare-var %_14_0 Bool)
(declare-var %_15_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb2.i.i.i.i ())
(declare-rel cp-rel-bb1.i.i (Real Real ))
(declare-rel E_cp-rel-entry_cp-rel-bb1.i.i (Real Real ))
(declare-rel E_cp-rel-bb1.i.i_cp-rel-bb1.i.i (Real Real Real Real ))
(declare-rel E_cp-rel-bb1.i.i_cp-rel-bb2.i.i.i.i (Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (E_cp-rel-entry_cp-rel-bb1.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )) (cp-rel-bb1.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )))

(rule (=> (let (($x19 (and (<= |%methaneLevelCritical.1_1_0| 0.0) (>= |%methaneLevelCritical.1_1_0| 0.0))))
(let (($x16 (and entry_0 (and (<= |%waterLevel.2_1_0| 1.0) (>= |%waterLevel.2_1_0| 1.0)))))
(and $x16 $x19))) (E_cp-rel-entry_cp-rel-bb1.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )))

(rule (=> (and (cp-rel-bb1.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| ) (E_cp-rel-bb1.i.i_cp-rel-bb1.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| |%waterLevel.2_1_1| |%methaneLevelCritical.1_1_1| )) (cp-rel-bb1.i.i |%waterLevel.2_1_1| |%methaneLevelCritical.1_1_1| )))

(rule (=> (let (($x182 (and (=  |%_6_0| (= |%_5_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_7_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_7_0| |%waterLevel.2_1_0|)) (=  |%_9_0| (= |%_8_0| 0.0)) (=  |%_10_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_10_0| 1.0 0.0)) (=  |%_12_0| (= |%_11_0| 0.0)) (=  |%_14_0| (= |%methaneLevelCritical.0_0| 0.0)) (=  |%_15_0| (= |%waterLevel.1_0| 2.0)) (=  |%or.cond_0| (and |%_14_0| |%_15_0|)))))
(let (($x140 (and (<= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|) (>= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|))))
(let (($x136 (and (<= |%waterLevel.2_1_1| |%waterLevel.1_0|) (>= |%waterLevel.2_1_1| |%waterLevel.1_0|))))
(let (($x127 (or (and E0x4f19a10 (not E0x4f19c90)) (and E0x4f19c90 (not E0x4f19a10)))))
(let (($x121 (or (and bb7.i.i_0 E0x4f19a10) (and (and bb5.i.i_0 E0x4f19c90) (not |%_12_0|)))))
(let (($x129 (and (=>  bb1.i.i.i_0 $x121) (=>  bb1.i.i.i_0 $x127))))
(let (($x112 (and (=>  bb7.i.i_0 (and (and bb5.i.i_0 E0x4f18fd0) |%_12_0|)) (=>  bb7.i.i_0 E0x4f18fd0))))
(let (($x100 (or (and E0x4f174d0 (not E0x4f17b10)) (and E0x4f17b10 (not E0x4f174d0)))))
(let (($x92 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x85 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x94 (or (and (and bb4.i.i_0 E0x4f174d0) $x85) (and (and (and bb3.i.i_0 E0x4f17b10) |%_9_0|) $x92))))
(let (($x102 (and (=>  bb5.i.i_0 $x94) (=>  bb5.i.i_0 $x100))))
(let (($x74 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x4f16480) (not |%_9_0|))) (=>  bb4.i.i_0 E0x4f16480))))
(let (($x61 (or (and E0x4efdc10 (not E0x4efe190)) (and E0x4efe190 (not E0x4efdc10)))))
(let (($x53 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x46 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x55 (or (and (and bb2.i.i_0 E0x4efdc10) $x46) (and (and (and bb1.i.i_0 E0x4efe190) |%_6_0|) $x53))))
(let (($x63 (and (=>  bb3.i.i_0 $x55) (=>  bb3.i.i_0 $x61))))
(let (($x35 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x4efc100) (not |%_6_0|))) (=>  bb2.i.i_0 E0x4efc100))))
(let (($x142 (and $x35 $x63 $x74 $x102 $x112 $x129 (and (and (and bb1.i.i.i_0 (not |%or.cond_0|)) $x136) $x140))))
(and $x142 $x182))))))))))))))))))))) (E_cp-rel-bb1.i.i_cp-rel-bb1.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| |%waterLevel.2_1_1| |%methaneLevelCritical.1_1_1| )))

(rule (=> (and (cp-rel-bb1.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| ) (E_cp-rel-bb1.i.i_cp-rel-bb2.i.i.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )) cp-rel-bb2.i.i.i.i))

(rule (=> (let (($x182 (and (=  |%_6_0| (= |%_5_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_7_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_7_0| |%waterLevel.2_1_0|)) (=  |%_9_0| (= |%_8_0| 0.0)) (=  |%_10_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_10_0| 1.0 0.0)) (=  |%_12_0| (= |%_11_0| 0.0)) (=  |%_14_0| (= |%methaneLevelCritical.0_0| 0.0)) (=  |%_15_0| (= |%waterLevel.1_0| 2.0)) (=  |%or.cond_0| (and |%_14_0| |%_15_0|)))))
(let (($x127 (or (and E0x4f19a10 (not E0x4f19c90)) (and E0x4f19c90 (not E0x4f19a10)))))
(let (($x121 (or (and bb7.i.i_0 E0x4f19a10) (and (and bb5.i.i_0 E0x4f19c90) (not |%_12_0|)))))
(let (($x129 (and (=>  bb1.i.i.i_0 $x121) (=>  bb1.i.i.i_0 $x127))))
(let (($x112 (and (=>  bb7.i.i_0 (and (and bb5.i.i_0 E0x4f18fd0) |%_12_0|)) (=>  bb7.i.i_0 E0x4f18fd0))))
(let (($x100 (or (and E0x4f174d0 (not E0x4f17b10)) (and E0x4f17b10 (not E0x4f174d0)))))
(let (($x92 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x85 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x94 (or (and (and bb4.i.i_0 E0x4f174d0) $x85) (and (and (and bb3.i.i_0 E0x4f17b10) |%_9_0|) $x92))))
(let (($x102 (and (=>  bb5.i.i_0 $x94) (=>  bb5.i.i_0 $x100))))
(let (($x74 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x4f16480) (not |%_9_0|))) (=>  bb4.i.i_0 E0x4f16480))))
(let (($x61 (or (and E0x4efdc10 (not E0x4efe190)) (and E0x4efe190 (not E0x4efdc10)))))
(let (($x53 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x46 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x55 (or (and (and bb2.i.i_0 E0x4efdc10) $x46) (and (and (and bb1.i.i_0 E0x4efe190) |%_6_0|) $x53))))
(let (($x63 (and (=>  bb3.i.i_0 $x55) (=>  bb3.i.i_0 $x61))))
(let (($x35 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x4efc100) (not |%_6_0|))) (=>  bb2.i.i_0 E0x4efc100))))
(and (and $x35 $x63 $x74 $x102 $x112 $x129 (and bb1.i.i.i_0 |%or.cond_0|)) $x182)))))))))))))))))) (E_cp-rel-bb1.i.i_cp-rel-bb2.i.i.i.i |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )))



(query cp-rel-bb2.i.i.i.i)
