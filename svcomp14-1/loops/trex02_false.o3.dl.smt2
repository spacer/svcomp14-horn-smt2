(declare-var entry_0 Bool)
(declare-var %_1_0 Bool)
(declare-var %_0_0 Real)
(declare-var __UFO__0_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-ERROR.i ())
(declare-rel cp-rel-__UFO__0 ())


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (let (($x13 (=  |%_1_0| (< |%_0_0| 0.0))))
(and (and entry_0 (not |%_1_0|)) $x13))) cp-rel-__UFO__0))

(rule (=> (and cp-rel-__UFO__0 __UFO__0_0) cp-rel-__UFO__0))

(rule (=> (and cp-rel-entry (let (($x13 (=  |%_1_0| (< |%_0_0| 0.0))))
(and (and entry_0 |%_1_0|) $x13))) cp-rel-ERROR.i))



(query cp-rel-ERROR.i)
