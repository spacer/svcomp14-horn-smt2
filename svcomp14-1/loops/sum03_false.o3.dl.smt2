(declare-var %sn.1_1_0 Real)
(declare-var %_0_1_0 Real)
(declare-var entry_0 Bool)
(declare-var %sn.1_1_1 Real)
(declare-var %_0_1_1 Real)
(declare-var bb_0 Bool)
(declare-var %or.cond_0 Bool)
(declare-var %sn.0_0 Real)
(declare-var %_3_0 Real)
(declare-var %tmp1_0 Real)
(declare-var %tmp_0 Real)
(declare-var %_1_0 Bool)
(declare-var %_2_0 Real)
(declare-var %_4_0 Bool)
(declare-var %_5_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-ERROR.i ())
(declare-rel cp-rel-bb (Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (and (and entry_0 (and (<= |%sn.1_1_0| 0.0) (>= |%sn.1_1_0| 0.0))) (and (<= |%_0_1_0| 0.0) (>= |%_0_1_0| 0.0)))) (cp-rel-bb |%sn.1_1_0| |%_0_1_0| )))

(rule (=> (and (cp-rel-bb |%sn.1_1_0| |%_0_1_0| ) (let (($x70 (and (= |%tmp1_0| (+ |%tmp_0| 2.0)) (=  |%_1_0| (> |%_0_1_0| 9.0)) (= |%_2_0| (+ |%sn.1_1_0| 2.0)) (= |%sn.0_0| (ite  |%_1_0| |%sn.1_1_0| |%_2_0|)) (= |%_3_0| (+ |%_0_1_0| 1.0)) (=  |%_4_0| (= |%sn.0_0| |%tmp1_0|)) (=  |%_5_0| (= |%sn.0_0| 0.0)) (=  |%or.cond_0| (or |%_4_0| |%_5_0|)))))
(let (($x33 (and (and bb_0 |%or.cond_0|) (and (<= |%sn.1_1_1| |%sn.0_0|) (>= |%sn.1_1_1| |%sn.0_0|)))))
(and (and $x33 (and (<= |%_0_1_1| |%_3_0|) (>= |%_0_1_1| |%_3_0|))) $x70)))) (cp-rel-bb |%sn.1_1_1| |%_0_1_1| )))

(rule (=> (and (cp-rel-bb |%sn.1_1_0| |%_0_1_0| ) (let (($x70 (and (= |%tmp1_0| (+ |%tmp_0| 2.0)) (=  |%_1_0| (> |%_0_1_0| 9.0)) (= |%_2_0| (+ |%sn.1_1_0| 2.0)) (= |%sn.0_0| (ite  |%_1_0| |%sn.1_1_0| |%_2_0|)) (= |%_3_0| (+ |%_0_1_0| 1.0)) (=  |%_4_0| (= |%sn.0_0| |%tmp1_0|)) (=  |%_5_0| (= |%sn.0_0| 0.0)) (=  |%or.cond_0| (or |%_4_0| |%_5_0|)))))
(and (and bb_0 (not |%or.cond_0|)) $x70))) cp-rel-ERROR.i))



(query cp-rel-ERROR.i)
