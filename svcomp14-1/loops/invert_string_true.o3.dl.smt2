(declare-var %_0_0 Real)
(declare-var %_1_0 Real)
(declare-var %_16_1_0 Real)
(declare-var entry_0 Bool)
(declare-var %_16_1_1 Real)
(declare-var bb6_0 Bool)
(declare-var bb7_0 Bool)
(declare-var E0x45fef70 Bool)
(declare-var %_17_0 Bool)
(declare-var __VERIFIER_assert.exit_0 Bool)
(declare-var E0x45ff330 Bool)
(declare-var %_14_0 Bool)
(declare-var %_15_0 Real)
(declare-var %tmp6_0 Real)
(declare-var %_12_0 Real)
(declare-var %_13_0 Real)
(declare-var %_16_0 Real)
(declare-var __UFO__0_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-ERROR.i ())
(declare-rel cp-rel-bb7 (Real Real Real ))
(declare-rel cp-rel-__UFO__0 (Real Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (and entry_0 (and (<= |%_16_1_0| 0.0) (>= |%_16_1_0| 0.0)))) (cp-rel-bb7 |%_0_0| |%_1_0| |%_16_1_0| )))

(rule (=> (and (cp-rel-bb7 |%_0_0| |%_1_0| |%_16_1_0| ) (let (($x63 (=  |%_14_0| (= |%_12_0| |%_13_0|))))
(let (($x57 (= |%tmp6_0| (+ 4.0 (* (~ 1.0) |%_16_1_0|)))))
(let (($x50 (=  |%_17_0| (< |%_16_1_0| 5.0))))
(let (($x46 (and __VERIFIER_assert.exit_0 (and (<= |%_16_1_1| |%_15_0|) (>= |%_16_1_1| |%_15_0|)))))
(let (($x38 (=>  __VERIFIER_assert.exit_0 (and (and bb6_0 E0x45ff330) |%_14_0|))))
(let (($x30 (and (=>  bb6_0 (and (and bb7_0 E0x45fef70) |%_17_0|)) (=>  bb6_0 E0x45fef70))))
(let (($x47 (and $x30 (and $x38 (=>  __VERIFIER_assert.exit_0 E0x45ff330)) $x46)))
(and $x47 (and $x50 $x57 $x63 (= |%_15_0| (+ |%_16_1_0| 1.0)))))))))))) (cp-rel-bb7 |%_0_0| |%_1_0| |%_16_1_1| )))

(rule (=> (and (cp-rel-bb7 |%_0_0| |%_1_0| |%_16_1_0| ) (let (($x50 (=  |%_17_0| (< |%_16_1_0| 5.0))))
(and (and bb7_0 (not |%_17_0|)) $x50))) (cp-rel-__UFO__0 |%_0_0| |%_1_0| |%_16_1_0| )))

(rule (=> (and (cp-rel-__UFO__0 |%_0_0| |%_1_0| |%_16_0| ) __UFO__0_0) (cp-rel-__UFO__0 |%_0_0| |%_1_0| |%_16_0| )))

(rule (=> (and (cp-rel-bb7 |%_0_0| |%_1_0| |%_16_1_0| ) (let (($x63 (=  |%_14_0| (= |%_12_0| |%_13_0|))))
(let (($x57 (= |%tmp6_0| (+ 4.0 (* (~ 1.0) |%_16_1_0|)))))
(let (($x50 (=  |%_17_0| (< |%_16_1_0| 5.0))))
(let (($x30 (and (=>  bb6_0 (and (and bb7_0 E0x45fef70) |%_17_0|)) (=>  bb6_0 E0x45fef70))))
(and (and $x30 (and bb6_0 (not |%_14_0|))) (and $x50 $x57 $x63))))))) cp-rel-ERROR.i))



(query cp-rel-ERROR.i)
