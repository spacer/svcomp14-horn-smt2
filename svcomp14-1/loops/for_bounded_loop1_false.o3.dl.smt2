(declare-var %_0_0 Real)
(declare-var %y.0_1_0 Real)
(declare-var %x.0_1_0 Real)
(declare-var %_11_1_0 Real)
(declare-var entry_0 Bool)
(declare-var %_1_0 Bool)
(declare-var %_2_0 Real)
(declare-var %y.0_1_1 Real)
(declare-var %x.0_1_1 Real)
(declare-var %_11_1_1 Real)
(declare-var bb_0 Bool)
(declare-var bb1_0 Bool)
(declare-var E0x4b32dc0 Bool)
(declare-var %_12_0 Bool)
(declare-var __VERIFIER_assert.exit_0 Bool)
(declare-var E0x4b33c40 Bool)
(declare-var %_3_0 Bool)
(declare-var __VERIFIER_assert.exit4_0 Bool)
(declare-var E0x4b4c850 Bool)
(declare-var %_6_0 Bool)
(declare-var %_9_0 Bool)
(declare-var %_5_0 Real)
(declare-var %_8_0 Real)
(declare-var %_10_0 Real)
(declare-var %_4_0 Real)
(declare-var %_7_0 Real)
(declare-var bb2_0 Bool)
(declare-var E0x4b26ac0 Bool)
(declare-var %_13_0 Bool)
(declare-var %y.0_0 Real)
(declare-var %x.0_0 Real)
(declare-var %_11_0 Real)
(declare-var __UFO__0_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-UnifiedReturnBlock ())
(declare-rel cp-rel-bb1 (Real Real Real Real ))
(declare-rel cp-rel-__UFO__0 (Real Real Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (let (($x39 (and (=  |%_1_0| (> |%_0_0| 0.0)) (= |%_2_0| (ite  |%_1_0| 1.0 0.0)))))
(let (($x24 (and (and entry_0 |%_1_0|) (and (<= |%y.0_1_0| 0.0) (>= |%y.0_1_0| 0.0)))))
(let (($x32 (and (and $x24 (and (<= |%x.0_1_0| 0.0) (>= |%x.0_1_0| 0.0))) (and (<= |%_11_1_0| 0.0) (>= |%_11_1_0| 0.0)))))
(and $x32 $x39))))) (cp-rel-bb1 |%_0_0| |%y.0_1_0| |%x.0_1_0| |%_11_1_0| )))

(rule (=> (and (cp-rel-bb1 |%_0_0| |%y.0_1_0| |%x.0_1_0| |%_11_1_0| ) (let (($x119 (=  |%_9_0| (= |%_8_0| 0.0))))
(let (($x117 (= |%_8_0| (+ |%_5_0| |%_4_0|))))
(let (($x115 (= |%_7_0| (ite  |%_6_0| 1.0 0.0))))
(let (($x111 (=  |%_6_0| (not (= |%_5_0| 0.0)))))
(let (($x108 (= |%_4_0| (+ |%x.0_1_0| (* (~ 1.0) |%y.0_1_0|)))))
(let (($x102 (=  |%_3_0| (= |%x.0_1_0| |%y.0_1_0|))))
(let (($x100 (=  |%_12_0| (< |%_11_1_0| |%_0_0|))))
(let (($x122 (and $x100 $x102 $x108 $x111 $x115 $x117 $x119 (= |%_10_0| (+ |%_11_1_0| 1.0)))))
(let (($x85 (and __VERIFIER_assert.exit4_0 (and (<= |%y.0_1_1| |%_5_0|) (>= |%y.0_1_1| |%_5_0|)))))
(let (($x97 (and (and $x85 (and (<= |%x.0_1_1| |%_8_0|) (>= |%x.0_1_1| |%_8_0|))) (and (<= |%_11_1_1| |%_10_0|) (>= |%_11_1_1| |%_10_0|)))))
(let (($x76 (and (and __VERIFIER_assert.exit_0 E0x4b4c850) (and |%_6_0| (not |%_9_0|)))))
(let (($x79 (and (=>  __VERIFIER_assert.exit4_0 $x76) (=>  __VERIFIER_assert.exit4_0 E0x4b4c850))))
(let (($x63 (=>  __VERIFIER_assert.exit_0 (and (and bb_0 E0x4b33c40) |%_3_0|))))
(let (($x65 (and $x63 (=>  __VERIFIER_assert.exit_0 E0x4b33c40))))
(let (($x55 (and (=>  bb_0 (and (and bb1_0 E0x4b32dc0) |%_12_0|)) (=>  bb_0 E0x4b32dc0))))
(and (and $x55 $x65 $x79 $x97) $x122))))))))))))))))) (cp-rel-bb1 |%_0_0| |%y.0_1_1| |%x.0_1_1| |%_11_1_1| )))

(rule (=> (and (cp-rel-bb1 |%_0_0| |%y.0_1_0| |%x.0_1_0| |%_11_1_0| ) (let (($x138 (=  |%_13_0| (= |%x.0_1_0| 0.0))))
(let (($x100 (=  |%_12_0| (< |%_11_1_0| |%_0_0|))))
(let (($x132 (and (=>  bb2_0 (and (and bb1_0 E0x4b26ac0) (not |%_12_0|))) (=>  bb2_0 E0x4b26ac0))))
(and (and $x132 (and bb2_0 |%_13_0|)) (and $x100 $x138)))))) (cp-rel-__UFO__0 |%_0_0| |%y.0_1_0| |%x.0_1_0| |%_11_1_0| )))

(rule (=> (and (cp-rel-__UFO__0 |%_0_0| |%y.0_0| |%x.0_0| |%_11_0| ) __UFO__0_0) (cp-rel-__UFO__0 |%_0_0| |%y.0_0| |%x.0_0| |%_11_0| )))

(rule (=> (and (cp-rel-bb1 |%_0_0| |%y.0_1_0| |%x.0_1_0| |%_11_1_0| ) (let (($x119 (=  |%_9_0| (= |%_8_0| 0.0))))
(let (($x117 (= |%_8_0| (+ |%_5_0| |%_4_0|))))
(let (($x115 (= |%_7_0| (ite  |%_6_0| 1.0 0.0))))
(let (($x111 (=  |%_6_0| (not (= |%_5_0| 0.0)))))
(let (($x108 (= |%_4_0| (+ |%x.0_1_0| (* (~ 1.0) |%y.0_1_0|)))))
(let (($x138 (=  |%_13_0| (= |%x.0_1_0| 0.0))))
(let (($x102 (=  |%_3_0| (= |%x.0_1_0| |%y.0_1_0|))))
(let (($x100 (=  |%_12_0| (< |%_11_1_0| |%_0_0|))))
(let (($x152 (or (and bb2_0 (not |%_13_0|)) (and __VERIFIER_assert.exit_0 (and |%_6_0| |%_9_0|)) (and bb_0 (not |%_3_0|)))))
(let (($x63 (=>  __VERIFIER_assert.exit_0 (and (and bb_0 E0x4b33c40) |%_3_0|))))
(let (($x65 (and $x63 (=>  __VERIFIER_assert.exit_0 E0x4b33c40))))
(let (($x132 (and (=>  bb2_0 (and (and bb1_0 E0x4b26ac0) (not |%_12_0|))) (=>  bb2_0 E0x4b26ac0))))
(let (($x55 (and (=>  bb_0 (and (and bb1_0 E0x4b32dc0) |%_12_0|)) (=>  bb_0 E0x4b32dc0))))
(and (and $x55 $x132 $x65 $x152) (and $x100 $x102 $x138 $x108 $x111 $x115 $x117 $x119)))))))))))))))) cp-rel-UnifiedReturnBlock))



(query cp-rel-UnifiedReturnBlock)
