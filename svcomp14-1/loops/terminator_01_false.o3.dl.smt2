(declare-var %_0_0 Real)
(declare-var %tmp_0 Real)
(declare-var %indvar_1_0 Real)
(declare-var bb.nph_0 Bool)
(declare-var entry_0 Bool)
(declare-var E0x3c07110 Bool)
(declare-var %_1_0 Bool)
(declare-var %indvar_1_1 Real)
(declare-var bb_0 Bool)
(declare-var %phitmp_0 Bool)
(declare-var %indvar.next_0 Real)
(declare-var %tmp2_0 Real)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb2 ())
(declare-rel cp-rel-bb (Real Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (let (($x32 (=  |%_1_0| (> |%_0_0| 99.0))))
(let (($x28 (and bb.nph_0 (and (<= |%indvar_1_0| 0.0) (>= |%indvar_1_0| 0.0)))))
(let (($x24 (and (=>  bb.nph_0 (and (and entry_0 E0x3c07110) (not |%_1_0|))) (=>  bb.nph_0 E0x3c07110))))
(and (and $x24 $x28) (and $x32 (= |%tmp_0| (+ |%_0_0| 1.0)))))))) (cp-rel-bb |%_0_0| |%tmp_0| |%indvar_1_0| )))

(rule (=> (and (cp-rel-bb |%_0_0| |%tmp_0| |%indvar_1_0| ) (let (($x58 (and (= |%tmp2_0| (+ |%indvar_1_0| |%tmp_0|)) (=  |%phitmp_0| (> |%tmp2_0| 99.0)) (= |%indvar.next_0| (+ |%indvar_1_0| 1.0)))))
(let (($x48 (and (<= |%indvar_1_1| |%indvar.next_0|) (>= |%indvar_1_1| |%indvar.next_0|))))
(and (and (and bb_0 (not |%phitmp_0|)) $x48) $x58)))) (cp-rel-bb |%_0_0| |%tmp_0| |%indvar_1_1| )))

(rule (=> (and cp-rel-entry (let (($x32 (=  |%_1_0| (> |%_0_0| 99.0))))
(and (and entry_0 |%_1_0|) $x32))) cp-rel-bb2))

(rule (=> (and (cp-rel-bb |%_0_0| |%tmp_0| |%indvar_1_0| ) (let (($x58 (and (= |%tmp2_0| (+ |%indvar_1_0| |%tmp_0|)) (=  |%phitmp_0| (> |%tmp2_0| 99.0)) (= |%indvar.next_0| (+ |%indvar_1_0| 1.0)))))
(and (and bb_0 |%phitmp_0|) $x58))) cp-rel-bb2))



(query cp-rel-bb2)
