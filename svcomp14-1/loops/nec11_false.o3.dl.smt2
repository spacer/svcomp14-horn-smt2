(declare-var entry_0 Bool)
(declare-var %toBool2_0 Bool)
(declare-var %_0_0 Real)
(declare-var bb_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb4 ())
(declare-rel cp-rel-bb ())


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (let (($x13 (=  |%toBool2_0| (= |%_0_0| 0.0))))
(and (and entry_0 (not |%toBool2_0|)) $x13))) cp-rel-bb))

(rule (=> (and cp-rel-bb bb_0) cp-rel-bb))

(rule (=> (and cp-rel-entry (let (($x13 (=  |%toBool2_0| (= |%_0_0| 0.0))))
(and (and entry_0 |%toBool2_0|) $x13))) cp-rel-bb4))



(query cp-rel-bb4)
