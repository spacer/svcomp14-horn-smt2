(declare-var entry_0 Bool)
(declare-var %_3_0 Bool)
(declare-var %_2_0 Bool)
(declare-var %_1_0 Real)
(declare-var %_0_0 Real)
(declare-var %menor.0_0 Real)
(declare-var __UFO__0_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-ERROR.i ())
(declare-rel cp-rel-__UFO__0 ())


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (let (($x23 (and (=  |%_2_0| (> |%_1_0| |%_0_0|)) (= |%menor.0_0| (ite  |%_2_0| |%_0_0| |%_1_0|)) (=  |%_3_0| (> |%_1_0| |%menor.0_0|)))))
(and (and entry_0 |%_3_0|) $x23))) cp-rel-__UFO__0))

(rule (=> (and cp-rel-__UFO__0 __UFO__0_0) cp-rel-__UFO__0))

(rule (=> (and cp-rel-entry (let (($x23 (and (=  |%_2_0| (> |%_1_0| |%_0_0|)) (= |%menor.0_0| (ite  |%_2_0| |%_0_0| |%_1_0|)) (=  |%_3_0| (> |%_1_0| |%menor.0_0|)))))
(and (and entry_0 (not |%_3_0|)) $x23))) cp-rel-ERROR.i))



(query cp-rel-ERROR.i)
