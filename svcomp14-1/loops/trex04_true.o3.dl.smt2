(declare-var %_0_0 Real)
(declare-var %d.0._0 Real)
(declare-var %tmp5_0 Real)
(declare-var %indvar_1_0 Real)
(declare-var bb.nph_0 Bool)
(declare-var entry_0 Bool)
(declare-var E0x4c21740 Bool)
(declare-var %_8_0 Bool)
(declare-var %_3_0 Bool)
(declare-var %_1_0 Real)
(declare-var %d.0_0 Real)
(declare-var %_6_0 Real)
(declare-var %_7_0 Bool)
(declare-var %_5_0 Real)
(declare-var %indvar_1_1 Real)
(declare-var bb8_0 Bool)
(declare-var %_9_0 Bool)
(declare-var %indvar.next_0 Real)
(declare-var %tmp_0 Real)
(declare-var %tmp6_0 Real)
(declare-var bb10_0 Bool)
(declare-var E0x4c24140 Bool)
(declare-var %x.0.lcssa_0 Real)
(declare-var %_10_0 Bool)
(declare-var E0x4c3e2a0 Bool)
(declare-var __UFO__0_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-ERROR.i ())
(declare-rel cp-rel-bb8 (Real Real Real Real ))
(declare-rel cp-rel-__UFO__0 (Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (let (($x55 (=  |%_8_0| (> |%_0_0| 0.0))))
(let (($x53 (= |%d.0._0| (ite  |%_7_0| |%d.0_0| |%_6_0|))))
(let (($x51 (=  |%_7_0| (= |%_5_0| 0.0))))
(let (($x45 (= |%_6_0| (ite  |%_3_0| 0.0 (~ 1.0)))))
(let (($x40 (= |%d.0_0| (ite  |%_3_0| 1.0 0.0))))
(let (($x36 (=  |%_3_0| (= |%_1_0| 0.0))))
(let (($x59 (and $x36 $x40 $x45 $x51 $x53 $x55 (= |%tmp5_0| (+ |%_0_0| (* (~ 1.0) |%d.0._0|))))))
(let (($x29 (and bb.nph_0 (and (<= |%indvar_1_0| 0.0) (>= |%indvar_1_0| 0.0)))))
(let (($x25 (and (=>  bb.nph_0 (and (and entry_0 E0x4c21740) |%_8_0|)) (=>  bb.nph_0 E0x4c21740))))
(and (and $x25 $x29) $x59))))))))))) (cp-rel-bb8 |%_0_0| |%d.0._0| |%tmp5_0| |%indvar_1_0| )))

(rule (=> (and (cp-rel-bb8 |%_0_0| |%d.0._0| |%tmp5_0| |%indvar_1_0| ) (let (($x85 (= |%indvar.next_0| (+ |%indvar_1_0| 1.0))))
(let (($x83 (=  |%_9_0| (> |%tmp6_0| 0.0))))
(let (($x81 (= |%tmp6_0| (+ |%tmp5_0| (* (~ 1.0) |%tmp_0|)))))
(let (($x76 (= |%tmp_0| (* |%d.0._0| |%indvar_1_0|))))
(let (($x71 (and (<= |%indvar_1_1| |%indvar.next_0|) (>= |%indvar_1_1| |%indvar.next_0|))))
(and (and (and bb8_0 |%_9_0|) $x71) (and $x76 $x81 $x83 $x85)))))))) (cp-rel-bb8 |%_0_0| |%d.0._0| |%tmp5_0| |%indvar_1_1| )))

(rule (=> (and cp-rel-entry (let (($x108 (=  |%_10_0| (< |%x.0.lcssa_0| 1.0))))
(let (($x55 (=  |%_8_0| (> |%_0_0| 0.0))))
(let (($x53 (= |%d.0._0| (ite  |%_7_0| |%d.0_0| |%_6_0|))))
(let (($x51 (=  |%_7_0| (= |%_5_0| 0.0))))
(let (($x45 (= |%_6_0| (ite  |%_3_0| 0.0 (~ 1.0)))))
(let (($x40 (= |%d.0_0| (ite  |%_3_0| 1.0 0.0))))
(let (($x36 (=  |%_3_0| (= |%_1_0| 0.0))))
(let (($x109 (and $x36 $x40 $x45 $x51 $x53 $x55 $x108)))
(let (($x105 (and bb10_0 |%_10_0|)))
(let (($x99 (and (and (and entry_0 E0x4c24140) (not |%_8_0|)) (and (<= |%x.0.lcssa_0| |%_0_0|) (>= |%x.0.lcssa_0| |%_0_0|)))))
(let (($x102 (and (=>  bb10_0 $x99) (=>  bb10_0 E0x4c24140))))
(and (and $x102 $x105) $x109))))))))))))) (cp-rel-__UFO__0 |%_0_0| |%d.0._0| )))

(rule (=> (and (cp-rel-bb8 |%_0_0| |%d.0._0| |%tmp5_0| |%indvar_1_0| ) (let (($x108 (=  |%_10_0| (< |%x.0.lcssa_0| 1.0))))
(let (($x85 (= |%indvar.next_0| (+ |%indvar_1_0| 1.0))))
(let (($x83 (=  |%_9_0| (> |%tmp6_0| 0.0))))
(let (($x81 (= |%tmp6_0| (+ |%tmp5_0| (* (~ 1.0) |%tmp_0|)))))
(let (($x76 (= |%tmp_0| (* |%d.0._0| |%indvar_1_0|))))
(let (($x123 (and $x76 $x81 $x83 $x85 $x108)))
(let (($x105 (and bb10_0 |%_10_0|)))
(let (($x118 (and (and (and bb8_0 E0x4c3e2a0) (not |%_9_0|)) (and (<= |%x.0.lcssa_0| |%tmp6_0|) (>= |%x.0.lcssa_0| |%tmp6_0|)))))
(let (($x121 (and (=>  bb10_0 $x118) (=>  bb10_0 E0x4c3e2a0))))
(and (and $x121 $x105) $x123))))))))))) (cp-rel-__UFO__0 |%_0_0| |%d.0._0| )))

(rule (=> (and (cp-rel-__UFO__0 |%_0_0| |%d.0._0| ) __UFO__0_0) (cp-rel-__UFO__0 |%_0_0| |%d.0._0| )))

(rule (=> (and cp-rel-entry (let (($x108 (=  |%_10_0| (< |%x.0.lcssa_0| 1.0))))
(let (($x55 (=  |%_8_0| (> |%_0_0| 0.0))))
(let (($x53 (= |%d.0._0| (ite  |%_7_0| |%d.0_0| |%_6_0|))))
(let (($x51 (=  |%_7_0| (= |%_5_0| 0.0))))
(let (($x45 (= |%_6_0| (ite  |%_3_0| 0.0 (~ 1.0)))))
(let (($x40 (= |%d.0_0| (ite  |%_3_0| 1.0 0.0))))
(let (($x36 (=  |%_3_0| (= |%_1_0| 0.0))))
(let (($x109 (and $x36 $x40 $x45 $x51 $x53 $x55 $x108)))
(let (($x128 (and bb10_0 (not |%_10_0|))))
(let (($x99 (and (and (and entry_0 E0x4c24140) (not |%_8_0|)) (and (<= |%x.0.lcssa_0| |%_0_0|) (>= |%x.0.lcssa_0| |%_0_0|)))))
(let (($x102 (and (=>  bb10_0 $x99) (=>  bb10_0 E0x4c24140))))
(and (and $x102 $x128) $x109))))))))))))) cp-rel-ERROR.i))

(rule (=> (and (cp-rel-bb8 |%_0_0| |%d.0._0| |%tmp5_0| |%indvar_1_0| ) (let (($x108 (=  |%_10_0| (< |%x.0.lcssa_0| 1.0))))
(let (($x85 (= |%indvar.next_0| (+ |%indvar_1_0| 1.0))))
(let (($x83 (=  |%_9_0| (> |%tmp6_0| 0.0))))
(let (($x81 (= |%tmp6_0| (+ |%tmp5_0| (* (~ 1.0) |%tmp_0|)))))
(let (($x76 (= |%tmp_0| (* |%d.0._0| |%indvar_1_0|))))
(let (($x123 (and $x76 $x81 $x83 $x85 $x108)))
(let (($x128 (and bb10_0 (not |%_10_0|))))
(let (($x118 (and (and (and bb8_0 E0x4c3e2a0) (not |%_9_0|)) (and (<= |%x.0.lcssa_0| |%tmp6_0|) (>= |%x.0.lcssa_0| |%tmp6_0|)))))
(let (($x121 (and (=>  bb10_0 $x118) (=>  bb10_0 E0x4c3e2a0))))
(and (and $x121 $x128) $x123))))))))))) cp-rel-ERROR.i))



(query cp-rel-ERROR.i)
