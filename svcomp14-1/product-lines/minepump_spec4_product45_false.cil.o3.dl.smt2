(declare-var %pumpRunning.2_1_0 Real)
(declare-var %waterLevel.2_1_0 Real)
(declare-var %methaneLevelCritical.1_1_0 Real)
(declare-var entry_0 Bool)
(declare-var %pumpRunning.2_1_1 Real)
(declare-var %waterLevel.2_1_1 Real)
(declare-var %methaneLevelCritical.1_1_1 Real)
(declare-var bb2.i.i_0 Bool)
(declare-var bb1.i.i_0 Bool)
(declare-var E0x47a4420 Bool)
(declare-var %_1_0 Bool)
(declare-var bb3.i.i_0 Bool)
(declare-var E0x47a5e70 Bool)
(declare-var %waterLevel.1_0 Real)
(declare-var %waterLevel.0_0 Real)
(declare-var E0x47a6490 Bool)
(declare-var bb4.i.i_0 Bool)
(declare-var E0x47be770 Bool)
(declare-var %_4_0 Bool)
(declare-var bb5.i.i_0 Bool)
(declare-var E0x47bf840 Bool)
(declare-var %methaneLevelCritical.0_0 Real)
(declare-var %storemerge.i.i.i_0 Real)
(declare-var E0x47bfe20 Bool)
(declare-var bb.i.i.i.i.i_0 Bool)
(declare-var E0x47c1100 Bool)
(declare-var %_6_0 Bool)
(declare-var bb.i3.i.i.i_0 Bool)
(declare-var E0x47c1d30 Bool)
(declare-var bb1.i.i.i.i.i_0 Bool)
(declare-var E0x47c34f0 Bool)
(declare-var %_10_0 Bool)
(declare-var bb2.i.i.i.i_0 Bool)
(declare-var E0x47c42f0 Bool)
(declare-var %_9_0 Bool)
(declare-var bb3.i.i.i_0 Bool)
(declare-var E0x47c4e70 Bool)
(declare-var %waterLevel.4.reg2mem.0_0 Real)
(declare-var %pumpRunning.0_0 Real)
(declare-var %pumpRunning.5_0 Real)
(declare-var E0x47c5830 Bool)
(declare-var E0x47c0440 Bool)
(declare-var %waterLevel.3_0 Real)
(declare-var E0x47c67a0 Bool)
(declare-var %or.cond_0 Bool)
(declare-var %_0_0 Real)
(declare-var %not._0 Bool)
(declare-var %_2_0 Real)
(declare-var %_3_0 Real)
(declare-var %_5_0 Bool)
(declare-var %_7_0 Bool)
(declare-var %_8_0 Real)
(declare-var %_11_0 Bool)
(declare-var %.not_0 Bool)
(declare-var %_12_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb1.i.i.i.i ())
(declare-rel cp-rel-bb1.i.i (Real Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (let (($x26 (and (<= |%methaneLevelCritical.1_1_0| 0.0) (>= |%methaneLevelCritical.1_1_0| 0.0))))
(let (($x19 (and entry_0 (and (<= |%pumpRunning.2_1_0| 0.0) (>= |%pumpRunning.2_1_0| 0.0)))))
(let (($x23 (and $x19 (and (<= |%waterLevel.2_1_0| 1.0) (>= |%waterLevel.2_1_0| 1.0)))))
(and $x23 $x26))))) (cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )))

(rule (=> (and (cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| ) (let (($x282 (and (=  |%_1_0| (= |%_0_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_2_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_2_0| |%waterLevel.2_1_0|)) (=  |%_4_0| (= |%_3_0| 0.0)) (=  |%_5_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_5_0| 1.0 0.0)) (=  |%_6_0| (= |%pumpRunning.2_1_0| 0.0)) (=  |%_10_0| (> |%waterLevel.1_0| 1.0)) (=  |%_7_0| (> |%waterLevel.1_0| 0.0)) (= |%_8_0| (+ |%waterLevel.1_0| (~ 1.0))) (= |%waterLevel.3_0| (ite  |%_7_0| |%_8_0| |%waterLevel.1_0|)) (=  |%_9_0| (= |%methaneLevelCritical.0_0| 0.0)) (=  |%_11_0| (= |%methaneLevelCritical.0_0| 0.0)) (= |%pumpRunning.5_0| (ite  |%_11_0| 1.0 |%pumpRunning.2_1_0|)) (=  |%.not_0| (not (= |%waterLevel.4.reg2mem.0_0| 0.0))) (=  |%_12_0| (= |%pumpRunning.0_0| 0.0)) (=  |%or.cond_0| (or |%_12_0| |%.not_0|)))))
(let (($x221 (and (<= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|) (>= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|))))
(let (($x217 (and (<= |%waterLevel.2_1_1| |%waterLevel.4.reg2mem.0_0|) (>= |%waterLevel.2_1_1| |%waterLevel.4.reg2mem.0_0|))))
(let (($x213 (and (<= |%pumpRunning.2_1_1| |%pumpRunning.0_0|) (>= |%pumpRunning.2_1_1| |%pumpRunning.0_0|))))
(let (($x198 (not E0x47c0440)))
(let (($x197 (not E0x47c5830)))
(let (($x201 (not E0x47c4e70)))
(let (($x205 (or (and E0x47c4e70 $x197 $x198 (not E0x47c67a0)) (and E0x47c5830 $x201 $x198 (not E0x47c67a0)) (and E0x47c0440 $x201 $x197 (not E0x47c67a0)) (and E0x47c67a0 $x201 $x197 $x198))))
(let (($x183 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.3_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.3_0|))))
(let (($x194 (and (and (and (and bb.i3.i.i.i_0 E0x47c67a0) (not |%_9_0|)) $x183) (and (<= |%pumpRunning.0_0| 0.0) (>= |%pumpRunning.0_0| 0.0)))))
(let (($x175 (and (<= |%pumpRunning.0_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.0_0| |%pumpRunning.2_1_0|))))
(let (($x158 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|))))
(let (($x176 (and (and (and (and bb.i.i.i.i.i_0 E0x47c5830) (not |%_10_0|)) $x158) $x175)))
(let (($x166 (and (<= |%pumpRunning.0_0| |%pumpRunning.5_0|) (>= |%pumpRunning.0_0| |%pumpRunning.5_0|))))
(let (($x195 (or (and (and (and bb1.i.i.i.i.i_0 E0x47c4e70) $x158) $x166) $x176 (and (and (and bb2.i.i.i.i_0 E0x47c0440) $x183) $x175) $x194)))
(let (($x207 (and (=>  bb3.i.i.i_0 $x195) (=>  bb3.i.i.i_0 $x205))))
(let (($x147 (=>  bb2.i.i.i.i_0 (and (and bb.i3.i.i.i_0 E0x47c42f0) |%_9_0|))))
(let (($x149 (and $x147 (=>  bb2.i.i.i.i_0 E0x47c42f0))))
(let (($x137 (=>  bb1.i.i.i.i.i_0 (and (and bb.i.i.i.i.i_0 E0x47c34f0) |%_10_0|))))
(let (($x139 (and $x137 (=>  bb1.i.i.i.i.i_0 E0x47c34f0))))
(let (($x127 (=>  bb.i3.i.i.i_0 (and (and bb5.i.i_0 E0x47c1d30) (not |%_6_0|)))))
(let (($x129 (and $x127 (=>  bb.i3.i.i.i_0 E0x47c1d30))))
(let (($x120 (and (=>  bb.i.i.i.i.i_0 (and (and bb5.i.i_0 E0x47c1100) |%_6_0|)) (=>  bb.i.i.i.i.i_0 E0x47c1100))))
(let (($x108 (or (and E0x47bf840 (not E0x47bfe20)) (and E0x47bfe20 (not E0x47bf840)))))
(let (($x100 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x93 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x102 (or (and (and bb4.i.i_0 E0x47bf840) $x93) (and (and (and bb3.i.i_0 E0x47bfe20) |%_4_0|) $x100))))
(let (($x110 (and (=>  bb5.i.i_0 $x102) (=>  bb5.i.i_0 $x108))))
(let (($x82 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x47be770) (not |%_4_0|))) (=>  bb4.i.i_0 E0x47be770))))
(let (($x69 (or (and E0x47a5e70 (not E0x47a6490)) (and E0x47a6490 (not E0x47a5e70)))))
(let (($x61 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x54 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x63 (or (and (and bb2.i.i_0 E0x47a5e70) $x54) (and (and (and bb1.i.i_0 E0x47a6490) |%_1_0|) $x61))))
(let (($x71 (and (=>  bb3.i.i_0 $x63) (=>  bb3.i.i_0 $x69))))
(let (($x43 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x47a4420) (not |%_1_0|))) (=>  bb2.i.i_0 E0x47a4420))))
(let (($x223 (and $x43 $x71 $x82 $x110 $x120 $x129 $x139 $x149 $x207 (and (and (and (and bb3.i.i.i_0 |%or.cond_0|) $x213) $x217) $x221))))
(and $x223 $x282)))))))))))))))))))))))))))))))))))))) (cp-rel-bb1.i.i |%pumpRunning.2_1_1| |%waterLevel.2_1_1| |%methaneLevelCritical.1_1_1| )))

(rule (=> (and (cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| ) (let (($x282 (and (=  |%_1_0| (= |%_0_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_2_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_2_0| |%waterLevel.2_1_0|)) (=  |%_4_0| (= |%_3_0| 0.0)) (=  |%_5_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_5_0| 1.0 0.0)) (=  |%_6_0| (= |%pumpRunning.2_1_0| 0.0)) (=  |%_10_0| (> |%waterLevel.1_0| 1.0)) (=  |%_7_0| (> |%waterLevel.1_0| 0.0)) (= |%_8_0| (+ |%waterLevel.1_0| (~ 1.0))) (= |%waterLevel.3_0| (ite  |%_7_0| |%_8_0| |%waterLevel.1_0|)) (=  |%_9_0| (= |%methaneLevelCritical.0_0| 0.0)) (=  |%_11_0| (= |%methaneLevelCritical.0_0| 0.0)) (= |%pumpRunning.5_0| (ite  |%_11_0| 1.0 |%pumpRunning.2_1_0|)) (=  |%.not_0| (not (= |%waterLevel.4.reg2mem.0_0| 0.0))) (=  |%_12_0| (= |%pumpRunning.0_0| 0.0)) (=  |%or.cond_0| (or |%_12_0| |%.not_0|)))))
(let (($x198 (not E0x47c0440)))
(let (($x197 (not E0x47c5830)))
(let (($x201 (not E0x47c4e70)))
(let (($x205 (or (and E0x47c4e70 $x197 $x198 (not E0x47c67a0)) (and E0x47c5830 $x201 $x198 (not E0x47c67a0)) (and E0x47c0440 $x201 $x197 (not E0x47c67a0)) (and E0x47c67a0 $x201 $x197 $x198))))
(let (($x183 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.3_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.3_0|))))
(let (($x194 (and (and (and (and bb.i3.i.i.i_0 E0x47c67a0) (not |%_9_0|)) $x183) (and (<= |%pumpRunning.0_0| 0.0) (>= |%pumpRunning.0_0| 0.0)))))
(let (($x175 (and (<= |%pumpRunning.0_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.0_0| |%pumpRunning.2_1_0|))))
(let (($x158 (and (<= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|) (>= |%waterLevel.4.reg2mem.0_0| |%waterLevel.1_0|))))
(let (($x176 (and (and (and (and bb.i.i.i.i.i_0 E0x47c5830) (not |%_10_0|)) $x158) $x175)))
(let (($x166 (and (<= |%pumpRunning.0_0| |%pumpRunning.5_0|) (>= |%pumpRunning.0_0| |%pumpRunning.5_0|))))
(let (($x195 (or (and (and (and bb1.i.i.i.i.i_0 E0x47c4e70) $x158) $x166) $x176 (and (and (and bb2.i.i.i.i_0 E0x47c0440) $x183) $x175) $x194)))
(let (($x207 (and (=>  bb3.i.i.i_0 $x195) (=>  bb3.i.i.i_0 $x205))))
(let (($x147 (=>  bb2.i.i.i.i_0 (and (and bb.i3.i.i.i_0 E0x47c42f0) |%_9_0|))))
(let (($x149 (and $x147 (=>  bb2.i.i.i.i_0 E0x47c42f0))))
(let (($x137 (=>  bb1.i.i.i.i.i_0 (and (and bb.i.i.i.i.i_0 E0x47c34f0) |%_10_0|))))
(let (($x139 (and $x137 (=>  bb1.i.i.i.i.i_0 E0x47c34f0))))
(let (($x127 (=>  bb.i3.i.i.i_0 (and (and bb5.i.i_0 E0x47c1d30) (not |%_6_0|)))))
(let (($x129 (and $x127 (=>  bb.i3.i.i.i_0 E0x47c1d30))))
(let (($x120 (and (=>  bb.i.i.i.i.i_0 (and (and bb5.i.i_0 E0x47c1100) |%_6_0|)) (=>  bb.i.i.i.i.i_0 E0x47c1100))))
(let (($x108 (or (and E0x47bf840 (not E0x47bfe20)) (and E0x47bfe20 (not E0x47bf840)))))
(let (($x100 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x93 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x102 (or (and (and bb4.i.i_0 E0x47bf840) $x93) (and (and (and bb3.i.i_0 E0x47bfe20) |%_4_0|) $x100))))
(let (($x110 (and (=>  bb5.i.i_0 $x102) (=>  bb5.i.i_0 $x108))))
(let (($x82 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x47be770) (not |%_4_0|))) (=>  bb4.i.i_0 E0x47be770))))
(let (($x69 (or (and E0x47a5e70 (not E0x47a6490)) (and E0x47a6490 (not E0x47a5e70)))))
(let (($x61 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x54 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x63 (or (and (and bb2.i.i_0 E0x47a5e70) $x54) (and (and (and bb1.i.i_0 E0x47a6490) |%_1_0|) $x61))))
(let (($x71 (and (=>  bb3.i.i_0 $x63) (=>  bb3.i.i_0 $x69))))
(let (($x43 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x47a4420) (not |%_1_0|))) (=>  bb2.i.i_0 E0x47a4420))))
(let (($x286 (and $x43 $x71 $x82 $x110 $x120 $x129 $x139 $x149 $x207 (and bb3.i.i.i_0 (not |%or.cond_0|)))))
(and $x286 $x282))))))))))))))))))))))))))))))))))) cp-rel-bb1.i.i.i.i))



(query cp-rel-bb1.i.i.i.i)
