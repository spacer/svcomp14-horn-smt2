(declare-var %pumpRunning.2_1_0 Real)
(declare-var %systemActive.1_1_0 Real)
(declare-var %waterLevel.2_1_0 Real)
(declare-var %methaneLevelCritical.1_1_0 Real)
(declare-var entry_0 Bool)
(declare-var %pumpRunning.2_1_1 Real)
(declare-var %systemActive.1_1_1 Real)
(declare-var %waterLevel.2_1_1 Real)
(declare-var %methaneLevelCritical.1_1_1 Real)
(declare-var bb2.i.i_0 Bool)
(declare-var bb1.i.i_0 Bool)
(declare-var E0x3ccc8a0 Bool)
(declare-var %_1_0 Bool)
(declare-var bb3.i.i_0 Bool)
(declare-var E0x3cce3f0 Bool)
(declare-var %waterLevel.1_0 Real)
(declare-var %waterLevel.0_0 Real)
(declare-var E0x3ce5670 Bool)
(declare-var bb4.i.i_0 Bool)
(declare-var E0x3ce6bf0 Bool)
(declare-var %_4_0 Bool)
(declare-var bb5.i.i_0 Bool)
(declare-var E0x3ce7cc0 Bool)
(declare-var %methaneLevelCritical.0_0 Real)
(declare-var %storemerge.i.i.i_0 Real)
(declare-var E0x3ce82a0 Bool)
(declare-var bb6.i.i_0 Bool)
(declare-var E0x3ce97c0 Bool)
(declare-var %_7_0 Bool)
(declare-var bb8.i.i_0 Bool)
(declare-var E0x3cea5f0 Bool)
(declare-var %_9_0 Bool)
(declare-var E0x3cea930 Bool)
(declare-var bb.i1.i.i_0 Bool)
(declare-var E0x3ceb980 Bool)
(declare-var %_10_0 Bool)
(declare-var bb1.i.i.i_0 Bool)
(declare-var E0x3ced300 Bool)
(declare-var %pumpRunning.1.reg2mem.0_0 Real)
(declare-var %systemActive.0.reg2mem.0_0 Real)
(declare-var %waterLevel.4_0 Real)
(declare-var %waterLevel.3_0 Real)
(declare-var E0x3ce85c0 Bool)
(declare-var E0x3cee970 Bool)
(declare-var bb1.i4.i.i.i_0 Bool)
(declare-var E0x3cf1310 Bool)
(declare-var %or.cond3_0 Bool)
(declare-var bb3.i.i.i_0 Bool)
(declare-var E0x3cf23a0 Bool)
(declare-var %pumpRunning.5_0 Real)
(declare-var %pumpRunning.3_0 Real)
(declare-var E0x3ce9d90 Bool)
(declare-var %or.cond_0 Bool)
(declare-var %_0_0 Real)
(declare-var %not._0 Bool)
(declare-var %_2_0 Real)
(declare-var %_3_0 Real)
(declare-var %_5_0 Bool)
(declare-var %_6_0 Real)
(declare-var %_8_0 Real)
(declare-var %_11_0 Bool)
(declare-var %_12_0 Real)
(declare-var %.not1_0 Bool)
(declare-var %_13_0 Bool)
(declare-var %_14_0 Bool)
(declare-var %or.cond2_0 Bool)
(declare-var %_15_0 Bool)
(declare-var %.not_0 Bool)
(declare-var %_16_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb1.i.i.i.i ())
(declare-rel cp-rel-bb1.i.i (Real Real Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (let (($x33 (and (<= |%methaneLevelCritical.1_1_0| 0.0) (>= |%methaneLevelCritical.1_1_0| 0.0))))
(let (($x22 (and entry_0 (and (<= |%pumpRunning.2_1_0| 0.0) (>= |%pumpRunning.2_1_0| 0.0)))))
(let (($x26 (and $x22 (and (<= |%systemActive.1_1_0| 1.0) (>= |%systemActive.1_1_0| 1.0)))))
(let (($x30 (and $x26 (and (<= |%waterLevel.2_1_0| 1.0) (>= |%waterLevel.2_1_0| 1.0)))))
(and $x30 $x33)))))) (cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| )))

(rule (=> (and (cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| ) (let (($x358 (and (=  |%_1_0| (= |%_0_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_2_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_2_0| |%waterLevel.2_1_0|)) (=  |%_4_0| (= |%_3_0| 0.0)) (=  |%_5_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_5_0| 1.0 0.0)) (=  |%_7_0| (= |%_6_0| 0.0)) (=  |%_9_0| (= |%_8_0| 0.0)) (=  |%_10_0| (= |%pumpRunning.2_1_0| 0.0)) (=  |%_11_0| (> |%waterLevel.1_0| 0.0)) (= |%_12_0| (+ |%waterLevel.1_0| (~ 1.0))) (= |%waterLevel.3_0| (ite  |%_11_0| |%_12_0| |%waterLevel.1_0|)) (=  |%.not1_0| (not (= |%systemActive.0.reg2mem.0_0| 0.0))) (=  |%_13_0| (= |%pumpRunning.1.reg2mem.0_0| 0.0)) (=  |%_14_0| (> |%waterLevel.4_0| 1.0)) (=  |%or.cond2_0| (and |%.not1_0| |%_13_0|)) (=  |%or.cond3_0| (and |%or.cond2_0| |%_14_0|)) (=  |%_15_0| (= |%methaneLevelCritical.0_0| 0.0)) (= |%pumpRunning.3_0| (ite  |%_15_0| 1.0 |%pumpRunning.1.reg2mem.0_0|)) (=  |%.not_0| (not (= |%waterLevel.4_0| 0.0))) (=  |%_16_0| (= |%pumpRunning.5_0| 0.0)) (=  |%or.cond_0| (or |%_16_0| |%.not_0|)))))
(let (($x273 (and (<= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|) (>= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|))))
(let (($x269 (and (<= |%waterLevel.2_1_1| |%waterLevel.4_0|) (>= |%waterLevel.2_1_1| |%waterLevel.4_0|))))
(let (($x265 (and (<= |%systemActive.1_1_1| |%systemActive.0.reg2mem.0_0|) (>= |%systemActive.1_1_1| |%systemActive.0.reg2mem.0_0|))))
(let (($x261 (and (<= |%pumpRunning.2_1_1| |%pumpRunning.5_0|) (>= |%pumpRunning.2_1_1| |%pumpRunning.5_0|))))
(let (($x274 (and (and (and (and (and bb3.i.i.i_0 |%or.cond_0|) $x261) $x265) $x269) $x273)))
(let (($x253 (or (and E0x3cf23a0 (not E0x3ce9d90)) (and E0x3ce9d90 (not E0x3cf23a0)))))
(let (($x245 (and (<= |%pumpRunning.5_0| |%pumpRunning.1.reg2mem.0_0|) (>= |%pumpRunning.5_0| |%pumpRunning.1.reg2mem.0_0|))))
(let (($x237 (and (<= |%pumpRunning.5_0| |%pumpRunning.3_0|) (>= |%pumpRunning.5_0| |%pumpRunning.3_0|))))
(let (($x247 (or (and (and bb1.i4.i.i.i_0 E0x3cf23a0) $x237) (and (and (and bb1.i.i.i_0 E0x3ce9d90) (not |%or.cond3_0|)) $x245))))
(let (($x255 (and (=>  bb3.i.i.i_0 $x247) (=>  bb3.i.i.i_0 $x253))))
(let (($x224 (=>  bb1.i4.i.i.i_0 (and (and bb1.i.i.i_0 E0x3cf1310) |%or.cond3_0|))))
(let (($x226 (and $x224 (=>  bb1.i4.i.i.i_0 E0x3cf1310))))
(let (($x214 (or (and E0x3ced300 (not E0x3ce85c0) (not E0x3cee970)) (and E0x3ce85c0 (not E0x3ced300) (not E0x3cee970)) (and E0x3cee970 (not E0x3ced300) (not E0x3ce85c0)))))
(let (($x191 (and (<= |%waterLevel.4_0| |%waterLevel.1_0|) (>= |%waterLevel.4_0| |%waterLevel.1_0|))))
(let (($x203 (and (<= |%systemActive.0.reg2mem.0_0| 0.0) (>= |%systemActive.0.reg2mem.0_0| 0.0))))
(let (($x199 (and (<= |%pumpRunning.1.reg2mem.0_0| 0.0) (>= |%pumpRunning.1.reg2mem.0_0| 0.0))))
(let (($x204 (and (and (and (and bb6.i.i_0 E0x3cee970) (not |%_9_0|)) $x199) $x203)))
(let (($x174 (and (<= |%systemActive.0.reg2mem.0_0| |%systemActive.1_1_0|) (>= |%systemActive.0.reg2mem.0_0| |%systemActive.1_1_0|))))
(let (($x168 (and (<= |%pumpRunning.1.reg2mem.0_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.1.reg2mem.0_0| |%pumpRunning.2_1_0|))))
(let (($x192 (and (and (and (and (and bb8.i.i_0 E0x3ce85c0) |%_10_0|) $x168) $x174) $x191)))
(let (($x182 (and (<= |%waterLevel.4_0| |%waterLevel.3_0|) (>= |%waterLevel.4_0| |%waterLevel.3_0|))))
(let (($x206 (or (and (and (and (and bb.i1.i.i_0 E0x3ced300) $x168) $x174) $x182) $x192 (and $x204 $x191))))
(let (($x216 (and (=>  bb1.i.i.i_0 $x206) (=>  bb1.i.i.i_0 $x214))))
(let (($x157 (=>  bb.i1.i.i_0 (and (and bb8.i.i_0 E0x3ceb980) (not |%_10_0|)))))
(let (($x159 (and $x157 (=>  bb.i1.i.i_0 E0x3ceb980))))
(let (($x146 (or (and E0x3cea5f0 (not E0x3cea930)) (and E0x3cea930 (not E0x3cea5f0)))))
(let (($x140 (or (and (and bb6.i.i_0 E0x3cea5f0) |%_9_0|) (and (and bb5.i.i_0 E0x3cea930) (not |%_7_0|)))))
(let (($x148 (and (=>  bb8.i.i_0 $x140) (=>  bb8.i.i_0 $x146))))
(let (($x128 (and (=>  bb6.i.i_0 (and (and bb5.i.i_0 E0x3ce97c0) |%_7_0|)) (=>  bb6.i.i_0 E0x3ce97c0))))
(let (($x116 (or (and E0x3ce7cc0 (not E0x3ce82a0)) (and E0x3ce82a0 (not E0x3ce7cc0)))))
(let (($x108 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x101 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x110 (or (and (and bb4.i.i_0 E0x3ce7cc0) $x101) (and (and (and bb3.i.i_0 E0x3ce82a0) |%_4_0|) $x108))))
(let (($x118 (and (=>  bb5.i.i_0 $x110) (=>  bb5.i.i_0 $x116))))
(let (($x90 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x3ce6bf0) (not |%_4_0|))) (=>  bb4.i.i_0 E0x3ce6bf0))))
(let (($x77 (or (and E0x3cce3f0 (not E0x3ce5670)) (and E0x3ce5670 (not E0x3cce3f0)))))
(let (($x69 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x62 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x71 (or (and (and bb2.i.i_0 E0x3cce3f0) $x62) (and (and (and bb1.i.i_0 E0x3ce5670) |%_1_0|) $x69))))
(let (($x79 (and (=>  bb3.i.i_0 $x71) (=>  bb3.i.i_0 $x77))))
(let (($x51 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x3ccc8a0) (not |%_1_0|))) (=>  bb2.i.i_0 E0x3ccc8a0))))
(and (and $x51 $x79 $x90 $x118 $x128 $x148 $x159 $x216 $x226 $x255 $x274) $x358)))))))))))))))))))))))))))))))))))))))))))) (cp-rel-bb1.i.i |%pumpRunning.2_1_1| |%systemActive.1_1_1| |%waterLevel.2_1_1| |%methaneLevelCritical.1_1_1| )))

(rule (=> (and (cp-rel-bb1.i.i |%pumpRunning.2_1_0| |%systemActive.1_1_0| |%waterLevel.2_1_0| |%methaneLevelCritical.1_1_0| ) (let (($x358 (and (=  |%_1_0| (= |%_0_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_2_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_2_0| |%waterLevel.2_1_0|)) (=  |%_4_0| (= |%_3_0| 0.0)) (=  |%_5_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_5_0| 1.0 0.0)) (=  |%_7_0| (= |%_6_0| 0.0)) (=  |%_9_0| (= |%_8_0| 0.0)) (=  |%_10_0| (= |%pumpRunning.2_1_0| 0.0)) (=  |%_11_0| (> |%waterLevel.1_0| 0.0)) (= |%_12_0| (+ |%waterLevel.1_0| (~ 1.0))) (= |%waterLevel.3_0| (ite  |%_11_0| |%_12_0| |%waterLevel.1_0|)) (=  |%.not1_0| (not (= |%systemActive.0.reg2mem.0_0| 0.0))) (=  |%_13_0| (= |%pumpRunning.1.reg2mem.0_0| 0.0)) (=  |%_14_0| (> |%waterLevel.4_0| 1.0)) (=  |%or.cond2_0| (and |%.not1_0| |%_13_0|)) (=  |%or.cond3_0| (and |%or.cond2_0| |%_14_0|)) (=  |%_15_0| (= |%methaneLevelCritical.0_0| 0.0)) (= |%pumpRunning.3_0| (ite  |%_15_0| 1.0 |%pumpRunning.1.reg2mem.0_0|)) (=  |%.not_0| (not (= |%waterLevel.4_0| 0.0))) (=  |%_16_0| (= |%pumpRunning.5_0| 0.0)) (=  |%or.cond_0| (or |%_16_0| |%.not_0|)))))
(let (($x253 (or (and E0x3cf23a0 (not E0x3ce9d90)) (and E0x3ce9d90 (not E0x3cf23a0)))))
(let (($x245 (and (<= |%pumpRunning.5_0| |%pumpRunning.1.reg2mem.0_0|) (>= |%pumpRunning.5_0| |%pumpRunning.1.reg2mem.0_0|))))
(let (($x237 (and (<= |%pumpRunning.5_0| |%pumpRunning.3_0|) (>= |%pumpRunning.5_0| |%pumpRunning.3_0|))))
(let (($x247 (or (and (and bb1.i4.i.i.i_0 E0x3cf23a0) $x237) (and (and (and bb1.i.i.i_0 E0x3ce9d90) (not |%or.cond3_0|)) $x245))))
(let (($x255 (and (=>  bb3.i.i.i_0 $x247) (=>  bb3.i.i.i_0 $x253))))
(let (($x224 (=>  bb1.i4.i.i.i_0 (and (and bb1.i.i.i_0 E0x3cf1310) |%or.cond3_0|))))
(let (($x226 (and $x224 (=>  bb1.i4.i.i.i_0 E0x3cf1310))))
(let (($x214 (or (and E0x3ced300 (not E0x3ce85c0) (not E0x3cee970)) (and E0x3ce85c0 (not E0x3ced300) (not E0x3cee970)) (and E0x3cee970 (not E0x3ced300) (not E0x3ce85c0)))))
(let (($x191 (and (<= |%waterLevel.4_0| |%waterLevel.1_0|) (>= |%waterLevel.4_0| |%waterLevel.1_0|))))
(let (($x203 (and (<= |%systemActive.0.reg2mem.0_0| 0.0) (>= |%systemActive.0.reg2mem.0_0| 0.0))))
(let (($x199 (and (<= |%pumpRunning.1.reg2mem.0_0| 0.0) (>= |%pumpRunning.1.reg2mem.0_0| 0.0))))
(let (($x204 (and (and (and (and bb6.i.i_0 E0x3cee970) (not |%_9_0|)) $x199) $x203)))
(let (($x174 (and (<= |%systemActive.0.reg2mem.0_0| |%systemActive.1_1_0|) (>= |%systemActive.0.reg2mem.0_0| |%systemActive.1_1_0|))))
(let (($x168 (and (<= |%pumpRunning.1.reg2mem.0_0| |%pumpRunning.2_1_0|) (>= |%pumpRunning.1.reg2mem.0_0| |%pumpRunning.2_1_0|))))
(let (($x192 (and (and (and (and (and bb8.i.i_0 E0x3ce85c0) |%_10_0|) $x168) $x174) $x191)))
(let (($x182 (and (<= |%waterLevel.4_0| |%waterLevel.3_0|) (>= |%waterLevel.4_0| |%waterLevel.3_0|))))
(let (($x206 (or (and (and (and (and bb.i1.i.i_0 E0x3ced300) $x168) $x174) $x182) $x192 (and $x204 $x191))))
(let (($x216 (and (=>  bb1.i.i.i_0 $x206) (=>  bb1.i.i.i_0 $x214))))
(let (($x157 (=>  bb.i1.i.i_0 (and (and bb8.i.i_0 E0x3ceb980) (not |%_10_0|)))))
(let (($x159 (and $x157 (=>  bb.i1.i.i_0 E0x3ceb980))))
(let (($x146 (or (and E0x3cea5f0 (not E0x3cea930)) (and E0x3cea930 (not E0x3cea5f0)))))
(let (($x140 (or (and (and bb6.i.i_0 E0x3cea5f0) |%_9_0|) (and (and bb5.i.i_0 E0x3cea930) (not |%_7_0|)))))
(let (($x148 (and (=>  bb8.i.i_0 $x140) (=>  bb8.i.i_0 $x146))))
(let (($x128 (and (=>  bb6.i.i_0 (and (and bb5.i.i_0 E0x3ce97c0) |%_7_0|)) (=>  bb6.i.i_0 E0x3ce97c0))))
(let (($x116 (or (and E0x3ce7cc0 (not E0x3ce82a0)) (and E0x3ce82a0 (not E0x3ce7cc0)))))
(let (($x108 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x101 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x110 (or (and (and bb4.i.i_0 E0x3ce7cc0) $x101) (and (and (and bb3.i.i_0 E0x3ce82a0) |%_4_0|) $x108))))
(let (($x118 (and (=>  bb5.i.i_0 $x110) (=>  bb5.i.i_0 $x116))))
(let (($x90 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x3ce6bf0) (not |%_4_0|))) (=>  bb4.i.i_0 E0x3ce6bf0))))
(let (($x77 (or (and E0x3cce3f0 (not E0x3ce5670)) (and E0x3ce5670 (not E0x3cce3f0)))))
(let (($x69 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x62 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x71 (or (and (and bb2.i.i_0 E0x3cce3f0) $x62) (and (and (and bb1.i.i_0 E0x3ce5670) |%_1_0|) $x69))))
(let (($x79 (and (=>  bb3.i.i_0 $x71) (=>  bb3.i.i_0 $x77))))
(let (($x51 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x3ccc8a0) (not |%_1_0|))) (=>  bb2.i.i_0 E0x3ccc8a0))))
(let (($x362 (and $x51 $x79 $x90 $x118 $x128 $x148 $x159 $x216 $x226 $x255 (and bb3.i.i.i_0 (not |%or.cond_0|)))))
(and $x362 $x358)))))))))))))))))))))))))))))))))))))))) cp-rel-bb1.i.i.i.i))



(query cp-rel-bb1.i.i.i.i)
