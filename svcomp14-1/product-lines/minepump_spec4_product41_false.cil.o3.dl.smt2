(declare-var %methaneLevelCritical.1_1_0 Real)
(declare-var %pumpRunning.3_1_0 Real)
(declare-var %waterLevel.2_1_0 Real)
(declare-var entry_0 Bool)
(declare-var %methaneLevelCritical.1_1_1 Real)
(declare-var %pumpRunning.3_1_1 Real)
(declare-var %waterLevel.2_1_1 Real)
(declare-var bb2.i.i_0 Bool)
(declare-var bb1.i.i_0 Bool)
(declare-var E0x36e3180 Bool)
(declare-var %_1_0 Bool)
(declare-var bb3.i.i_0 Bool)
(declare-var E0x36e4bd0 Bool)
(declare-var %waterLevel.1_0 Real)
(declare-var %waterLevel.0_0 Real)
(declare-var E0x36e51f0 Bool)
(declare-var bb4.i.i_0 Bool)
(declare-var E0x36fd4d0 Bool)
(declare-var %_4_0 Bool)
(declare-var bb5.i.i_0 Bool)
(declare-var E0x36fe5a0 Bool)
(declare-var %methaneLevelCritical.0_0 Real)
(declare-var %storemerge.i.i.i_0 Real)
(declare-var E0x36feb80 Bool)
(declare-var bb.i1.i.i_0 Bool)
(declare-var E0x36ffe60 Bool)
(declare-var %_6_0 Bool)
(declare-var bb2.i.i.i_0 Bool)
(declare-var E0x37015b0 Bool)
(declare-var %waterLevel.4_0 Real)
(declare-var %waterLevel.3_0 Real)
(declare-var E0x3701b30 Bool)
(declare-var bb1.i4.i.i.i_0 Bool)
(declare-var E0x3703180 Bool)
(declare-var %or.cond1_0 Bool)
(declare-var bb3.i.i.i_0 Bool)
(declare-var E0x37041c0 Bool)
(declare-var %pumpRunning.1_0 Real)
(declare-var %pumpRunning.0_0 Real)
(declare-var E0x3704880 Bool)
(declare-var %or.cond_0 Bool)
(declare-var %_0_0 Real)
(declare-var %not._0 Bool)
(declare-var %_2_0 Real)
(declare-var %_3_0 Real)
(declare-var %_5_0 Bool)
(declare-var %_7_0 Bool)
(declare-var %_8_0 Real)
(declare-var %_9_0 Bool)
(declare-var %_10_0 Bool)
(declare-var %.not_0 Bool)
(declare-var %_11_0 Bool)
(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb1.i.i.i.i ())
(declare-rel cp-rel-bb1.i.i (Real Real Real ))


(rule cp-rel-entry)

(rule (=> (and cp-rel-entry (let (($x18 (and (<= |%methaneLevelCritical.1_1_0| 0.0) (>= |%methaneLevelCritical.1_1_0| 0.0))))
(let (($x23 (and (and entry_0 $x18) (and (<= |%pumpRunning.3_1_0| 0.0) (>= |%pumpRunning.3_1_0| 0.0)))))
(and $x23 (and (<= |%waterLevel.2_1_0| 1.0) (>= |%waterLevel.2_1_0| 1.0)))))) (cp-rel-bb1.i.i |%methaneLevelCritical.1_1_0| |%pumpRunning.3_1_0| |%waterLevel.2_1_0| )))

(rule (=> (and (cp-rel-bb1.i.i |%methaneLevelCritical.1_1_0| |%pumpRunning.3_1_0| |%waterLevel.2_1_0| ) (let (($x266 (and (=  |%_1_0| (= |%_0_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_2_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_2_0| |%waterLevel.2_1_0|)) (=  |%_4_0| (= |%_3_0| 0.0)) (=  |%_5_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_5_0| 1.0 0.0)) (=  |%_6_0| (= |%pumpRunning.3_1_0| 0.0)) (=  |%_7_0| (> |%waterLevel.1_0| 0.0)) (= |%_8_0| (+ |%waterLevel.1_0| (~ 1.0))) (= |%waterLevel.3_0| (ite  |%_7_0| |%_8_0| |%waterLevel.1_0|)) (=  |%_9_0| (> |%waterLevel.4_0| 1.0)) (=  |%or.cond1_0| (and |%_9_0| |%_6_0|)) (=  |%_10_0| (= |%methaneLevelCritical.0_0| 0.0)) (= |%pumpRunning.0_0| (ite  |%_10_0| 1.0 |%pumpRunning.3_1_0|)) (=  |%.not_0| (not (= |%waterLevel.4_0| 0.0))) (=  |%_11_0| (= |%pumpRunning.1_0| 0.0)) (=  |%or.cond_0| (or |%_11_0| |%.not_0|)))))
(let (($x202 (and (<= |%waterLevel.2_1_1| |%waterLevel.4_0|) (>= |%waterLevel.2_1_1| |%waterLevel.4_0|))))
(let (($x198 (and (<= |%pumpRunning.3_1_1| |%pumpRunning.1_0|) (>= |%pumpRunning.3_1_1| |%pumpRunning.1_0|))))
(let (($x194 (and (<= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|) (>= |%methaneLevelCritical.1_1_1| |%methaneLevelCritical.0_0|))))
(let (($x186 (or (and E0x37041c0 (not E0x3704880)) (and E0x3704880 (not E0x37041c0)))))
(let (($x178 (and (<= |%pumpRunning.1_0| |%pumpRunning.3_1_0|) (>= |%pumpRunning.1_0| |%pumpRunning.3_1_0|))))
(let (($x170 (and (<= |%pumpRunning.1_0| |%pumpRunning.0_0|) (>= |%pumpRunning.1_0| |%pumpRunning.0_0|))))
(let (($x180 (or (and (and bb1.i4.i.i.i_0 E0x37041c0) $x170) (and (and (and bb2.i.i.i_0 E0x3704880) (not |%or.cond1_0|)) $x178))))
(let (($x188 (and (=>  bb3.i.i.i_0 $x180) (=>  bb3.i.i.i_0 $x186))))
(let (($x157 (=>  bb1.i4.i.i.i_0 (and (and bb2.i.i.i_0 E0x3703180) |%or.cond1_0|))))
(let (($x159 (and $x157 (=>  bb1.i4.i.i.i_0 E0x3703180))))
(let (($x147 (or (and E0x37015b0 (not E0x3701b30)) (and E0x3701b30 (not E0x37015b0)))))
(let (($x139 (and (<= |%waterLevel.4_0| |%waterLevel.1_0|) (>= |%waterLevel.4_0| |%waterLevel.1_0|))))
(let (($x132 (and (<= |%waterLevel.4_0| |%waterLevel.3_0|) (>= |%waterLevel.4_0| |%waterLevel.3_0|))))
(let (($x141 (or (and (and bb.i1.i.i_0 E0x37015b0) $x132) (and (and (and bb5.i.i_0 E0x3701b30) |%_6_0|) $x139))))
(let (($x149 (and (=>  bb2.i.i.i_0 $x141) (=>  bb2.i.i.i_0 $x147))))
(let (($x119 (=>  bb.i1.i.i_0 (and (and bb5.i.i_0 E0x36ffe60) (not |%_6_0|)))))
(let (($x121 (and $x119 (=>  bb.i1.i.i_0 E0x36ffe60))))
(let (($x108 (or (and E0x36fe5a0 (not E0x36feb80)) (and E0x36feb80 (not E0x36fe5a0)))))
(let (($x100 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x93 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x102 (or (and (and bb4.i.i_0 E0x36fe5a0) $x93) (and (and (and bb3.i.i_0 E0x36feb80) |%_4_0|) $x100))))
(let (($x110 (and (=>  bb5.i.i_0 $x102) (=>  bb5.i.i_0 $x108))))
(let (($x82 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x36fd4d0) (not |%_4_0|))) (=>  bb4.i.i_0 E0x36fd4d0))))
(let (($x69 (or (and E0x36e4bd0 (not E0x36e51f0)) (and E0x36e51f0 (not E0x36e4bd0)))))
(let (($x61 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x54 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x63 (or (and (and bb2.i.i_0 E0x36e4bd0) $x54) (and (and (and bb1.i.i_0 E0x36e51f0) |%_1_0|) $x61))))
(let (($x71 (and (=>  bb3.i.i_0 $x63) (=>  bb3.i.i_0 $x69))))
(let (($x43 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x36e3180) (not |%_1_0|))) (=>  bb2.i.i_0 E0x36e3180))))
(let (($x204 (and $x43 $x71 $x82 $x110 $x121 $x149 $x159 $x188 (and (and (and (and bb3.i.i.i_0 |%or.cond_0|) $x194) $x198) $x202))))
(and $x204 $x266))))))))))))))))))))))))))))))))) (cp-rel-bb1.i.i |%methaneLevelCritical.1_1_1| |%pumpRunning.3_1_1| |%waterLevel.2_1_1| )))

(rule (=> (and (cp-rel-bb1.i.i |%methaneLevelCritical.1_1_0| |%pumpRunning.3_1_0| |%waterLevel.2_1_0| ) (let (($x266 (and (=  |%_1_0| (= |%_0_0| 0.0)) (=  |%not._0| (< |%waterLevel.2_1_0| 2.0)) (= |%_2_0| (ite  |%not._0| 1.0 0.0)) (= |%waterLevel.0_0| (+ |%_2_0| |%waterLevel.2_1_0|)) (=  |%_4_0| (= |%_3_0| 0.0)) (=  |%_5_0| (= |%methaneLevelCritical.1_1_0| 0.0)) (= |%storemerge.i.i.i_0| (ite  |%_5_0| 1.0 0.0)) (=  |%_6_0| (= |%pumpRunning.3_1_0| 0.0)) (=  |%_7_0| (> |%waterLevel.1_0| 0.0)) (= |%_8_0| (+ |%waterLevel.1_0| (~ 1.0))) (= |%waterLevel.3_0| (ite  |%_7_0| |%_8_0| |%waterLevel.1_0|)) (=  |%_9_0| (> |%waterLevel.4_0| 1.0)) (=  |%or.cond1_0| (and |%_9_0| |%_6_0|)) (=  |%_10_0| (= |%methaneLevelCritical.0_0| 0.0)) (= |%pumpRunning.0_0| (ite  |%_10_0| 1.0 |%pumpRunning.3_1_0|)) (=  |%.not_0| (not (= |%waterLevel.4_0| 0.0))) (=  |%_11_0| (= |%pumpRunning.1_0| 0.0)) (=  |%or.cond_0| (or |%_11_0| |%.not_0|)))))
(let (($x186 (or (and E0x37041c0 (not E0x3704880)) (and E0x3704880 (not E0x37041c0)))))
(let (($x178 (and (<= |%pumpRunning.1_0| |%pumpRunning.3_1_0|) (>= |%pumpRunning.1_0| |%pumpRunning.3_1_0|))))
(let (($x170 (and (<= |%pumpRunning.1_0| |%pumpRunning.0_0|) (>= |%pumpRunning.1_0| |%pumpRunning.0_0|))))
(let (($x180 (or (and (and bb1.i4.i.i.i_0 E0x37041c0) $x170) (and (and (and bb2.i.i.i_0 E0x3704880) (not |%or.cond1_0|)) $x178))))
(let (($x188 (and (=>  bb3.i.i.i_0 $x180) (=>  bb3.i.i.i_0 $x186))))
(let (($x157 (=>  bb1.i4.i.i.i_0 (and (and bb2.i.i.i_0 E0x3703180) |%or.cond1_0|))))
(let (($x159 (and $x157 (=>  bb1.i4.i.i.i_0 E0x3703180))))
(let (($x147 (or (and E0x37015b0 (not E0x3701b30)) (and E0x3701b30 (not E0x37015b0)))))
(let (($x139 (and (<= |%waterLevel.4_0| |%waterLevel.1_0|) (>= |%waterLevel.4_0| |%waterLevel.1_0|))))
(let (($x132 (and (<= |%waterLevel.4_0| |%waterLevel.3_0|) (>= |%waterLevel.4_0| |%waterLevel.3_0|))))
(let (($x141 (or (and (and bb.i1.i.i_0 E0x37015b0) $x132) (and (and (and bb5.i.i_0 E0x3701b30) |%_6_0|) $x139))))
(let (($x149 (and (=>  bb2.i.i.i_0 $x141) (=>  bb2.i.i.i_0 $x147))))
(let (($x119 (=>  bb.i1.i.i_0 (and (and bb5.i.i_0 E0x36ffe60) (not |%_6_0|)))))
(let (($x121 (and $x119 (=>  bb.i1.i.i_0 E0x36ffe60))))
(let (($x108 (or (and E0x36fe5a0 (not E0x36feb80)) (and E0x36feb80 (not E0x36fe5a0)))))
(let (($x100 (and (<= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|) (>= |%methaneLevelCritical.0_0| |%methaneLevelCritical.1_1_0|))))
(let (($x93 (and (<= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|) (>= |%methaneLevelCritical.0_0| |%storemerge.i.i.i_0|))))
(let (($x102 (or (and (and bb4.i.i_0 E0x36fe5a0) $x93) (and (and (and bb3.i.i_0 E0x36feb80) |%_4_0|) $x100))))
(let (($x110 (and (=>  bb5.i.i_0 $x102) (=>  bb5.i.i_0 $x108))))
(let (($x82 (and (=>  bb4.i.i_0 (and (and bb3.i.i_0 E0x36fd4d0) (not |%_4_0|))) (=>  bb4.i.i_0 E0x36fd4d0))))
(let (($x69 (or (and E0x36e4bd0 (not E0x36e51f0)) (and E0x36e51f0 (not E0x36e4bd0)))))
(let (($x61 (and (<= |%waterLevel.1_0| |%waterLevel.2_1_0|) (>= |%waterLevel.1_0| |%waterLevel.2_1_0|))))
(let (($x54 (and (<= |%waterLevel.1_0| |%waterLevel.0_0|) (>= |%waterLevel.1_0| |%waterLevel.0_0|))))
(let (($x63 (or (and (and bb2.i.i_0 E0x36e4bd0) $x54) (and (and (and bb1.i.i_0 E0x36e51f0) |%_1_0|) $x61))))
(let (($x71 (and (=>  bb3.i.i_0 $x63) (=>  bb3.i.i_0 $x69))))
(let (($x43 (and (=>  bb2.i.i_0 (and (and bb1.i.i_0 E0x36e3180) (not |%_1_0|))) (=>  bb2.i.i_0 E0x36e3180))))
(let (($x270 (and $x43 $x71 $x82 $x110 $x121 $x149 $x159 $x188 (and bb3.i.i.i_0 (not |%or.cond_0|)))))
(and $x270 $x266)))))))))))))))))))))))))))))) cp-rel-bb1.i.i.i.i))



(query cp-rel-bb1.i.i.i.i)
