(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb.i71.i.outer20_proc (Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real))
(declare-rel cp-rel-bb1.i79.i.i ())
(declare-rel cp-rel-__UFO__0_proc ())
(declare-rel cp-rel-bb.i71.i_proc (Bool Real Real Real Real Real Real Real Real Real Bool Real Real Real Real Real Real Real Real Real))
(declare-var A Bool)
(declare-var B Bool)
(declare-var C Real)
(declare-var D Real)
(declare-var E Real)
(declare-var F Real)
(declare-var G Real)
(declare-var H Real)
(declare-var I Real)
(declare-var J Real)
(declare-var K Real)
(declare-var L Real)
(declare-var M Real)
(declare-var N Real)
(declare-var O Real)
(declare-var P Real)
(declare-var Q Real)
(declare-var R Real)
(declare-var S Real)
(declare-var T Real)
(declare-var U Bool)
(declare-var V Bool)
(declare-var W Bool)
(declare-var X Bool)
(declare-var Y Bool)
(declare-var Z Bool)
(declare-var A1 Bool)
(declare-var B1 Bool)
(declare-var C1 Bool)
(declare-var D1 Bool)
(declare-var E1 Bool)
(declare-var F1 Bool)
(declare-var G1 Bool)
(declare-var H1 Bool)
(declare-var I1 Bool)
(declare-var J1 Bool)
(declare-var K1 Bool)
(declare-var L1 Bool)
(declare-var M1 Bool)
(declare-var N1 Real)
(declare-var O1 Real)
(declare-var P1 Real)
(declare-var Q1 Real)
(declare-var R1 Real)
(declare-var S1 Real)
(declare-var T1 Real)
(declare-var U1 Bool)
(declare-var V1 Bool)
(declare-var W1 Bool)
(declare-var X1 Bool)
(declare-var Y1 Bool)
(declare-var Z1 Bool)
(declare-var A2 Bool)
(declare-var B2 Bool)
(declare-var C2 Bool)
(declare-var D2 Bool)
(declare-var E2 Bool)
(declare-var F2 Bool)
(declare-var G2 Bool)
(declare-var H2 Real)
(declare-var I2 Real)
(declare-var J2 Real)
(declare-var K2 Real)
(declare-var L2 Real)
(declare-var M2 Real)
(declare-var N2 Bool)
(declare-var O2 Bool)
(declare-var P2 Bool)
(declare-var Q2 Bool)
(declare-var R2 Bool)
(declare-var S2 Bool)
(declare-var T2 Bool)
(declare-var U2 Bool)
(declare-var V2 Bool)
(declare-var W2 Bool)
(declare-var X2 Bool)
(declare-var Y2 Bool)
(declare-var Z2 Bool)
(declare-var A3 Bool)
(declare-var B3 Real)
(declare-var C3 Real)
(declare-var D3 Real)
(declare-var E3 Real)
(declare-var F3 Real)
(declare-var G3 Real)
(declare-var H3 Bool)
(declare-var I3 Bool)
(declare-var J3 Bool)
(declare-var K3 Bool)
(declare-var L3 Bool)
(declare-var M3 Bool)
(declare-var N3 Bool)
(declare-var O3 Bool)
(declare-var P3 Bool)
(declare-var Q3 Bool)
(declare-var R3 Bool)
(declare-var S3 Bool)
(declare-var T3 Bool)
(declare-var U3 Real)
(declare-var V3 Real)
(declare-var W3 Bool)
(declare-var X3 Real)
(declare-var Y3 Bool)
(declare-var Z3 Real)
(declare-var A4 Real)
(declare-var B4 Real)
(declare-var C4 Real)
(declare-var D4 Real)
(declare-var E4 Real)
(declare-var F4 Real)
(declare-var G4 Real)
(declare-var H4 Real)
(declare-var I4 Real)
(declare-var J4 Real)
(declare-var K4 Bool)
(declare-var L4 Bool)
(declare-var M4 Bool)
(declare-var N4 Bool)
(declare-var O4 Bool)
(declare-var P4 Bool)
(declare-var Q4 Bool)
(declare-var R4 Bool)
(declare-var S4 Bool)
(declare-var T4 Bool)
(declare-var U4 Bool)
(declare-var V4 Bool)
(declare-var W4 Bool)
(declare-var X4 Bool)
(declare-var Y4 Real)
(declare-var Z4 Real)
(declare-var A5 Real)
(declare-var B5 Real)
(declare-var C5 Real)
(declare-var D5 Real)
(declare-var E5 Real)
(declare-var F5 Real)
(declare-var G5 Real)
(declare-var H5 Real)
(declare-var I5 Real)
(declare-var J5 Real)
(declare-var K5 Real)
(declare-var L5 Real)
(declare-var M5 Bool)
(declare-var N5 Bool)
(declare-var O5 Real)
(declare-var P5 Real)
(declare-var Q5 Real)
(declare-var R5 Real)
(declare-var S5 Real)
(declare-var T5 Real)
(declare-var U5 Real)
(declare-var V5 Real)
(declare-var W5 Real)
(declare-var X5 Real)
(rule cp-rel-entry)
(rule (=> (and cp-rel-__UFO__0_proc U) cp-rel-__UFO__0_proc))
(rule (let ((a!1 (=> H1 (or (and E1 I1 J1) (and B1 K1 (not G1)))))
      (a!2 (=> H1 (or (and I1 (not K1)) (and K1 (not I1)))))
      (a!3 (or (and H1
                    M1
                    (<= N1 O1)
                    (>= N1 O1)
                    (<= P1 O1)
                    (>= P1 O1)
                    (<= Q1 2.0)
                    (>= Q1 2.0)
                    (<= R1 S1)
                    (>= R1 S1)
                    (<= T1 1.0)
                    (>= T1 1.0))
               (and Y
                    U1
                    D1
                    (and (<= N1 L) (>= N1 L))
                    (and (<= P1 M) (>= P1 M))
                    (and (<= Q1 N) (>= Q1 N))
                    (and (<= R1 O) (>= R1 O))
                    (and (<= T1 T) (>= T1 T)))
               (and W
                    V1
                    (not A1)
                    (and (<= N1 L) (>= N1 L))
                    (and (<= P1 M) (>= P1 M))
                    (and (<= Q1 N) (>= Q1 N))
                    (and (<= R1 O) (>= R1 O))
                    (and (<= T1 T) (>= T1 T)))))
      (a!4 (=> L1
               (or (and M1 (not U1) (not V1))
                   (and U1 (not M1) (not V1))
                   (and V1 (not M1) (not U1)))))
      (a!5 (or (and C2
                    G2
                    (<= H2 I2)
                    (>= H2 I2)
                    (and (<= J2 2.0) (>= J2 2.0))
                    (<= K2 L2)
                    (>= K2 L2)
                    (and (<= M2 1.0) (>= M2 1.0)))
               (and Z1
                    N2
                    (not E2)
                    (and (<= H2 N1) (>= H2 N1))
                    (and (<= J2 2.0) (>= J2 2.0))
                    (and (<= K2 P) (>= K2 P))
                    (and (<= M2 1.0) (>= M2 1.0)))
               (and W1
                    O2
                    B2
                    (and (<= H2 N1) (>= H2 N1))
                    (and (<= J2 R1) (>= J2 R1))
                    (and (<= K2 P) (>= K2 P))
                    (and (<= M2 S) (>= M2 S)))
               (and L1
                    P2
                    (not Y1)
                    (and (<= H2 N1) (>= H2 N1))
                    (and (<= J2 R1) (>= J2 R1))
                    (and (<= K2 P) (>= K2 P))
                    (and (<= M2 S) (>= M2 S)))))
      (a!6 (=> F2
               (or (and G2 (not N2) (not O2) (not P2))
                   (and N2 (not G2) (not O2) (not P2))
                   (and O2 (not G2) (not N2) (not P2))
                   (and P2 (not G2) (not N2) (not O2)))))
      (a!7 (or (and W2
                    A3
                    (<= B3 C3)
                    (>= B3 C3)
                    (and (<= D3 2.0) (>= D3 2.0))
                    (<= E3 F3)
                    (>= E3 F3)
                    (and (<= G3 1.0) (>= G3 1.0)))
               (and T2
                    H3
                    (not Y2)
                    (and (<= B3 H2) (>= B3 H2))
                    (and (<= D3 2.0) (>= D3 2.0))
                    (and (<= E3 Q) (>= E3 Q))
                    (and (<= G3 1.0) (>= G3 1.0)))
               (and Q2
                    I3
                    V2
                    (and (<= B3 H2) (>= B3 H2))
                    (and (<= D3 K2) (>= D3 K2))
                    (and (<= E3 Q) (>= E3 Q))
                    (and (<= G3 R) (>= G3 R)))
               (and F2
                    J3
                    (not S2)
                    (and (<= B3 H2) (>= B3 H2))
                    (and (<= D3 K2) (>= D3 K2))
                    (and (<= E3 Q) (>= E3 Q))
                    (and (<= G3 R) (>= G3 R)))))
      (a!8 (=> Z2
               (or (and A3 (not H3) (not I3) (not J3))
                   (and H3 (not A3) (not I3) (not J3))
                   (and I3 (not A3) (not H3) (not J3))
                   (and J3 (not A3) (not H3) (not I3)))))
      (a!9 (or (and K3
                    N3
                    (and (<= B4 B3) (>= B4 B3))
                    (and (<= C4 P1) (>= C4 P1))
                    (and (<= D4 Q1) (>= D4 Q1))
                    (and (<= E4 J2) (>= E4 J2))
                    (and (<= F4 D3) (>= F4 D3))
                    (and (<= G4 E3) (>= G4 E3))
                    (and (<= H4 G3) (>= H4 G3))
                    (and (<= I4 M2) (>= I4 M2))
                    (and (<= J4 T1) (>= J4 T1)))
               (and Z2
                    (not M3)
                    (and (<= B4 B3) (>= B4 B3))
                    (and (<= C4 P1) (>= C4 P1))
                    (and (<= D4 Q1) (>= D4 Q1))
                    (and (<= E4 J2) (>= E4 J2))
                    (and (<= F4 D3) (>= F4 D3))
                    (and (<= G4 E3) (>= G4 E3))
                    (and (<= H4 G3) (>= H4 G3))
                    (and (<= I4 M2) (>= I4 M2))
                    (and (<= J4 T1) (>= J4 T1))))))
(let ((a!10 (and (cp-rel-bb.i71.i_proc
                   B
                   B4
                   C4
                   D4
                   E4
                   F4
                   G4
                   H4
                   I4
                   J4
                   A
                   K
                   J
                   I
                   H
                   G
                   F
                   E
                   D
                   C)
                 (=> W (and U X (not V)))
                 (=> W X)
                 (=> Y (and W Z A1))
                 (=> Y Z)
                 (=> B1 (and Y C1 (not D1)))
                 (=> B1 C1)
                 (=> E1 (and B1 F1 G1))
                 (=> E1 F1)
                 a!1
                 a!2
                 (=> L1 a!3)
                 a!4
                 (=> W1 (and L1 X1 Y1))
                 (=> W1 X1)
                 (=> Z1 (and W1 A2 (not B2)))
                 (=> Z1 A2)
                 (=> C2 (and Z1 D2 E2))
                 (=> C2 D2)
                 (=> F2 a!5)
                 a!6
                 (=> Q2 (and F2 R2 S2))
                 (=> Q2 R2)
                 (=> T2 (and Q2 U2 (not V2)))
                 (=> T2 U2)
                 (=> W2 (and T2 X2 Y2))
                 (=> W2 X2)
                 (=> Z2 a!7)
                 a!8
                 (=> K3 (and Z2 L3 M3))
                 (=> K3 L3)
                 a!9
                 (= A1 (= N 0.0))
                 (= O3 (= O 0.0))
                 (= P3 (= P 0.0))
                 (= Q3 (or O3 A1))
                 (= R3 (or Q3 P3))
                 (= S3 (not (= Q 0.0)))
                 (= T3 (xor R3 true))
                 (= V (and S3 T3))
                 (= D1 (= U3 0.0))
                 (= G1 (= T 1.0))
                 (= V3 (+ M 3.0))
                 (= J1 (= V3 L))
                 (= W3 (= S 1.0))
                 (= S1 (ite W3 0.0 O))
                 (= Y1 (= R1 0.0))
                 (= B2 (= X3 0.0))
                 (= E2 (= S 1.0))
                 (= I2 (+ N1 1.0))
                 (= Y3 (= R 1.0))
                 (= L2 (ite Y3 0.0 P))
                 (= S2 (= K2 0.0))
                 (= V2 (= Z3 0.0))
                 (= Y2 (= R 1.0))
                 (= C3 (+ H2 1.0))
                 (= F3 (ite B 0.0 Q))
                 (= M3 (= E3 0.0))
                 (= N3 (= A4 0.0)))))
  (=> a!10 (cp-rel-bb.i71.i_proc B L M N O P Q R S T A K J I H G F E D C)))))
(rule (let ((a!1 (=> I1 (or (and F1 J1 K1) (and C1 L1 (not H1)))))
      (a!2 (=> I1 (or (and J1 (not L1)) (and L1 (not J1)))))
      (a!3 (or (and I1
                    U1
                    (<= O1 P1)
                    (>= O1 P1)
                    (<= Q1 P1)
                    (>= Q1 P1)
                    (<= R1 2.0)
                    (>= R1 2.0)
                    (<= S1 T1)
                    (>= S1 T1)
                    (<= H2 1.0)
                    (>= H2 1.0))
               (and Z
                    V1
                    E1
                    (and (<= O1 M) (>= O1 M))
                    (and (<= Q1 N) (>= Q1 N))
                    (and (<= R1 O) (>= R1 O))
                    (and (<= S1 P) (>= S1 P))
                    (and (<= H2 N1) (>= H2 N1)))
               (and X
                    W1
                    (not B1)
                    (and (<= O1 M) (>= O1 M))
                    (and (<= Q1 N) (>= Q1 N))
                    (and (<= R1 O) (>= R1 O))
                    (and (<= S1 P) (>= S1 P))
                    (and (<= H2 N1) (>= H2 N1)))))
      (a!4 (=> M1
               (or (and U1 (not V1) (not W1))
                   (and V1 (not U1) (not W1))
                   (and W1 (not U1) (not V1)))))
      (a!5 (or (and D2
                    N2
                    (<= I2 J2)
                    (>= I2 J2)
                    (and (<= K2 2.0) (>= K2 2.0))
                    (<= L2 M2)
                    (>= L2 M2)
                    (and (<= B3 1.0) (>= B3 1.0)))
               (and A2
                    O2
                    (not F2)
                    (and (<= I2 O1) (>= I2 O1))
                    (and (<= K2 2.0) (>= K2 2.0))
                    (and (<= L2 Q) (>= L2 Q))
                    (and (<= B3 1.0) (>= B3 1.0)))
               (and X1
                    P2
                    C2
                    (and (<= I2 O1) (>= I2 O1))
                    (and (<= K2 S1) (>= K2 S1))
                    (and (<= L2 Q) (>= L2 Q))
                    (and (<= B3 T) (>= B3 T)))
               (and M1
                    Q2
                    (not Z1)
                    (and (<= I2 O1) (>= I2 O1))
                    (and (<= K2 S1) (>= K2 S1))
                    (and (<= L2 Q) (>= L2 Q))
                    (and (<= B3 T) (>= B3 T)))))
      (a!6 (=> G2
               (or (and N2 (not O2) (not P2) (not Q2))
                   (and O2 (not N2) (not P2) (not Q2))
                   (and P2 (not N2) (not O2) (not Q2))
                   (and Q2 (not N2) (not O2) (not P2)))))
      (a!7 (or (and X2
                    H3
                    (<= C3 D3)
                    (>= C3 D3)
                    (and (<= E3 2.0) (>= E3 2.0))
                    (<= F3 G3)
                    (>= F3 G3)
                    (and (<= U3 1.0) (>= U3 1.0)))
               (and U2
                    I3
                    (not Z2)
                    (and (<= C3 I2) (>= C3 I2))
                    (and (<= E3 2.0) (>= E3 2.0))
                    (and (<= F3 R) (>= F3 R))
                    (and (<= U3 1.0) (>= U3 1.0)))
               (and R2
                    J3
                    W2
                    (and (<= C3 I2) (>= C3 I2))
                    (and (<= E3 L2) (>= E3 L2))
                    (and (<= F3 R) (>= F3 R))
                    (and (<= U3 S) (>= U3 S)))
               (and G2
                    K3
                    (not T2)
                    (and (<= C3 I2) (>= C3 I2))
                    (and (<= E3 L2) (>= E3 L2))
                    (and (<= F3 R) (>= F3 R))
                    (and (<= U3 S) (>= U3 S)))))
      (a!8 (=> A3
               (or (and H3 (not I3) (not J3) (not K3))
                   (and I3 (not H3) (not J3) (not K3))
                   (and J3 (not H3) (not I3) (not K3))
                   (and K3 (not H3) (not I3) (not J3)))))
      (a!9 (or (and B
                    (not T3)
                    (<= C M)
                    (>= C M)
                    (<= D N)
                    (>= D N)
                    (<= E O)
                    (>= E O)
                    (<= F P)
                    (>= F P)
                    (<= G Q)
                    (>= G Q)
                    (<= H R)
                    (>= H R)
                    (<= I V3)
                    (>= I V3)
                    (<= J S)
                    (>= J S)
                    (<= K T)
                    (>= K T)
                    (<= L N1)
                    (>= L N1))
               (and R3
                    (<= C X3)
                    (>= C X3)
                    (and (<= D Q1) (>= D Q1))
                    (<= E Z3)
                    (>= E Z3)
                    (and (<= F K2) (>= F K2))
                    (and (<= G E3) (>= G E3))
                    (and (<= H 2.0) (>= H 2.0))
                    (and (<= I 1.0) (>= I 1.0))
                    (and (<= J U3) (>= J U3))
                    (and (<= K B3) (>= K B3))
                    (and (<= L H2) (>= L H2)))
               (and O3
                    (not A)
                    (<= C C3)
                    (>= C C3)
                    (and (<= D Q1) (>= D Q1))
                    (<= E R1)
                    (>= E R1)
                    (and (<= F K2) (>= F K2))
                    (and (<= G E3) (>= G E3))
                    (and (<= H 2.0) (>= H 2.0))
                    (and (<= I 1.0) (>= I 1.0))
                    (and (<= J U3) (>= J U3))
                    (and (<= K B3) (>= K B3))
                    (and (<= L H2) (>= L H2))))))
(let ((a!10 (and (=> B (and U V W))
                 (=> B V)
                 (=> X (and U Y (not W)))
                 (=> X Y)
                 (=> Z (and X A1 B1))
                 (=> Z A1)
                 (=> C1 (and Z D1 (not E1)))
                 (=> C1 D1)
                 (=> F1 (and C1 G1 H1))
                 (=> F1 G1)
                 a!1
                 a!2
                 (=> M1 a!3)
                 a!4
                 (=> X1 (and M1 Y1 Z1))
                 (=> X1 Y1)
                 (=> A2 (and X1 B2 (not C2)))
                 (=> A2 B2)
                 (=> D2 (and A2 E2 F2))
                 (=> D2 E2)
                 (=> G2 a!5)
                 a!6
                 (=> R2 (and G2 S2 T2))
                 (=> R2 S2)
                 (=> U2 (and R2 V2 (not W2)))
                 (=> U2 V2)
                 (=> X2 (and U2 Y2 Z2))
                 (=> X2 Y2)
                 (=> A3 a!7)
                 a!8
                 (=> L3 (and A3 M3 N3))
                 (=> L3 M3)
                 (=> O3 (and L3 P3 (not Q3)))
                 (=> O3 P3)
                 (=> R3 (and O3 S3 A))
                 (=> R3 S3)
                 a!9
                 (= B1 (= O 0.0))
                 (= W3 (= P 0.0))
                 (= Y3 (= Q 0.0))
                 (= K4 (or W3 B1))
                 (= L4 (or K4 Y3))
                 (= M4 (not (= R 0.0)))
                 (= N4 (xor L4 true))
                 (= W (and M4 N4))
                 (= O4 (= O 0.0))
                 (= P4 (= P 0.0))
                 (= Q4 (= Q 0.0))
                 (= R4 (or P4 O4))
                 (= S4 (or R4 Q4))
                 (= T4 (not (= R 0.0)))
                 (= U4 (xor S4 true))
                 (= T3 (and T4 U4))
                 (= E1 (= A4 0.0))
                 (= H1 (= N1 1.0))
                 (= B4 (+ N 3.0))
                 (= K1 (= B4 M))
                 (= V4 (= T 1.0))
                 (= T1 (ite V4 0.0 P))
                 (= Z1 (= S1 0.0))
                 (= C2 (= C4 0.0))
                 (= F2 (= T 1.0))
                 (= J2 (+ O1 1.0))
                 (= W4 (= S 1.0))
                 (= M2 (ite W4 0.0 Q))
                 (= T2 (= L2 0.0))
                 (= W2 (= D4 0.0))
                 (= Z2 (= S 1.0))
                 (= D3 (+ I2 1.0))
                 (= G3 (ite A 0.0 R))
                 (= N3 (= F3 0.0))
                 (= Q3 (= E4 0.0))
                 (= X3 (+ C3 1.0))
                 (= X4 (= H2 1.0))
                 (= Z3 (ite X4 0.0 R1)))))
  (=> a!10 (cp-rel-bb.i71.i_proc A M N O P Q R S T N1 A M N O P Q R S T N1)))))
(rule (let ((a!1 (and (=> V (and B W (not U)))
                (=> V W)
                (=> X (and V Y Z))
                (=> X Y)
                (=> A1 (and X B1 (not C1)))
                (=> A1 B1)
                (=> D1 (and A1 E1 F1))
                (=> D1 E1)
                D1
                (not G1)
                (= Z (= E 0.0))
                (= H1 (= F 0.0))
                (= I1 (= G 0.0))
                (= J1 (or H1 Z))
                (= K1 (or J1 I1))
                (= L1 (not (= H 0.0)))
                (= M1 (xor K1 true))
                (= U (and L1 M1))
                (= C1 (= L 0.0))
                (= F1 (= K 1.0))
                (= M (+ D 3.0))
                (= G1 (= M C)))))
  (=> a!1 (cp-rel-bb.i71.i_proc A C D E F G H I J K A C D E F G H I J K))))
(rule (let ((a!1 (=> K4 (or (and N4 Y3 W3) (and Q4 T3 (not L4)))))
      (a!2 (=> K4 (or (and Y3 (not T3)) (and T3 (not Y3)))))
      (a!3 (or (and K4
                    R3
                    (<= L5 K5)
                    (>= L5 K5)
                    (<= J5 K5)
                    (>= J5 K5)
                    (<= I5 2.0)
                    (>= I5 2.0)
                    (<= H5 G5)
                    (>= H5 G5)
                    (<= F5 1.0)
                    (>= F5 1.0))
               (and T4
                    Q3
                    O4
                    (and (<= L5 L2) (>= L5 L2))
                    (and (<= J5 M2) (>= J5 M2))
                    (and (<= I5 B3) (>= I5 B3))
                    (and (<= H5 C3) (>= H5 C3))
                    (and (<= F5 U3) (>= F5 U3)))
               (and V4
                    P3
                    (not R4)
                    (and (<= L5 L2) (>= L5 L2))
                    (and (<= J5 M2) (>= J5 M2))
                    (and (<= I5 B3) (>= I5 B3))
                    (and (<= H5 C3) (>= H5 C3))
                    (and (<= F5 U3) (>= F5 U3)))))
      (a!4 (=> S3
               (or (and R3 (not Q3) (not P3))
                   (and Q3 (not R3) (not P3))
                   (and P3 (not R3) (not Q3)))))
      (a!5 (or (and I3
                    Y2
                    (<= E5 D5)
                    (>= E5 D5)
                    (and (<= C5 2.0) (>= C5 2.0))
                    (<= B5 A5)
                    (>= B5 A5)
                    (and (<= Z4 1.0) (>= Z4 1.0)))
               (and L3
                    X2
                    (not A3)
                    (and (<= E5 L5) (>= E5 L5))
                    (and (<= C5 2.0) (>= C5 2.0))
                    (and (<= B5 D3) (>= B5 D3))
                    (and (<= Z4 1.0) (>= Z4 1.0)))
               (and O3
                    W2
                    J3
                    (and (<= E5 L5) (>= E5 L5))
                    (and (<= C5 H5) (>= C5 H5))
                    (and (<= B5 D3) (>= B5 D3))
                    (and (<= Z4 G3) (>= Z4 G3)))
               (and S3
                    V2
                    (not M3)
                    (and (<= E5 L5) (>= E5 L5))
                    (and (<= C5 H5) (>= C5 H5))
                    (and (<= B5 D3) (>= B5 D3))
                    (and (<= Z4 G3) (>= Z4 G3)))))
      (a!6 (=> Z2
               (or (and Y2 (not X2) (not W2) (not V2))
                   (and X2 (not Y2) (not W2) (not V2))
                   (and W2 (not Y2) (not X2) (not V2))
                   (and V2 (not Y2) (not X2) (not W2)))))
      (a!7 (or (and O2
                    E2
                    (<= Y4 J4)
                    (>= Y4 J4)
                    (and (<= I4 2.0) (>= I4 2.0))
                    (<= H4 G4)
                    (>= H4 G4)
                    (and (<= F4 1.0) (>= F4 1.0)))
               (and R2
                    D2
                    (not G2)
                    (and (<= Y4 E5) (>= Y4 E5))
                    (and (<= I4 2.0) (>= I4 2.0))
                    (and (<= H4 E3) (>= H4 E3))
                    (and (<= F4 1.0) (>= F4 1.0)))
               (and U2
                    C2
                    P2
                    (and (<= Y4 E5) (>= Y4 E5))
                    (and (<= I4 B5) (>= I4 B5))
                    (and (<= H4 E3) (>= H4 E3))
                    (and (<= F4 F3) (>= F4 F3)))
               (and Z2
                    B2
                    (not S2)
                    (and (<= Y4 E5) (>= Y4 E5))
                    (and (<= I4 B5) (>= I4 B5))
                    (and (<= H4 E3) (>= H4 E3))
                    (and (<= F4 F3) (>= F4 F3)))))
      (a!8 (=> F2
               (or (and E2 (not D2) (not C2) (not B2))
                   (and D2 (not E2) (not C2) (not B2))
                   (and C2 (not E2) (not D2) (not B2))
                   (and B2 (not E2) (not D2) (not C2)))))
      (a!9 (or (and N5
                    (not L1)
                    (<= X5 L2)
                    (>= X5 L2)
                    (<= W5 M2)
                    (>= W5 M2)
                    (<= V5 B3)
                    (>= V5 B3)
                    (<= U5 C3)
                    (>= U5 C3)
                    (<= T5 D3)
                    (>= T5 D3)
                    (<= S5 E3)
                    (>= S5 E3)
                    (<= R5 E4)
                    (>= R5 E4)
                    (<= Q5 F3)
                    (>= Q5 F3)
                    (<= P5 G3)
                    (>= P5 G3)
                    (<= O5 U3)
                    (>= O5 U3))
               (and U1
                    (<= X5 D4)
                    (>= X5 D4)
                    (and (<= W5 J5) (>= W5 J5))
                    (<= V5 C4)
                    (>= V5 C4)
                    (and (<= U5 C5) (>= U5 C5))
                    (and (<= T5 I4) (>= T5 I4))
                    (and (<= S5 2.0) (>= S5 2.0))
                    (and (<= R5 1.0) (>= R5 1.0))
                    (and (<= Q5 F4) (>= Q5 F4))
                    (and (<= P5 Z4) (>= P5 Z4))
                    (and (<= O5 F5) (>= O5 F5)))
               (and X1
                    (not U)
                    (<= X5 Y4)
                    (>= X5 Y4)
                    (and (<= W5 J5) (>= W5 J5))
                    (<= V5 I5)
                    (>= V5 I5)
                    (and (<= U5 C5) (>= U5 C5))
                    (and (<= T5 I4) (>= T5 I4))
                    (and (<= S5 2.0) (>= S5 2.0))
                    (and (<= R5 1.0) (>= R5 1.0))
                    (and (<= Q5 F4) (>= Q5 F4))
                    (and (<= P5 Z4) (>= P5 Z4))
                    (and (<= O5 F5) (>= O5 F5))))))
(let ((a!10 (and (cp-rel-bb.i71.i.outer20_proc
                   X5
                   W5
                   V5
                   U5
                   T5
                   S5
                   R5
                   Q5
                   P5
                   O5
                   L
                   K
                   J
                   I
                   H
                   G
                   F
                   E
                   D
                   C)
                 A
                 (<= N1 K2)
                 (>= N1 K2)
                 (<= T J2)
                 (>= T J2)
                 (<= S I2)
                 (>= S I2)
                 (<= R H2)
                 (>= R H2)
                 (<= Q T1)
                 (>= Q T1)
                 (<= P S1)
                 (>= P S1)
                 (<= O Q1)
                 (>= O Q1)
                 (<= N P1)
                 (>= N P1)
                 (<= M O1)
                 (>= M O1)
                 (= B (= R1 1.0))
                 (cp-rel-bb.i71.i_proc
                   B
                   N1
                   T
                   S
                   R
                   Q
                   P
                   O
                   N
                   M
                   U
                   L2
                   M2
                   B3
                   C3
                   D3
                   E3
                   F3
                   G3
                   U3)
                 (=> N5 (and M5 X4 W4))
                 (=> N5 X4)
                 (=> V4 (and M5 U4 (not W4)))
                 (=> V4 U4)
                 (=> T4 (and V4 S4 R4))
                 (=> T4 S4)
                 (=> Q4 (and T4 P4 (not O4)))
                 (=> Q4 P4)
                 (=> N4 (and Q4 M4 L4))
                 (=> N4 M4)
                 a!1
                 a!2
                 (=> S3 a!3)
                 a!4
                 (=> O3 (and S3 N3 M3))
                 (=> O3 N3)
                 (=> L3 (and O3 K3 (not J3)))
                 (=> L3 K3)
                 (=> I3 (and L3 H3 A3))
                 (=> I3 H3)
                 (=> Z2 a!5)
                 a!6
                 (=> U2 (and Z2 T2 S2))
                 (=> U2 T2)
                 (=> R2 (and U2 Q2 (not P2)))
                 (=> R2 Q2)
                 (=> O2 (and R2 N2 G2))
                 (=> O2 N2)
                 (=> F2 a!7)
                 a!8
                 (=> A2 (and F2 Z1 Y1))
                 (=> A2 Z1)
                 (=> X1 (and A2 W1 (not V1)))
                 (=> X1 W1)
                 (=> U1 (and X1 M1 U))
                 (=> U1 M1)
                 a!9
                 (= R4 (= B3 0.0))
                 (= K1 (= C3 0.0))
                 (= J1 (= D3 0.0))
                 (= I1 (or K1 R4))
                 (= H1 (or I1 J1))
                 (= G1 (not (= E3 0.0)))
                 (= F1 (xor H1 true))
                 (= W4 (and G1 F1))
                 (= E1 (= B3 0.0))
                 (= D1 (= C3 0.0))
                 (= C1 (= D3 0.0))
                 (= B1 (or D1 E1))
                 (= A1 (or B1 C1))
                 (= Z (not (= E3 0.0)))
                 (= Y (xor A1 true))
                 (= L1 (and Z Y))
                 (= O4 (= B4 0.0))
                 (= L4 (= U3 1.0))
                 (= A4 (+ M2 3.0))
                 (= W3 (= A4 L2))
                 (= X (= G3 1.0))
                 (= G5 (ite X 0.0 C3))
                 (= M3 (= H5 0.0))
                 (= J3 (= Z3 0.0))
                 (= A3 (= G3 1.0))
                 (= D5 (+ L5 1.0))
                 (= W (= F3 1.0))
                 (= A5 (ite W 0.0 D3))
                 (= S2 (= B5 0.0))
                 (= P2 (= X3 0.0))
                 (= G2 (= F3 1.0))
                 (= J4 (+ E5 1.0))
                 (= G4 (ite U 0.0 E3))
                 (= Y1 (= H4 0.0))
                 (= V1 (= V3 0.0))
                 (= D4 (+ Y4 1.0))
                 (= V (= F5 1.0))
                 (= C4 (ite V 0.0 I5)))))
  (=> a!10
      (cp-rel-bb.i71.i.outer20_proc
        K2
        J2
        I2
        H2
        T1
        S1
        R1
        Q1
        P1
        O1
        L
        K
        J
        I
        H
        G
        F
        E
        D
        C)))))
(rule (let ((a!1 (and A
                (<= K N1)
                (>= K N1)
                (<= J T)
                (>= J T)
                (<= I S)
                (>= I S)
                (<= H R)
                (>= H R)
                (<= G Q)
                (>= G Q)
                (<= F P)
                (>= F P)
                (<= E N)
                (>= E N)
                (<= D M)
                (>= D M)
                (<= C L)
                (>= C L)
                (= B (= O 1.0))
                (cp-rel-bb.i71.i_proc
                  B
                  K
                  J
                  I
                  H
                  G
                  F
                  E
                  D
                  C
                  U
                  O1
                  P1
                  Q1
                  R1
                  S1
                  T1
                  H2
                  I2
                  J2)
                (=> M1 (and V1 L1 (not U1)))
                (=> M1 L1)
                (=> K1 (and M1 J1 I1))
                (=> K1 J1)
                (=> H1 (and K1 G1 (not F1)))
                (=> H1 G1)
                (=> E1 (and H1 D1 C1))
                (=> E1 D1)
                E1
                (not B1)
                (= I1 (= Q1 0.0))
                (= A1 (= R1 0.0))
                (= Z (= S1 0.0))
                (= Y (or A1 I1))
                (= X (or Y Z))
                (= W (not (= T1 0.0)))
                (= V (xor X true))
                (= U1 (and W V))
                (= F1 (= L2 0.0))
                (= C1 (= J2 1.0))
                (= K2 (+ P1 3.0))
                (= B1 (= K2 O1)))))
  (=> a!1
      (cp-rel-bb.i71.i.outer20_proc N1 T S R Q P O N M L N1 T S R Q P O N M L))))
(rule (let ((a!1 (and cp-rel-entry
                W1
                (<= M2 0.0)
                (>= M2 0.0)
                (<= B3 0.0)
                (>= B3 0.0)
                (<= C3 0.0)
                (>= C3 0.0)
                (<= D3 0.0)
                (>= D3 0.0)
                (<= E3 0.0)
                (>= E3 0.0)
                (<= F3 0.0)
                (>= F3 0.0)
                (<= G3 0.0)
                (>= G3 0.0)
                (<= U3 0.0)
                (>= U3 0.0)
                (<= V3 0.0)
                (>= V3 0.0)
                (<= X3 0.0)
                (>= X3 0.0)
                (cp-rel-bb.i71.i.outer20_proc
                  M2
                  B3
                  C3
                  D3
                  E3
                  F3
                  G3
                  U3
                  V3
                  X3
                  L2
                  K2
                  J2
                  I2
                  H2
                  T1
                  S1
                  R1
                  Q1
                  P1)
                A
                (<= K L2)
                (>= K L2)
                (<= J K2)
                (>= J K2)
                (<= I J2)
                (>= I J2)
                (<= H I2)
                (>= H I2)
                (<= G H2)
                (>= G H2)
                (<= F T1)
                (>= F T1)
                (<= E R1)
                (>= E R1)
                (<= D Q1)
                (>= D Q1)
                (<= C P1)
                (>= C P1)
                (= B (= S1 1.0))
                (cp-rel-bb.i71.i_proc B K J I H G F E D C U L M N O P Q R S T)
                (=> M1 (and V1 L1 (not U1)))
                (=> M1 L1)
                (=> K1 (and M1 J1 I1))
                (=> K1 J1)
                (=> H1 (and K1 G1 (not F1)))
                (=> H1 G1)
                (=> E1 (and H1 D1 C1))
                (=> E1 D1)
                E1
                (not B1)
                (= I1 (= N 0.0))
                (= A1 (= O 0.0))
                (= Z (= P 0.0))
                (= Y (or A1 I1))
                (= X (or Y Z))
                (= W (not (= Q 0.0)))
                (= V (xor X true))
                (= U1 (and W V))
                (= F1 (= O1 0.0))
                (= C1 (= T 1.0))
                (= N1 (+ M 3.0))
                (= B1 (= N1 L)))))
  (=> a!1 cp-rel-bb1.i79.i.i)))
(query cp-rel-bb1.i79.i.i)
