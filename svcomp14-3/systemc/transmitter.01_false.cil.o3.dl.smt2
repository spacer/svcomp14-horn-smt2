(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb.i33.i.us_proc (Real Real Real Real Real Real))
(declare-rel cp-rel-bb.i33.i.outer_proc (Real Real Real Real Real Real Real Real))
(declare-rel cp-rel-__UFO__0_proc ())
(declare-rel cp-rel-bb.i33.i_proc (Real Real Real Real Real Real))
(declare-rel cp-rel-T1_WAIT.i.i.i ())
(declare-var A Real)
(declare-var B Real)
(declare-var C Real)
(declare-var D Real)
(declare-var E Real)
(declare-var F Real)
(declare-var G Bool)
(declare-var H Real)
(declare-var I Real)
(declare-var J Real)
(declare-var K Bool)
(declare-var L Bool)
(declare-var M Bool)
(declare-var N Bool)
(declare-var O Bool)
(declare-var P Bool)
(declare-var Q Bool)
(declare-var R Bool)
(declare-var S Bool)
(declare-var T Bool)
(declare-var U Bool)
(declare-var V Bool)
(declare-var W Bool)
(declare-var X Bool)
(declare-var Y Bool)
(declare-var Z Real)
(declare-var A1 Real)
(declare-var B1 Real)
(declare-var C1 Bool)
(declare-var D1 Bool)
(declare-var E1 Bool)
(declare-var F1 Bool)
(declare-var G1 Bool)
(declare-var H1 Bool)
(declare-var I1 Bool)
(declare-var J1 Bool)
(declare-var K1 Bool)
(declare-var L1 Real)
(declare-var M1 Real)
(declare-var N1 Real)
(declare-var O1 Real)
(declare-var P1 Real)
(declare-var Q1 Real)
(declare-var R1 Bool)
(declare-var S1 Bool)
(declare-var T1 Bool)
(declare-var U1 Bool)
(declare-var V1 Bool)
(declare-var W1 Bool)
(declare-var X1 Bool)
(declare-var Y1 Bool)
(declare-var Z1 Real)
(declare-var A2 Real)
(declare-var B2 Bool)
(declare-var C2 Real)
(declare-var D2 Bool)
(declare-var E2 Bool)
(declare-var F2 Bool)
(declare-var G2 Bool)
(declare-var H2 Real)
(declare-var I2 Bool)
(declare-var J2 Bool)
(declare-var K2 Bool)
(declare-var L2 Real)
(declare-var M2 Real)
(declare-var N2 Real)
(declare-var O2 Real)
(declare-var P2 Real)
(declare-var Q2 Bool)
(declare-var R2 Real)
(declare-var S2 Real)
(declare-var T2 Real)
(declare-var U2 Real)
(rule cp-rel-entry)
(rule (let ((a!1 (or (and K
                    Y
                    (not Q)
                    (and (<= Z H) (>= Z H))
                    (and (<= A1 I) (>= A1 I))
                    (and (<= B1 J) (>= B1 J)))
               (and O
                    C1
                    T
                    (and (<= Z H) (>= Z H))
                    (and (<= A1 I) (>= A1 I))
                    (and (<= B1 J) (>= B1 J)))
               (and U
                    D1
                    (and (<= Z 2.0) (>= Z 2.0))
                    (and (<= A1 I) (>= A1 I))
                    (and (<= B1 1.0) (>= B1 1.0)))
               (and R
                    E1
                    (not W)
                    (and (<= Z 2.0) (>= Z 2.0))
                    (<= A1 0.0)
                    (>= A1 0.0)
                    (and (<= B1 1.0) (>= B1 1.0)))))
      (a!2 (=> X
               (or (and Y (not C1) (not D1) (not E1))
                   (and C1 (not Y) (not D1) (not E1))
                   (and D1 (not Y) (not C1) (not E1))
                   (and E1 (not Y) (not C1) (not D1)))))
      (a!3 (or (and F1
                    I1
                    (and (<= N1 Z) (>= N1 Z))
                    (and (<= O1 A1) (>= O1 A1))
                    (and (<= P1 B1) (>= P1 B1)))
               (and X
                    (not H1)
                    (and (<= N1 Z) (>= N1 Z))
                    (and (<= O1 A1) (>= O1 A1))
                    (and (<= P1 B1) (>= P1 B1))))))
(let ((a!4 (and (cp-rel-bb.i33.i.us_proc N1 O1 P1 C B A)
                (=> K (and L M (not N)))
                (=> K M)
                (=> O (and K P Q))
                (=> O P)
                (=> R (and O S (not T)))
                (=> R S)
                (=> U (and R V W))
                (=> U V)
                (=> X a!1)
                a!2
                (=> F1 (and X G1 H1))
                (=> F1 G1)
                a!3
                (= J1 (not (= I 0.0)))
                (= K1 (not (= H 0.0)))
                (= N (and J1 K1))
                (= Q (= H 0.0))
                (= T (= L1 0.0))
                (= W (= J 1.0))
                (= H1 (= A1 0.0))
                (= I1 (= M1 0.0)))))
  (=> a!4 (cp-rel-bb.i33.i.us_proc H I J C B A)))))
(rule (let ((a!1 (=> C1
               (or (and T D1 (<= M1 N1) (>= M1 N1))
                   (and O E1 (not V) (<= M1 L1) (>= M1 L1)))))
      (a!2 (=> C1 (or (and D1 (not E1)) (and E1 (not D1)))))
      (a!3 (or (and K
                    J1
                    (not S)
                    (and (<= O1 I) (>= O1 I))
                    (and (<= P1 J) (>= P1 J))
                    (and (<= Q1 Z) (>= Q1 Z)))
               (and Q
                    K1
                    Y
                    (and (<= O1 I) (>= O1 I))
                    (and (<= P1 J) (>= P1 J))
                    (and (<= Q1 Z) (>= Q1 Z)))
               (and F1
                    R1
                    (and (<= O1 2.0) (>= O1 2.0))
                    (and (<= P1 J) (>= P1 J))
                    (and (<= Q1 1.0) (>= Q1 1.0)))
               (and W
                    S1
                    (not H1)
                    (and (<= O1 2.0) (>= O1 2.0))
                    (<= P1 0.0)
                    (>= P1 0.0)
                    (and (<= Q1 1.0) (>= Q1 1.0)))))
      (a!4 (=> I1
               (or (and J1 (not K1) (not R1) (not S1))
                   (and K1 (not J1) (not R1) (not S1))
                   (and R1 (not J1) (not K1) (not S1))
                   (and S1 (not J1) (not K1) (not R1))))))
(let ((a!5 (and (=> K (and L M (not N)))
                (=> K M)
                (=> O
                    (and L
                         P
                         N
                         (<= A1 Z)
                         (>= A1 Z)
                         (<= B1 J)
                         (>= B1 J)
                         (<= L1 I)
                         (>= L1 I)))
                (=> O P)
                (=> Q (and K R S))
                (=> Q R)
                (=> T (and O U V))
                (=> T U)
                (=> W (and Q X (not Y)))
                (=> W X)
                a!1
                a!2
                (=> F1 (and W G1 H1))
                (=> F1 G1)
                (=> I1 a!3)
                a!4
                (=> T1 (and I1 U1 V1))
                (=> T1 U1)
                (=> W1
                    (and T1
                         X1
                         (not Y1)
                         (<= Z1 Q1)
                         (>= Z1 Q1)
                         (<= A2 O1)
                         (>= A2 O1)))
                (=> W1 X1)
                (or (and C1
                         (not B2)
                         (<= A M1)
                         (>= A M1)
                         (<= B B1)
                         (>= B B1)
                         (<= C C2)
                         (>= C C2)
                         (<= D A1)
                         (>= D A1))
                    (and W1
                         (not G)
                         (<= A A2)
                         (>= A A2)
                         (<= B 2.0)
                         (>= B 2.0)
                         (<= C 1.0)
                         (>= C 1.0)
                         (<= D Z1)
                         (>= D Z1)))
                (= D2 (not (= J 0.0)))
                (= E2 (not (= I 0.0)))
                (= N (and D2 E2))
                (= S (= I 0.0))
                (= F2 (not (= B1 0.0)))
                (= G2 (not (= L1 0.0)))
                (= V (and G2 F2))
                (= Y (= H2 0.0))
                (= I2 (= A1 1.0))
                (= N1 (ite I2 0.0 L1))
                (= H1 (= Z 1.0))
                (= J2 (not (= B1 0.0)))
                (= K2 (not (= M1 0.0)))
                (= B2 (and J2 K2))
                (= V1 (= P1 0.0))
                (= Y1 (= L2 0.0)))))
  (=> a!5 (cp-rel-bb.i33.i.us_proc I J Z I J Z)))))
(rule (let ((a!1 (=> R
               (or (and O S (<= Z A1) (>= Z A1))
                   (and M T (not Q) (<= Z J) (>= Z J)))))
      (a!2 (=> R (or (and S (not T)) (and T (not S))))))
(let ((a!3 (and (=> M
                    (and K
                         N
                         L
                         (<= H F)
                         (>= H F)
                         (<= I E)
                         (>= I E)
                         (<= J D)
                         (>= J D)))
                (=> M N)
                (=> O (and M P Q))
                (=> O P)
                a!1
                a!2
                R
                U
                (= V (not (= E 0.0)))
                (= W (not (= D 0.0)))
                (= L (and V W))
                (= X (not (= I 0.0)))
                (= Y (not (= J 0.0)))
                (= Q (and Y X))
                (= C1 (= H 1.0))
                (= A1 (ite C1 0.0 J))
                (= D1 (not (= I 0.0)))
                (= E1 (not (= Z 0.0)))
                (= U (and D1 E1)))))
  (=> a!3 (cp-rel-bb.i33.i.us_proc D E F D E F)))))
(rule (let ((a!1 (or (and K
                    Y
                    (not Q)
                    (and (<= H D) (>= H D))
                    (and (<= I E) (>= I E))
                    (and (<= J F) (>= J F)))
               (and O
                    C1
                    T
                    (and (<= H D) (>= H D))
                    (and (<= I E) (>= I E))
                    (and (<= J F) (>= J F)))
               (and U
                    D1
                    (and (<= H 2.0) (>= H 2.0))
                    (and (<= I E) (>= I E))
                    (and (<= J 1.0) (>= J 1.0)))
               (and R
                    E1
                    (not W)
                    (and (<= H 2.0) (>= H 2.0))
                    (<= I 0.0)
                    (>= I 0.0)
                    (and (<= J 1.0) (>= J 1.0)))))
      (a!2 (=> X
               (or (and Y (not C1) (not D1) (not E1))
                   (and C1 (not Y) (not D1) (not E1))
                   (and D1 (not Y) (not C1) (not E1))
                   (and E1 (not Y) (not C1) (not D1))))))
(let ((a!3 (and (=> K (and L M (not N)))
                (=> K M)
                (=> O (and K P Q))
                (=> O P)
                (=> R (and O S (not T)))
                (=> R S)
                (=> U (and R V W))
                (=> U V)
                (=> X a!1)
                a!2
                (=> F1 (and X G1 H1))
                (=> F1 G1)
                (=> I1
                    (and F1 J1 (not K1) (<= Z J) (>= Z J) (<= A1 H) (>= A1 H)))
                (=> I1 J1)
                I1
                G
                (= R1 (not (= E 0.0)))
                (= S1 (not (= D 0.0)))
                (= N (and R1 S1))
                (= Q (= D 0.0))
                (= T (= B1 0.0))
                (= W (= F 1.0))
                (= H1 (= I 0.0))
                (= K1 (= L1 0.0)))))
  (=> a!3 (cp-rel-bb.i33.i.us_proc D E F D E F)))))
(rule (=> (and cp-rel-__UFO__0_proc K) cp-rel-__UFO__0_proc))
(rule (let ((a!1 (or (and R V (<= J 2.0) (>= J 2.0) (<= Z 1.0) (>= Z 1.0))
               (and O W T (and (<= J H) (>= J H)) (and (<= Z I) (>= Z I)))
               (and M X (not Q) (and (<= J H) (>= J H)) (and (<= Z I) (>= Z I)))))
      (a!2 (=> U
               (or (and V (not W) (not X))
                   (and W (not V) (not X))
                   (and X (not V) (not W)))))
      (a!3 (or (and Y E1 (and (<= L1 J) (>= L1 J)) (and (<= M1 Z) (>= M1 Z)))
               (and U
                    (not D1)
                    (and (<= L1 J) (>= L1 J))
                    (and (<= M1 Z) (>= M1 Z))))))
(let ((a!4 (and (cp-rel-bb.i33.i_proc E L1 M1 C B A)
                (=> M (and K N (not L)))
                (=> M N)
                (=> O (and M P Q))
                (=> O P)
                (=> R (and O S (not T)))
                (=> R S)
                (=> U a!1)
                a!2
                (=> Y (and U C1 D1))
                (=> Y C1)
                a!3
                (= F1 (not (= E 0.0)))
                (= G1 (not (= H 0.0)))
                (= L (and F1 G1))
                (= Q (= H 0.0))
                (= T (= A1 0.0))
                (= D1 (= E 0.0))
                (= E1 (= B1 0.0)))))
  (=> a!4 (cp-rel-bb.i33.i_proc E H I C B A)))))
(rule (let ((a!1 (=> O
               (or (and L P (<= A1 B1) (>= A1 B1))
                   (and K Q (not N) (<= A1 Z) (>= A1 Z)))))
      (a!2 (=> O (or (and P (not Q)) (and Q (not P)))))
      (a!3 (or (and J1 T1 (<= Q1 2.0) (>= Q1 2.0) (<= Z1 1.0) (>= Z1 1.0))
               (and G1
                    U1
                    R1
                    (and (<= Q1 O1) (>= Q1 O1))
                    (and (<= Z1 P1) (>= Z1 P1)))
               (and E1
                    V1
                    (not I1)
                    (and (<= Q1 O1) (>= Q1 O1))
                    (and (<= Z1 P1) (>= Z1 P1)))))
      (a!4 (=> S1
               (or (and T1 (not U1) (not V1))
                   (and U1 (not T1) (not V1))
                   (and V1 (not T1) (not U1))))))
(let ((a!5 (and (=> K
                    (and Y
                         C1
                         D1
                         (<= I P1)
                         (>= I P1)
                         (<= J F)
                         (>= J F)
                         (<= Z O1)
                         (>= Z O1)))
                (=> K C1)
                (=> E1 (and Y F1 (not D1)))
                (=> E1 F1)
                (=> L (and K M N))
                (=> L M)
                (=> G1 (and E1 H1 I1))
                (=> G1 H1)
                a!1
                a!2
                (=> J1 (and G1 K1 (not R1)))
                (=> J1 K1)
                (=> S1 a!3)
                a!4
                (=> W1 (and S1 X1 Y1))
                (=> W1 X1)
                (=> R
                    (and W1
                         B2
                         (not D2)
                         (<= L1 Z1)
                         (>= L1 Z1)
                         (<= M1 Q1)
                         (>= M1 Q1)))
                (=> R B2)
                (or (and O
                         (not S)
                         (<= A A1)
                         (>= A A1)
                         (<= B J)
                         (>= B J)
                         (<= C N1)
                         (>= C N1)
                         (<= D I)
                         (>= D I))
                    (and R
                         (not G)
                         (<= A M1)
                         (>= A M1)
                         (<= B 2.0)
                         (>= B 2.0)
                         (<= C 1.0)
                         (>= C 1.0)
                         (<= D L1)
                         (>= D L1)))
                (= E2 (not (= F 0.0)))
                (= F2 (not (= O1 0.0)))
                (= D1 (and E2 F2))
                (= T (not (= J 0.0)))
                (= U (not (= Z 0.0)))
                (= N (and U T))
                (= I1 (= O1 0.0))
                (= V (= I 1.0))
                (= B1 (ite V 0.0 Z))
                (= R1 (= A2 0.0))
                (= W (not (= J 0.0)))
                (= X (not (= A1 0.0)))
                (= S (and W X))
                (= Y1 (= F 0.0))
                (= D2 (= C2 0.0)))))
  (=> a!5 (cp-rel-bb.i33.i_proc F O1 P1 F O1 P1)))))
(rule (let ((a!1 (or (and S W (<= I 2.0) (>= I 2.0) (<= J 1.0) (>= J 1.0))
               (and P X U (and (<= I F) (>= I F)) (and (<= J H) (>= J H)))
               (and N Y (not R) (and (<= I F) (>= I F)) (and (<= J H) (>= J H)))))
      (a!2 (=> V
               (or (and W (not X) (not Y))
                   (and X (not W) (not Y))
                   (and Y (not W) (not X))))))
(let ((a!3 (and (=> N (and L O (not M)))
                (=> N O)
                (=> P (and N Q R))
                (=> P Q)
                (=> S (and P T (not U)))
                (=> S T)
                (=> V a!1)
                a!2
                (=> C1 (and V D1 E1))
                (=> C1 D1)
                (=> K (and C1 F1 (not G1) (<= D J) (>= D J) (<= E I) (>= E I)))
                (=> K F1)
                K
                G
                (= H1 (not (= B 0.0)))
                (= I1 (not (= F 0.0)))
                (= M (and H1 I1))
                (= R (= F 0.0))
                (= U (= Z 0.0))
                (= E1 (= B 0.0))
                (= G1 (= A1 0.0)))))
  (=> a!3 (cp-rel-bb.i33.i_proc B F H B F H)))))
(rule (let ((a!1 (=> S1
               (or (and Y1 R1 (<= M2 L2) (>= M2 L2))
                   (and G2 K1 (not W1) (<= M2 N2) (>= M2 N2)))))
      (a!2 (=> S1 (or (and R1 (not K1)) (and K1 (not R1)))))
      (a!3 (or (and Q2
                    F1
                    (not B2)
                    (and (<= H2 B1) (>= H2 B1))
                    (and (<= C2 L1) (>= C2 L1))
                    (and (<= A2 M1) (>= A2 M1)))
               (and E2
                    E1
                    T1
                    (and (<= H2 B1) (>= H2 B1))
                    (and (<= C2 L1) (>= C2 L1))
                    (and (<= A2 M1) (>= A2 M1)))
               (and J1
                    D1
                    (and (<= H2 2.0) (>= H2 2.0))
                    (and (<= C2 L1) (>= C2 L1))
                    (and (<= A2 1.0) (>= A2 1.0)))
               (and V1
                    C1
                    (not H1)
                    (and (<= H2 2.0) (>= H2 2.0))
                    (<= C2 0.0)
                    (>= C2 0.0)
                    (and (<= A2 1.0) (>= A2 1.0)))))
      (a!4 (=> G1
               (or (and F1 (not E1) (not D1) (not C1))
                   (and E1 (not F1) (not D1) (not C1))
                   (and D1 (not F1) (not E1) (not C1))
                   (and C1 (not F1) (not E1) (not D1))))))
(let ((a!5 (and (cp-rel-bb.i33.i.outer_proc U2 T2 S2 R2 D C B A)
                G
                K
                (<= H A1)
                (>= H A1)
                (<= F Z)
                (>= F Z)
                (<= E I)
                (>= E I)
                (= K (= J 1.0))
                (cp-rel-bb.i33.i.us_proc H F E B1 L1 M1)
                (=> Q2 (and K2 J2 (not I2)))
                (=> Q2 J2)
                (=> G2
                    (and K2
                         F2
                         I2
                         (<= P2 M1)
                         (>= P2 M1)
                         (<= O2 L1)
                         (>= O2 L1)
                         (<= N2 B1)
                         (>= N2 B1)))
                (=> G2 F2)
                (=> E2 (and Q2 D2 B2))
                (=> E2 D2)
                (=> Y1 (and G2 X1 W1))
                (=> Y1 X1)
                (=> V1 (and E2 U1 (not T1)))
                (=> V1 U1)
                a!1
                a!2
                (=> J1 (and V1 I1 H1))
                (=> J1 I1)
                (=> G1 a!3)
                a!4
                (=> Y (and G1 X W))
                (=> Y X)
                (=> V
                    (and Y
                         U
                         (not T)
                         (<= Z1 A2)
                         (>= Z1 A2)
                         (<= Q1 H2)
                         (>= Q1 H2)))
                (=> V U)
                (or (and S1
                         (not S)
                         (<= U2 M2)
                         (>= U2 M2)
                         (<= T2 O2)
                         (>= T2 O2)
                         (<= S2 P1)
                         (>= S2 P1)
                         (<= R2 P2)
                         (>= R2 P2))
                    (and V
                         (not K)
                         (<= U2 Q1)
                         (>= U2 Q1)
                         (<= T2 2.0)
                         (>= T2 2.0)
                         (<= S2 1.0)
                         (>= S2 1.0)
                         (<= R2 Z1)
                         (>= R2 Z1)))
                (= R (not (= L1 0.0)))
                (= Q (not (= B1 0.0)))
                (= I2 (and R Q))
                (= B2 (= B1 0.0))
                (= P (not (= O2 0.0)))
                (= O (not (= N2 0.0)))
                (= W1 (and O P))
                (= T1 (= O1 0.0))
                (= N (= P2 1.0))
                (= L2 (ite N 0.0 N2))
                (= H1 (= M1 1.0))
                (= M (not (= O2 0.0)))
                (= L (not (= M2 0.0)))
                (= S (and M L))
                (= W (= C2 0.0))
                (= T (= N1 0.0)))))
  (=> a!5 (cp-rel-bb.i33.i.outer_proc A1 Z J I D C B A)))))
(rule (let ((a!1 (=> B2
               (or (and F2 Y1 (<= H2 C2) (>= H2 C2))
                   (and G2 X1 (not D2) (<= H2 L2) (>= H2 L2)))))
      (a!2 (=> B2 (or (and Y1 (not X1)) (and X1 (not Y1)))))
      (a!3 (or (and Y U (<= P1 2.0) (>= P1 2.0) (<= O1 1.0) (>= O1 1.0))
               (and E1
                    T
                    W
                    (and (<= P1 B1) (>= P1 B1))
                    (and (<= O1 L1) (>= O1 L1)))
               (and G1
                    S
                    (not C1)
                    (and (<= P1 B1) (>= P1 B1))
                    (and (<= O1 L1) (>= O1 L1)))))
      (a!4 (=> V
               (or (and U (not T) (not S))
                   (and T (not U) (not S))
                   (and S (not U) (not T))))))
(let ((a!5 (and (cp-rel-bb.i33.i.outer_proc S2 R2 P2 O2 D C B A)
                G
                (not K)
                (<= F Z)
                (>= F Z)
                (<= E H)
                (>= E H)
                (= K (= I 1.0))
                (cp-rel-bb.i33.i_proc J F E A1 B1 L1)
                (=> G2
                    (and J1
                         I1
                         H1
                         (<= N2 L1)
                         (>= N2 L1)
                         (<= M2 A1)
                         (>= M2 A1)
                         (<= L2 B1)
                         (>= L2 B1)))
                (=> G2 I1)
                (=> G1 (and J1 F1 (not H1)))
                (=> G1 F1)
                (=> F2 (and G2 E2 D2))
                (=> F2 E2)
                (=> E1 (and G1 D1 C1))
                (=> E1 D1)
                a!1
                a!2
                (=> Y (and E1 X (not W)))
                (=> Y X)
                (=> V a!3)
                a!4
                (=> R (and V Q P))
                (=> R Q)
                (=> W1
                    (and R
                         O
                         (not N)
                         (<= A2 O1)
                         (>= A2 O1)
                         (<= Z1 P1)
                         (>= Z1 P1)))
                (=> W1 O)
                (or (and B2
                         (not V1)
                         (<= S2 H2)
                         (>= S2 H2)
                         (<= R2 M2)
                         (>= R2 M2)
                         (<= P2 Q1)
                         (>= P2 Q1)
                         (<= O2 N2)
                         (>= O2 N2))
                    (and W1
                         (not K)
                         (<= S2 Z1)
                         (>= S2 Z1)
                         (<= R2 2.0)
                         (>= R2 2.0)
                         (<= P2 1.0)
                         (>= P2 1.0)
                         (<= O2 A2)
                         (>= O2 A2)))
                (= M (not (= A1 0.0)))
                (= L (not (= B1 0.0)))
                (= H1 (and M L))
                (= U1 (not (= M2 0.0)))
                (= T1 (not (= L2 0.0)))
                (= D2 (and T1 U1))
                (= C1 (= B1 0.0))
                (= S1 (= N2 1.0))
                (= C2 (ite S1 0.0 L2))
                (= W (= N1 0.0))
                (= R1 (not (= M2 0.0)))
                (= K1 (not (= H2 0.0)))
                (= V1 (and R1 K1))
                (= P (= A1 0.0))
                (= N (= M1 0.0)))))
  (=> a!5 (cp-rel-bb.i33.i.outer_proc Z J I H D C B A)))))
(rule (let ((a!1 (or (and T1
                    W
                    (not H1)
                    (and (<= P1 I) (>= P1 I))
                    (and (<= O1 J) (>= O1 J))
                    (and (<= N1 Z) (>= N1 Z)))
               (and J1
                    V
                    E1
                    (and (<= P1 I) (>= P1 I))
                    (and (<= O1 J) (>= O1 J))
                    (and (<= N1 Z) (>= N1 Z)))
               (and D1
                    U
                    (and (<= P1 2.0) (>= P1 2.0))
                    (and (<= O1 J) (>= O1 J))
                    (and (<= N1 1.0) (>= N1 1.0)))
               (and G1
                    T
                    (not Y)
                    (and (<= P1 2.0) (>= P1 2.0))
                    (<= O1 0.0)
                    (>= O1 0.0)
                    (and (<= N1 1.0) (>= N1 1.0)))))
      (a!2 (=> X
               (or (and W (not V) (not U) (not T))
                   (and V (not W) (not U) (not T))
                   (and U (not W) (not V) (not T))
                   (and T (not W) (not V) (not U))))))
(let ((a!3 (and G
                K
                (<= C H)
                (>= C H)
                (<= B F)
                (>= B F)
                (<= A D)
                (>= A D)
                (= K (= E 1.0))
                (cp-rel-bb.i33.i.us_proc C B A I J Z)
                (=> T1 (and S1 R1 (not K1)))
                (=> T1 R1)
                (=> J1 (and T1 I1 H1))
                (=> J1 I1)
                (=> G1 (and J1 F1 (not E1)))
                (=> G1 F1)
                (=> D1 (and G1 C1 Y))
                (=> D1 C1)
                (=> X a!1)
                a!2
                (=> S (and X R Q))
                (=> S R)
                (=> P
                    (and S
                         O
                         (not N)
                         (<= M1 N1)
                         (>= M1 N1)
                         (<= L1 P1)
                         (>= L1 P1)))
                (=> P O)
                P
                K
                (= M (not (= J 0.0)))
                (= L (not (= I 0.0)))
                (= K1 (and M L))
                (= H1 (= I 0.0))
                (= E1 (= B1 0.0))
                (= Y (= Z 1.0))
                (= Q (= O1 0.0))
                (= N (= A1 0.0)))))
  (=> a!3 (cp-rel-bb.i33.i.outer_proc H F E D H F E D)))))
(rule (let ((a!1 (or (and Y U (<= L1 2.0) (>= L1 2.0) (<= B1 1.0) (>= B1 1.0))
               (and E1 T W (and (<= L1 I) (>= L1 I)) (and (<= B1 J) (>= B1 J)))
               (and G1
                    S
                    (not C1)
                    (and (<= L1 I) (>= L1 I))
                    (and (<= B1 J) (>= B1 J)))))
      (a!2 (=> V
               (or (and U (not T) (not S))
                   (and T (not U) (not S))
                   (and S (not U) (not T))))))
(let ((a!3 (and G
                (not K)
                (<= B F)
                (>= B F)
                (<= A C)
                (>= A C)
                (= K (= D 1.0))
                (cp-rel-bb.i33.i_proc E B A H I J)
                (=> G1 (and I1 F1 (not H1)))
                (=> G1 F1)
                (=> E1 (and G1 D1 C1))
                (=> E1 D1)
                (=> Y (and E1 X (not W)))
                (=> Y X)
                (=> V a!1)
                a!2
                (=> R (and V Q P))
                (=> R Q)
                (=> J1
                    (and R
                         O
                         (not N)
                         (<= N1 B1)
                         (>= N1 B1)
                         (<= M1 L1)
                         (>= M1 L1)))
                (=> J1 O)
                J1
                K
                (= M (not (= H 0.0)))
                (= L (not (= I 0.0)))
                (= H1 (and M L))
                (= C1 (= I 0.0))
                (= W (= A1 0.0))
                (= P (= H 0.0))
                (= N (= Z 0.0)))))
  (=> a!3 (cp-rel-bb.i33.i.outer_proc F E D C F E D C)))))
(rule (let ((a!1 (or (and T1
                    W
                    (not H1)
                    (and (<= L1 D) (>= L1 D))
                    (and (<= B1 E) (>= B1 E))
                    (and (<= A1 F) (>= A1 F)))
               (and J1
                    V
                    E1
                    (and (<= L1 D) (>= L1 D))
                    (and (<= B1 E) (>= B1 E))
                    (and (<= A1 F) (>= A1 F)))
               (and D1
                    U
                    (and (<= L1 2.0) (>= L1 2.0))
                    (and (<= B1 E) (>= B1 E))
                    (and (<= A1 1.0) (>= A1 1.0)))
               (and G1
                    T
                    (not Y)
                    (and (<= L1 2.0) (>= L1 2.0))
                    (<= B1 0.0)
                    (>= B1 0.0)
                    (and (<= A1 1.0) (>= A1 1.0)))))
      (a!2 (=> X
               (or (and W (not V) (not U) (not T))
                   (and V (not W) (not U) (not T))
                   (and U (not W) (not V) (not T))
                   (and T (not W) (not V) (not U))))))
(let ((a!3 (and cp-rel-entry
                U1
                (<= Q1 0.0)
                (>= Q1 0.0)
                (<= Z1 0.0)
                (>= Z1 0.0)
                (<= A2 0.0)
                (>= A2 0.0)
                (<= C2 0.0)
                (>= C2 0.0)
                (cp-rel-bb.i33.i.outer_proc Q1 Z1 A2 C2 P1 O1 N1 M1)
                G
                K
                (<= C P1)
                (>= C P1)
                (<= B O1)
                (>= B O1)
                (<= A M1)
                (>= A M1)
                (= K (= N1 1.0))
                (cp-rel-bb.i33.i.us_proc C B A D E F)
                (=> T1 (and S1 R1 (not K1)))
                (=> T1 R1)
                (=> J1 (and T1 I1 H1))
                (=> J1 I1)
                (=> G1 (and J1 F1 (not E1)))
                (=> G1 F1)
                (=> D1 (and G1 C1 Y))
                (=> D1 C1)
                (=> X a!1)
                a!2
                (=> S (and X R Q))
                (=> S R)
                (=> P (and S O (not N) (<= Z A1) (>= Z A1) (<= J L1) (>= J L1)))
                (=> P O)
                P
                K
                (= M (not (= E 0.0)))
                (= L (not (= D 0.0)))
                (= K1 (and M L))
                (= H1 (= D 0.0))
                (= E1 (= I 0.0))
                (= Y (= F 1.0))
                (= Q (= B1 0.0))
                (= N (= H 0.0)))))
  (=> a!3 cp-rel-T1_WAIT.i.i.i))))
(rule (let ((a!1 (or (and Y U (<= J 2.0) (>= J 2.0) (<= I 1.0) (>= I 1.0))
               (and E1 T W (and (<= J D) (>= J D)) (and (<= I E) (>= I E)))
               (and G1
                    S
                    (not C1)
                    (and (<= J D) (>= J D))
                    (and (<= I E) (>= I E)))))
      (a!2 (=> V
               (or (and U (not T) (not S))
                   (and T (not U) (not S))
                   (and S (not U) (not T))))))
(let ((a!3 (and cp-rel-entry
                K1
                (<= O1 0.0)
                (>= O1 0.0)
                (<= P1 0.0)
                (>= P1 0.0)
                (<= Q1 0.0)
                (>= Q1 0.0)
                (<= Z1 0.0)
                (>= Z1 0.0)
                (cp-rel-bb.i33.i.outer_proc O1 P1 Q1 Z1 N1 M1 L1 B1)
                G
                (not K)
                (<= B N1)
                (>= B N1)
                (<= A B1)
                (>= A B1)
                (= K (= L1 1.0))
                (cp-rel-bb.i33.i_proc M1 B A C D E)
                (=> G1 (and I1 F1 (not H1)))
                (=> G1 F1)
                (=> E1 (and G1 D1 C1))
                (=> E1 D1)
                (=> Y (and E1 X (not W)))
                (=> Y X)
                (=> V a!1)
                a!2
                (=> R (and V Q P))
                (=> R Q)
                (=> J1 (and R O (not N) (<= A1 I) (>= A1 I) (<= Z J) (>= Z J)))
                (=> J1 O)
                J1
                K
                (= M (not (= C 0.0)))
                (= L (not (= D 0.0)))
                (= H1 (and M L))
                (= C1 (= D 0.0))
                (= W (= H 0.0))
                (= P (= C 0.0))
                (= N (= F 0.0)))))
  (=> a!3 cp-rel-T1_WAIT.i.i.i))))
(query cp-rel-T1_WAIT.i.i.i)
