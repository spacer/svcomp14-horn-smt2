(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb.i67.i_proc (Bool Real Real Real Real Real Real Real Bool Real Real Real Real Real Real Real))
(declare-rel cp-rel-bb.i67.i.outer_proc (Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real))
(declare-rel cp-rel-__UFO__0_proc ())
(declare-rel cp-rel-T3_WAIT.i.i.i ())
(declare-var A Bool)
(declare-var B Bool)
(declare-var C Real)
(declare-var D Real)
(declare-var E Real)
(declare-var F Real)
(declare-var G Real)
(declare-var H Real)
(declare-var I Real)
(declare-var J Real)
(declare-var K Real)
(declare-var L Real)
(declare-var M Real)
(declare-var N Real)
(declare-var O Real)
(declare-var P Real)
(declare-var Q Real)
(declare-var R Bool)
(declare-var S Bool)
(declare-var T Bool)
(declare-var U Bool)
(declare-var V Bool)
(declare-var W Bool)
(declare-var X Bool)
(declare-var Y Bool)
(declare-var Z Bool)
(declare-var A1 Bool)
(declare-var B1 Bool)
(declare-var C1 Bool)
(declare-var D1 Bool)
(declare-var E1 Bool)
(declare-var F1 Bool)
(declare-var G1 Real)
(declare-var H1 Real)
(declare-var I1 Real)
(declare-var J1 Bool)
(declare-var K1 Bool)
(declare-var L1 Bool)
(declare-var M1 Bool)
(declare-var N1 Bool)
(declare-var O1 Bool)
(declare-var P1 Bool)
(declare-var Q1 Bool)
(declare-var R1 Bool)
(declare-var S1 Bool)
(declare-var T1 Bool)
(declare-var U1 Bool)
(declare-var V1 Bool)
(declare-var W1 Bool)
(declare-var X1 Real)
(declare-var Y1 Real)
(declare-var Z1 Real)
(declare-var A2 Real)
(declare-var B2 Bool)
(declare-var C2 Bool)
(declare-var D2 Bool)
(declare-var E2 Bool)
(declare-var F2 Bool)
(declare-var G2 Bool)
(declare-var H2 Bool)
(declare-var I2 Bool)
(declare-var J2 Bool)
(declare-var K2 Bool)
(declare-var L2 Bool)
(declare-var M2 Bool)
(declare-var N2 Bool)
(declare-var O2 Bool)
(declare-var P2 Real)
(declare-var Q2 Real)
(declare-var R2 Real)
(declare-var S2 Real)
(declare-var T2 Bool)
(declare-var U2 Bool)
(declare-var V2 Bool)
(declare-var W2 Bool)
(declare-var X2 Bool)
(declare-var Y2 Bool)
(declare-var Z2 Bool)
(declare-var A3 Bool)
(declare-var B3 Bool)
(declare-var C3 Bool)
(declare-var D3 Bool)
(declare-var E3 Bool)
(declare-var F3 Bool)
(declare-var G3 Real)
(declare-var H3 Bool)
(declare-var I3 Real)
(declare-var J3 Bool)
(declare-var K3 Real)
(declare-var L3 Real)
(declare-var M3 Real)
(declare-var N3 Real)
(declare-var O3 Real)
(declare-var P3 Real)
(declare-var Q3 Real)
(declare-var R3 Real)
(declare-var S3 Real)
(declare-var T3 Bool)
(declare-var U3 Bool)
(declare-var V3 Bool)
(declare-var W3 Bool)
(declare-var X3 Bool)
(declare-var Y3 Bool)
(declare-var Z3 Bool)
(declare-var A4 Bool)
(declare-var B4 Bool)
(declare-var C4 Bool)
(declare-var D4 Bool)
(declare-var E4 Bool)
(declare-var F4 Bool)
(declare-var G4 Bool)
(declare-var H4 Bool)
(declare-var I4 Bool)
(declare-var J4 Bool)
(declare-var K4 Bool)
(declare-var L4 Bool)
(declare-var M4 Bool)
(declare-var N4 Bool)
(declare-var O4 Bool)
(declare-var P4 Bool)
(declare-var Q4 Bool)
(declare-var R4 Bool)
(declare-var S4 Real)
(declare-var T4 Real)
(declare-var U4 Real)
(declare-var V4 Real)
(declare-var W4 Real)
(declare-var X4 Real)
(declare-var Y4 Real)
(declare-var Z4 Real)
(declare-var A5 Real)
(declare-var B5 Real)
(declare-var C5 Real)
(declare-var D5 Real)
(declare-var E5 Bool)
(declare-var F5 Bool)
(declare-var G5 Real)
(declare-var H5 Real)
(declare-var I5 Real)
(declare-var J5 Real)
(declare-var K5 Real)
(declare-var L5 Real)
(declare-var M5 Real)
(declare-var N5 Real)
(rule cp-rel-entry)
(rule (=> (and cp-rel-__UFO__0_proc R) cp-rel-__UFO__0_proc))
(rule (let ((a!1 (or (and B1
                    F1
                    (and (<= Q 2.0) (>= Q 2.0))
                    (<= G1 H1)
                    (>= G1 H1)
                    (and (<= I1 1.0) (>= I1 1.0)))
               (and Y
                    J1
                    D1
                    (and (<= Q 2.0) (>= Q 2.0))
                    (and (<= G1 K) (>= G1 K))
                    (and (<= I1 1.0) (>= I1 1.0)))
               (and V
                    K1
                    A1
                    (and (<= Q J) (>= Q J))
                    (and (<= G1 K) (>= G1 K))
                    (and (<= I1 P) (>= I1 P)))
               (and T
                    L1
                    (not X)
                    (and (<= Q J) (>= Q J))
                    (and (<= G1 K) (>= G1 K))
                    (and (<= I1 P) (>= I1 P)))))
      (a!2 (=> E1
               (or (and F1 (not J1) (not K1) (not L1))
                   (and J1 (not F1) (not K1) (not L1))
                   (and K1 (not F1) (not J1) (not L1))
                   (and L1 (not F1) (not J1) (not K1)))))
      (a!3 (or (and S1
                    W1
                    (and (<= X1 2.0) (>= X1 2.0))
                    (<= Y1 Z1)
                    (>= Y1 Z1)
                    (and (<= A2 1.0) (>= A2 1.0)))
               (and P1
                    B2
                    (not U1)
                    (and (<= X1 2.0) (>= X1 2.0))
                    (and (<= Y1 L) (>= Y1 L))
                    (and (<= A2 1.0) (>= A2 1.0)))
               (and M1
                    C2
                    R1
                    (and (<= X1 G1) (>= X1 G1))
                    (and (<= Y1 L) (>= Y1 L))
                    (and (<= A2 O) (>= A2 O)))
               (and E1
                    D2
                    (not O1)
                    (and (<= X1 G1) (>= X1 G1))
                    (and (<= Y1 L) (>= Y1 L))
                    (and (<= A2 O) (>= A2 O)))))
      (a!4 (=> V1
               (or (and W1 (not B2) (not C2) (not D2))
                   (and B2 (not W1) (not C2) (not D2))
                   (and C2 (not W1) (not B2) (not D2))
                   (and D2 (not W1) (not B2) (not C2)))))
      (a!5 (or (and K2
                    O2
                    (and (<= P2 2.0) (>= P2 2.0))
                    (<= Q2 R2)
                    (>= Q2 R2)
                    (and (<= S2 1.0) (>= S2 1.0)))
               (and H2
                    T2
                    (not M2)
                    (and (<= P2 2.0) (>= P2 2.0))
                    (and (<= Q2 M) (>= Q2 M))
                    (and (<= S2 1.0) (>= S2 1.0)))
               (and E2
                    U2
                    J2
                    (and (<= P2 Y1) (>= P2 Y1))
                    (and (<= Q2 M) (>= Q2 M))
                    (and (<= S2 N) (>= S2 N)))
               (and V1
                    V2
                    (not G2)
                    (and (<= P2 Y1) (>= P2 Y1))
                    (and (<= Q2 M) (>= Q2 M))
                    (and (<= S2 N) (>= S2 N)))))
      (a!6 (=> N2
               (or (and O2 (not T2) (not U2) (not V2))
                   (and T2 (not O2) (not U2) (not V2))
                   (and U2 (not O2) (not T2) (not V2))
                   (and V2 (not O2) (not T2) (not U2)))))
      (a!7 (or (and W2
                    Z2
                    (and (<= M3 Q) (>= M3 Q))
                    (and (<= N3 X1) (>= N3 X1))
                    (and (<= O3 P2) (>= O3 P2))
                    (and (<= P3 Q2) (>= P3 Q2))
                    (and (<= Q3 S2) (>= Q3 S2))
                    (and (<= R3 A2) (>= R3 A2))
                    (and (<= S3 I1) (>= S3 I1)))
               (and N2
                    (not Y2)
                    (and (<= M3 Q) (>= M3 Q))
                    (and (<= N3 X1) (>= N3 X1))
                    (and (<= O3 P2) (>= O3 P2))
                    (and (<= P3 Q2) (>= P3 Q2))
                    (and (<= Q3 S2) (>= Q3 S2))
                    (and (<= R3 A2) (>= R3 A2))
                    (and (<= S3 I1) (>= S3 I1))))))
(let ((a!8 (and (cp-rel-bb.i67.i_proc B M3 N3 O3 P3 Q3 R3 S3 A I H G F E D C)
                (=> T (and R U (not S)))
                (=> T U)
                (=> V (and T W X))
                (=> V W)
                (=> Y (and V Z (not A1)))
                (=> Y Z)
                (=> B1 (and Y C1 (not D1)))
                (=> B1 C1)
                (=> E1 a!1)
                a!2
                (=> M1 (and E1 N1 O1))
                (=> M1 N1)
                (=> P1 (and M1 Q1 (not R1)))
                (=> P1 Q1)
                (=> S1 (and P1 T1 U1))
                (=> S1 T1)
                (=> V1 a!3)
                a!4
                (=> E2 (and V1 F2 G2))
                (=> E2 F2)
                (=> H2 (and E2 I2 (not J2)))
                (=> H2 I2)
                (=> K2 (and H2 L2 M2))
                (=> K2 L2)
                (=> N2 a!5)
                a!6
                (=> W2 (and N2 X2 Y2))
                (=> W2 X2)
                a!7
                (= X (= J 0.0))
                (= A3 (= K 0.0))
                (= B3 (= L 0.0))
                (= C3 (or A3 X))
                (= D3 (or C3 B3))
                (= E3 (not (= M 0.0)))
                (= F3 (xor D3 true))
                (= S (and E3 F3))
                (= A1 (= G3 0.0))
                (= D1 (= P 1.0))
                (= H3 (= O 1.0))
                (= H1 (ite H3 0.0 K))
                (= O1 (= G1 0.0))
                (= R1 (= I3 0.0))
                (= U1 (= O 1.0))
                (= J3 (= N 1.0))
                (= Z1 (ite J3 0.0 L))
                (= G2 (= Y1 0.0))
                (= J2 (= K3 0.0))
                (= M2 (= N 1.0))
                (= R2 (ite B 0.0 M))
                (= Y2 (= Q2 0.0))
                (= Z2 (= L3 0.0)))))
  (=> a!8 (cp-rel-bb.i67.i_proc B J K L M N O P A I H G F E D C)))))
(rule (let ((a!1 (=> C1
               (or (and W D1 (<= G1 H1) (>= G1 H1))
                   (and B E1 (not Y) (<= G1 K) (>= G1 K)))))
      (a!2 (=> C1 (or (and D1 (not E1)) (and E1 (not D1)))))
      (a!3 (or (and L1
                    P1
                    (and (<= I1 2.0) (>= I1 2.0))
                    (<= X1 Y1)
                    (>= X1 Y1)
                    (and (<= Z1 1.0) (>= Z1 1.0)))
               (and F1
                    Q1
                    N1
                    (and (<= I1 2.0) (>= I1 2.0))
                    (and (<= X1 L) (>= X1 L))
                    (and (<= Z1 1.0) (>= Z1 1.0)))
               (and Z
                    R1
                    K1
                    (and (<= I1 K) (>= I1 K))
                    (and (<= X1 L) (>= X1 L))
                    (and (<= Z1 Q) (>= Z1 Q)))
               (and U
                    S1
                    (not B1)
                    (and (<= I1 K) (>= I1 K))
                    (and (<= X1 L) (>= X1 L))
                    (and (<= Z1 Q) (>= Z1 Q)))))
      (a!4 (=> O1
               (or (and P1 (not Q1) (not R1) (not S1))
                   (and Q1 (not P1) (not R1) (not S1))
                   (and R1 (not P1) (not Q1) (not S1))
                   (and S1 (not P1) (not Q1) (not R1)))))
      (a!5 (or (and D2
                    H2
                    (and (<= A2 2.0) (>= A2 2.0))
                    (<= P2 Q2)
                    (>= P2 Q2)
                    (and (<= R2 1.0) (>= R2 1.0)))
               (and W1
                    I2
                    (not F2)
                    (and (<= A2 2.0) (>= A2 2.0))
                    (and (<= P2 M) (>= P2 M))
                    (and (<= R2 1.0) (>= R2 1.0)))
               (and T1
                    J2
                    C2
                    (and (<= A2 X1) (>= A2 X1))
                    (and (<= P2 M) (>= P2 M))
                    (and (<= R2 P) (>= R2 P)))
               (and O1
                    K2
                    (not V1)
                    (and (<= A2 X1) (>= A2 X1))
                    (and (<= P2 M) (>= P2 M))
                    (and (<= R2 P) (>= R2 P)))))
      (a!6 (=> G2
               (or (and H2 (not I2) (not J2) (not K2))
                   (and I2 (not H2) (not J2) (not K2))
                   (and J2 (not H2) (not I2) (not K2))
                   (and K2 (not H2) (not I2) (not J2)))))
      (a!7 (or (and V2
                    Z2
                    (and (<= S2 2.0) (>= S2 2.0))
                    (<= G3 I3)
                    (>= G3 I3)
                    (and (<= K3 1.0) (>= K3 1.0)))
               (and O2
                    A3
                    (not X2)
                    (and (<= S2 2.0) (>= S2 2.0))
                    (and (<= G3 N) (>= G3 N))
                    (and (<= K3 1.0) (>= K3 1.0)))
               (and L2
                    B3
                    U2
                    (and (<= S2 P2) (>= S2 P2))
                    (and (<= G3 N) (>= G3 N))
                    (and (<= K3 O) (>= K3 O)))
               (and G2
                    C3
                    (not N2)
                    (and (<= S2 P2) (>= S2 P2))
                    (and (<= G3 N) (>= G3 N))
                    (and (<= K3 O) (>= K3 O)))))
      (a!8 (=> Y2
               (or (and Z2 (not A3) (not B3) (not C3))
                   (and A3 (not Z2) (not B3) (not C3))
                   (and B3 (not Z2) (not A3) (not C3))
                   (and C3 (not Z2) (not A3) (not B3))))))
(let ((a!9 (and (=> B (and R S T))
                (=> B S)
                (=> U (and R V (not T)))
                (=> U V)
                (=> W (and B X Y))
                (=> W X)
                (=> Z (and U A1 B1))
                (=> Z A1)
                a!1
                a!2
                (=> F1 (and Z J1 (not K1)))
                (=> F1 J1)
                (=> L1 (and F1 M1 (not N1)))
                (=> L1 M1)
                (=> O1 a!3)
                a!4
                (=> T1 (and O1 U1 V1))
                (=> T1 U1)
                (=> W1 (and T1 B2 (not C2)))
                (=> W1 B2)
                (=> D2 (and W1 E2 F2))
                (=> D2 E2)
                (=> G2 a!5)
                a!6
                (=> L2 (and G2 M2 N2))
                (=> L2 M2)
                (=> O2 (and L2 T2 (not U2)))
                (=> O2 T2)
                (=> V2 (and O2 W2 X2))
                (=> V2 W2)
                (=> Y2 a!7)
                a!8
                (=> D3 (and Y2 E3 F3))
                (=> D3 E3)
                (=> H3 (and D3 J3 (not T3)))
                (=> H3 J3)
                (or (and C1
                         (not U3)
                         (<= C G1)
                         (>= C G1)
                         (<= D L)
                         (>= D L)
                         (<= E M)
                         (>= E M)
                         (<= F N)
                         (>= F N)
                         (<= G L3)
                         (>= G L3)
                         (<= H O)
                         (>= H O)
                         (<= I P)
                         (>= I P)
                         (<= J Q)
                         (>= J Q))
                    (and H3
                         (not A)
                         (<= C I1)
                         (>= C I1)
                         (<= D A2)
                         (>= D A2)
                         (<= E S2)
                         (>= E S2)
                         (<= F 2.0)
                         (>= F 2.0)
                         (<= G 1.0)
                         (>= G 1.0)
                         (<= H K3)
                         (>= H K3)
                         (<= I R2)
                         (>= I R2)
                         (<= J Z1)
                         (>= J Z1)))
                (= B1 (= K 0.0))
                (= V3 (= L 0.0))
                (= W3 (= M 0.0))
                (= X3 (or V3 B1))
                (= Y3 (or X3 W3))
                (= Z3 (not (= N 0.0)))
                (= A4 (xor Y3 true))
                (= T (and Z3 A4))
                (= B4 (= K 0.0))
                (= C4 (= L 0.0))
                (= D4 (= M 0.0))
                (= E4 (or C4 D4))
                (= F4 (or E4 B4))
                (= G4 (not (= N 0.0)))
                (= H4 (xor F4 true))
                (= Y (and G4 H4))
                (= I4 (= Q 1.0))
                (= H1 (ite I4 0.0 K))
                (= K1 (= M3 0.0))
                (= J4 (= G1 0.0))
                (= K4 (= L 0.0))
                (= L4 (= M 0.0))
                (= M4 (or K4 J4))
                (= N4 (or M4 L4))
                (= O4 (not (= N 0.0)))
                (= P4 (xor N4 true))
                (= U3 (and O4 P4))
                (= N1 (= Q 1.0))
                (= Q4 (= P 1.0))
                (= Y1 (ite Q4 0.0 L))
                (= V1 (= X1 0.0))
                (= C2 (= N3 0.0))
                (= F2 (= P 1.0))
                (= R4 (= O 1.0))
                (= Q2 (ite R4 0.0 M))
                (= N2 (= P2 0.0))
                (= U2 (= O3 0.0))
                (= X2 (= O 1.0))
                (= I3 (ite A 0.0 N))
                (= F3 (= G3 0.0))
                (= T3 (= P3 0.0)))))
  (=> a!9 (cp-rel-bb.i67.i_proc A K L M N O P Q A K L M N O P Q)))))
(rule (let ((a!1 (or (and A1
                    E1
                    (and (<= J 2.0) (>= J 2.0))
                    (<= K L)
                    (>= K L)
                    (and (<= M 1.0) (>= M 1.0)))
               (and X
                    F1
                    C1
                    (and (<= J 2.0) (>= J 2.0))
                    (and (<= K D) (>= K D))
                    (and (<= M 1.0) (>= M 1.0)))
               (and U
                    J1
                    Z
                    (and (<= J C) (>= J C))
                    (and (<= K D) (>= K D))
                    (and (<= M I) (>= M I)))
               (and S
                    K1
                    (not W)
                    (and (<= J C) (>= J C))
                    (and (<= K D) (>= K D))
                    (and (<= M I) (>= M I)))))
      (a!2 (=> D1
               (or (and E1 (not F1) (not J1) (not K1))
                   (and F1 (not E1) (not J1) (not K1))
                   (and J1 (not E1) (not F1) (not K1))
                   (and K1 (not E1) (not F1) (not J1)))))
      (a!3 (or (and R1
                    V1
                    (and (<= N 2.0) (>= N 2.0))
                    (<= O P)
                    (>= O P)
                    (and (<= Q 1.0) (>= Q 1.0)))
               (and O1
                    W1
                    (not T1)
                    (and (<= N 2.0) (>= N 2.0))
                    (and (<= O E) (>= O E))
                    (and (<= Q 1.0) (>= Q 1.0)))
               (and L1
                    B2
                    Q1
                    (and (<= N K) (>= N K))
                    (and (<= O E) (>= O E))
                    (and (<= Q H) (>= Q H)))
               (and D1
                    C2
                    (not N1)
                    (and (<= N K) (>= N K))
                    (and (<= O E) (>= O E))
                    (and (<= Q H) (>= Q H)))))
      (a!4 (=> U1
               (or (and V1 (not W1) (not B2) (not C2))
                   (and W1 (not V1) (not B2) (not C2))
                   (and B2 (not V1) (not W1) (not C2))
                   (and C2 (not V1) (not W1) (not B2)))))
      (a!5 (or (and J2
                    N2
                    (and (<= G1 2.0) (>= G1 2.0))
                    (<= H1 I1)
                    (>= H1 I1)
                    (and (<= X1 1.0) (>= X1 1.0)))
               (and G2
                    O2
                    (not L2)
                    (and (<= G1 2.0) (>= G1 2.0))
                    (and (<= H1 F) (>= H1 F))
                    (and (<= X1 1.0) (>= X1 1.0)))
               (and D2
                    T2
                    I2
                    (and (<= G1 O) (>= G1 O))
                    (and (<= H1 F) (>= H1 F))
                    (and (<= X1 G) (>= X1 G)))
               (and U1
                    U2
                    (not F2)
                    (and (<= G1 O) (>= G1 O))
                    (and (<= H1 F) (>= H1 F))
                    (and (<= X1 G) (>= X1 G)))))
      (a!6 (=> M2
               (or (and N2 (not O2) (not T2) (not U2))
                   (and O2 (not N2) (not T2) (not U2))
                   (and T2 (not N2) (not O2) (not U2))
                   (and U2 (not N2) (not O2) (not T2))))))
(let ((a!7 (and (=> S (and B T (not R)))
                (=> S T)
                (=> U (and S V W))
                (=> U V)
                (=> X (and U Y (not Z)))
                (=> X Y)
                (=> A1 (and X B1 (not C1)))
                (=> A1 B1)
                (=> D1 a!1)
                a!2
                (=> L1 (and D1 M1 N1))
                (=> L1 M1)
                (=> O1 (and L1 P1 (not Q1)))
                (=> O1 P1)
                (=> R1 (and O1 S1 T1))
                (=> R1 S1)
                (=> U1 a!3)
                a!4
                (=> D2 (and U1 E2 F2))
                (=> D2 E2)
                (=> G2 (and D2 H2 (not I2)))
                (=> G2 H2)
                (=> J2 (and G2 K2 L2))
                (=> J2 K2)
                (=> M2 a!5)
                a!6
                (=> V2 (and M2 W2 X2))
                (=> V2 W2)
                (=> Y2 (and V2 Z2 (not A3)))
                (=> Y2 Z2)
                Y2
                A
                (= W (= C 0.0))
                (= B3 (= D 0.0))
                (= C3 (= E 0.0))
                (= D3 (or B3 W))
                (= E3 (or D3 C3))
                (= F3 (not (= F 0.0)))
                (= H3 (xor E3 true))
                (= R (and F3 H3))
                (= Z (= Y1 0.0))
                (= C1 (= I 1.0))
                (= J3 (= H 1.0))
                (= L (ite J3 0.0 D))
                (= N1 (= K 0.0))
                (= Q1 (= Z1 0.0))
                (= T1 (= H 1.0))
                (= T3 (= G 1.0))
                (= P (ite T3 0.0 E))
                (= F2 (= O 0.0))
                (= I2 (= A2 0.0))
                (= L2 (= G 1.0))
                (= I1 (ite A 0.0 F))
                (= X2 (= H1 0.0))
                (= A3 (= P2 0.0)))))
  (=> a!7 (cp-rel-bb.i67.i_proc A C D E F G H I A C D E F G H I)))))
(rule (let ((a!1 (=> H4
               (or (and N4 G4 (<= D5 C5) (>= D5 C5))
                   (and F5 F4 (not L4) (<= D5 Q2) (>= D5 Q2)))))
      (a!2 (=> H4 (or (and G4 (not F4)) (and F4 (not G4)))))
      (a!3 (or (and B4
                    X3
                    (and (<= B5 2.0) (>= B5 2.0))
                    (<= A5 Z4)
                    (>= A5 Z4)
                    (and (<= Y4 1.0) (>= Y4 1.0)))
               (and E4
                    W3
                    Z3
                    (and (<= B5 2.0) (>= B5 2.0))
                    (and (<= A5 R2) (>= A5 R2))
                    (and (<= Y4 1.0) (>= Y4 1.0)))
               (and K4
                    V3
                    C4
                    (and (<= B5 Q2) (>= B5 Q2))
                    (and (<= A5 R2) (>= A5 R2))
                    (and (<= Y4 L3) (>= Y4 L3)))
               (and P4
                    U3
                    (not I4)
                    (and (<= B5 Q2) (>= B5 Q2))
                    (and (<= A5 R2) (>= A5 R2))
                    (and (<= Y4 L3) (>= Y4 L3)))))
      (a!4 (=> Y3
               (or (and X3 (not W3) (not V3) (not U3))
                   (and W3 (not X3) (not V3) (not U3))
                   (and V3 (not X3) (not W3) (not U3))
                   (and U3 (not X3) (not W3) (not V3)))))
      (a!5 (or (and C3
                    Y2
                    (and (<= X4 2.0) (>= X4 2.0))
                    (<= W4 V4)
                    (>= W4 V4)
                    (and (<= U4 1.0) (>= U4 1.0)))
               (and F3
                    X2
                    (not A3)
                    (and (<= X4 2.0) (>= X4 2.0))
                    (and (<= W4 S2) (>= W4 S2))
                    (and (<= U4 1.0) (>= U4 1.0)))
               (and T3
                    W2
                    D3
                    (and (<= X4 A5) (>= X4 A5))
                    (and (<= W4 S2) (>= W4 S2))
                    (and (<= U4 K3) (>= U4 K3)))
               (and Y3
                    V2
                    (not H3)
                    (and (<= X4 A5) (>= X4 A5))
                    (and (<= W4 S2) (>= W4 S2))
                    (and (<= U4 K3) (>= U4 K3)))))
      (a!6 (=> Z2
               (or (and Y2 (not X2) (not W2) (not V2))
                   (and X2 (not Y2) (not W2) (not V2))
                   (and W2 (not Y2) (not X2) (not V2))
                   (and V2 (not Y2) (not X2) (not W2)))))
      (a!7 (or (and K2
                    G2
                    (and (<= T4 2.0) (>= T4 2.0))
                    (<= S4 S3)
                    (>= S4 S3)
                    (and (<= R3 1.0) (>= R3 1.0)))
               (and N2
                    F2
                    (not I2)
                    (and (<= T4 2.0) (>= T4 2.0))
                    (and (<= S4 G3) (>= S4 G3))
                    (and (<= R3 1.0) (>= R3 1.0)))
               (and U2
                    E2
                    L2
                    (and (<= T4 W4) (>= T4 W4))
                    (and (<= S4 G3) (>= S4 G3))
                    (and (<= R3 I3) (>= R3 I3)))
               (and Z2
                    D2
                    (not O2)
                    (and (<= T4 W4) (>= T4 W4))
                    (and (<= S4 G3) (>= S4 G3))
                    (and (<= R3 I3) (>= R3 I3)))))
      (a!8 (=> H2
               (or (and G2 (not F2) (not E2) (not D2))
                   (and F2 (not G2) (not E2) (not D2))
                   (and E2 (not G2) (not F2) (not D2))
                   (and D2 (not G2) (not F2) (not E2))))))
(let ((a!9 (and (cp-rel-bb.i67.i.outer_proc
                  N5
                  M5
                  L5
                  K5
                  J5
                  I5
                  H5
                  G5
                  J
                  I
                  H
                  G
                  F
                  E
                  D
                  C)
                A
                (<= Q P2)
                (>= Q P2)
                (<= P A2)
                (>= P A2)
                (<= O Z1)
                (>= O Z1)
                (<= N Y1)
                (>= N Y1)
                (<= M I1)
                (>= M I1)
                (<= L H1)
                (>= L H1)
                (<= K G1)
                (>= K G1)
                (= B (= X1 1.0))
                (cp-rel-bb.i67.i_proc B Q P O N M L K R Q2 R2 S2 G3 I3 K3 L3)
                (=> F5 (and E5 R4 Q4))
                (=> F5 R4)
                (=> P4 (and E5 O4 (not Q4)))
                (=> P4 O4)
                (=> N4 (and F5 M4 L4))
                (=> N4 M4)
                (=> K4 (and P4 J4 I4))
                (=> K4 J4)
                a!1
                a!2
                (=> E4 (and K4 D4 (not C4)))
                (=> E4 D4)
                (=> B4 (and E4 A4 (not Z3)))
                (=> B4 A4)
                (=> Y3 a!3)
                a!4
                (=> T3 (and Y3 J3 H3))
                (=> T3 J3)
                (=> F3 (and T3 E3 (not D3)))
                (=> F3 E3)
                (=> C3 (and F3 B3 A3))
                (=> C3 B3)
                (=> Z2 a!5)
                a!6
                (=> U2 (and Z2 T2 O2))
                (=> U2 T2)
                (=> N2 (and U2 M2 (not L2)))
                (=> N2 M2)
                (=> K2 (and N2 J2 I2))
                (=> K2 J2)
                (=> H2 a!7)
                a!8
                (=> C2 (and H2 B2 W1))
                (=> C2 B2)
                (=> V1 (and C2 U1 (not T1)))
                (=> V1 U1)
                (or (and H4
                         (not S1)
                         (<= N5 D5)
                         (>= N5 D5)
                         (<= M5 R2)
                         (>= M5 R2)
                         (<= L5 S2)
                         (>= L5 S2)
                         (<= K5 G3)
                         (>= K5 G3)
                         (<= J5 Q3)
                         (>= J5 Q3)
                         (<= I5 I3)
                         (>= I5 I3)
                         (<= H5 K3)
                         (>= H5 K3)
                         (<= G5 L3)
                         (>= G5 L3))
                    (and V1
                         (not R)
                         (<= N5 B5)
                         (>= N5 B5)
                         (<= M5 X4)
                         (>= M5 X4)
                         (<= L5 T4)
                         (>= L5 T4)
                         (<= K5 2.0)
                         (>= K5 2.0)
                         (<= J5 1.0)
                         (>= J5 1.0)
                         (<= I5 R3)
                         (>= I5 R3)
                         (<= H5 U4)
                         (>= H5 U4)
                         (<= G5 Y4)
                         (>= G5 Y4)))
                (= I4 (= Q2 0.0))
                (= R1 (= R2 0.0))
                (= Q1 (= S2 0.0))
                (= P1 (or R1 I4))
                (= O1 (or P1 Q1))
                (= N1 (not (= G3 0.0)))
                (= M1 (xor O1 true))
                (= Q4 (and N1 M1))
                (= L1 (= Q2 0.0))
                (= K1 (= R2 0.0))
                (= J1 (= S2 0.0))
                (= F1 (or K1 J1))
                (= E1 (or F1 L1))
                (= D1 (not (= G3 0.0)))
                (= C1 (xor E1 true))
                (= L4 (and D1 C1))
                (= B1 (= L3 1.0))
                (= C5 (ite B1 0.0 Q2))
                (= C4 (= P3 0.0))
                (= A1 (= D5 0.0))
                (= Z (= R2 0.0))
                (= Y (= S2 0.0))
                (= X (or Z A1))
                (= W (or X Y))
                (= V (not (= G3 0.0)))
                (= U (xor W true))
                (= S1 (and V U))
                (= Z3 (= L3 1.0))
                (= T (= K3 1.0))
                (= Z4 (ite T 0.0 R2))
                (= H3 (= A5 0.0))
                (= D3 (= O3 0.0))
                (= A3 (= K3 1.0))
                (= S (= I3 1.0))
                (= V4 (ite S 0.0 S2))
                (= O2 (= W4 0.0))
                (= L2 (= N3 0.0))
                (= I2 (= I3 1.0))
                (= S3 (ite R 0.0 G3))
                (= W1 (= S4 0.0))
                (= T1 (= M3 0.0)))))
  (=> a!9 (cp-rel-bb.i67.i.outer_proc P2 A2 Z1 Y1 X1 I1 H1 G1 J I H G F E D C)))))
(rule (let ((a!1 (or (and A3
                    W2
                    (and (<= S4 2.0) (>= S4 2.0))
                    (<= S3 R3)
                    (>= S3 R3)
                    (and (<= Q3 1.0) (>= Q3 1.0)))
               (and D3
                    V2
                    Y2
                    (and (<= S4 2.0) (>= S4 2.0))
                    (and (<= S3 H1) (>= S3 H1))
                    (and (<= Q3 1.0) (>= Q3 1.0)))
               (and H3
                    U2
                    B3
                    (and (<= S4 G1) (>= S4 G1))
                    (and (<= S3 H1) (>= S3 H1))
                    (and (<= Q3 A2) (>= Q3 A2)))
               (and T3
                    T2
                    (not E3)
                    (and (<= S4 G1) (>= S4 G1))
                    (and (<= S3 H1) (>= S3 H1))
                    (and (<= Q3 A2) (>= Q3 A2)))))
      (a!2 (=> X2
               (or (and W2 (not V2) (not U2) (not T2))
                   (and V2 (not W2) (not U2) (not T2))
                   (and U2 (not W2) (not V2) (not T2))
                   (and T2 (not W2) (not V2) (not U2)))))
      (a!3 (or (and I2
                    E2
                    (and (<= P3 2.0) (>= P3 2.0))
                    (<= O3 N3)
                    (>= O3 N3)
                    (and (<= M3 1.0) (>= M3 1.0)))
               (and L2
                    D2
                    (not G2)
                    (and (<= P3 2.0) (>= P3 2.0))
                    (and (<= O3 I1) (>= O3 I1))
                    (and (<= M3 1.0) (>= M3 1.0)))
               (and O2
                    C2
                    J2
                    (and (<= P3 S3) (>= P3 S3))
                    (and (<= O3 I1) (>= O3 I1))
                    (and (<= M3 Z1) (>= M3 Z1)))
               (and X2
                    B2
                    (not M2)
                    (and (<= P3 S3) (>= P3 S3))
                    (and (<= O3 I1) (>= O3 I1))
                    (and (<= M3 Z1) (>= M3 Z1)))))
      (a!4 (=> F2
               (or (and E2 (not D2) (not C2) (not B2))
                   (and D2 (not E2) (not C2) (not B2))
                   (and C2 (not E2) (not D2) (not B2))
                   (and B2 (not E2) (not D2) (not C2)))))
      (a!5 (or (and Q1
                    M1
                    (and (<= L3 2.0) (>= L3 2.0))
                    (<= K3 I3)
                    (>= K3 I3)
                    (and (<= G3 1.0) (>= G3 1.0)))
               (and T1
                    L1
                    (not O1)
                    (and (<= L3 2.0) (>= L3 2.0))
                    (and (<= K3 X1) (>= K3 X1))
                    (and (<= G3 1.0) (>= G3 1.0)))
               (and W1
                    K1
                    R1
                    (and (<= L3 O3) (>= L3 O3))
                    (and (<= K3 X1) (>= K3 X1))
                    (and (<= G3 Y1) (>= G3 Y1)))
               (and F2
                    J1
                    (not U1)
                    (and (<= L3 O3) (>= L3 O3))
                    (and (<= K3 X1) (>= K3 X1))
                    (and (<= G3 Y1) (>= G3 Y1)))))
      (a!6 (=> N1
               (or (and M1 (not L1) (not K1) (not J1))
                   (and L1 (not M1) (not K1) (not J1))
                   (and K1 (not M1) (not L1) (not J1))
                   (and J1 (not M1) (not L1) (not K1))))))
(let ((a!7 (and A
                (<= I Q)
                (>= I Q)
                (<= H P)
                (>= H P)
                (<= G O)
                (>= G O)
                (<= F N)
                (>= F N)
                (<= E L)
                (>= E L)
                (<= D K)
                (>= D K)
                (<= C J)
                (>= C J)
                (= B (= M 1.0))
                (cp-rel-bb.i67.i_proc B I H G F E D C R G1 H1 I1 X1 Y1 Z1 A2)
                (=> T3 (and V3 J3 (not U3)))
                (=> T3 J3)
                (=> H3 (and T3 F3 E3))
                (=> H3 F3)
                (=> D3 (and H3 C3 (not B3)))
                (=> D3 C3)
                (=> A3 (and D3 Z2 (not Y2)))
                (=> A3 Z2)
                (=> X2 a!1)
                a!2
                (=> O2 (and X2 N2 M2))
                (=> O2 N2)
                (=> L2 (and O2 K2 (not J2)))
                (=> L2 K2)
                (=> I2 (and L2 H2 G2))
                (=> I2 H2)
                (=> F2 a!3)
                a!4
                (=> W1 (and F2 V1 U1))
                (=> W1 V1)
                (=> T1 (and W1 S1 (not R1)))
                (=> T1 S1)
                (=> Q1 (and T1 P1 O1))
                (=> Q1 P1)
                (=> N1 a!5)
                a!6
                (=> F1 (and N1 E1 D1))
                (=> F1 E1)
                (=> C1 (and F1 B1 (not A1)))
                (=> C1 B1)
                C1
                R
                (= E3 (= G1 0.0))
                (= Z (= H1 0.0))
                (= Y (= I1 0.0))
                (= X (or Z E3))
                (= W (or X Y))
                (= V (not (= X1 0.0)))
                (= U (xor W true))
                (= U3 (and V U))
                (= B3 (= S2 0.0))
                (= Y2 (= A2 1.0))
                (= T (= Z1 1.0))
                (= R3 (ite T 0.0 H1))
                (= M2 (= S3 0.0))
                (= J2 (= R2 0.0))
                (= G2 (= Z1 1.0))
                (= S (= Y1 1.0))
                (= N3 (ite S 0.0 I1))
                (= U1 (= O3 0.0))
                (= R1 (= Q2 0.0))
                (= O1 (= Y1 1.0))
                (= I3 (ite R 0.0 X1))
                (= D1 (= K3 0.0))
                (= A1 (= P2 0.0)))))
  (=> a!7 (cp-rel-bb.i67.i.outer_proc Q P O N M L K J Q P O N M L K J)))))
(rule (let ((a!1 (or (and A3
                    W2
                    (and (<= L3 2.0) (>= L3 2.0))
                    (<= K3 I3)
                    (>= K3 I3)
                    (and (<= G3 1.0) (>= G3 1.0)))
               (and D3
                    V2
                    Y2
                    (and (<= L3 2.0) (>= L3 2.0))
                    (and (<= K3 K) (>= K3 K))
                    (and (<= G3 1.0) (>= G3 1.0)))
               (and H3
                    U2
                    B3
                    (and (<= L3 J) (>= L3 J))
                    (and (<= K3 K) (>= K3 K))
                    (and (<= G3 P) (>= G3 P)))
               (and T3
                    T2
                    (not E3)
                    (and (<= L3 J) (>= L3 J))
                    (and (<= K3 K) (>= K3 K))
                    (and (<= G3 P) (>= G3 P)))))
      (a!2 (=> X2
               (or (and W2 (not V2) (not U2) (not T2))
                   (and V2 (not W2) (not U2) (not T2))
                   (and U2 (not W2) (not V2) (not T2))
                   (and T2 (not W2) (not V2) (not U2)))))
      (a!3 (or (and I2
                    E2
                    (and (<= S2 2.0) (>= S2 2.0))
                    (<= R2 Q2)
                    (>= R2 Q2)
                    (and (<= P2 1.0) (>= P2 1.0)))
               (and L2
                    D2
                    (not G2)
                    (and (<= S2 2.0) (>= S2 2.0))
                    (and (<= R2 L) (>= R2 L))
                    (and (<= P2 1.0) (>= P2 1.0)))
               (and O2
                    C2
                    J2
                    (and (<= S2 K3) (>= S2 K3))
                    (and (<= R2 L) (>= R2 L))
                    (and (<= P2 O) (>= P2 O)))
               (and X2
                    B2
                    (not M2)
                    (and (<= S2 K3) (>= S2 K3))
                    (and (<= R2 L) (>= R2 L))
                    (and (<= P2 O) (>= P2 O)))))
      (a!4 (=> F2
               (or (and E2 (not D2) (not C2) (not B2))
                   (and D2 (not E2) (not C2) (not B2))
                   (and C2 (not E2) (not D2) (not B2))
                   (and B2 (not E2) (not D2) (not C2)))))
      (a!5 (or (and Q1
                    M1
                    (and (<= A2 2.0) (>= A2 2.0))
                    (<= Z1 Y1)
                    (>= Z1 Y1)
                    (and (<= X1 1.0) (>= X1 1.0)))
               (and T1
                    L1
                    (not O1)
                    (and (<= A2 2.0) (>= A2 2.0))
                    (and (<= Z1 M) (>= Z1 M))
                    (and (<= X1 1.0) (>= X1 1.0)))
               (and W1
                    K1
                    R1
                    (and (<= A2 R2) (>= A2 R2))
                    (and (<= Z1 M) (>= Z1 M))
                    (and (<= X1 N) (>= X1 N)))
               (and F2
                    J1
                    (not U1)
                    (and (<= A2 R2) (>= A2 R2))
                    (and (<= Z1 M) (>= Z1 M))
                    (and (<= X1 N) (>= X1 N)))))
      (a!6 (=> N1
               (or (and M1 (not L1) (not K1) (not J1))
                   (and L1 (not M1) (not K1) (not J1))
                   (and K1 (not M1) (not L1) (not J1))
                   (and J1 (not M1) (not L1) (not K1))))))
(let ((a!7 (and cp-rel-entry
                W3
                (<= T4 0.0)
                (>= T4 0.0)
                (<= U4 0.0)
                (>= U4 0.0)
                (<= V4 0.0)
                (>= V4 0.0)
                (<= W4 0.0)
                (>= W4 0.0)
                (<= X4 0.0)
                (>= X4 0.0)
                (<= Y4 0.0)
                (>= Y4 0.0)
                (<= Z4 0.0)
                (>= Z4 0.0)
                (<= A5 0.0)
                (>= A5 0.0)
                (cp-rel-bb.i67.i.outer_proc
                  T4
                  U4
                  V4
                  W4
                  X4
                  Y4
                  Z4
                  A5
                  S4
                  S3
                  R3
                  Q3
                  P3
                  O3
                  N3
                  M3)
                A
                (<= I S4)
                (>= I S4)
                (<= H S3)
                (>= H S3)
                (<= G R3)
                (>= G R3)
                (<= F Q3)
                (>= F Q3)
                (<= E O3)
                (>= E O3)
                (<= D N3)
                (>= D N3)
                (<= C M3)
                (>= C M3)
                (= B (= P3 1.0))
                (cp-rel-bb.i67.i_proc B I H G F E D C R J K L M N O P)
                (=> T3 (and V3 J3 (not U3)))
                (=> T3 J3)
                (=> H3 (and T3 F3 E3))
                (=> H3 F3)
                (=> D3 (and H3 C3 (not B3)))
                (=> D3 C3)
                (=> A3 (and D3 Z2 (not Y2)))
                (=> A3 Z2)
                (=> X2 a!1)
                a!2
                (=> O2 (and X2 N2 M2))
                (=> O2 N2)
                (=> L2 (and O2 K2 (not J2)))
                (=> L2 K2)
                (=> I2 (and L2 H2 G2))
                (=> I2 H2)
                (=> F2 a!3)
                a!4
                (=> W1 (and F2 V1 U1))
                (=> W1 V1)
                (=> T1 (and W1 S1 (not R1)))
                (=> T1 S1)
                (=> Q1 (and T1 P1 O1))
                (=> Q1 P1)
                (=> N1 a!5)
                a!6
                (=> F1 (and N1 E1 D1))
                (=> F1 E1)
                (=> C1 (and F1 B1 (not A1)))
                (=> C1 B1)
                C1
                R
                (= E3 (= J 0.0))
                (= Z (= K 0.0))
                (= Y (= L 0.0))
                (= X (or Z E3))
                (= W (or X Y))
                (= V (not (= M 0.0)))
                (= U (xor W true))
                (= U3 (and V U))
                (= B3 (= I1 0.0))
                (= Y2 (= P 1.0))
                (= T (= O 1.0))
                (= I3 (ite T 0.0 K))
                (= M2 (= K3 0.0))
                (= J2 (= H1 0.0))
                (= G2 (= O 1.0))
                (= S (= N 1.0))
                (= Q2 (ite S 0.0 L))
                (= U1 (= R2 0.0))
                (= R1 (= G1 0.0))
                (= O1 (= N 1.0))
                (= Y1 (ite R 0.0 M))
                (= D1 (= Z1 0.0))
                (= A1 (= Q 0.0)))))
  (=> a!7 cp-rel-T3_WAIT.i.i.i))))
(query cp-rel-T3_WAIT.i.i.i)
