(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb1.i.i_proc (Real Real Real Real Real Real))
(declare-rel cp-rel-bb2.i.i.i.i ())
(declare-var A Real)
(declare-var B Real)
(declare-var C Real)
(declare-var D Real)
(declare-var E Real)
(declare-var F Real)
(declare-var G Real)
(declare-var H Real)
(declare-var I Real)
(declare-var J Bool)
(declare-var K Bool)
(declare-var L Bool)
(declare-var M Bool)
(declare-var N Bool)
(declare-var O Bool)
(declare-var P Real)
(declare-var Q Real)
(declare-var R Bool)
(declare-var S Bool)
(declare-var T Bool)
(declare-var U Bool)
(declare-var V Bool)
(declare-var W Bool)
(declare-var X Real)
(declare-var Y Real)
(declare-var Z Bool)
(declare-var A1 Bool)
(declare-var B1 Bool)
(declare-var C1 Bool)
(declare-var D1 Bool)
(declare-var E1 Bool)
(declare-var F1 Bool)
(declare-var G1 Bool)
(declare-var H1 Bool)
(declare-var I1 Bool)
(declare-var J1 Bool)
(declare-var K1 Bool)
(declare-var L1 Bool)
(declare-var M1 Bool)
(declare-var N1 Real)
(declare-var O1 Real)
(declare-var P1 Real)
(declare-var Q1 Bool)
(declare-var R1 Bool)
(declare-var S1 Real)
(declare-var T1 Bool)
(declare-var U1 Bool)
(declare-var V1 Real)
(declare-var W1 Bool)
(declare-var X1 Real)
(declare-var Y1 Real)
(declare-var Z1 Bool)
(declare-var A2 Bool)
(declare-var B2 Real)
(declare-var C2 Bool)
(declare-var D2 Bool)
(declare-var E2 Bool)
(declare-var F2 Bool)
(declare-var G2 Bool)
(declare-var H2 Bool)
(rule cp-rel-entry)
(rule (let ((a!1 (=> N (or (and J O (<= P Q) (>= P Q)) (and K R M (<= P F) (>= P F)))))
      (a!2 (=> N (or (and O (not R)) (and R (not O)))))
      (a!3 (=> V (or (and S W (<= X Y) (>= X Y)) (and N Z U (<= X D) (>= X D)))))
      (a!4 (=> V (or (and W (not Z)) (and Z (not W)))))
      (a!5 (or (and F1 M1 (and (<= N1 P) (>= N1 P)) (<= O1 P1) (>= O1 P1))
               (and A1
                    Q1
                    (not H1)
                    (and (<= N1 P) (>= N1 P))
                    (and (<= O1 E) (>= O1 E)))
               (and I1 R1 (and (<= N1 S1) (>= N1 S1)) (and (<= O1 E) (>= O1 E)))
               (and D1
                    T1
                    (not K1)
                    (and (<= N1 S1) (>= N1 S1))
                    (<= O1 0.0)
                    (>= O1 0.0))))
      (a!6 (=> L1
               (or (and M1 (not Q1) (not R1) (not T1))
                   (and Q1 (not M1) (not R1) (not T1))
                   (and R1 (not M1) (not Q1) (not T1))
                   (and T1 (not M1) (not Q1) (not R1))))))
(let ((a!7 (and (cp-rel-bb1.i.i_proc G H I C B A)
                (=> J (and K L (not M)))
                (=> J L)
                a!1
                a!2
                (=> S (and N T (not U)))
                (=> S T)
                a!3
                a!4
                (=> A1 (and V B1 C1))
                (=> A1 B1)
                (=> D1 (and V E1 (not C1)))
                (=> D1 E1)
                (=> F1 (and A1 G1 H1))
                (=> F1 G1)
                (=> I1 (and D1 J1 K1))
                (=> I1 J1)
                (=> L1 a!5)
                a!6
                L1
                (not U1)
                (<= G X)
                (>= G X)
                (<= H O1)
                (>= H O1)
                (<= I N1)
                (>= I N1)
                (= M (= V1 0.0))
                (= W1 (< F 2.0))
                (= X1 (ite W1 1.0 0.0))
                (= Q (+ X1 F))
                (= U (= Y1 0.0))
                (= Z1 (= D 0.0))
                (= Y (ite Z1 1.0 0.0))
                (= C1 (= E 0.0))
                (= H1 (> P 1.0))
                (= A2 (> P 0.0))
                (= B2 (+ P (- 1.0)))
                (= S1 (ite A2 B2 P))
                (= K1 (= S1 0.0))
                (= C2 (= X 0.0))
                (= P1 (ite C2 1.0 E))
                (= D2 (= N1 2.0))
                (= E2 (= O1 0.0))
                (= F2 (or E2 D2))
                (= G2 (xor F2 true))
                (= U1 (and C1 G2)))))
  (=> a!7 (cp-rel-bb1.i.i_proc D E F C B A)))))
(rule (let ((a!1 (=> N (or (and J O (<= D E) (>= D E)) (and K R M (<= D C) (>= D C)))))
      (a!2 (=> N (or (and O (not R)) (and R (not O)))))
      (a!3 (=> V (or (and S W (<= F G) (>= F G)) (and N Z U (<= F A) (>= F A)))))
      (a!4 (=> V (or (and W (not Z)) (and Z (not W)))))
      (a!5 (or (and F1 M1 (and (<= H D) (>= H D)) (<= I P) (>= I P))
               (and A1
                    Q1
                    (not H1)
                    (and (<= H D) (>= H D))
                    (and (<= I B) (>= I B)))
               (and I1 R1 (and (<= H Q) (>= H Q)) (and (<= I B) (>= I B)))
               (and D1
                    T1
                    (not K1)
                    (and (<= H Q) (>= H Q))
                    (<= I 0.0)
                    (>= I 0.0))))
      (a!6 (=> L1
               (or (and M1 (not Q1) (not R1) (not T1))
                   (and Q1 (not M1) (not R1) (not T1))
                   (and R1 (not M1) (not Q1) (not T1))
                   (and T1 (not M1) (not Q1) (not R1))))))
(let ((a!7 (and (=> J (and K L (not M)))
                (=> J L)
                a!1
                a!2
                (=> S (and N T (not U)))
                (=> S T)
                a!3
                a!4
                (=> A1 (and V B1 C1))
                (=> A1 B1)
                (=> D1 (and V E1 (not C1)))
                (=> D1 E1)
                (=> F1 (and A1 G1 H1))
                (=> F1 G1)
                (=> I1 (and D1 J1 K1))
                (=> I1 J1)
                (=> L1 a!5)
                a!6
                L1
                U1
                (= M (= X 0.0))
                (= W1 (< C 2.0))
                (= Y (ite W1 1.0 0.0))
                (= E (+ Y C))
                (= U (= N1 0.0))
                (= Z1 (= A 0.0))
                (= G (ite Z1 1.0 0.0))
                (= C1 (= B 0.0))
                (= H1 (> D 1.0))
                (= A2 (> D 0.0))
                (= O1 (+ D (- 1.0)))
                (= Q (ite A2 O1 D))
                (= K1 (= Q 0.0))
                (= C2 (= F 0.0))
                (= P (ite C2 1.0 B))
                (= D2 (= H 2.0))
                (= E2 (= I 0.0))
                (= F2 (or E2 D2))
                (= G2 (xor F2 true))
                (= U1 (and C1 G2)))))
  (=> a!7 (cp-rel-bb1.i.i_proc A B C A B C)))))
(rule (let ((a!1 (=> N (or (and J O (<= A B) (>= A B)) (and K R M (<= A Y) (>= A Y)))))
      (a!2 (=> N (or (and O (not R)) (and R (not O)))))
      (a!3 (=> V
               (or (and S W (<= C D) (>= C D)) (and N Z U (<= C O1) (>= C O1)))))
      (a!4 (=> V (or (and W (not Z)) (and Z (not W)))))
      (a!5 (or (and F1 M1 (and (<= E A) (>= E A)) (<= F G) (>= F G))
               (and A1
                    Q1
                    (not H1)
                    (and (<= E A) (>= E A))
                    (and (<= F N1) (>= F N1)))
               (and I1 R1 (and (<= E H) (>= E H)) (and (<= F N1) (>= F N1)))
               (and D1
                    T1
                    (not K1)
                    (and (<= E H) (>= E H))
                    (<= F 0.0)
                    (>= F 0.0))))
      (a!6 (=> L1
               (or (and M1 (not Q1) (not R1) (not T1))
                   (and Q1 (not M1) (not R1) (not T1))
                   (and R1 (not M1) (not Q1) (not T1))
                   (and T1 (not M1) (not Q1) (not R1))))))
(let ((a!7 (and cp-rel-entry
                H2
                (<= P1 0.0)
                (>= P1 0.0)
                (<= S1 0.0)
                (>= S1 0.0)
                (<= V1 1.0)
                (>= V1 1.0)
                (cp-rel-bb1.i.i_proc P1 S1 V1 O1 N1 Y)
                (=> J (and K L (not M)))
                (=> J L)
                a!1
                a!2
                (=> S (and N T (not U)))
                (=> S T)
                a!3
                a!4
                (=> A1 (and V B1 C1))
                (=> A1 B1)
                (=> D1 (and V E1 (not C1)))
                (=> D1 E1)
                (=> F1 (and A1 G1 H1))
                (=> F1 G1)
                (=> I1 (and D1 J1 K1))
                (=> I1 J1)
                (=> L1 a!5)
                a!6
                L1
                U1
                (= M (= I 0.0))
                (= W1 (< Y 2.0))
                (= P (ite W1 1.0 0.0))
                (= B (+ P Y))
                (= U (= Q 0.0))
                (= Z1 (= O1 0.0))
                (= D (ite Z1 1.0 0.0))
                (= C1 (= N1 0.0))
                (= H1 (> A 1.0))
                (= A2 (> A 0.0))
                (= X (+ A (- 1.0)))
                (= H (ite A2 X A))
                (= K1 (= H 0.0))
                (= C2 (= C 0.0))
                (= G (ite C2 1.0 N1))
                (= D2 (= E 2.0))
                (= E2 (= F 0.0))
                (= F2 (or E2 D2))
                (= G2 (xor F2 true))
                (= U1 (and C1 G2)))))
  (=> a!7 cp-rel-bb2.i.i.i.i))))
(query cp-rel-bb2.i.i.i.i)
