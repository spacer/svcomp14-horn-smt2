(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb2.i.i.i.i ())
(declare-rel cp-rel-bb1.i.i_proc (Real Real Real Real Real Real Real Real))
(declare-var A Real)
(declare-var B Real)
(declare-var C Real)
(declare-var D Real)
(declare-var E Real)
(declare-var F Real)
(declare-var G Real)
(declare-var H Real)
(declare-var I Real)
(declare-var J Real)
(declare-var K Real)
(declare-var L Real)
(declare-var M Bool)
(declare-var N Bool)
(declare-var O Bool)
(declare-var P Bool)
(declare-var Q Bool)
(declare-var R Bool)
(declare-var S Real)
(declare-var T Real)
(declare-var U Bool)
(declare-var V Bool)
(declare-var W Bool)
(declare-var X Bool)
(declare-var Y Bool)
(declare-var Z Bool)
(declare-var A1 Real)
(declare-var B1 Real)
(declare-var C1 Bool)
(declare-var D1 Bool)
(declare-var E1 Bool)
(declare-var F1 Bool)
(declare-var G1 Bool)
(declare-var H1 Bool)
(declare-var I1 Bool)
(declare-var J1 Bool)
(declare-var K1 Bool)
(declare-var L1 Bool)
(declare-var M1 Bool)
(declare-var N1 Bool)
(declare-var O1 Bool)
(declare-var P1 Bool)
(declare-var Q1 Real)
(declare-var R1 Real)
(declare-var S1 Real)
(declare-var T1 Bool)
(declare-var U1 Bool)
(declare-var V1 Real)
(declare-var W1 Bool)
(declare-var X1 Bool)
(declare-var Y1 Bool)
(declare-var Z1 Bool)
(declare-var A2 Bool)
(declare-var B2 Real)
(declare-var C2 Bool)
(declare-var D2 Real)
(declare-var E2 Real)
(declare-var F2 Bool)
(declare-var G2 Bool)
(declare-var H2 Real)
(declare-var I2 Bool)
(declare-var J2 Bool)
(declare-var K2 Bool)
(declare-var L2 Bool)
(declare-var M2 Bool)
(declare-var N2 Bool)
(rule cp-rel-entry)
(rule (let ((a!1 (=> Q (or (and M R (<= S T) (>= S T)) (and N U P (<= S E) (>= S E)))))
      (a!2 (=> Q (or (and R (not U)) (and U (not R)))))
      (a!3 (=> Y
               (or (and V Z (<= A1 B1) (>= A1 B1))
                   (and Q C1 X (<= A1 F) (>= A1 F)))))
      (a!4 (=> Y (or (and Z (not C1)) (and C1 (not Z)))))
      (a!5 (or (and I1 P1 (and (<= Q1 S) (>= Q1 S)) (<= R1 S1) (>= R1 S1))
               (and D1
                    T1
                    (not K1)
                    (and (<= Q1 S) (>= Q1 S))
                    (and (<= R1 G) (>= R1 G)))
               (and L1 U1 (and (<= Q1 V1) (>= Q1 V1)) (and (<= R1 G) (>= R1 G)))
               (and G1
                    W1
                    (not N1)
                    (and (<= Q1 V1) (>= Q1 V1))
                    (<= R1 0.0)
                    (>= R1 0.0))))
      (a!6 (=> O1
               (or (and P1 (not T1) (not U1) (not W1))
                   (and T1 (not P1) (not U1) (not W1))
                   (and U1 (not P1) (not T1) (not W1))
                   (and W1 (not P1) (not T1) (not U1)))))
      (a!7 (or (and X1
                    A2
                    (and (<= I Q1) (>= I Q1))
                    (and (<= J A1) (>= J A1))
                    (and (<= K R1) (>= K R1))
                    (<= L 1.0)
                    (>= L 1.0))
               (and O1
                    Z1
                    (and (<= I Q1) (>= I Q1))
                    (and (<= J A1) (>= J A1))
                    (and (<= K R1) (>= K R1))
                    (<= L 0.0)
                    (>= L 0.0)))))
(let ((a!8 (and (cp-rel-bb1.i.i_proc I J K L D C B A)
                (=> M (and N O (not P)))
                (=> M O)
                a!1
                a!2
                (=> V (and Q W (not X)))
                (=> V W)
                a!3
                a!4
                (=> D1 (and Y E1 F1))
                (=> D1 E1)
                (=> G1 (and Y H1 (not F1)))
                (=> G1 H1)
                (=> I1 (and D1 J1 K1))
                (=> I1 J1)
                (=> L1 (and G1 M1 N1))
                (=> L1 M1)
                (=> O1 a!5)
                a!6
                (=> X1 (and O1 Y1 (not Z1)))
                (=> X1 Y1)
                a!7
                (= P (= B2 0.0))
                (= C2 (< E 2.0))
                (= D2 (ite C2 1.0 0.0))
                (= T (+ D2 E))
                (= X (= E2 0.0))
                (= F2 (= F 0.0))
                (= B1 (ite F2 1.0 0.0))
                (= F1 (= G 0.0))
                (= K1 (> S 1.0))
                (= G2 (> S 0.0))
                (= H2 (+ S (- 1.0)))
                (= V1 (ite G2 H2 S))
                (= I2 (= A1 0.0))
                (= J2 (= V1 0.0))
                (= N1 (and I2 J2))
                (= K2 (= A1 0.0))
                (= S1 (ite K2 1.0 G))
                (= L2 (= A1 0.0))
                (= M2 (= R1 0.0))
                (= Z1 (or M2 L2))
                (= A2 (= H 0.0)))))
  (=> a!8 (cp-rel-bb1.i.i_proc E F G H D C B A)))))
(rule (let ((a!1 (=> Q (or (and M R (<= E F) (>= E F)) (and N U P (<= E A) (>= E A)))))
      (a!2 (=> Q (or (and R (not U)) (and U (not R)))))
      (a!3 (=> Y
               (or (and V Z (<= G H) (>= G H)) (and Q C1 X (<= G B) (>= G B)))))
      (a!4 (=> Y (or (and Z (not C1)) (and C1 (not Z)))))
      (a!5 (or (and I1 P1 (and (<= I E) (>= I E)) (<= J K) (>= J K))
               (and D1
                    T1
                    (not K1)
                    (and (<= I E) (>= I E))
                    (and (<= J C) (>= J C)))
               (and L1 U1 (and (<= I L) (>= I L)) (and (<= J C) (>= J C)))
               (and G1
                    W1
                    (not N1)
                    (and (<= I L) (>= I L))
                    (<= J 0.0)
                    (>= J 0.0))))
      (a!6 (=> O1
               (or (and P1 (not T1) (not U1) (not W1))
                   (and T1 (not P1) (not U1) (not W1))
                   (and U1 (not P1) (not T1) (not W1))
                   (and W1 (not P1) (not T1) (not U1))))))
(let ((a!7 (and (=> M (and N O (not P)))
                (=> M O)
                a!1
                a!2
                (=> V (and Q W (not X)))
                (=> V W)
                a!3
                a!4
                (=> D1 (and Y E1 F1))
                (=> D1 E1)
                (=> G1 (and Y H1 (not F1)))
                (=> G1 H1)
                (=> I1 (and D1 J1 K1))
                (=> I1 J1)
                (=> L1 (and G1 M1 N1))
                (=> L1 M1)
                (=> O1 a!5)
                a!6
                (=> X1 (and O1 Y1 (not Z1)))
                (=> X1 Y1)
                X1
                (not A2)
                (= P (= S 0.0))
                (= C2 (< A 2.0))
                (= T (ite C2 1.0 0.0))
                (= F (+ T A))
                (= X (= A1 0.0))
                (= F2 (= B 0.0))
                (= H (ite F2 1.0 0.0))
                (= F1 (= C 0.0))
                (= K1 (> E 1.0))
                (= G2 (> E 0.0))
                (= B1 (+ E (- 1.0)))
                (= L (ite G2 B1 E))
                (= I2 (= G 0.0))
                (= J2 (= L 0.0))
                (= N1 (and I2 J2))
                (= K2 (= G 0.0))
                (= K (ite K2 1.0 C))
                (= L2 (= G 0.0))
                (= M2 (= J 0.0))
                (= Z1 (or M2 L2))
                (= A2 (= D 0.0)))))
  (=> a!7 (cp-rel-bb1.i.i_proc A B C D A B C D)))))
(rule (let ((a!1 (=> Q
               (or (and M R (<= A B) (>= A B)) (and N U P (<= A B1) (>= A B1)))))
      (a!2 (=> Q (or (and R (not U)) (and U (not R)))))
      (a!3 (=> Y
               (or (and V Z (<= C D) (>= C D)) (and Q C1 X (<= C A1) (>= C A1)))))
      (a!4 (=> Y (or (and Z (not C1)) (and C1 (not Z)))))
      (a!5 (or (and I1 P1 (and (<= E A) (>= E A)) (<= F G) (>= F G))
               (and D1
                    T1
                    (not K1)
                    (and (<= E A) (>= E A))
                    (and (<= F T) (>= F T)))
               (and L1 U1 (and (<= E H) (>= E H)) (and (<= F T) (>= F T)))
               (and G1
                    W1
                    (not N1)
                    (and (<= E H) (>= E H))
                    (<= F 0.0)
                    (>= F 0.0))))
      (a!6 (=> O1
               (or (and P1 (not T1) (not U1) (not W1))
                   (and T1 (not P1) (not U1) (not W1))
                   (and U1 (not P1) (not T1) (not W1))
                   (and W1 (not P1) (not T1) (not U1))))))
(let ((a!7 (and cp-rel-entry
                N2
                (<= Q1 1.0)
                (>= Q1 1.0)
                (<= R1 0.0)
                (>= R1 0.0)
                (<= S1 0.0)
                (>= S1 0.0)
                (<= V1 0.0)
                (>= V1 0.0)
                (cp-rel-bb1.i.i_proc Q1 R1 S1 V1 B1 A1 T S)
                (=> M (and N O (not P)))
                (=> M O)
                a!1
                a!2
                (=> V (and Q W (not X)))
                (=> V W)
                a!3
                a!4
                (=> D1 (and Y E1 F1))
                (=> D1 E1)
                (=> G1 (and Y H1 (not F1)))
                (=> G1 H1)
                (=> I1 (and D1 J1 K1))
                (=> I1 J1)
                (=> L1 (and G1 M1 N1))
                (=> L1 M1)
                (=> O1 a!5)
                a!6
                (=> X1 (and O1 Y1 (not Z1)))
                (=> X1 Y1)
                X1
                (not A2)
                (= P (= I 0.0))
                (= C2 (< B1 2.0))
                (= J (ite C2 1.0 0.0))
                (= B (+ J B1))
                (= X (= K 0.0))
                (= F2 (= A1 0.0))
                (= D (ite F2 1.0 0.0))
                (= F1 (= T 0.0))
                (= K1 (> A 1.0))
                (= G2 (> A 0.0))
                (= L (+ A (- 1.0)))
                (= H (ite G2 L A))
                (= I2 (= C 0.0))
                (= J2 (= H 0.0))
                (= N1 (and I2 J2))
                (= K2 (= C 0.0))
                (= G (ite K2 1.0 T))
                (= L2 (= C 0.0))
                (= M2 (= F 0.0))
                (= Z1 (or M2 L2))
                (= A2 (= S 0.0)))))
  (=> a!7 cp-rel-bb2.i.i.i.i))))
(query cp-rel-bb2.i.i.i.i)
