(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb2.i.i.i.i ())
(declare-rel cp-rel-bb1.i.i_proc (Real Real Real Real Real Real Real Real))
(declare-var A Real)
(declare-var B Real)
(declare-var C Real)
(declare-var D Real)
(declare-var E Real)
(declare-var F Real)
(declare-var G Real)
(declare-var H Real)
(declare-var I Real)
(declare-var J Real)
(declare-var K Real)
(declare-var L Real)
(declare-var M Bool)
(declare-var N Bool)
(declare-var O Bool)
(declare-var P Bool)
(declare-var Q Bool)
(declare-var R Bool)
(declare-var S Real)
(declare-var T Real)
(declare-var U Bool)
(declare-var V Bool)
(declare-var W Bool)
(declare-var X Bool)
(declare-var Y Bool)
(declare-var Z Bool)
(declare-var A1 Real)
(declare-var B1 Real)
(declare-var C1 Bool)
(declare-var D1 Bool)
(declare-var E1 Bool)
(declare-var F1 Bool)
(declare-var G1 Bool)
(declare-var H1 Bool)
(declare-var I1 Bool)
(declare-var J1 Bool)
(declare-var K1 Bool)
(declare-var L1 Bool)
(declare-var M1 Bool)
(declare-var N1 Bool)
(declare-var O1 Bool)
(declare-var P1 Real)
(declare-var Q1 Real)
(declare-var R1 Real)
(declare-var S1 Real)
(declare-var T1 Bool)
(declare-var U1 Bool)
(declare-var V1 Bool)
(declare-var W1 Bool)
(declare-var X1 Bool)
(declare-var Y1 Bool)
(declare-var Z1 Bool)
(declare-var A2 Real)
(declare-var B2 Real)
(declare-var C2 Bool)
(declare-var D2 Bool)
(declare-var E2 Real)
(declare-var F2 Bool)
(declare-var G2 Real)
(declare-var H2 Real)
(declare-var I2 Bool)
(declare-var J2 Real)
(declare-var K2 Real)
(declare-var L2 Bool)
(declare-var M2 Real)
(declare-var N2 Bool)
(declare-var O2 Bool)
(declare-var P2 Bool)
(declare-var Q2 Bool)
(declare-var R2 Bool)
(declare-var S2 Bool)
(declare-var T2 Bool)
(declare-var U2 Bool)
(declare-var V2 Bool)
(declare-var W2 Bool)
(rule cp-rel-entry)
(rule (let ((a!1 (=> Q (or (and M R (<= S T) (>= S T)) (and N U P (<= S G) (>= S G)))))
      (a!2 (=> Q (or (and R (not U)) (and U (not R)))))
      (a!3 (=> Y
               (or (and V Z (<= A1 B1) (>= A1 B1))
                   (and Q C1 X (<= A1 H) (>= A1 H)))))
      (a!4 (=> Y (or (and Z (not C1)) (and C1 (not Z)))))
      (a!5 (=> G1 (or (and D1 H1 I1) (and Y J1 (not F1)))))
      (a!6 (=> G1 (or (and H1 (not J1)) (and J1 (not H1)))))
      (a!7 (or (and K1
                    O1
                    (and (<= P1 E) (>= P1 E))
                    (and (<= Q1 F) (>= Q1 F))
                    (<= R1 S1)
                    (>= R1 S1))
               (and G1
                    T1
                    M1
                    (and (<= P1 E) (>= P1 E))
                    (and (<= Q1 F) (>= Q1 F))
                    (and (<= R1 S) (>= R1 S)))
               (and D1
                    U1
                    (not I1)
                    (<= P1 0.0)
                    (>= P1 0.0)
                    (<= Q1 0.0)
                    (>= Q1 0.0)
                    (and (<= R1 S) (>= R1 S)))))
      (a!8 (=> N1
               (or (and O1 (not T1) (not U1))
                   (and T1 (not O1) (not U1))
                   (and U1 (not O1) (not T1)))))
      (a!9 (=> Y1
               (or (and V1 Z1 (<= A2 B2) (>= A2 B2))
                   (and N1 C2 (not X1) (<= A2 P1) (>= A2 P1)))))
      (a!10 (=> Y1 (or (and Z1 (not C2)) (and C2 (not Z1))))))
(let ((a!11 (and (cp-rel-bb1.i.i_proc I J K L D C B A)
                 (=> M (and N O (not P)))
                 (=> M O)
                 a!1
                 a!2
                 (=> V (and Q W (not X)))
                 (=> V W)
                 a!3
                 a!4
                 (=> D1 (and Y E1 F1))
                 (=> D1 E1)
                 a!5
                 a!6
                 (=> K1 (and G1 L1 (not M1)))
                 (=> K1 L1)
                 (=> N1 a!7)
                 a!8
                 (=> V1 (and N1 W1 X1))
                 (=> V1 W1)
                 a!9
                 a!10
                 Y1
                 (not D2)
                 (<= I A2)
                 (>= I A2)
                 (<= J Q1)
                 (>= J Q1)
                 (<= K R1)
                 (>= K R1)
                 (<= L A1)
                 (>= L A1)
                 (= P (= E2 0.0))
                 (= F2 (< G 2.0))
                 (= G2 (ite F2 1.0 0.0))
                 (= T (+ G2 G))
                 (= X (= H2 0.0))
                 (= I2 (= H 0.0))
                 (= B1 (ite I2 1.0 0.0))
                 (= F1 (= J2 0.0))
                 (= I1 (= K2 0.0))
                 (= M1 (= E 0.0))
                 (= L2 (> S 0.0))
                 (= M2 (+ S (- 1.0)))
                 (= S1 (ite L2 M2 S))
                 (= N2 (not (= Q1 0.0)))
                 (= O2 (= P1 0.0))
                 (= P2 (> R1 1.0))
                 (= Q2 (and N2 O2))
                 (= X1 (and Q2 P2))
                 (= R2 (= A1 0.0))
                 (= B2 (ite R2 1.0 P1))
                 (= S2 (= A1 0.0))
                 (= T2 (= R1 2.0))
                 (= U2 (= A2 0.0))
                 (= V2 (and T2 S2))
                 (= D2 (and V2 U2)))))
  (=> a!11 (cp-rel-bb1.i.i_proc E F G H D C B A)))))
(rule (let ((a!1 (=> Q (or (and M R (<= E F) (>= E F)) (and N U P (<= E C) (>= E C)))))
      (a!2 (=> Q (or (and R (not U)) (and U (not R)))))
      (a!3 (=> Y
               (or (and V Z (<= G H) (>= G H)) (and Q C1 X (<= G D) (>= G D)))))
      (a!4 (=> Y (or (and Z (not C1)) (and C1 (not Z)))))
      (a!5 (=> G1 (or (and D1 H1 I1) (and Y J1 (not F1)))))
      (a!6 (=> G1 (or (and H1 (not J1)) (and J1 (not H1)))))
      (a!7 (or (and K1
                    O1
                    (and (<= I A) (>= I A))
                    (and (<= J B) (>= J B))
                    (<= K L)
                    (>= K L))
               (and G1
                    T1
                    M1
                    (and (<= I A) (>= I A))
                    (and (<= J B) (>= J B))
                    (and (<= K E) (>= K E)))
               (and D1
                    U1
                    (not I1)
                    (<= I 0.0)
                    (>= I 0.0)
                    (<= J 0.0)
                    (>= J 0.0)
                    (and (<= K E) (>= K E)))))
      (a!8 (=> N1
               (or (and O1 (not T1) (not U1))
                   (and T1 (not O1) (not U1))
                   (and U1 (not O1) (not T1)))))
      (a!9 (=> Y1
               (or (and V1 Z1 (<= S T) (>= S T))
                   (and N1 C2 (not X1) (<= S I) (>= S I)))))
      (a!10 (=> Y1 (or (and Z1 (not C2)) (and C2 (not Z1))))))
(let ((a!11 (and (=> M (and N O (not P)))
                 (=> M O)
                 a!1
                 a!2
                 (=> V (and Q W (not X)))
                 (=> V W)
                 a!3
                 a!4
                 (=> D1 (and Y E1 F1))
                 (=> D1 E1)
                 a!5
                 a!6
                 (=> K1 (and G1 L1 (not M1)))
                 (=> K1 L1)
                 (=> N1 a!7)
                 a!8
                 (=> V1 (and N1 W1 X1))
                 (=> V1 W1)
                 a!9
                 a!10
                 Y1
                 D2
                 (= P (= A1 0.0))
                 (= F2 (< C 2.0))
                 (= B1 (ite F2 1.0 0.0))
                 (= F (+ B1 C))
                 (= X (= P1 0.0))
                 (= I2 (= D 0.0))
                 (= H (ite I2 1.0 0.0))
                 (= F1 (= Q1 0.0))
                 (= I1 (= R1 0.0))
                 (= M1 (= A 0.0))
                 (= L2 (> E 0.0))
                 (= S1 (+ E (- 1.0)))
                 (= L (ite L2 S1 E))
                 (= N2 (not (= J 0.0)))
                 (= O2 (= I 0.0))
                 (= P2 (> K 1.0))
                 (= Q2 (and N2 O2))
                 (= X1 (and Q2 P2))
                 (= R2 (= G 0.0))
                 (= T (ite R2 1.0 I))
                 (= S2 (= G 0.0))
                 (= T2 (= K 2.0))
                 (= U2 (= S 0.0))
                 (= V2 (and T2 S2))
                 (= D2 (and V2 U2)))))
  (=> a!11 (cp-rel-bb1.i.i_proc A B C D A B C D)))))
(rule (let ((a!1 (=> Q
               (or (and M R (<= A B) (>= A B)) (and N U P (<= A Q1) (>= A Q1)))))
      (a!2 (=> Q (or (and R (not U)) (and U (not R)))))
      (a!3 (=> Y
               (or (and V Z (<= C D) (>= C D)) (and Q C1 X (<= C P1) (>= C P1)))))
      (a!4 (=> Y (or (and Z (not C1)) (and C1 (not Z)))))
      (a!5 (=> G1 (or (and D1 H1 I1) (and Y J1 (not F1)))))
      (a!6 (=> G1 (or (and H1 (not J1)) (and J1 (not H1)))))
      (a!7 (or (and K1
                    O1
                    (and (<= E S1) (>= E S1))
                    (and (<= F R1) (>= F R1))
                    (<= G H)
                    (>= G H))
               (and G1
                    T1
                    M1
                    (and (<= E S1) (>= E S1))
                    (and (<= F R1) (>= F R1))
                    (and (<= G A) (>= G A)))
               (and D1
                    U1
                    (not I1)
                    (<= E 0.0)
                    (>= E 0.0)
                    (<= F 0.0)
                    (>= F 0.0)
                    (and (<= G A) (>= G A)))))
      (a!8 (=> N1
               (or (and O1 (not T1) (not U1))
                   (and T1 (not O1) (not U1))
                   (and U1 (not O1) (not T1)))))
      (a!9 (=> Y1
               (or (and V1 Z1 (<= I J) (>= I J))
                   (and N1 C2 (not X1) (<= I E) (>= I E)))))
      (a!10 (=> Y1 (or (and Z1 (not C2)) (and C2 (not Z1))))))
(let ((a!11 (and cp-rel-entry
                 W2
                 (<= A2 0.0)
                 (>= A2 0.0)
                 (<= B2 1.0)
                 (>= B2 1.0)
                 (<= E2 1.0)
                 (>= E2 1.0)
                 (<= G2 0.0)
                 (>= G2 0.0)
                 (cp-rel-bb1.i.i_proc A2 B2 E2 G2 S1 R1 Q1 P1)
                 (=> M (and N O (not P)))
                 (=> M O)
                 a!1
                 a!2
                 (=> V (and Q W (not X)))
                 (=> V W)
                 a!3
                 a!4
                 (=> D1 (and Y E1 F1))
                 (=> D1 E1)
                 a!5
                 a!6
                 (=> K1 (and G1 L1 (not M1)))
                 (=> K1 L1)
                 (=> N1 a!7)
                 a!8
                 (=> V1 (and N1 W1 X1))
                 (=> V1 W1)
                 a!9
                 a!10
                 Y1
                 D2
                 (= P (= K 0.0))
                 (= F2 (< Q1 2.0))
                 (= L (ite F2 1.0 0.0))
                 (= B (+ L Q1))
                 (= X (= S 0.0))
                 (= I2 (= P1 0.0))
                 (= D (ite I2 1.0 0.0))
                 (= F1 (= T 0.0))
                 (= I1 (= A1 0.0))
                 (= M1 (= S1 0.0))
                 (= L2 (> A 0.0))
                 (= B1 (+ A (- 1.0)))
                 (= H (ite L2 B1 A))
                 (= N2 (not (= F 0.0)))
                 (= O2 (= E 0.0))
                 (= P2 (> G 1.0))
                 (= Q2 (and N2 O2))
                 (= X1 (and Q2 P2))
                 (= R2 (= C 0.0))
                 (= J (ite R2 1.0 E))
                 (= S2 (= C 0.0))
                 (= T2 (= G 2.0))
                 (= U2 (= I 0.0))
                 (= V2 (and T2 S2))
                 (= D2 (and V2 U2)))))
  (=> a!11 cp-rel-bb2.i.i.i.i))))
(query cp-rel-bb2.i.i.i.i)
