(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb2.i.i.i.i ())
(declare-rel cp-rel-bb1.i.i_proc (Real Real Real Real Real Real Real Real))
(declare-var A Real)
(declare-var B Real)
(declare-var C Real)
(declare-var D Real)
(declare-var E Real)
(declare-var F Real)
(declare-var G Real)
(declare-var H Real)
(declare-var I Real)
(declare-var J Real)
(declare-var K Real)
(declare-var L Real)
(declare-var M Bool)
(declare-var N Bool)
(declare-var O Bool)
(declare-var P Bool)
(declare-var Q Bool)
(declare-var R Bool)
(declare-var S Real)
(declare-var T Real)
(declare-var U Bool)
(declare-var V Bool)
(declare-var W Bool)
(declare-var X Bool)
(declare-var Y Bool)
(declare-var Z Bool)
(declare-var A1 Real)
(declare-var B1 Real)
(declare-var C1 Bool)
(declare-var D1 Bool)
(declare-var E1 Bool)
(declare-var F1 Bool)
(declare-var G1 Bool)
(declare-var H1 Bool)
(declare-var I1 Bool)
(declare-var J1 Real)
(declare-var K1 Bool)
(declare-var L1 Bool)
(declare-var M1 Bool)
(declare-var N1 Bool)
(declare-var O1 Bool)
(declare-var P1 Bool)
(declare-var Q1 Real)
(declare-var R1 Real)
(declare-var S1 Bool)
(declare-var T1 Bool)
(declare-var U1 Bool)
(declare-var V1 Bool)
(declare-var W1 Bool)
(declare-var X1 Bool)
(declare-var Y1 Bool)
(declare-var Z1 Bool)
(declare-var A2 Bool)
(declare-var B2 Bool)
(declare-var C2 Bool)
(declare-var D2 Bool)
(declare-var E2 Bool)
(declare-var F2 Bool)
(declare-var G2 Bool)
(declare-var H2 Bool)
(declare-var I2 Real)
(declare-var J2 Real)
(declare-var K2 Real)
(declare-var L2 Real)
(declare-var M2 Real)
(declare-var N2 Bool)
(declare-var O2 Bool)
(declare-var P2 Bool)
(declare-var Q2 Bool)
(declare-var R2 Bool)
(declare-var S2 Bool)
(declare-var T2 Real)
(declare-var U2 Bool)
(declare-var V2 Real)
(declare-var W2 Real)
(declare-var X2 Bool)
(declare-var Y2 Real)
(declare-var Z2 Real)
(declare-var A3 Bool)
(declare-var B3 Real)
(declare-var C3 Bool)
(declare-var D3 Bool)
(declare-var E3 Bool)
(declare-var F3 Bool)
(declare-var G3 Bool)
(declare-var H3 Bool)
(declare-var I3 Bool)
(declare-var J3 Bool)
(declare-var K3 Bool)
(rule cp-rel-entry)
(rule (let ((a!1 (=> Q (or (and M R (<= S T) (>= S T)) (and N U P (<= S G) (>= S G)))))
      (a!2 (=> Q (or (and R (not U)) (and U (not R)))))
      (a!3 (=> Y
               (or (and V Z (<= A1 B1) (>= A1 B1))
                   (and Q C1 X (<= A1 H) (>= A1 H)))))
      (a!4 (=> Y (or (and Z (not C1)) (and C1 (not Z)))))
      (a!5 (=> G1
               (or (and D1 H1 I1 (<= J1 F) (>= J1 F))
                   (and Y K1 (not F1) (<= J1 1.0) (>= J1 1.0)))))
      (a!6 (=> G1 (or (and H1 (not K1)) (and K1 (not H1)))))
      (a!7 (=> O1
               (or (and L1 P1 (<= Q1 R1) (>= Q1 R1))
                   (and G1 S1 N1 (<= Q1 S) (>= Q1 S)))))
      (a!8 (=> O1 (or (and P1 (not S1)) (and S1 (not P1)))))
      (a!9 (or (and A2
                    H2
                    (and (<= I2 E) (>= I2 E))
                    (and (<= J2 J1) (>= J2 J1))
                    (and (<= K2 Q1) (>= K2 Q1))
                    (<= L2 M2)
                    (>= L2 M2))
               (and W1
                    N2
                    (not C2)
                    (and (<= I2 E) (>= I2 E))
                    (and (<= J2 J1) (>= J2 J1))
                    (and (<= K2 Q1) (>= K2 Q1))
                    (and (<= L2 E) (>= L2 E)))
               (and D2
                    O2
                    (and (<= I2 E) (>= I2 E))
                    (and (<= J2 J1) (>= J2 J1))
                    (and (<= K2 Q1) (>= K2 Q1))
                    (and (<= L2 E) (>= L2 E)))
               (and Y1
                    P2
                    (not F2)
                    (and (<= I2 E) (>= I2 E))
                    (and (<= J2 J1) (>= J2 J1))
                    (and (<= K2 Q1) (>= K2 Q1))
                    (and (<= L2 0.0) (>= L2 0.0)))
               (and O1
                    Q2
                    V1
                    (and (<= I2 E) (>= I2 E))
                    (and (<= J2 J1) (>= J2 J1))
                    (and (<= K2 Q1) (>= K2 Q1))
                    (and (<= L2 E) (>= L2 E)))
               (and D1
                    R2
                    (not I1)
                    (<= I2 0.0)
                    (>= I2 0.0)
                    (<= J2 0.0)
                    (>= J2 0.0)
                    (<= K2 S)
                    (>= K2 S)
                    (and (<= L2 0.0) (>= L2 0.0)))))
      (a!10 (=> G2
                (or (and H2 (not N2) (not O2) (not P2) (not Q2) (not R2))
                    (and N2 (not H2) (not O2) (not P2) (not Q2) (not R2))
                    (and O2 (not H2) (not N2) (not P2) (not Q2) (not R2))
                    (and P2 (not H2) (not N2) (not O2) (not Q2) (not R2))
                    (and Q2 (not H2) (not N2) (not O2) (not P2) (not R2))
                    (and R2 (not H2) (not N2) (not O2) (not P2) (not Q2))))))
(let ((a!11 (and (cp-rel-bb1.i.i_proc I J K L D C B A)
                 (=> M (and N O (not P)))
                 (=> M O)
                 a!1
                 a!2
                 (=> V (and Q W (not X)))
                 (=> V W)
                 a!3
                 a!4
                 (=> D1 (and Y E1 F1))
                 (=> D1 E1)
                 a!5
                 a!6
                 (=> L1 (and G1 M1 (not N1)))
                 (=> L1 M1)
                 a!7
                 a!8
                 (=> T1 (and O1 U1 (not V1)))
                 (=> T1 U1)
                 (=> W1 (and T1 X1 N1))
                 (=> W1 X1)
                 (=> Y1 (and T1 Z1 (not N1)))
                 (=> Y1 Z1)
                 (=> A2 (and W1 B2 C2))
                 (=> A2 B2)
                 (=> D2 (and Y1 E2 F2))
                 (=> D2 E2)
                 (=> G2 a!9)
                 a!10
                 G2
                 (not S2)
                 (<= I L2)
                 (>= I L2)
                 (<= J J2)
                 (>= J J2)
                 (<= K K2)
                 (>= K K2)
                 (<= L A1)
                 (>= L A1)
                 (= P (= T2 0.0))
                 (= U2 (< G 2.0))
                 (= V2 (ite U2 1.0 0.0))
                 (= T (+ V2 G))
                 (= X (= W2 0.0))
                 (= X2 (= H 0.0))
                 (= B1 (ite X2 1.0 0.0))
                 (= F1 (= Y2 0.0))
                 (= I1 (= Z2 0.0))
                 (= N1 (= E 0.0))
                 (= A3 (> S 0.0))
                 (= B3 (+ S (- 1.0)))
                 (= R1 (ite A3 B3 S))
                 (= V1 (= J1 0.0))
                 (= C2 (> Q1 1.0))
                 (= C3 (= A1 0.0))
                 (= D3 (= Q1 0.0))
                 (= F2 (and C3 D3))
                 (= E3 (= A1 0.0))
                 (= M2 (ite E3 1.0 E))
                 (= F3 (= K2 2.0))
                 (= G3 (= L2 0.0))
                 (= H3 (or G3 F3))
                 (= I3 (xor H3 true))
                 (= J3 (= I2 0.0))
                 (= S2 (and J3 I3)))))
  (=> a!11 (cp-rel-bb1.i.i_proc E F G H D C B A)))))
(rule (let ((a!1 (=> Q (or (and M R (<= E F) (>= E F)) (and N U P (<= E C) (>= E C)))))
      (a!2 (=> Q (or (and R (not U)) (and U (not R)))))
      (a!3 (=> Y
               (or (and V Z (<= G H) (>= G H)) (and Q C1 X (<= G D) (>= G D)))))
      (a!4 (=> Y (or (and Z (not C1)) (and C1 (not Z)))))
      (a!5 (=> G1
               (or (and D1 H1 I1 (<= I B) (>= I B))
                   (and Y K1 (not F1) (<= I 1.0) (>= I 1.0)))))
      (a!6 (=> G1 (or (and H1 (not K1)) (and K1 (not H1)))))
      (a!7 (=> O1
               (or (and L1 P1 (<= J K) (>= J K))
                   (and G1 S1 N1 (<= J E) (>= J E)))))
      (a!8 (=> O1 (or (and P1 (not S1)) (and S1 (not P1)))))
      (a!9 (or (and A2
                    H2
                    (and (<= L A) (>= L A))
                    (and (<= S I) (>= S I))
                    (and (<= T J) (>= T J))
                    (<= A1 B1)
                    (>= A1 B1))
               (and W1
                    N2
                    (not C2)
                    (and (<= L A) (>= L A))
                    (and (<= S I) (>= S I))
                    (and (<= T J) (>= T J))
                    (and (<= A1 A) (>= A1 A)))
               (and D2
                    O2
                    (and (<= L A) (>= L A))
                    (and (<= S I) (>= S I))
                    (and (<= T J) (>= T J))
                    (and (<= A1 A) (>= A1 A)))
               (and Y1
                    P2
                    (not F2)
                    (and (<= L A) (>= L A))
                    (and (<= S I) (>= S I))
                    (and (<= T J) (>= T J))
                    (and (<= A1 0.0) (>= A1 0.0)))
               (and O1
                    Q2
                    V1
                    (and (<= L A) (>= L A))
                    (and (<= S I) (>= S I))
                    (and (<= T J) (>= T J))
                    (and (<= A1 A) (>= A1 A)))
               (and D1
                    R2
                    (not I1)
                    (<= L 0.0)
                    (>= L 0.0)
                    (<= S 0.0)
                    (>= S 0.0)
                    (<= T E)
                    (>= T E)
                    (and (<= A1 0.0) (>= A1 0.0)))))
      (a!10 (=> G2
                (or (and H2 (not N2) (not O2) (not P2) (not Q2) (not R2))
                    (and N2 (not H2) (not O2) (not P2) (not Q2) (not R2))
                    (and O2 (not H2) (not N2) (not P2) (not Q2) (not R2))
                    (and P2 (not H2) (not N2) (not O2) (not Q2) (not R2))
                    (and Q2 (not H2) (not N2) (not O2) (not P2) (not R2))
                    (and R2 (not H2) (not N2) (not O2) (not P2) (not Q2))))))
(let ((a!11 (and (=> M (and N O (not P)))
                 (=> M O)
                 a!1
                 a!2
                 (=> V (and Q W (not X)))
                 (=> V W)
                 a!3
                 a!4
                 (=> D1 (and Y E1 F1))
                 (=> D1 E1)
                 a!5
                 a!6
                 (=> L1 (and G1 M1 (not N1)))
                 (=> L1 M1)
                 a!7
                 a!8
                 (=> T1 (and O1 U1 (not V1)))
                 (=> T1 U1)
                 (=> W1 (and T1 X1 N1))
                 (=> W1 X1)
                 (=> Y1 (and T1 Z1 (not N1)))
                 (=> Y1 Z1)
                 (=> A2 (and W1 B2 C2))
                 (=> A2 B2)
                 (=> D2 (and Y1 E2 F2))
                 (=> D2 E2)
                 (=> G2 a!9)
                 a!10
                 G2
                 S2
                 (= P (= J1 0.0))
                 (= U2 (< C 2.0))
                 (= Q1 (ite U2 1.0 0.0))
                 (= F (+ Q1 C))
                 (= X (= R1 0.0))
                 (= X2 (= D 0.0))
                 (= H (ite X2 1.0 0.0))
                 (= F1 (= I2 0.0))
                 (= I1 (= J2 0.0))
                 (= N1 (= A 0.0))
                 (= A3 (> E 0.0))
                 (= K2 (+ E (- 1.0)))
                 (= K (ite A3 K2 E))
                 (= V1 (= I 0.0))
                 (= C2 (> J 1.0))
                 (= C3 (= G 0.0))
                 (= D3 (= J 0.0))
                 (= F2 (and C3 D3))
                 (= E3 (= G 0.0))
                 (= B1 (ite E3 1.0 A))
                 (= F3 (= T 2.0))
                 (= G3 (= A1 0.0))
                 (= H3 (or G3 F3))
                 (= I3 (xor H3 true))
                 (= J3 (= L 0.0))
                 (= S2 (and J3 I3)))))
  (=> a!11 (cp-rel-bb1.i.i_proc A B C D A B C D)))))
(rule (let ((a!1 (=> Q
               (or (and M R (<= A B) (>= A B)) (and N U P (<= A I2) (>= A I2)))))
      (a!2 (=> Q (or (and R (not U)) (and U (not R)))))
      (a!3 (=> Y
               (or (and V Z (<= C D) (>= C D)) (and Q C1 X (<= C R1) (>= C R1)))))
      (a!4 (=> Y (or (and Z (not C1)) (and C1 (not Z)))))
      (a!5 (=> G1
               (or (and D1 H1 I1 (<= E J2) (>= E J2))
                   (and Y K1 (not F1) (<= E 1.0) (>= E 1.0)))))
      (a!6 (=> G1 (or (and H1 (not K1)) (and K1 (not H1)))))
      (a!7 (=> O1
               (or (and L1 P1 (<= F G) (>= F G))
                   (and G1 S1 N1 (<= F A) (>= F A)))))
      (a!8 (=> O1 (or (and P1 (not S1)) (and S1 (not P1)))))
      (a!9 (or (and A2
                    H2
                    (and (<= H K2) (>= H K2))
                    (and (<= I E) (>= I E))
                    (and (<= J F) (>= J F))
                    (<= K L)
                    (>= K L))
               (and W1
                    N2
                    (not C2)
                    (and (<= H K2) (>= H K2))
                    (and (<= I E) (>= I E))
                    (and (<= J F) (>= J F))
                    (and (<= K K2) (>= K K2)))
               (and D2
                    O2
                    (and (<= H K2) (>= H K2))
                    (and (<= I E) (>= I E))
                    (and (<= J F) (>= J F))
                    (and (<= K K2) (>= K K2)))
               (and Y1
                    P2
                    (not F2)
                    (and (<= H K2) (>= H K2))
                    (and (<= I E) (>= I E))
                    (and (<= J F) (>= J F))
                    (and (<= K 0.0) (>= K 0.0)))
               (and O1
                    Q2
                    V1
                    (and (<= H K2) (>= H K2))
                    (and (<= I E) (>= I E))
                    (and (<= J F) (>= J F))
                    (and (<= K K2) (>= K K2)))
               (and D1
                    R2
                    (not I1)
                    (<= H 0.0)
                    (>= H 0.0)
                    (<= I 0.0)
                    (>= I 0.0)
                    (<= J A)
                    (>= J A)
                    (and (<= K 0.0) (>= K 0.0)))))
      (a!10 (=> G2
                (or (and H2 (not N2) (not O2) (not P2) (not Q2) (not R2))
                    (and N2 (not H2) (not O2) (not P2) (not Q2) (not R2))
                    (and O2 (not H2) (not N2) (not P2) (not Q2) (not R2))
                    (and P2 (not H2) (not N2) (not O2) (not Q2) (not R2))
                    (and Q2 (not H2) (not N2) (not O2) (not P2) (not R2))
                    (and R2 (not H2) (not N2) (not O2) (not P2) (not Q2))))))
(let ((a!11 (and cp-rel-entry
                 K3
                 (<= L2 0.0)
                 (>= L2 0.0)
                 (<= M2 1.0)
                 (>= M2 1.0)
                 (<= T2 1.0)
                 (>= T2 1.0)
                 (<= V2 0.0)
                 (>= V2 0.0)
                 (cp-rel-bb1.i.i_proc L2 M2 T2 V2 K2 J2 I2 R1)
                 (=> M (and N O (not P)))
                 (=> M O)
                 a!1
                 a!2
                 (=> V (and Q W (not X)))
                 (=> V W)
                 a!3
                 a!4
                 (=> D1 (and Y E1 F1))
                 (=> D1 E1)
                 a!5
                 a!6
                 (=> L1 (and G1 M1 (not N1)))
                 (=> L1 M1)
                 a!7
                 a!8
                 (=> T1 (and O1 U1 (not V1)))
                 (=> T1 U1)
                 (=> W1 (and T1 X1 N1))
                 (=> W1 X1)
                 (=> Y1 (and T1 Z1 (not N1)))
                 (=> Y1 Z1)
                 (=> A2 (and W1 B2 C2))
                 (=> A2 B2)
                 (=> D2 (and Y1 E2 F2))
                 (=> D2 E2)
                 (=> G2 a!9)
                 a!10
                 G2
                 S2
                 (= P (= S 0.0))
                 (= U2 (< I2 2.0))
                 (= T (ite U2 1.0 0.0))
                 (= B (+ T I2))
                 (= X (= A1 0.0))
                 (= X2 (= R1 0.0))
                 (= D (ite X2 1.0 0.0))
                 (= F1 (= B1 0.0))
                 (= I1 (= J1 0.0))
                 (= N1 (= K2 0.0))
                 (= A3 (> A 0.0))
                 (= Q1 (+ A (- 1.0)))
                 (= G (ite A3 Q1 A))
                 (= V1 (= E 0.0))
                 (= C2 (> F 1.0))
                 (= C3 (= C 0.0))
                 (= D3 (= F 0.0))
                 (= F2 (and C3 D3))
                 (= E3 (= C 0.0))
                 (= L (ite E3 1.0 K2))
                 (= F3 (= J 2.0))
                 (= G3 (= K 0.0))
                 (= H3 (or G3 F3))
                 (= I3 (xor H3 true))
                 (= J3 (= H 0.0))
                 (= S2 (and J3 I3)))))
  (=> a!11 cp-rel-bb2.i.i.i.i))))
(query cp-rel-bb2.i.i.i.i)
