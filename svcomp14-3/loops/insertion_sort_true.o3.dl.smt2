(declare-rel cp-rel-entry ())
(declare-rel cp-rel-bb_proc (Real Real Real Real))
(declare-rel cp-rel-bb2_proc (Real Real Real Real Real Real Real Real))
(declare-rel cp-rel-__UFO__0_proc ())
(declare-rel cp-rel-bb8_proc (Real Real Real Real))
(declare-rel cp-rel-ERROR.i ())
(declare-var A Real)
(declare-var B Real)
(declare-var C Real)
(declare-var D Real)
(declare-var E Real)
(declare-var F Real)
(declare-var G Real)
(declare-var H Real)
(declare-var I Real)
(declare-var J Real)
(declare-var K Real)
(declare-var L Bool)
(declare-var M Bool)
(declare-var N Bool)
(declare-var O Bool)
(declare-var P Bool)
(declare-var Q Real)
(declare-var R Real)
(declare-var S Real)
(declare-var T Real)
(declare-var U Bool)
(declare-var V Bool)
(declare-var W Real)
(declare-var X Bool)
(declare-var Y Bool)
(declare-var Z Bool)
(declare-var A1 Real)
(declare-var B1 Bool)
(declare-var C1 Bool)
(declare-var D1 Bool)
(declare-var E1 Bool)
(declare-var F1 Bool)
(declare-var G1 Bool)
(declare-var H1 Bool)
(declare-var I1 Bool)
(declare-var J1 Real)
(declare-var K1 Real)
(declare-var L1 Real)
(declare-var M1 Real)
(declare-var N1 Real)
(declare-var O1 Bool)
(rule cp-rel-entry)
(rule (let ((a!1 (= Q (+ I (* (- 1.0) K)))) (a!2 (= R (+ H (* (- 1.0) K)))))
(let ((a!3 (and (cp-rel-bb2_proc H I J T D C B A)
                (=> L (and M N (not O)))
                (=> L N)
                (=> U (and L V P))
                (=> U V)
                U
                (<= T W)
                (>= T W)
                a!1
                a!2
                (= O (< R 0.0))
                (= P (> S J))
                (= W (+ K 1.0)))))
  (=> a!3 (cp-rel-bb2_proc H I J K D C B A)))))
(rule (let ((a!1 (=> P (or (and L U (not V)) (and M X O))))
      (a!2 (=> P (or (and U (not X)) (and X (not U)))))
      (a!3 (= I (+ F (* (- 1.0) H))))
      (a!4 (= J (+ E (* (- 1.0) H)))))
(let ((a!5 (and (=> L (and M N (not O)))
                (=> L N)
                a!1
                a!2
                P
                (not Y)
                (<= D F)
                (>= D F)
                a!3
                a!4
                (= O (< J 0.0))
                (= V (> K G))
                (= Y (= F C)))))
  (=> a!5 (cp-rel-bb2_proc E F G H E F G H)))))
(rule (let ((a!1 (=> P (or (and L U (not V)) (and M X O))))
      (a!2 (=> P (or (and U (not X)) (and X (not U)))))
      (a!3 (= H (+ E (* (- 1.0) G))))
      (a!4 (= I (+ D (* (- 1.0) G)))))
(let ((a!5 (and (=> L (and M N (not O)))
                (=> L N)
                a!1
                a!2
                P
                Y
                (<= K 0.0)
                (>= K 0.0)
                a!3
                a!4
                (= O (< I 0.0))
                (= V (> J F))
                (= Y (= E C)))))
  (=> a!5 (cp-rel-bb2_proc D E F G D E F G)))))
(rule (let ((a!1 (=> U (or (and Z P (not O)) (and Y N V))))
      (a!2 (=> U (or (and P (not N)) (and N (not P)))))
      (a!3 (= W (+ K (* (- 1.0) R))))
      (a!4 (= T (+ J (* (- 1.0) R)))))
(let ((a!5 (and (cp-rel-bb_proc G A1 B A)
                L
                (<= C 0.0)
                (>= C 0.0)
                (= E (+ F 1.0))
                (cp-rel-bb2_proc F E D C J K Q R)
                (=> Z (and Y X (not V)))
                (=> Z X)
                a!1
                a!2
                U
                (not M)
                (<= A1 K)
                (>= A1 K)
                a!3
                a!4
                (= V (< T 0.0))
                (= O (> S Q))
                (= M (= K G)))))
  (=> a!5 (cp-rel-bb_proc G F B A)))))
(rule (let ((a!1 (=> U (or (and Z P (not O)) (and Y N V))))
      (a!2 (=> U (or (and P (not N)) (and N (not P)))))
      (a!3 (= T (+ I (* (- 1.0) K))))
      (a!4 (= S (+ H (* (- 1.0) K)))))
(let ((a!5 (and L
                (<= A 0.0)
                (>= A 0.0)
                (= C (+ D 1.0))
                (cp-rel-bb2_proc D C B A H I J K)
                (=> Z (and Y X (not V)))
                (=> Z X)
                a!1
                a!2
                U
                M
                (<= Q 0.0)
                (>= Q 0.0)
                a!3
                a!4
                (= V (< S 0.0))
                (= O (> R J))
                (= M (= I E)))))
  (=> a!5 (cp-rel-bb_proc E D E D)))))
(rule (=> (and cp-rel-__UFO__0_proc L) cp-rel-__UFO__0_proc))
(rule (=> (and (cp-rel-bb8_proc C F B A)
         (=> L (and M N O))
         (=> L N)
         L
         (not P)
         (<= F G)
         (>= F G)
         (= G (+ E 1.0))
         (= O (< G C))
         (= P (> H I)))
    (cp-rel-bb8_proc C E B A)))
(rule (=> (and (=> L (and M N O))
         (=> L N)
         L
         P
         (= D (+ C 1.0))
         (= O (< D A))
         (= P (> E F)))
    (cp-rel-bb8_proc A C A C)))
(rule (=> (and cp-rel-entry
         U
         (not V)
         (<= H 0.0)
         (>= H 0.0)
         (= V (> F 1.0))
         (cp-rel-bb8_proc F H E D)
         (=> L (and M N O))
         (=> L N)
         L
         P
         (= A (+ D 1.0))
         (= O (< A E))
         (= P (> B C)))
    cp-rel-ERROR.i))
(rule (let ((a!1 (=> E1 (or (and Z F1 (not G1)) (and B1 H1 D1))))
      (a!2 (=> E1 (or (and F1 (not H1)) (and H1 (not F1)))))
      (a!3 (= Q (+ J1 (* (- 1.0) W))))
      (a!4 (= R (+ K1 (* (- 1.0) W)))))
(let ((a!5 (and cp-rel-entry
                (=> Y (and X V U))
                (=> Y V)
                Y
                (<= F 0.0)
                (>= F 0.0)
                (= U (> I 1.0))
                (= G (+ I (- 1.0)))
                (cp-rel-bb_proc G F J K)
                O1
                (<= N1 0.0)
                (>= N1 0.0)
                (= L1 (+ K 1.0))
                (cp-rel-bb2_proc K L1 M1 N1 K1 J1 A1 W)
                (=> Z (and B1 C1 (not D1)))
                (=> Z C1)
                a!1
                a!2
                E1
                I1
                (<= T 0.0)
                (>= T 0.0)
                a!3
                a!4
                (= D1 (< R 0.0))
                (= G1 (> S A1))
                (= I1 (= J1 J))
                (cp-rel-bb8_proc I T E D)
                (=> L (and M N O))
                (=> L N)
                L
                P
                (= A (+ D 1.0))
                (= O (< A E))
                (= P (> B C)))))
  (=> a!5 cp-rel-ERROR.i))))
(query cp-rel-ERROR.i)
