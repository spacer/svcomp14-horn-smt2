(declare-rel cp-rel-entry ())
(declare-rel cp-rel-__UFO__0_proc ())
(declare-rel cp-rel-ERROR.i ())
(declare-rel cp-rel-bb_proc (Real Real Real Real Real Real))
(declare-var A Bool)
(declare-var B Real)
(declare-var C Bool)
(declare-var D Bool)
(declare-var E Real)
(declare-var F Bool)
(declare-var G Bool)
(declare-var H Bool)
(declare-var I Bool)
(declare-var J Real)
(declare-var K Real)
(declare-var L Real)
(declare-var M Real)
(declare-var N Real)
(declare-var O Real)
(declare-var P Real)
(declare-var Q Real)
(declare-var R Real)
(declare-var S Real)
(declare-var T Bool)
(declare-var U Bool)
(declare-var V Bool)
(rule cp-rel-entry)
(rule (let ((a!1 (and cp-rel-entry
                (=> G (and I F H (<= E 0.0) (>= E 0.0)))
                (=> G F)
                G
                (not D)
                (= H (< J 1.0))
                (= C (= B E))
                (= A (= E 0.0))
                (= D (or C A)))))
  (=> a!1 cp-rel-ERROR.i)))
(rule (=> (and (cp-rel-bb_proc K N O J E B)
         A
         (not C)
         (<= N P)
         (>= N P)
         (<= O Q)
         (>= O Q)
         (= P (+ L 1.0))
         (= R (+ M 2.0))
         (= D (> P 9.0))
         (= Q (ite D M R))
         (= S (+ L 2.0))
         (= C (> S K)))
    (cp-rel-bb_proc K L M J E B)))
(rule (let ((a!1 (and (=> F (and A T C (<= O L) (>= O L)))
                (=> F T)
                F
                G
                (= K (+ E 1.0))
                (= M (+ J 2.0))
                (= D (> K 9.0))
                (= L (ite D J M))
                (= N (+ E 2.0))
                (= C (> N B))
                (= H (= P O))
                (= I (= O 0.0))
                (= G (or H I)))))
  (=> a!1 (cp-rel-bb_proc B E J B E J))))
(rule (let ((a!1 (and (=> F (and A T C (<= O L) (>= O L)))
                (=> F T)
                F
                (not G)
                (= K (+ E 1.0))
                (= M (+ J 2.0))
                (= D (> K 9.0))
                (= L (ite D J M))
                (= N (+ E 2.0))
                (= C (> N B))
                (= H (= P O))
                (= I (= O 0.0))
                (= G (or H I)))))
  (=> a!1 (cp-rel-bb_proc B E J B E J))))
(rule (let ((a!1 (and cp-rel-entry
                U
                (not V)
                (<= R 0.0)
                (>= R 0.0)
                (<= S 0.0)
                (>= S 0.0)
                (= V (< Q 1.0))
                (cp-rel-bb_proc Q R S P O N)
                (=> F (and A T C (<= L E) (>= L E)))
                (=> F T)
                F
                (not G)
                (= B (+ O 1.0))
                (= J (+ N 2.0))
                (= D (> B 9.0))
                (= E (ite D N J))
                (= K (+ O 2.0))
                (= C (> K P))
                (= H (= M L))
                (= I (= L 0.0))
                (= G (or H I)))))
  (=> a!1 cp-rel-ERROR.i)))
(rule (=> (and cp-rel-__UFO__0_proc A) cp-rel-__UFO__0_proc))
(query cp-rel-ERROR.i)
