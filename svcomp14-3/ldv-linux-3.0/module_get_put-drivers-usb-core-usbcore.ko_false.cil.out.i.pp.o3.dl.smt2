(declare-rel cp-rel-entry ())
(declare-rel cp-rel-UnifiedReturnBlock ())
(declare-rel cp-rel-__UFO__0_proc ())
(declare-rel cp-rel-ldv_23956.outer_proc (Real Real))
(declare-rel cp-rel-ldv_23956_proc ())
(declare-var A Real)
(declare-var B Real)
(declare-var C Real)
(declare-var D Real)
(declare-var E Bool)
(declare-var F Bool)
(declare-var G Bool)
(declare-var H Bool)
(declare-var I Bool)
(declare-var J Real)
(declare-var K Real)
(declare-var L Bool)
(declare-var M Bool)
(declare-var N Bool)
(declare-var O Bool)
(declare-var P Bool)
(declare-var Q Bool)
(declare-var R Bool)
(declare-var S Bool)
(declare-var T Bool)
(declare-var U Bool)
(declare-var V Bool)
(declare-var W Bool)
(declare-var X Bool)
(declare-var Y Bool)
(declare-var Z Bool)
(declare-var A1 Bool)
(declare-var B1 Bool)
(declare-var C1 Bool)
(declare-var D1 Bool)
(declare-var E1 Real)
(declare-var F1 Bool)
(declare-var G1 Bool)
(declare-var H1 Bool)
(declare-var I1 Bool)
(declare-var J1 Bool)
(declare-var K1 Bool)
(declare-var L1 Bool)
(declare-var M1 Bool)
(declare-var N1 Bool)
(declare-var O1 Bool)
(declare-var P1 Real)
(declare-var Q1 Bool)
(declare-var R1 Real)
(declare-var S1 Bool)
(declare-var T1 Bool)
(declare-var U1 Bool)
(declare-var V1 Bool)
(declare-var W1 Bool)
(declare-var X1 Bool)
(declare-var Y1 Bool)
(declare-var Z1 Bool)
(declare-var A2 Bool)
(declare-var B2 Bool)
(declare-var C2 Bool)
(declare-var D2 Bool)
(declare-var E2 Bool)
(declare-var F2 Bool)
(declare-var G2 Bool)
(declare-var H2 Bool)
(declare-var I2 Bool)
(declare-var J2 Real)
(declare-var K2 Real)
(declare-var L2 Bool)
(declare-var M2 Bool)
(declare-var N2 Real)
(declare-var O2 Bool)
(declare-var P2 Bool)
(declare-var Q2 Bool)
(declare-var R2 Bool)
(declare-var S2 Bool)
(declare-var T2 Bool)
(declare-var U2 Bool)
(declare-var V2 Bool)
(declare-var W2 Bool)
(declare-var X2 Bool)
(declare-var Y2 Bool)
(declare-var Z2 Bool)
(declare-var A3 Real)
(declare-var B3 Real)
(declare-var C3 Real)
(declare-var D3 Bool)
(declare-var E3 Bool)
(declare-var F3 Bool)
(declare-var G3 Bool)
(declare-var H3 Bool)
(declare-var I3 Bool)
(declare-var J3 Real)
(declare-var K3 Real)
(declare-var L3 Real)
(declare-var M3 Real)
(declare-var N3 Real)
(declare-var O3 Real)
(declare-var P3 Real)
(declare-var Q3 Real)
(declare-var R3 Real)
(declare-var S3 Real)
(declare-var T3 Real)
(declare-var U3 Real)
(declare-var V3 Real)
(declare-var W3 Real)
(declare-var X3 Bool)
(declare-var Y3 Bool)
(declare-var Z3 Bool)
(declare-var A4 Bool)
(declare-var B4 Real)
(declare-var C4 Bool)
(declare-var D4 Bool)
(rule cp-rel-entry)
(rule (=> (and cp-rel-__UFO__0_proc E) cp-rel-__UFO__0_proc))
(rule (let ((a!1 (and cp-rel-ldv_23956_proc
                (=> E (and F G (not H)))
                (=> E G)
                E
                (not I)
                (= H (= J 0.0))
                (= I (= K 0.0)))))
  (=> a!1 cp-rel-ldv_23956_proc)))
(rule (let ((a!1 (or (and Z D1 (<= K E1) (>= K E1))
               (and T F1 Y (<= K (- 19.0)) (>= K (- 19.0)))))
      (a!2 (=> C1 (or (and D1 (not F1)) (and F1 (not D1)))))
      (a!3 (or (and J1 N1 O1 (<= P1 J) (>= P1 J))
               (and G1 Q1 L1 (and (<= P1 R1) (>= P1 R1)))
               (and C1 S1 I1 (and (<= P1 R1) (>= P1 R1)))))
      (a!4 (=> M1
               (or (and N1 (not Q1) (not S1))
                   (and Q1 (not N1) (not S1))
                   (and S1 (not N1) (not Q1)))))
      (a!5 (=> E2 (or (and Y1 F2 (not D2)) (and W1 G2 A2))))
      (a!6 (=> E2 (or (and F2 (not G2)) (and G2 (not F2)))))
      (a!7 (or (and T1 I2 (and (<= J2 P1) (>= J2 P1)) (and (<= K2 K) (>= K2 K)))
               (and E2 L2 (and (<= J2 P1) (>= J2 P1)) (and (<= K2 K) (>= K2 K)))
               (and B2 M2 (<= J2 N2) (>= J2 N2) (and (<= K2 K) (>= K2 K)))
               (and W O2 B1 (<= J2 R1) (>= J2 R1) (<= K2 E1) (>= K2 E1))))
      (a!8 (=> H2
               (or (and I2 (not L2) (not M2) (not O2))
                   (and L2 (not I2) (not M2) (not O2))
                   (and M2 (not I2) (not L2) (not O2))
                   (and O2 (not I2) (not L2) (not M2)))))
      (a!9 (and Q
                F3
                (not V)
                (and (<= A3 J) (>= A3 J))
                (and (<= C3 (- 19.0)) (>= C3 (- 19.0)))))
      (a!10 (and N
                 G3
                 S
                 (and (<= A3 J) (>= A3 J))
                 (and (<= C3 (- 19.0)) (>= C3 (- 19.0)))))
      (a!11 (and I
                 H3
                 P
                 (and (<= A3 J) (>= A3 J))
                 (and (<= C3 (- 19.0)) (>= C3 (- 19.0)))))
      (a!13 (=> Y2
                (or (and Z2 (not D3) (not E3) (not F3) (not G3) (not H3))
                    (and D3 (not Z2) (not E3) (not F3) (not G3) (not H3))
                    (and E3 (not Z2) (not D3) (not F3) (not G3) (not H3))
                    (and F3 (not Z2) (not D3) (not E3) (not G3) (not H3))
                    (and G3 (not Z2) (not D3) (not E3) (not F3) (not H3))
                    (and H3 (not Z2) (not D3) (not E3) (not F3) (not G3))))))
(let ((a!12 (or (and V2 Z2 (<= A3 B3) (>= A3 B3) (and (<= C3 K2) (>= C3 K2)))
                (and P2
                     D3
                     U2
                     (and (<= A3 J2) (>= A3 J2))
                     (and (<= C3 K2) (>= C3 K2)))
                (and H2
                     E3
                     R2
                     (and (<= A3 J2) (>= A3 J2))
                     (and (<= C3 K2) (>= C3 K2)))
                a!9
                a!10
                a!11)))
(let ((a!14 (and (=> E (and F G (not H)))
                 (=> E G)
                 (=> I (and E L M))
                 (=> I L)
                 (=> N (and I O (not P)))
                 (=> N O)
                 (=> Q (and N R (not S)))
                 (=> Q R)
                 (=> T (and Q U V))
                 (=> T U)
                 (=> W (and T X (not Y)))
                 (=> W X)
                 (=> Z (and W A1 (not B1)))
                 (=> Z A1)
                 (=> C1 a!1)
                 a!2
                 (=> G1 (and C1 H1 (not I1)))
                 (=> G1 H1)
                 (=> J1 (and G1 K1 (not L1)))
                 (=> J1 K1)
                 (=> M1 a!3)
                 a!4
                 (=> T1 (and M1 U1 V1))
                 (=> T1 U1)
                 (=> W1 (and M1 X1 (not V1)))
                 (=> W1 X1)
                 (=> Y1 (and W1 Z1 (not A2)))
                 (=> Y1 Z1)
                 (=> B2 (and Y1 C2 D2))
                 (=> B2 C2)
                 a!5
                 a!6
                 (=> H2 a!7)
                 a!8
                 (=> P2 (and H2 Q2 (not R2)))
                 (=> P2 Q2)
                 (=> S2 (and P2 T2 (not U2)))
                 (=> S2 T2)
                 (=> V2 (and S2 W2 X2))
                 (=> V2 W2)
                 (=> Y2 a!12)
                 a!13
                 Y2
                 I3
                 (<= D A3)
                 (>= D A3)
                 (= H (= J3 0.0))
                 (= M (= K3 0.0))
                 (= L3 M3)
                 (= P (= N3 0.0))
                 (= S (= O3 0.0))
                 (= V (= P3 1.0))
                 (= R1 (+ J 1.0))
                 (= Y (= Q3 0.0))
                 (= B1 (= E1 0.0))
                 (= I1 (= R3 0.0))
                 (= L1 (= S3 0.0))
                 (= O1 (> R1 1.0))
                 (= V1 (= T3 0.0))
                 (= A2 (= U3 0.0))
                 (= D2 (= V3 1.0))
                 (= N2 (+ P1 1.0))
                 (= R2 (= T3 0.0))
                 (= U2 (= W3 0.0))
                 (= X2 (> J2 1.0))
                 (= B3 (+ J2 (- 1.0)))
                 (= I3 (= C3 0.0)))))
  (=> a!14 cp-rel-ldv_23956_proc)))))
(rule (let ((a!1 (or (and Z D1 (<= J K) (>= J K))
               (and T F1 Y (<= J (- 19.0)) (>= J (- 19.0)))))
      (a!2 (=> C1 (or (and D1 (not F1)) (and F1 (not D1)))))
      (a!3 (or (and J1 N1 O1 (<= E1 D) (>= E1 D))
               (and G1 Q1 L1 (and (<= E1 P1) (>= E1 P1)))
               (and C1 S1 I1 (and (<= E1 P1) (>= E1 P1)))))
      (a!4 (=> M1
               (or (and N1 (not Q1) (not S1))
                   (and Q1 (not N1) (not S1))
                   (and S1 (not N1) (not Q1)))))
      (a!5 (=> E2 (or (and Y1 F2 (not D2)) (and W1 G2 A2))))
      (a!6 (=> E2 (or (and F2 (not G2)) (and G2 (not F2)))))
      (a!7 (or (and T1 I2 (and (<= R1 E1) (>= R1 E1)) (and (<= J2 J) (>= J2 J)))
               (and E2 L2 (and (<= R1 E1) (>= R1 E1)) (and (<= J2 J) (>= J2 J)))
               (and B2 M2 (<= R1 K2) (>= R1 K2) (and (<= J2 J) (>= J2 J)))
               (and W O2 B1 (<= R1 P1) (>= R1 P1) (<= J2 K) (>= J2 K))))
      (a!8 (=> H2
               (or (and I2 (not L2) (not M2) (not O2))
                   (and L2 (not I2) (not M2) (not O2))
                   (and M2 (not I2) (not L2) (not O2))
                   (and O2 (not I2) (not L2) (not M2)))))
      (a!9 (and Q
                F3
                (not V)
                (and (<= N2 D) (>= N2 D))
                (and (<= B3 (- 19.0)) (>= B3 (- 19.0)))))
      (a!10 (and N
                 G3
                 S
                 (and (<= N2 D) (>= N2 D))
                 (and (<= B3 (- 19.0)) (>= B3 (- 19.0)))))
      (a!11 (and I
                 H3
                 P
                 (and (<= N2 D) (>= N2 D))
                 (and (<= B3 (- 19.0)) (>= B3 (- 19.0)))))
      (a!13 (=> Y2
                (or (and Z2 (not D3) (not E3) (not F3) (not G3) (not H3))
                    (and D3 (not Z2) (not E3) (not F3) (not G3) (not H3))
                    (and E3 (not Z2) (not D3) (not F3) (not G3) (not H3))
                    (and F3 (not Z2) (not D3) (not E3) (not G3) (not H3))
                    (and G3 (not Z2) (not D3) (not E3) (not F3) (not H3))
                    (and H3 (not Z2) (not D3) (not E3) (not F3) (not G3)))))
      (a!14 (=> X3
                (or (and F Y3 H (<= W3 D) (>= W3 D))
                    (and Y2 Z3 (not I3) (<= W3 N2) (>= W3 N2)))))
      (a!15 (=> X3 (or (and Y3 (not Z3)) (and Z3 (not Y3))))))
(let ((a!12 (or (and V2 Z2 (<= N2 A3) (>= N2 A3) (and (<= B3 J2) (>= B3 J2)))
                (and P2
                     D3
                     U2
                     (and (<= N2 R1) (>= N2 R1))
                     (and (<= B3 J2) (>= B3 J2)))
                (and H2
                     E3
                     R2
                     (and (<= N2 R1) (>= N2 R1))
                     (and (<= B3 J2) (>= B3 J2)))
                a!9
                a!10
                a!11)))
(let ((a!16 (and (=> E (and F G (not H)))
                 (=> E G)
                 (=> I (and E L M))
                 (=> I L)
                 (=> N (and I O (not P)))
                 (=> N O)
                 (=> Q (and N R (not S)))
                 (=> Q R)
                 (=> T (and Q U V))
                 (=> T U)
                 (=> W (and T X (not Y)))
                 (=> W X)
                 (=> Z (and W A1 (not B1)))
                 (=> Z A1)
                 (=> C1 a!1)
                 a!2
                 (=> G1 (and C1 H1 (not I1)))
                 (=> G1 H1)
                 (=> J1 (and G1 K1 (not L1)))
                 (=> J1 K1)
                 (=> M1 a!3)
                 a!4
                 (=> T1 (and M1 U1 V1))
                 (=> T1 U1)
                 (=> W1 (and M1 X1 (not V1)))
                 (=> W1 X1)
                 (=> Y1 (and W1 Z1 (not A2)))
                 (=> Y1 Z1)
                 (=> B2 (and Y1 C2 D2))
                 (=> B2 C2)
                 a!5
                 a!6
                 (=> H2 a!7)
                 a!8
                 (=> P2 (and H2 Q2 (not R2)))
                 (=> P2 Q2)
                 (=> S2 (and P2 T2 (not U2)))
                 (=> S2 T2)
                 (=> V2 (and S2 W2 X2))
                 (=> V2 W2)
                 (=> Y2 a!12)
                 a!13
                 a!14
                 a!15
                 (or (and X3 (not A4)) (and S2 (not X2)) (and J1 (not O1)))
                 (= H (= C3 0.0))
                 (= M (= J3 0.0))
                 (= K3 L3)
                 (= P (= M3 0.0))
                 (= S (= N3 0.0))
                 (= V (= O3 1.0))
                 (= P1 (+ D 1.0))
                 (= Y (= P3 0.0))
                 (= B1 (= K 0.0))
                 (= I1 (= Q3 0.0))
                 (= L1 (= R3 0.0))
                 (= O1 (> P1 1.0))
                 (= V1 (= S3 0.0))
                 (= A2 (= T3 0.0))
                 (= D2 (= U3 1.0))
                 (= K2 (+ E1 1.0))
                 (= R2 (= S3 0.0))
                 (= U2 (= V3 0.0))
                 (= X2 (> R1 1.0))
                 (= A3 (+ R1 (- 1.0)))
                 (= I3 (= B3 0.0))
                 (= A4 (= W3 1.0)))))
  (=> a!16 cp-rel-ldv_23956_proc)))))
(rule (let ((a!1 (or (and M2 G2 (<= W3 V3) (>= W3 V3))
               (and T2 F2 O2 (<= W3 (- 19.0)) (>= W3 (- 19.0)))))
      (a!2 (=> H2 (or (and G2 (not F2)) (and F2 (not G2)))))
      (a!3 (or (and B2 X1 W1 (<= U3 B) (>= U3 B))
               (and E2 V1 Z1 (and (<= U3 T3) (>= U3 T3)))
               (and H2 U1 C2 (and (<= U3 T3) (>= U3 T3)))))
      (a!4 (=> Y1
               (or (and X1 (not V1) (not U1))
                   (and V1 (not X1) (not U1))
                   (and U1 (not X1) (not V1)))))
      (a!5 (=> G1 (or (and M1 F1 (not H1)) (and O1 D1 K1))))
      (a!6 (=> G1 (or (and F1 (not D1)) (and D1 (not F1)))))
      (a!7 (or (and T1
                    B1
                    (and (<= S3 U3) (>= S3 U3))
                    (and (<= R3 W3) (>= R3 W3)))
               (and G1
                    A1
                    (and (<= S3 U3) (>= S3 U3))
                    (and (<= R3 W3) (>= R3 W3)))
               (and J1 Z (<= S3 Q3) (>= S3 Q3) (and (<= R3 W3) (>= R3 W3)))
               (and Q2 Y I2 (<= S3 T3) (>= S3 T3) (<= R3 V3) (>= R3 V3))))
      (a!8 (=> C1
               (or (and B1 (not A1) (not Z) (not Y))
                   (and A1 (not B1) (not Z) (not Y))
                   (and Z (not B1) (not A1) (not Y))
                   (and Y (not B1) (not A1) (not Z)))))
      (a!9 (and W2
                I
                (not R2)
                (and (<= P3 B) (>= P3 B))
                (and (<= N3 (- 19.0)) (>= N3 (- 19.0)))))
      (a!10 (and Z2
                 H
                 U2
                 (and (<= P3 B) (>= P3 B))
                 (and (<= N3 (- 19.0)) (>= N3 (- 19.0)))))
      (a!11 (and F3
                 G
                 X2
                 (and (<= P3 B) (>= P3 B))
                 (and (<= N3 (- 19.0)) (>= N3 (- 19.0)))))
      (a!13 (=> O
                (or (and N (not M) (not L) (not I) (not H) (not G))
                    (and M (not N) (not L) (not I) (not H) (not G))
                    (and L (not N) (not M) (not I) (not H) (not G))
                    (and I (not N) (not M) (not L) (not H) (not G))
                    (and H (not N) (not M) (not L) (not I) (not G))
                    (and G (not N) (not M) (not L) (not I) (not H))))))
(let ((a!12 (or (and R N (<= P3 O3) (>= P3 O3) (and (<= N3 R3) (>= N3 R3)))
                (and X
                     M
                     S
                     (and (<= P3 S3) (>= P3 S3))
                     (and (<= N3 R3) (>= N3 R3)))
                (and C1
                     L
                     V
                     (and (<= P3 S3) (>= P3 S3))
                     (and (<= N3 R3) (>= N3 R3)))
                a!9
                a!10
                a!11)))
(let ((a!14 (and (cp-rel-ldv_23956.outer_proc B4 A)
                 E
                 cp-rel-ldv_23956_proc
                 (=> X3 (and I3 H3 (not G3)))
                 (=> X3 H3)
                 (=> F3 (and X3 E3 D3))
                 (=> F3 E3)
                 (=> Z2 (and F3 Y2 (not X2)))
                 (=> Z2 Y2)
                 (=> W2 (and Z2 V2 (not U2)))
                 (=> W2 V2)
                 (=> T2 (and W2 S2 R2))
                 (=> T2 S2)
                 (=> Q2 (and T2 P2 (not O2)))
                 (=> Q2 P2)
                 (=> M2 (and Q2 L2 (not I2)))
                 (=> M2 L2)
                 (=> H2 a!1)
                 a!2
                 (=> E2 (and H2 D2 (not C2)))
                 (=> E2 D2)
                 (=> B2 (and E2 A2 (not Z1)))
                 (=> B2 A2)
                 (=> Y1 a!3)
                 a!4
                 (=> T1 (and Y1 S1 Q1))
                 (=> T1 S1)
                 (=> O1 (and Y1 N1 (not Q1)))
                 (=> O1 N1)
                 (=> M1 (and O1 L1 (not K1)))
                 (=> M1 L1)
                 (=> J1 (and M1 I1 H1))
                 (=> J1 I1)
                 a!5
                 a!6
                 (=> C1 a!7)
                 a!8
                 (=> X (and C1 W (not V)))
                 (=> X W)
                 (=> U (and X T (not S)))
                 (=> U T)
                 (=> R (and U Q P))
                 (=> R Q)
                 (=> O a!12)
                 a!13
                 O
                 F
                 (<= B4 P3)
                 (>= B4 P3)
                 (= G3 (= M3 0.0))
                 (= D3 (= L3 0.0))
                 (= K3 J3)
                 (= X2 (= C3 0.0))
                 (= U2 (= B3 0.0))
                 (= R2 (= A3 1.0))
                 (= T3 (+ B 1.0))
                 (= O2 (= N2 0.0))
                 (= I2 (= V3 0.0))
                 (= C2 (= K2 0.0))
                 (= Z1 (= J2 0.0))
                 (= W1 (> T3 1.0))
                 (= Q1 (= R1 0.0))
                 (= K1 (= P1 0.0))
                 (= H1 (= E1 1.0))
                 (= Q3 (+ U3 1.0))
                 (= V (= R1 0.0))
                 (= S (= K 0.0))
                 (= P (> S3 1.0))
                 (= O3 (+ S3 (- 1.0)))
                 (= F (= N3 0.0)))))
  (=> a!14 (cp-rel-ldv_23956.outer_proc B A))))))
(rule (let ((a!1 (or (and R2 M2 (<= W3 V3) (>= W3 V3))
               (and X2 L2 S2 (<= W3 (- 19.0)) (>= W3 (- 19.0)))))
      (a!2 (=> O2 (or (and M2 (not L2)) (and L2 (not M2)))))
      (a!3 (or (and F2 B2 A2 (<= U3 A) (>= U3 A))
               (and I2 Z1 D2 (and (<= U3 T3) (>= U3 T3)))
               (and O2 Y1 G2 (and (<= U3 T3) (>= U3 T3)))))
      (a!4 (=> C2
               (or (and B2 (not Z1) (not Y1))
                   (and Z1 (not B2) (not Y1))
                   (and Y1 (not B2) (not Z1)))))
      (a!5 (=> K1 (or (and S1 J1 (not L1)) (and U1 I1 O1))))
      (a!6 (=> K1 (or (and J1 (not I1)) (and I1 (not J1)))))
      (a!7 (or (and X1
                    G1
                    (and (<= S3 U3) (>= S3 U3))
                    (and (<= R3 W3) (>= R3 W3)))
               (and K1
                    F1
                    (and (<= S3 U3) (>= S3 U3))
                    (and (<= R3 W3) (>= R3 W3)))
               (and N1 D1 (<= S3 Q3) (>= S3 Q3) (and (<= R3 W3) (>= R3 W3)))
               (and U2 C1 P2 (<= S3 T3) (>= S3 T3) (<= R3 V3) (>= R3 V3))))
      (a!8 (=> H1
               (or (and G1 (not F1) (not D1) (not C1))
                   (and F1 (not G1) (not D1) (not C1))
                   (and D1 (not G1) (not F1) (not C1))
                   (and C1 (not G1) (not F1) (not D1)))))
      (a!9 (and D3
                O
                (not V2)
                (and (<= P3 A) (>= P3 A))
                (and (<= N3 (- 19.0)) (>= N3 (- 19.0)))))
      (a!10 (and G3
                 N
                 Y2
                 (and (<= P3 A) (>= P3 A))
                 (and (<= N3 (- 19.0)) (>= N3 (- 19.0)))))
      (a!11 (and X3
                 M
                 E3
                 (and (<= P3 A) (>= P3 A))
                 (and (<= N3 (- 19.0)) (>= N3 (- 19.0)))))
      (a!13 (=> S
                (or (and R (not Q) (not P) (not O) (not N) (not M))
                    (and Q (not R) (not P) (not O) (not N) (not M))
                    (and P (not R) (not Q) (not O) (not N) (not M))
                    (and O (not R) (not Q) (not P) (not N) (not M))
                    (and N (not R) (not Q) (not P) (not O) (not M))
                    (and M (not R) (not Q) (not P) (not O) (not N)))))
      (a!14 (=> I
                (or (and A4 H Y3 (<= J A) (>= J A))
                    (and S G (not L) (<= J P3) (>= J P3)))))
      (a!15 (=> I (or (and H (not G)) (and G (not H))))))
(let ((a!12 (or (and V R (<= P3 O3) (>= P3 O3) (and (<= N3 R3) (>= N3 R3)))
                (and B1
                     Q
                     W
                     (and (<= P3 S3) (>= P3 S3))
                     (and (<= N3 R3) (>= N3 R3)))
                (and H1
                     P
                     Z
                     (and (<= P3 S3) (>= P3 S3))
                     (and (<= N3 R3) (>= N3 R3)))
                a!9
                a!10
                a!11)))
(let ((a!16 (and E
                 cp-rel-ldv_23956_proc
                 (=> C4 (and A4 Z3 (not Y3)))
                 (=> C4 Z3)
                 (=> X3 (and C4 I3 H3))
                 (=> X3 I3)
                 (=> G3 (and X3 F3 (not E3)))
                 (=> G3 F3)
                 (=> D3 (and G3 Z2 (not Y2)))
                 (=> D3 Z2)
                 (=> X2 (and D3 W2 V2))
                 (=> X2 W2)
                 (=> U2 (and X2 T2 (not S2)))
                 (=> U2 T2)
                 (=> R2 (and U2 Q2 (not P2)))
                 (=> R2 Q2)
                 (=> O2 a!1)
                 a!2
                 (=> I2 (and O2 H2 (not G2)))
                 (=> I2 H2)
                 (=> F2 (and I2 E2 (not D2)))
                 (=> F2 E2)
                 (=> C2 a!3)
                 a!4
                 (=> X1 (and C2 W1 V1))
                 (=> X1 W1)
                 (=> U1 (and C2 T1 (not V1)))
                 (=> U1 T1)
                 (=> S1 (and U1 Q1 (not O1)))
                 (=> S1 Q1)
                 (=> N1 (and S1 M1 L1))
                 (=> N1 M1)
                 a!5
                 a!6
                 (=> H1 a!7)
                 a!8
                 (=> B1 (and H1 A1 (not Z)))
                 (=> B1 A1)
                 (=> Y (and B1 X (not W)))
                 (=> Y X)
                 (=> V (and Y U T))
                 (=> V U)
                 (=> S a!12)
                 a!13
                 a!14
                 a!15
                 (or (and I (not F)) (and Y (not T)) (and F2 (not A2)))
                 (= Y3 (= M3 0.0))
                 (= H3 (= L3 0.0))
                 (= K3 J3)
                 (= E3 (= C3 0.0))
                 (= Y2 (= B3 0.0))
                 (= V2 (= A3 1.0))
                 (= T3 (+ A 1.0))
                 (= S2 (= N2 0.0))
                 (= P2 (= V3 0.0))
                 (= G2 (= K2 0.0))
                 (= D2 (= J2 0.0))
                 (= A2 (> T3 1.0))
                 (= V1 (= R1 0.0))
                 (= O1 (= P1 0.0))
                 (= L1 (= E1 1.0))
                 (= Q3 (+ U3 1.0))
                 (= Z (= R1 0.0))
                 (= W (= K 0.0))
                 (= T (> S3 1.0))
                 (= O3 (+ S3 (- 1.0)))
                 (= L (= N3 0.0))
                 (= F (= J 1.0)))))
  (=> a!16 (cp-rel-ldv_23956.outer_proc A A))))))
(rule (let ((a!1 (or (and R2 M2 (<= S3 R3) (>= S3 R3))
               (and X2 L2 S2 (<= S3 (- 19.0)) (>= S3 (- 19.0)))))
      (a!2 (=> O2 (or (and M2 (not L2)) (and L2 (not M2)))))
      (a!3 (or (and F2 B2 A2 (<= Q3 T3) (>= Q3 T3))
               (and I2 Z1 D2 (and (<= Q3 P3) (>= Q3 P3)))
               (and O2 Y1 G2 (and (<= Q3 P3) (>= Q3 P3)))))
      (a!4 (=> C2
               (or (and B2 (not Z1) (not Y1))
                   (and Z1 (not B2) (not Y1))
                   (and Y1 (not B2) (not Z1)))))
      (a!5 (=> K1 (or (and S1 J1 (not L1)) (and U1 I1 O1))))
      (a!6 (=> K1 (or (and J1 (not I1)) (and I1 (not J1)))))
      (a!7 (or (and X1
                    G1
                    (and (<= O3 Q3) (>= O3 Q3))
                    (and (<= N3 S3) (>= N3 S3)))
               (and K1
                    F1
                    (and (<= O3 Q3) (>= O3 Q3))
                    (and (<= N3 S3) (>= N3 S3)))
               (and N1 D1 (<= O3 M3) (>= O3 M3) (and (<= N3 S3) (>= N3 S3)))
               (and U2 C1 P2 (<= O3 P3) (>= O3 P3) (<= N3 R3) (>= N3 R3))))
      (a!8 (=> H1
               (or (and G1 (not F1) (not D1) (not C1))
                   (and F1 (not G1) (not D1) (not C1))
                   (and D1 (not G1) (not F1) (not C1))
                   (and C1 (not G1) (not F1) (not D1)))))
      (a!9 (and D3
                O
                (not V2)
                (and (<= L3 T3) (>= L3 T3))
                (and (<= J3 (- 19.0)) (>= J3 (- 19.0)))))
      (a!10 (and G3
                 N
                 Y2
                 (and (<= L3 T3) (>= L3 T3))
                 (and (<= J3 (- 19.0)) (>= J3 (- 19.0)))))
      (a!11 (and X3
                 M
                 E3
                 (and (<= L3 T3) (>= L3 T3))
                 (and (<= J3 (- 19.0)) (>= J3 (- 19.0)))))
      (a!13 (=> S
                (or (and R (not Q) (not P) (not O) (not N) (not M))
                    (and Q (not R) (not P) (not O) (not N) (not M))
                    (and P (not R) (not Q) (not O) (not N) (not M))
                    (and O (not R) (not Q) (not P) (not N) (not M))
                    (and N (not R) (not Q) (not P) (not O) (not M))
                    (and M (not R) (not Q) (not P) (not O) (not N)))))
      (a!14 (=> I
                (or (and A4 H Y3 (<= A T3) (>= A T3))
                    (and S G (not L) (<= A L3) (>= A L3)))))
      (a!15 (=> I (or (and H (not G)) (and G (not H))))))
(let ((a!12 (or (and V R (<= L3 K3) (>= L3 K3) (and (<= J3 N3) (>= J3 N3)))
                (and B1
                     Q
                     W
                     (and (<= L3 O3) (>= L3 O3))
                     (and (<= J3 N3) (>= J3 N3)))
                (and H1
                     P
                     Z
                     (and (<= L3 O3) (>= L3 O3))
                     (and (<= J3 N3) (>= J3 N3)))
                a!9
                a!10
                a!11)))
(let ((a!16 (and cp-rel-entry
                 D4
                 (<= B4 1.0)
                 (>= B4 1.0)
                 (cp-rel-ldv_23956.outer_proc B4 T3)
                 E
                 cp-rel-ldv_23956_proc
                 (=> C4 (and A4 Z3 (not Y3)))
                 (=> C4 Z3)
                 (=> X3 (and C4 I3 H3))
                 (=> X3 I3)
                 (=> G3 (and X3 F3 (not E3)))
                 (=> G3 F3)
                 (=> D3 (and G3 Z2 (not Y2)))
                 (=> D3 Z2)
                 (=> X2 (and D3 W2 V2))
                 (=> X2 W2)
                 (=> U2 (and X2 T2 (not S2)))
                 (=> U2 T2)
                 (=> R2 (and U2 Q2 (not P2)))
                 (=> R2 Q2)
                 (=> O2 a!1)
                 a!2
                 (=> I2 (and O2 H2 (not G2)))
                 (=> I2 H2)
                 (=> F2 (and I2 E2 (not D2)))
                 (=> F2 E2)
                 (=> C2 a!3)
                 a!4
                 (=> X1 (and C2 W1 V1))
                 (=> X1 W1)
                 (=> U1 (and C2 T1 (not V1)))
                 (=> U1 T1)
                 (=> S1 (and U1 Q1 (not O1)))
                 (=> S1 Q1)
                 (=> N1 (and S1 M1 L1))
                 (=> N1 M1)
                 a!5
                 a!6
                 (=> H1 a!7)
                 a!8
                 (=> B1 (and H1 A1 (not Z)))
                 (=> B1 A1)
                 (=> Y (and B1 X (not W)))
                 (=> Y X)
                 (=> V (and Y U T))
                 (=> V U)
                 (=> S a!12)
                 a!13
                 a!14
                 a!15
                 (or (and I (not F)) (and Y (not T)) (and F2 (not A2)))
                 (= Y3 (= C3 0.0))
                 (= H3 (= B3 0.0))
                 (= A3 N2)
                 (= E3 (= K2 0.0))
                 (= Y2 (= J2 0.0))
                 (= V2 (= R1 1.0))
                 (= P3 (+ T3 1.0))
                 (= S2 (= P1 0.0))
                 (= P2 (= R3 0.0))
                 (= G2 (= E1 0.0))
                 (= D2 (= K 0.0))
                 (= A2 (> P3 1.0))
                 (= V1 (= J 0.0))
                 (= O1 (= D 0.0))
                 (= L1 (= C 1.0))
                 (= M3 (+ Q3 1.0))
                 (= Z (= J 0.0))
                 (= W (= B 0.0))
                 (= T (> O3 1.0))
                 (= K3 (+ O3 (- 1.0)))
                 (= L (= J3 0.0))
                 (= F (= A 1.0)))))
  (=> a!16 cp-rel-UnifiedReturnBlock)))))
(query cp-rel-UnifiedReturnBlock)
